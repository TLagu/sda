package pl.sda.s03_podstawy_testy.tasks;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class TddArrayUtilTest {

    @Test
    void shouldReturnEmptyArrayWhenArrayIsNullAndElementsDoesNotExist() {
        //given
        String[] sampleArray = null;
        //when
        String[] result = ArrayUtil.removeElements(sampleArray, null);
        //then
        Assertions.assertThat(result).isNotNull().hasSize(0);
    }

    @Test
    void shouldReturnArrayWhenArrayContainingElement() {
        //given
        String[] sampleArray = {"first", "second"};
        //when
        String[] result = ArrayUtil.removeElements(sampleArray, "first");
        //then
        Assertions.assertThat(result).isNotNull().hasSize(1).contains("second");
    }

    @Test
    void shouldReturnArrayContainingAllElement() {
        //given
        String[] sampleArray = {"first", "second"};
        //when
        String[] result = ArrayUtil.removeElements(sampleArray, "third");
        //then
        Assertions.assertThat(result).isNotNull().hasSize(2).contains("first", "second");
    }


    @Test
    void shouldReturnArrayContainingElement() {
        //given
        String[] sampleArray = {"first", "second"};
        //when
        String[] result = ArrayUtil.removeElements(sampleArray);
        //then
        Assertions.assertThat(result).isNotNull().hasSize(2).contains("first", "second");
    }
}
