package pl.sda.s03_podstawy_testy.tasks;

import org.junit.jupiter.api.*;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class SampleTest {

    //@BeforeAll
    static void beforeAllTest() {
        // na przykłąd utworzenie stuktur w bazie
        System.out.println("beforeAllTest");
    }

//    @Test
    @DisplayName("Method 'Test one'")
    void shouldPrintOneTest() {
        System.out.println("Test one");
    }

//    @Test
    void shouldPrintTwoTest() {
        System.out.println("Test two");
    }

//    @AfterAll
    static void afterAllTest() {
        // Na przykład wyczyścić struktury i dane testowe
        System.out.println("\nafterAllTest");
    }

//    @BeforeEach
     void beforeEachTest() {
        // Na przykład czyszczenie danych przed wykonaniem testu
        System.out.println("\nbeforeEachTest");
    }

//    @AfterEach
    void afterEachAllTest() {
        // Na przykład czyszczenie danych po wykonaniu testu
        System.out.println("afterEachAllTest");
    }

//    @Test
    @Disabled
    void shouldCompareStringsSample() {
        //given
        String actualValue = "aa";
        System.out.println("shouldCompareStringsSample");

        //then
        assertNotNull(actualValue, "Text should not be null");
        assertEquals(new String("aa"), actualValue);
    }

//    @Test
    @Disabled
    void shouldCompareNumbersSample() {
        //given
        Integer actualValue = 20;
        System.out.println("shouldCompareNumbersSample");

        //then
        assertNotNull(actualValue, "Number should not be null");
        assertEquals(20, actualValue);
    }

    @Test
    void shouldAddTwoNumbers() {
        final double valueA = 3;
        final double valueB = 5;
        Calculator calculator = new Calculator();

        final double result = calculator.add(valueA, valueB);

        assertEquals(8.0d, result);
    }

    @Test
    void bothArraysShouldBeEqual() {
        final int[] intsA = { 1, 2, 3 };
        final int[] intsB = { 1, 2, 3 };

        assertArrayEquals(intsA, intsB, "Elements in arrays are not equals");
    }

    @Test
    void twoListsShouldBeEqual() {
        List<Integer> intsA = Arrays.asList(1, 2, 3);
        List<Integer> intsB = List.of(1, 2, 3);

        assertIterableEquals(intsA, intsB);
    }
}
