package pl.sda.s03_podstawy_testy.tasks;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ValidatorTest {

    @Test
    void shouldReturnFalseForNullDate() {
        //given
        LocalDate date = null;
        // when
        boolean result = Validator.isValid(date);
        //then
        assertFalse(result);
    }

    @Test
    void shouldReturnFalseForToSmallDate() {
        //given
        LocalDate date = LocalDate.of(2020, 1, 1);
        // when
        boolean result = Validator.isValid(date);
        //then
        assertFalse(result);
    }

    @Test
    void shouldReturnFalseForLowerEdgeDate() {
        //given
        LocalDate date = LocalDate.of(2021, 1, 1);
        // when
        boolean result = Validator.isValid(date);
        //then
        assertTrue(result);
    }

    @Test
    void shouldReturnFalseForInsideDate() {
        //given
        LocalDate date = LocalDate.of(2021, 2, 1);
        // when
        boolean result = Validator.isValid(date);
        //then
        assertTrue(result);
    }

    @Test
    void shouldReturnFalseForUpperEdgeDate() {
        //given
        LocalDate date = LocalDate.of(2021, 4, 1);
        // when
        boolean result = Validator.isValid(date);
        //then
        assertTrue(result);
    }

    @Test
    void shouldReturnFalseForToBigDate() {
        //given
        LocalDate date = LocalDate.of(2021, 4, 2);
        // when
        boolean result = Validator.isValid(date);
        //then
        assertFalse(result);
    }

    @Test
    void shouldReturnFalseForNullString() {
        //given
        String text = null;
        // when
        boolean result = Validator.isValid(text);
        //then
        assertFalse(result);
    }

    @Test
    void shouldReturnFalseForToSmallString() {
        //given
        String text = "1234";
        // when
        boolean result = Validator.isValid(text);
        //then
        assertFalse(result);
    }

    @Test
    void shouldReturnFalseForLowerEdgeString() {
        //given
        String text = "12345";
        // when
        boolean result = Validator.isValid(text);
        //then
        assertTrue(result);
    }

    @Test
    void shouldReturnFalseForInsideString() {
        //given
        String text = "1234567";
        // when
        boolean result = Validator.isValid(text);
        //then
        assertTrue(result);
    }

    @Test
    void shouldReturnFalseForUpperEdgeString() {
        //given
        String text = "1234567890";
        // when
        boolean result = Validator.isValid(text);
        //then
        assertTrue(result);
    }

    @Test
    void shouldReturnFalseForToBigString() {
        //given
        String text = "12345678901";
        // when
        boolean result = Validator.isValid(text);
        //then
        assertFalse(result);
    }
}