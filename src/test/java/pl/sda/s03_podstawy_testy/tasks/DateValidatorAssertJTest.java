package pl.sda.s03_podstawy_testy.tasks;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.DateTimeException;
import java.time.LocalDate;

public class DateValidatorAssertJTest {
    @Test
    void shouldReturnFalseIfNull() {
        LocalDate date = null;

        Assertions.assertThat(Validator.isValid(date)).isFalse();
    }

    @Test
    void shouldReturnFalseIfOutOfRangeToSmall() {
        LocalDate date = LocalDate.of (2020, 12, 31);
        Assertions.assertThat(Validator.isValid(date)).isFalse();
    }

    @Test
    void shouldReturnFalseForLowerEdgeDate() {
        LocalDate date = LocalDate.of (2021, 1, 1);
        Assertions.assertThat(Validator.isValid(date)).isTrue();
    }

    @Test
    void shouldReturnFalseForInsideDate() {
        LocalDate date = LocalDate.of (2021, 2, 1);
        Assertions.assertThat(Validator.isValid(date)).isTrue();
    }

    @Test
    void shouldReturnFalseForUpperEdgeDate() {
        LocalDate date = LocalDate.of (2021, 4, 1);
        Assertions.assertThat(Validator.isValid(date)).isTrue();
    }

    @Test
    void shouldReturnFalseForOutOfRangeToBig() {
        LocalDate date = LocalDate.of (2021, 4, 2);
        Assertions.assertThat(Validator.isValid(date)).isFalse();
    }

    @Test
    void shouldReturnFalseForNullPesel() {
        String pesel = null;
        Assertions.assertThat(PeselUtils.getBirthDate(pesel)).isNull();
    }

    @Test
    void shouldReturnFalseForToShortPesel() {
        String pesel = "012345678";
        Assertions.assertThat(PeselUtils.getBirthDate(pesel)).isNull();
    }

    @Test
    void shouldReturnFalseForToLongPesel() {
        String pesel = "012345678901";
        Assertions.assertThat(PeselUtils.getBirthDate(pesel)).isNull();
    }

    @Test
    void shouldReturnFalseForToWrongDateFormatPesel() {
        String pesel = "01132567890";
        Assertions.assertThatThrownBy(() -> {PeselUtils.getBirthDate(pesel);}).isInstanceOf(DateTimeException.class);
    }

    @Test
    void shouldReturnValidDateForCorrectPesel() {
        String pesel = "69012526423";
        Assertions.assertThat(PeselUtils.getBirthDate(pesel)).isNotNull();
    }

}
