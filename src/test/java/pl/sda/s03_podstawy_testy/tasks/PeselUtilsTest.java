package pl.sda.s03_podstawy_testy.tasks;

import org.junit.jupiter.api.Test;

import java.time.DateTimeException;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class PeselUtilsTest {

    @Test
    void shouldReturnFalseForNullPesel() {
        //given
        String pesel = null;
        // when
        LocalDate result = PeselUtils.getBirthDate(pesel);
        //then
        assertNull(result);
    }

    @Test
    void shouldReturnFalseForToShortPesel() {
        //given
        String pesel = "012345678";
        // when
        LocalDate result = PeselUtils.getBirthDate(pesel);
        //then
        assertNull(result);
    }

    @Test
    void shouldReturnFalseForToLongPesel() {
        //given
        String pesel = "012345678901";
        // when
        LocalDate result = PeselUtils.getBirthDate(pesel);
        //then
        assertNull(result);
    }

    @Test
    void shouldReturnFalseForToWrongDateFormatPesel() {
        //given
        String pesel = "01132567890";
        // when
        assertThrows(DateTimeException.class, () -> {PeselUtils.getBirthDate(pesel);});
    }

    @Test
    void shouldReturnValidDateForCorrectPesel() {
        //given
        String pesel = "69012526423";
        // when
        LocalDate result = PeselUtils.getBirthDate(pesel);
        //then
        assertEquals(LocalDate.of(1969, 1, 25), result);
    }

}