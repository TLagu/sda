package pl.sda.s05_good_practices.e03_factory_method.examples.e01;

public class BoardGame implements Game {

    private final String name;
    private final String type;
    private final int maxPlayersNum;

    public BoardGame(String name, String type, int maxPlayersNum) {
        this.name = name;
        this.type = type;
        this.maxPlayersNum = maxPlayersNum;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public int getMinNumberOfPlayers() {
        return 2;
    }

    @Override
    public int getMaxNumberOfPlayers() {
        return maxPlayersNum;
    }

    @Override
    public boolean canBePlayedRemotely() {
        return false;
    }
}
