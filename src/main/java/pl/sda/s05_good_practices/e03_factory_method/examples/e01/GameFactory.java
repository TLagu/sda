package pl.sda.s05_good_practices.e03_factory_method.examples.e01;

public interface GameFactory {
    Game create();
}
