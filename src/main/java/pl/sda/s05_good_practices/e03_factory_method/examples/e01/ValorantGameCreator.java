package pl.sda.s05_good_practices.e03_factory_method.examples.e01;

public class ValorantGameCreator implements GameFactory {

    @Override
    public Game create() {
        return new PCGame("Valorant", "FPS", 4, 10, true);
    }
}
