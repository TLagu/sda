package pl.sda.s05_good_practices.e03_factory_method.examples.e01;

public interface Game {
    String getName();

    String getType();

    int getMinNumberOfPlayers();

    int getMaxNumberOfPlayers();

    boolean canBePlayedRemotely();
}
