package pl.sda.s05_good_practices.e03_factory_method.examples.e01;

public class PCGame implements Game {

    private final String name;
    private final String type;
    private final int minNumberOdPlayers;
    private final int maxNumberOdPlayers;
    private final boolean isOnline;

    public PCGame(String name, String type, int minNumberOdPlayers, int maxNumberOdPlayers, boolean isOnline) {
        this.name = name;
        this.type = type;
        this.minNumberOdPlayers = minNumberOdPlayers;
        this.maxNumberOdPlayers = maxNumberOdPlayers;
        this.isOnline = isOnline;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public int getMinNumberOfPlayers() {
        return minNumberOdPlayers;
    }

    @Override
    public int getMaxNumberOfPlayers() {
        return maxNumberOdPlayers;
    }

    @Override
    public boolean canBePlayedRemotely() {
        return isOnline;
    }
}
