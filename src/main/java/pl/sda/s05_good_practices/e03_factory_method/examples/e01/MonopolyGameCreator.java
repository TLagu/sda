package pl.sda.s05_good_practices.e03_factory_method.examples.e01;

public class MonopolyGameCreator implements GameFactory {
    @Override
    public Game create() {
        return new BoardGame("Monopoly", "Family Game", 4);
    }
}
