package pl.sda.s05_good_practices.e11_composite.examples.e01;

public class DottedLine implements Line{

    private Point point = new Point(10, 10);
    @Override
    public void draw(double lengthInPixels) {
        System.out.println("Drawing d.o.t.t.e.d line starting(" + point.getX() + ", " + point.getY());
    }

    @Override
    public void setStartingPosition(Point position) {
        this.point = position;
    }

    @Override
    public Point getStartingPoint() {
        return point;
    }
}
