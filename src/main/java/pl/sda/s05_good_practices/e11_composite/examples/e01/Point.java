package pl.sda.s05_good_practices.e11_composite.examples.e01;

import lombok.RequiredArgsConstructor;
import lombok.Value;

@Value
@RequiredArgsConstructor
public class Point {
    private final int x;
    private final int y;
}
