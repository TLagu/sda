package pl.sda.s05_good_practices.e11_composite.examples.e01;

public interface Line {
    void draw(double lengthInPixels);
    void setStartingPosition(Point position);
    Point getStartingPoint();
}
