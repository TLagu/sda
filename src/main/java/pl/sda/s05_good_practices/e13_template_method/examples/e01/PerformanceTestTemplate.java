package pl.sda.s05_good_practices.e13_template_method.examples.e01;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class PerformanceTestTemplate {
    protected abstract void iteration();

    protected int getWarmUpIterationsNum() {
        return 2;
    }

    protected int getIterationsNum() {
        return 100;
    }

    public final void run() {
        for (int warmUpIterationIndex = 0; warmUpIterationIndex < getWarmUpIterationsNum(); warmUpIterationIndex++) {
            iteration();
        }
        final List<Long> iterationsExecutionTimes = new ArrayList<>();
        for (int iterationIndex = 0; iterationIndex < getIterationsNum(); iterationIndex++) {
            long startTimestamp = System.currentTimeMillis();
            iteration();
            long endTimestamp = System.currentTimeMillis();
            iterationsExecutionTimes.add(endTimestamp - startTimestamp);
        }
        showStatistics(iterationsExecutionTimes);
    }

    private void showStatistics(final List<Long> iterationsDurations) {
        System.out.println("Shortest iteration took: " + calculateShortestIteration(iterationsDurations));
        System.out.println("Longest iteration took: " + calculateLongestIteration(iterationsDurations));
        System.out.println("All iteration took: " + calculateTotalIteration(iterationsDurations));
    }

    private Long calculateShortestIteration(final List<Long> iterationsDurations) {
        return iterationsDurations.stream()
                .min(Comparator.naturalOrder())
                .orElseThrow();
    }

    private Long calculateLongestIteration(final List<Long> iterationsDurations) {
        return iterationsDurations.stream()
                .max(Comparator.naturalOrder())
                .orElseThrow();
    }

    private Long calculateTotalIteration(final List<Long> iterationsDurations) {
        return iterationsDurations.stream()
                .mapToLong(x -> x)
                .sum();
    }
}
