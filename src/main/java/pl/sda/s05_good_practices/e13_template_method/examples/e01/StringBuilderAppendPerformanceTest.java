package pl.sda.s05_good_practices.e13_template_method.examples.e01;

import java.util.Random;

public class StringBuilderAppendPerformanceTest extends PerformanceTestTemplate {
    private static final int CHAR_NUM = 1000000;

    @Override
    protected void iteration() {
        final Random random = new Random();
        final StringBuilder stringBuilder = new StringBuilder();
        for (int idx = 0; idx < CHAR_NUM; idx++) {
            stringBuilder.append(Math.abs(random.nextInt() % 128));
        }
        //System.out.println(stringBuilder.toString());
    }
}
