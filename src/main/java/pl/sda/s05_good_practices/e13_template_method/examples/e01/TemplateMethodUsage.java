package pl.sda.s05_good_practices.e13_template_method.examples.e01;

public class TemplateMethodUsage {
    public static void main(String[] args) {
        PerformanceTestTemplate testTemplate = new RandomListSortingPerformanceTest();
        testTemplate.run();

        testTemplate = new StringBuilderAppendPerformanceTest();
        testTemplate.run();
    }
}
