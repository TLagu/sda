package pl.sda.s05_good_practices.e13_template_method.examples.task03;

import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AllArgsConstructor
public class PerformanceTest extends PerformanceTestTemplate {
    private final int warmupIterationsNum;
    private final int iterationsNum;
    private final int numberOfIterations;

    @Override
    public void testIteration() {
        Random random = new Random();
        List<Long> numbers = new ArrayList<>();
        for(int i = 0; i < numberOfIterations; i++){
            numbers.add(random.nextLong());
        }
        List<Long> sorted = numbers.stream().sorted().collect(Collectors.toList());
    }

    @Override
    public int getWarmupIterationsNum() {
        return warmupIterationsNum;
    }

    @Override
    public int getIterationsNum() {
        return iterationsNum;
    }
}
