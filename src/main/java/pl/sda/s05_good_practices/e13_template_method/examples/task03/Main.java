package pl.sda.s05_good_practices.e13_template_method.examples.task03;

public class Main {
    public static void main(String[] args) {
        PerformanceTest test = new PerformanceTest(2, 100, 1000000);
        test.run();
    }
}
