package pl.sda.s05_good_practices.e13_template_method.examples.task03;

import lombok.AllArgsConstructor;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public abstract class PerformanceTestTemplate {

    public abstract void testIteration();
    public abstract int getWarmupIterationsNum();
    public abstract int getIterationsNum();

    public void run() {
        for(int i = 0; i < getWarmupIterationsNum(); i++){
            testIteration();
        }
        LocalTime startTime = LocalTime.now();
        for(int i = 0; i < getIterationsNum(); i++){
            testIteration();
        }
        LocalTime endTime = LocalTime.now();
        System.out.println("Average duration of a single iteration: "
                + (ChronoUnit.MILLIS.between(startTime, endTime) / getIterationsNum()));
    }

}
