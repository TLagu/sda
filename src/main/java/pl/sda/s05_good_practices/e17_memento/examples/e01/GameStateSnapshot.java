package pl.sda.s05_good_practices.e17_memento.examples.e01;

import lombok.Data;

import java.util.List;

@Data
public class GameStateSnapshot {
    private Integer health;
    private Integer mana;
    private List<String> items;

    public GameStateSnapshot(final GameState gameState){
        this.health = gameState.getHealth();
        this.mana = gameState.getMana();
        this.items = List.copyOf(gameState.getItems());
    }
}
