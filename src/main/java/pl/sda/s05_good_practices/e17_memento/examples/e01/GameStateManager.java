package pl.sda.s05_good_practices.e17_memento.examples.e01;

import java.util.ArrayDeque;
import java.util.Deque;

public class GameStateManager {
    private final Deque<GameStateSnapshot> snapshots = new ArrayDeque<>();

    public void saveGame(final GameState gameState){
        snapshots.push(new GameStateSnapshot(gameState));
    }

    public GameStateSnapshot restorePreviousCheckpoint(){
        return snapshots.pop();
    }
}
