package pl.sda.s05_good_practices.e17_memento.task04;

public class Main {
    public static void main(String[] args) {
        final EditorTextMementoManager editorTextMementoManager = new EditorTextMementoManager();

        final EditorText editorText = new EditorText("");
        editorText.addToValue("1. ");
        editorTextMementoManager.save(editorText);
        editorText.addToValue("Wstęp");
        editorTextMementoManager.save(editorText);
        editorText.addToValue("\n2. ");
        editorTextMementoManager.save(editorText);
        editorText.addToValue("zakończenie");
        System.out.println(editorText.getValue());

        final EditorTextMemento editorTextMemento = editorTextMementoManager.restore();
        editorText.restoreFromMemento(editorTextMemento);
        System.out.println(editorText.getValue());
        editorText.addToValue("Zakończenie");
        System.out.println(editorText.getValue());
    }
}
