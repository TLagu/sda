package pl.sda.s05_good_practices.e17_memento.task04;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EditorText {
    private String value;

    public void addToValue(String value) {
        this.value += value;
    }

    public void restoreFromMemento(final EditorTextMemento editorText) {
        value = editorText.getValue();
    }
}
