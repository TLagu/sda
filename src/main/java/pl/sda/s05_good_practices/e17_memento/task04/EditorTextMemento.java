package pl.sda.s05_good_practices.e17_memento.task04;

import lombok.Data;

@Data
public class EditorTextMemento {
    private String value;
    private EditorText editorText;

    public EditorTextMemento(final EditorText editorText) {
        this.value = editorText.getValue();
    }
}
