package pl.sda.s05_good_practices.e17_memento.task04;

import java.util.ArrayDeque;
import java.util.Deque;

public class EditorTextMementoManager {
    private final Deque<EditorTextMemento> snapshots = new ArrayDeque<>();

    public void save(final EditorText editorText) {
        snapshots.push(new EditorTextMemento(editorText));
    }

    public EditorTextMemento restore() {
        return snapshots.pop();
    }
}
