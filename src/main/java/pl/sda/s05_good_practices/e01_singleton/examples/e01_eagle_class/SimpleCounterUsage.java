package pl.sda.s05_good_practices.e01_singleton.examples.e01_eagle_class;

public class SimpleCounterUsage {
    public static void main(String[] args) {
        SimpleCounter simpleCounterA = SimpleCounter.getInstance();
        SimpleCounter simpleCounterB = SimpleCounter.getInstance();

        System.out.println(simpleCounterA == simpleCounterB);

        simpleCounterA.increment();
        simpleCounterB.increment();
        System.out.println(simpleCounterA.getCurrentCount());
    }
}
