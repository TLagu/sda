package pl.sda.s05_good_practices.e01_singleton.examples.e02_eagle_enum;

public enum SimpleCounter {
    INSTANCE;

    private int currentCount = 0;

    public int getCurrentCount() {
        return currentCount;
    }

    public void increment() {
        currentCount++;
    }
}
