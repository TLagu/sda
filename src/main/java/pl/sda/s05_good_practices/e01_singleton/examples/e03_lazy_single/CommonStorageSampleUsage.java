package pl.sda.s05_good_practices.e01_singleton.examples.e03_lazy_single;

public class CommonStorageSampleUsage {
    public static void main(String[] args) {
        CommonStorage commonStorageA = CommonStorage.getInstance();
        CommonStorage commonStorageB = CommonStorage.getInstance();

        System.out.println(commonStorageA == commonStorageB);

        commonStorageA.addValue(1);
        commonStorageB.addValue(2);

        System.out.println(commonStorageA.getValues().size());
    }
}
