package pl.sda.s05_good_practices.e01_singleton.examples.e03_lazy_single;

import java.util.ArrayList;
import java.util.List;

public class CommonStorage {
    private static CommonStorage instance;
    private List<Integer> values = new ArrayList<>();

    private CommonStorage() {
    }

    public static CommonStorage getInstance() {
        if (instance == null) {
            instance = new CommonStorage();
        }
        return instance;
    }

    public void addValue(final int value) {
        values.add(value);
    }

    public List<Integer> getValues() {
        return values;
    }
}
