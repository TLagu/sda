package pl.sda.s05_good_practices.e01_singleton.examples.e02_eagle_enum;

public class SimpleCounterUsage {
    public static void main(String[] args) {
        SimpleCounter simpleCounterA = SimpleCounter.INSTANCE;
        SimpleCounter simpleCounterB = SimpleCounter.INSTANCE;

        // porównanie enum powinno być ==
        System.out.println(simpleCounterA == simpleCounterB);

        simpleCounterA.increment();
        simpleCounterB.increment();

        System.out.println(simpleCounterA.getCurrentCount());
    }
}
