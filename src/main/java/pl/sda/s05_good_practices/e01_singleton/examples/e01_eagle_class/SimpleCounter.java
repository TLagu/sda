package pl.sda.s05_good_practices.e01_singleton.examples.e01_eagle_class;

public class SimpleCounter {
    private static final SimpleCounter INSTANCE = new SimpleCounter();
    private int currentCount = 0;

    private SimpleCounter() {
    }

    public static SimpleCounter getInstance() {
        return INSTANCE;
    }

    public int getCurrentCount() {
        return currentCount;
    }

    public void increment() {
        currentCount++;
    }
}
