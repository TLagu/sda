package pl.sda.s05_good_practices.e01_singleton.task03;

public class ServersUsage {
    public static void main(String[] args) {
        Servers serversA = Servers.getInstance();
        Servers serversB = Servers.getInstance();

        System.out.println(serversA == serversB);

        serversA.addServer("http://www.tu.i.tu");
        serversA.addServer("http://www.tu.i.tam");
        serversA.addServer("http://www.tu.i.tam.sobie.mam");
        serversA.addServer("https://www.tu.i.nigdzie");
        serversA.addServer("https://www.tu.i.wszedzie");

        System.out.println(serversA.getServers());
        System.out.println(serversA.getHttpServers());
        System.out.println(serversA.getHttpsServers());
    }
}
