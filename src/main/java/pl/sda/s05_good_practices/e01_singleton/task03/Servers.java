package pl.sda.s05_good_practices.e01_singleton.task03;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Servers {
    private static final Servers INSTANCE = new Servers();
    private List<String> servers = new ArrayList<>();

    private Servers() {
    }

    public static Servers getInstance() {
        return INSTANCE;
    }

    public boolean addServer(final String server) {
        if ((server.startsWith("http:") || server.startsWith("https:"))
                && !servers.contains(server)) {
            return servers.add(server);
        }
        return false;
    }

    public List<String> getServers() {
        return servers;
    }

    public List<String> getHttpServers() {
        return getServersByPrefix("http:");
    }

    public List<String> getHttpsServers() {
        return getServersByPrefix("https:");
    }

    private List<String> getServersByPrefix(String prefix) {
        return servers.stream()
                .filter(s -> s.startsWith(prefix))
                .collect(Collectors.toList());
    }
}
