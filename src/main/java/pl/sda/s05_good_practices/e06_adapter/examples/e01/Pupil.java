package pl.sda.s05_good_practices.e06_adapter.examples.e01;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class Pupil {
    private final String firstName;
    private final String lastName;
    private final String email;
    private final Integer age;
    private final List<Integer> grades;
}
