package pl.sda.s05_good_practices.e06_adapter.examples.e01;

import lombok.AllArgsConstructor;

import java.util.Collection;

@AllArgsConstructor
public class PupilAdapter implements Student {
    private final Pupil pupil;


    @Override
    public String getFullName() {
        return pupil.getFirstName() + " " + pupil.getLastName();
    }

    @Override
    public String getContactDetails() {
        return pupil.getEmail();
    }

    @Override
    public boolean isAdult() {
        return pupil.getAge() >= 18;
    }

    @Override
    public Collection<Integer> getResults() {
        return pupil.getGrades();
    }
}
