package pl.sda.s05_good_practices.e06_adapter.examples.e01;

import java.util.Collection;

public interface Student {
    String getFullName();
    String getContactDetails();
    boolean isAdult();
    Collection<Integer> getResults();
}
