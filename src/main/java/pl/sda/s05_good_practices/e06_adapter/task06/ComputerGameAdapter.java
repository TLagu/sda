package pl.sda.s05_good_practices.e06_adapter.task06;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ComputerGameAdapter implements PCGame {
    private final ComputerGame computerGame;

    @Override
    public String getTitle() {
        return computerGame.getName();
    }

    @Override
    public Integer getPegiAllowedAge() {
        return (Integer.parseInt(computerGame.getPegiAgeRating().toString().replaceAll("[^0-9]", "")));
    }

    @Override
    public boolean isTripleAGame() {
        return computerGame.getBudgetInMillionsOfDollars() > 50.0;
    }

    @Override
    public Requirements getRequirements() {
        return new Requirements(
                computerGame.getMinimumGpuMemoryInMegabytes() / 1024,
                computerGame.getDiskSpaceNeededInGB(),
                computerGame.getRamNeededInGb(),
                computerGame.getCoreSpeedInGhz(),
                computerGame.getCoresNeeded());
    }
}
