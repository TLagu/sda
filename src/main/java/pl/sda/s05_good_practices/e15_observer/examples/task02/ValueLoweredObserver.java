package pl.sda.s05_good_practices.e15_observer.examples.task02;

public class ValueLoweredObserver extends Observer{
    public ValueLoweredObserver(Subject subject){
        super(subject);
    }

    @Override
    public void update(final int change) {
        if (change < 0) {
            System.out.println("ValueLoweredObserver: " + this.getSubject().getValue());
        }
    }
}
