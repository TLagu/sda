package pl.sda.s05_good_practices.e15_observer.examples.task02;

public class ByTenChangedObserver extends Observer{
    public ByTenChangedObserver(Subject subject){
        super(subject);
    }

    @Override
    public void update(final int change) {
        if (Math.abs(change) > 10) {
            System.out.println("ValueLoweredObserver: " + this.getSubject().getValue());
        }
    }
}
