package pl.sda.s05_good_practices.e15_observer.examples.task02;

import lombok.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Subject {
    private final List<Observer> observers = new ArrayList<>();
    private int value = 0;

    public void subscribe(final Observer observer) {
        if (!observers.contains(observer)) {
            observers.add(observer);
        }
    }

    public void changeValue(final int change) {
        System.out.println("\nChange value: " + change);
        value += change;
        notifyAboutChange(change);
    }

    private void notifyAboutChange(int change) {
        for (final var observer : observers) {
            observer.update(change);
        }
    }

}
