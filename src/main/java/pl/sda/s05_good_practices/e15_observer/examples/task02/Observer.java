package pl.sda.s05_good_practices.e15_observer.examples.task02;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public abstract class Observer {
    private Subject subject;

    public abstract void update(final int value);
}
