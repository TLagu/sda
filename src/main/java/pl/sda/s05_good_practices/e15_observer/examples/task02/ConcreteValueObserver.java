package pl.sda.s05_good_practices.e15_observer.examples.task02;

public class ConcreteValueObserver extends Observer {
    public ConcreteValueObserver(Subject subject){
        super(subject);
    }

    @Override
    public void update(final int change) {
        System.out.println("ConcreteValueObserver: " + this.getSubject().getValue());
    }
}
