package pl.sda.s05_good_practices.e15_observer.examples.e01;

public class ObserverUsage {
    public static void main(String[] args) {
        final ChatChannel chatChannel = new ChatChannel("design-patterns");

        final BaseObserver userA = new UserObserver(chatChannel, "andrzej");
        final BaseObserver userB = new UserObserver(chatChannel, "ala");
        final BaseObserver adminA = new AdminObserver(chatChannel, "janusz");
        final BaseObserver adminB = new AdminObserver(chatChannel, "ania");

        userA.sendMessage("Hello all");
        userB.sendMessage("Hi Andrzej");
        adminA.sendMessage("Ania they can't see what we write.");
        adminB.sendMessage("Yes I know");

    }
}
