package pl.sda.s05_good_practices.e15_observer.examples.e01;

public class UserObserver extends BaseObserver{
    private final String userName;

    public UserObserver(final ChatChannel chatChannel, final String userName) {
        super(chatChannel);
        this.userName = userName;
        System.out.println(userName + " is joining the " + chatChannel.getName());
    }

    @Override
    public void handleMessage(String message, String userType) {
        if (!userType.equals("ADMIN")) {
            System.out.println(userName + " sees message " + message);
        }
    }

    @Override
    public String getObserverType() {
        return "USER";
    }
}
