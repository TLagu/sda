package pl.sda.s05_good_practices.e15_observer.examples.task02;

public class Main {
    public static void main(String[] args) {
        Subject subject = new Subject();
        subject.subscribe(new ConcreteValueObserver(subject));
        subject.subscribe(new ValueLoweredObserver(subject));
        subject.subscribe(new ByTenChangedObserver(subject));
        subject.changeValue(10);
        subject.changeValue(-20);
        subject.changeValue(100);
        subject.changeValue(1);
    }
}
