package pl.sda.s05_good_practices.e15_observer.examples.e01;

public class AdminObserver extends BaseObserver{
    private final String adminName;
    public AdminObserver(final ChatChannel chatChannel, final String adminName) {
        super(chatChannel);
        this.adminName = adminName;
        System.out.println(adminName + " joined " + chatChannel.getName() + " as admin.");
    }

    @Override
    public void handleMessage(String message, String userType) {
        System.out.println(adminName + " sees " + message + " from user whose type is USER");
    }

    @Override
    public String getObserverType() {
        return "ADMIN";
    }
}
