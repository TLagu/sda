package pl.sda.s05_good_practices.e02_builder.e01;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.ToString;

import java.util.List;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
public class Weapon {
    private final String type;
    private final String name;
    private final Integer damage;
    private final Long durability;
    private final List<String> perks;

    public static class Builder {
        private String type;
        private String name;
        private Integer damage;
        private Long durability;
        private List<String> perks;

        public Builder(String type, String name) {
            this.type = type;
            this.name = name;
        }

        //metody budownicze
//        public Builder withType(final String type) {
//            this.type = type;
//            return this;
//        }
//
//        public Builder withName(final String name) {
//            this.name = name;
//            return this;
//        }

        public Builder withDamage(final Integer damage) {
            this.damage = damage;
            return this;
        }

        public Builder withDurability(final Long durability) {
            this.durability = durability;
            return this;
        }

        public Builder withPerks(final List<String> perks) {
            this.perks = perks;
            return this;
        }

        public Weapon build() {
            return new Weapon(type, name, damage, durability, perks);
        }
    }
}
