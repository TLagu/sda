package pl.sda.s05_good_practices.e02_builder.e01;

import java.util.List;

public class WeaponUsage {
    public static void main(String[] args) {
        final Weapon laserGun = new Weapon.Builder("Gun", "LaserGun")
                .withDamage(123)
                .withPerks(List.of("Color:red"))
                .withDurability(50L)
                .build();

        final Weapon shutGun = new Weapon.Builder("Gun", "ShutGun")
                .withDurability(25L)
                .build();
        System.out.println(laserGun);
        System.out.println(shutGun);
    }
}
