package pl.sda.s05_good_practices.e02_builder.e03;

public class LombokBuilderUsage {
    public static void main(String[] args) {
        GraphicCard graphicCard = GraphicCard.builder()
                .memoryInMb(2048)
                .modelName("GF 1660")
                .producer("Asus")
                .series("1xxx")
                .build();
        System.out.println(graphicCard);
    }
}
