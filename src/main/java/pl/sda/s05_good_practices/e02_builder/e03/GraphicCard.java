package pl.sda.s05_good_practices.e02_builder.e03;

import lombok.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Builder
@ToString
public class GraphicCard {
    private int memoryInMb;
    private String producer;
    private String series;
    private String modelName;
}
