package pl.sda.s05_good_practices.e02_builder.e02;

public class ToyBuilderUsage {
    public static void main(String[] args) {
        final Toy toy = new ToyBuilder()
                .withMadeOf("plastic")
                .withName("Resorak")
                .withType("Small car")
                .build();
        System.out.println(toy);
    }
}
