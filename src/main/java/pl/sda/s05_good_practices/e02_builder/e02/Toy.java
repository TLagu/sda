package pl.sda.s05_good_practices.e02_builder.e02;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class Toy {
    private String name;
    private String type;
    private String madeOf;
}
