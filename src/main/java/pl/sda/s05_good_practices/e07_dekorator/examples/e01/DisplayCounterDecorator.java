package pl.sda.s05_good_practices.e07_dekorator.examples.e01;

public class DisplayCounterDecorator implements FragStatistics{
    private final FragStatistics fragStatistics;

    public DisplayCounterDecorator(FragStatistics fragStatistics) {
        this.fragStatistics = fragStatistics;
    }

    @Override
    public int incrementFragCount() {
        int fragCount = fragStatistics.incrementFragCount();
        System.out.println("Your frag count in now " + fragCount);
        return fragCount;
    }

    @Override
    public int incrementDeathCount() {
        int deathCount = fragStatistics.incrementDeathCount();
        System.out.println("Your death count in now " + deathCount);
        return deathCount;
    }

    @Override
    public void reset() {
        fragStatistics.reset();
        System.out.println("Stats reset - KDR is equal to 0");
    }
}
