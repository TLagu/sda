package pl.sda.s05_good_practices.e07_dekorator.examples.e01;

public interface FragStatistics {
    int incrementFragCount();
    int incrementDeathCount();
    void reset();
}
