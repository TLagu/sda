package pl.sda.s05_good_practices.e07_dekorator.examples.e01;

public class DecoratorUsage {
    public static void main(String[] args) {
        FragStatistics statistics = new FirstPersonShooterFragStatistics();

        statistics.incrementDeathCount(); // na ekranie nic sie nie pojawi
        statistics.incrementFragCount(); // na ekranie nic sie nie pojawi

        // wykorzystanie dekoratorów
        //FragStatistics decoratedStatistics = new FragDeathRatioDecorator(new FragInfoDecorator(statistics));
        FragStatistics decoratedStatistics = new FragDeathRatioDecorator(
                new FragInfoDecorator(
                        new DeathCountInfoDecorator(
                                new DisplayCounterDecorator(statistics))));
        decoratedStatistics.incrementFragCount();
        decoratedStatistics.incrementFragCount();
        decoratedStatistics.incrementFragCount();
        decoratedStatistics.incrementDeathCount();
    }
}
