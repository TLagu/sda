package pl.sda.s05_good_practices.e07_dekorator.examples.e01;

public class DeathCountInfoDecorator implements FragStatistics{
    private final FragStatistics fragStatistics;

    public DeathCountInfoDecorator(FragStatistics fragStatistics) {
        this.fragStatistics = fragStatistics;
    }

    @Override
    public int incrementFragCount() {
        return fragStatistics.incrementFragCount();
    }

    @Override
    public int incrementDeathCount() {
        System.out.println("Fragged by an enemy!!!");
        return fragStatistics.incrementDeathCount();
    }

    @Override
    public void reset() {
        fragStatistics.reset();
    }
}
