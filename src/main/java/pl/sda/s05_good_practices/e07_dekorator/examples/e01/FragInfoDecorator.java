package pl.sda.s05_good_practices.e07_dekorator.examples.e01;

public class FragInfoDecorator implements FragStatistics{
    private final FragStatistics fragStatistics;

    public FragInfoDecorator(FragStatistics fragStatistics) {
        this.fragStatistics = fragStatistics;
    }

    @Override
    public int incrementFragCount() {
        System.out.println("Enemy fragged");
        return fragStatistics.incrementFragCount();
    }

    @Override
    public int incrementDeathCount() {
        return fragStatistics.incrementDeathCount();
    }

    @Override
    public void reset() {
        fragStatistics.reset();
    }
}
