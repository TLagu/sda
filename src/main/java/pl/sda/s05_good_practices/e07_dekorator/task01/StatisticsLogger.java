package pl.sda.s05_good_practices.e07_dekorator.task01;

import java.util.List;

public interface StatisticsLogger {
    void displayStatistics();
    List<Double> getExecutionTimes();
}
