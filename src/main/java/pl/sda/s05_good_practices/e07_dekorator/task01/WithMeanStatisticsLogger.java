package pl.sda.s05_good_practices.e07_dekorator.task01;

import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class WithMeanStatisticsLogger implements StatisticsLogger {
    private final StatisticsLogger statistics;

    @Override
    public void displayStatistics() {
        List<Double> et = statistics.getExecutionTimes();
        System.out.println("Srednia wynikow: "
                + (et.stream().reduce(0.0, Double::sum) / et.size()));
        System.out.println("Srednia wynikow: "
                + (et.stream().mapToDouble((Double x) -> x).sum() / et.size()));
        System.out.println(getExecutionTimes());
    }

    @Override
    public List<Double> getExecutionTimes() {
        return statistics.getExecutionTimes();
    }
}
