package pl.sda.s05_good_practices.e07_dekorator.task01;

import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class WithSummaryStatisticsLogger implements StatisticsLogger{
    private final StatisticsLogger statistics;
    @Override
    public void displayStatistics() {
        List<Double> et = statistics.getExecutionTimes();
        System.out.println("Liczba rekordow: " + et.size());
        System.out.println("Suma: " + et.stream().reduce((double) 0, Double::sum));
        // et.stream().reduce(Double.MAX_VALUE, (minValue, time) -> minValue > time ? time : minValue);
        et.stream().min(Double::compare).ifPresent(m -> System.out.println("Wartosc min: " + m));
        et.stream().max(Double::compare).ifPresent(m -> System.out.println("Wartosc max: " + m));
    }

    @Override
    public List<Double> getExecutionTimes() {
        return statistics.getExecutionTimes();
    }
}
