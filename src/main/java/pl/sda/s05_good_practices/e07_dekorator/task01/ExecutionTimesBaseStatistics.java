package pl.sda.s05_good_practices.e07_dekorator.task01;

import java.util.List;

public class ExecutionTimesBaseStatistics implements StatisticsLogger{
    private final List<Double> executionTimes;
    public ExecutionTimesBaseStatistics(final List<Double> executionTimes) {
        this.executionTimes = executionTimes;
    }
    @Override
    public void displayStatistics() {
        final StringBuilder stringBuilder = new StringBuilder();
        executionTimes.forEach(executionTime -> stringBuilder.append("Execution time: ")
                .append(executionTime).append("\n"));
        System.out.println(stringBuilder);
    }
    @Override
    public List<Double> getExecutionTimes() {
        return executionTimes;
    }

}
