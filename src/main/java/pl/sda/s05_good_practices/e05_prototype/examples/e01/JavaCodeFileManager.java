package pl.sda.s05_good_practices.e05_prototype.examples.e01;

public class JavaCodeFileManager {
    private static final JavaCodeFile BASE_FILE = new JavaCodeFile("SDA license", "into", "file", "java");

    public JavaCodeFile createFileWithContent(final String fileName, final String content) throws CloneNotSupportedException {
        JavaCodeFile baseFileClone = BASE_FILE.createClone();
        baseFileClone.setCode(content);
        baseFileClone.setFileName(fileName);
        return baseFileClone;
    }
}
