package pl.sda.s05_good_practices.e05_prototype.examples.e01;

public class SimplePrototypeDemo {
    public static void main(String[] args) throws CloneNotSupportedException {
        final JavaCodeFileManager javaCodeFileManager = new JavaCodeFileManager();
        JavaCodeFile fileA = javaCodeFileManager.createFileWithContent("Integers", "int");
        JavaCodeFile fileB = javaCodeFileManager.createFileWithContent("Strings", "String");
        System.out.println(fileA);
        System.out.println(fileB);
    }
}
