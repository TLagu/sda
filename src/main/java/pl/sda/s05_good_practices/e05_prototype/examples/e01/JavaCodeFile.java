package pl.sda.s05_good_practices.e05_prototype.examples.e01;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class JavaCodeFile implements Cloneable {
    private String licenseContent;
    private String code;
    private String fileName;
    private String fileExtension;

    public JavaCodeFile(String licenseContent, String fileExtension) {
        this.licenseContent = licenseContent;
        this.fileExtension = fileExtension;
    }

    public JavaCodeFile createClone() throws CloneNotSupportedException {
        return (JavaCodeFile) clone();
    }

}
