package pl.sda.s05_good_practices.e08_fasada.examples.e02;

public interface Encryptor {
    String encrypt(String toEncrypt);
}
