package pl.sda.s05_good_practices.e08_fasada.examples.e01;

public interface PaymentService {
    void pay(Long productId, int amount);
}
