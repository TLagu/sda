package pl.sda.s05_good_practices.e08_fasada.examples.e02;

public class SCryptEncryptor implements Encryptor{
    @Override
    public String encrypt(String toEncrypt) {
        return "encrypting " + toEncrypt + " with SCrypt";
    }
}
