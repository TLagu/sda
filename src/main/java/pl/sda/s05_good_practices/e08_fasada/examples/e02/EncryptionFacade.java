package pl.sda.s05_good_practices.e08_fasada.examples.e02;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class EncryptionFacade implements Encryptors {
    private final SCryptEncryptor sCryptEncryptor;
    private final BCryptEncryptor bCryptEncryptor;
    private final NoOpEncryptor noOpEncryptor;

    @Override
    public String encryptWithoutModification(String toEncrypt) {
        return noOpEncryptor.encrypt(toEncrypt);
    }

    @Override
    public String encryptWithBCrypt(String toEncrypt) {
        return bCryptEncryptor.encrypt(toEncrypt);
    }

    @Override
    public String encryptWithSCrypt(String toEncrypt) {
        return sCryptEncryptor.encrypt(toEncrypt);
    }
}
