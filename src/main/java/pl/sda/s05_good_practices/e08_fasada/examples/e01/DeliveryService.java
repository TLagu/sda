package pl.sda.s05_good_practices.e08_fasada.examples.e01;

public interface DeliveryService {
    void deliverProduct(Long productsId, int amount, String recipient);
}
