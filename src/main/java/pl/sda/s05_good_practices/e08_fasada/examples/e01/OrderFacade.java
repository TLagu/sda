package pl.sda.s05_good_practices.e08_fasada.examples.e01;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class OrderFacade {
    private final DeliveryService deliveryService;
    private final PaymentService paymentService;
    private final ProductAvailabilityService productAvailabilityService;

    public boolean placeOrder(final Long productId, final int amount, final String recipient) {
        if (productAvailabilityService.isAvailable(productId)) {
            paymentService.pay(productId, amount);
            deliveryService.deliverProduct(productId, amount, recipient);
            return true;
        }
        return false;
    }
}
