package pl.sda.s05_good_practices.e14_strategy.examples.e02;

public class ReplaceWithUnderscoreStrategy implements SpacesModificationTemplate {
    @Override
    public void processIfSpace(StringBuilder stringBuilder) {
        stringBuilder.append('_');
    }
}
