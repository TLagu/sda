package pl.sda.s05_good_practices.e14_strategy.examples.e02;

public interface SpacesModificationTemplate {

    default void processIfSpace(StringBuilder stringBuilder) {
        // noop
    }

    default void processIfNotSpace(StringBuilder stringBuilder, char c) {
        stringBuilder.append(c);
    }

    default String modify(String input) {
        StringBuilder stringBuilder = new StringBuilder();
        for (char c : input.toCharArray()) {
            if (c == ' ') {
                processIfSpace(stringBuilder);
            } else {
                processIfNotSpace(stringBuilder, c);
            }
        }
        return stringBuilder.toString();
    }
}
