package pl.sda.s05_good_practices.e14_strategy.examples.e01;

public interface SpacesModificationStrategy {
    String modify(String input);
}
