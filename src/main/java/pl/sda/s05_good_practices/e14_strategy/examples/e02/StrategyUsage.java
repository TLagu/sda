package pl.sda.s05_good_practices.e14_strategy.examples.e02;

public class StrategyUsage {
    public static void main(String[] args) {
        final String input = "hello from SDA knowledge base!";

        final SpacesModificationStrategyProvider provider = new SpacesModificationStrategyProvider();

        StrategyType strategyType = StrategyType.DOUBLE;
        SpacesModificationTemplate strategy = provider.get(strategyType);
        String output = strategy.modify(input);
        System.out.println("Result is (double): " + output);

        strategyType = StrategyType.REMOVE;
        strategy = provider.get(strategyType);
        output = strategy.modify(input);
        System.out.println("Result is (remove): " + output);

        strategyType = StrategyType.REPLACE;
        strategy = provider.get(strategyType);
        output = strategy.modify(input);
        System.out.println("Result is (replace): " + output);
    }
}
