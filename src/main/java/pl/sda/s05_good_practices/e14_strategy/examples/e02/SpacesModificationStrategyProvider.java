package pl.sda.s05_good_practices.e14_strategy.examples.e02;

public class SpacesModificationStrategyProvider {
    public SpacesModificationTemplate get(final StrategyType strategyType) {
        switch (strategyType) {
            case DOUBLE:
                return new DoubleSpacesStrategy();
            case REMOVE:
                return new RemoveSpacesStrategy();
            case REPLACE:
                return new ReplaceWithUnderscoreStrategy();
        }
        throw new UnsupportedOperationException("Unsupported strategy type");
    }
}
