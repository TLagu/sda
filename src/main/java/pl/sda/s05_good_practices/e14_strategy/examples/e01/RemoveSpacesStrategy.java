package pl.sda.s05_good_practices.e14_strategy.examples.e01;

public class RemoveSpacesStrategy implements SpacesModificationStrategy{
    @Override
    public String modify(String input) {
        final StringBuilder stringBuilder = new StringBuilder(input.length());
        for (final char c : input.toCharArray()) {
            if (c != ' ') {
                stringBuilder.append(c);
            }
        }
        return stringBuilder.toString();
    }
}
