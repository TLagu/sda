package pl.sda.s05_good_practices.e14_strategy.examples.e02;

public enum StrategyType {
    DOUBLE,
    REMOVE,
    REPLACE
}
