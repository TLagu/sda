package pl.sda.s05_good_practices.e12_flyweight.examples.e01;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Car {
    private String producer;
    private String VIN;
    private String version;
    private String modelName;
    private Engine engine;
}
