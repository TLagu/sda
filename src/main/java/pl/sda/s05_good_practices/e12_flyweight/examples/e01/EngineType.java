package pl.sda.s05_good_practices.e12_flyweight.examples.e01;

public enum EngineType {
    DIESEL, GASOLINE, ELECTRIC
}
