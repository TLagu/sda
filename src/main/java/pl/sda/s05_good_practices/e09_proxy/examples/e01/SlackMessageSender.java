package pl.sda.s05_good_practices.e09_proxy.examples.e01;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

//@Slf4j
public class SlackMessageSender implements MessageSender{
    private final List<Message> messages = new ArrayList<>();
    @Override
    public void sendMessage(String channelName, String username, String messageText) {
        final Message message = new Message(channelName, username, messageText);
        System.out.println("Send message " + message);
        //log.info("Send message {}", message);
        messages.add(message);
        System.out.println("Messages: " + messages);
        //log.info("Messages: {}", messages);
    }
}
