package pl.sda.s05_good_practices.e09_proxy.examples.e01;

public interface MessageSender {
    void sendMessage(String channelName, String username, String message);
}
