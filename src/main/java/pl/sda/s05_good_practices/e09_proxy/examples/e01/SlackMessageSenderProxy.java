package pl.sda.s05_good_practices.e09_proxy.examples.e01;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;
import java.util.UUID;

//@Slf4j
@AllArgsConstructor
public class SlackMessageSenderProxy implements MessageSender{
    private final MessageSender messageSender;
    private final SessionTokens sessionTokens;
    private final TokenValidator tokenValidator;

    @Override
    public void sendMessage(String channelName, String username, String message) {
        final Optional<UUID> userTokenOptional = sessionTokens.getUserToken(username);
        if (userTokenOptional.isPresent()) {
            final UUID existingToken = userTokenOptional.get();
            if (tokenValidator.isExpired(existingToken)) {
                messageSender.sendMessage(channelName, username, message);
            } else {
                System.out.println("Message from " + username + " not sent to channel " + channelName + " because user has no valid authorization.");
                //log.info("Message from {} not sent to channel {} because user has no valid authorization.", username, channelName);
            }
        }
    }
}
