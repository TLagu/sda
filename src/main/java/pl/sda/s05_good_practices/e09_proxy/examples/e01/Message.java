package pl.sda.s05_good_practices.e09_proxy.examples.e01;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Message {
    private String channelName;
    private LocalDateTime postDate;
    private String author;
    private String text;

    public Message(final String channelName, final String author, final String text) {
        this.channelName = channelName;
        this.author = author;
        this.text = text;
        this.postDate = LocalDateTime.now();
    }
}
