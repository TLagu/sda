package pl.sda.s05_good_practices.e04_abstract_factory.examples.e01;

public enum Type {
    SEDAN, HATCHBACK, COMBI
}
