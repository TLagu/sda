package pl.sda.s05_good_practices.e04_abstract_factory.examples.e01;

public class ToyotaCorollaSedan extends ToyotaCorolla {
    @Override
    public Type getType() {
        return Type.SEDAN;
    }

    @Override
    public Integer getCylindersNum() {
        return 4;
    }

    @Override
    public Float getEngineVolume() {
        return 1.8F;
    }

    @Override
    public Integer getTrunkSize() {
        return 400;
    }
}
