package pl.sda.s05_good_practices.e04_abstract_factory.examples.e01;

public abstract class AudiA4 extends AbstractCar {
    @Override
    public String getModelName() {
        return "A4";
    }

    @Override
    public String getProducer() {
        return "Audi";
    }
}
