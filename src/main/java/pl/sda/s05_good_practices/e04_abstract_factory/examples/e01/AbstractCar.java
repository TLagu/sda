package pl.sda.s05_good_practices.e04_abstract_factory.examples.e01;

public abstract class AbstractCar implements Car {
    @Override
    public String toString() {
        return "Car: "
                + getProducer() + ", "
                + getModelName() + ", "
                + getType() + " has cylinders "
                + getCylindersNum() + " and engine "
                + getEngineVolume() + " and trunk with size "
                + getTrunkSize();
    }
}
