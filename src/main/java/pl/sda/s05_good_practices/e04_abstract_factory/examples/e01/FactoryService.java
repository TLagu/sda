package pl.sda.s05_good_practices.e04_abstract_factory.examples.e01;

public class FactoryService {
    public static void main(String[] args) {
        FactoryProvider car = new FactoryProvider();
        car.createFactory(CarType.AUDI_A4);
        //System.out.println(car.);
    }
}
