package pl.sda.s05_good_practices.e04_abstract_factory.examples.e01;

public class ToyotaCorollaFactory implements CarFactory {
    @Override
    public Car createSedan() {
        return new ToyotaCorollaSedan();
    }

    @Override
    public Car createCombi() {
        return new ToyotaCorollaCombi();
    }

    @Override
    public Car createHatchback() {
        return new ToyotaCorollaHatchback();
    }
}
