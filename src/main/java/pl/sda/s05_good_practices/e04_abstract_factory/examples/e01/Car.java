package pl.sda.s05_good_practices.e04_abstract_factory.examples.e01;

public interface Car {
    Type getType();

    String getModelName();

    Integer getCylindersNum();

    String getProducer();

    Float getEngineVolume();

    Integer getTrunkSize();
}
