package pl.sda.s05_good_practices.e04_abstract_factory.examples.e01;

public interface CarFactory {
    Car createSedan();

    Car createCombi();

    Car createHatchback();
}
