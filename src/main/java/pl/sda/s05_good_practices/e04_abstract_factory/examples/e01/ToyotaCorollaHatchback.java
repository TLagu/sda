package pl.sda.s05_good_practices.e04_abstract_factory.examples.e01;

public class ToyotaCorollaHatchback extends ToyotaCorolla {

    @Override
    public Type getType() {
        return Type.HATCHBACK;
    }

    @Override
    public Integer getCylindersNum() {
        return 4;
    }

    @Override
    public Float getEngineVolume() {
        return 2.0F;
    }

    @Override
    public Integer getTrunkSize() {
        return 420;
    }
}
