package pl.sda.s05_good_practices.e16_chain_of_responsibility.examples.e01;

public interface Credentials {
    String getCredentials(String userId);
}
