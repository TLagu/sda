package pl.sda.s05_good_practices.e16_chain_of_responsibility.examples.e01;

public class BearerToken implements Credentials{
    @Override
    public String getCredentials(String userId) {
        return "1/mZ1edKKACtPAb7zGlwSzvs72PvhAbGmB8K1ZrGxpcNM"; // dummy implementation
    }
}
