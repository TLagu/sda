package pl.sda.s05_good_practices.e16_chain_of_responsibility.examples.e01;

import java.util.UUID;

public class AwsSignature implements Credentials {
    @Override
    public String getCredentials(String userId) {
        return UUID.randomUUID().toString(); // dummy implementation
    }
}
