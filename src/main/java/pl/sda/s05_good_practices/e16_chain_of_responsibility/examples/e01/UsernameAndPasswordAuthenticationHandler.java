package pl.sda.s05_good_practices.e16_chain_of_responsibility.examples.e01;

import java.util.Random;

public class UsernameAndPasswordAuthenticationHandler implements AuthenticationHandler{
    @Override
    public boolean authenticate(Credentials credentials) {
        if (supports(credentials.getClass())) {
            return isPasswordValid(credentials);
        }
        return false;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isInstance(UsernameAndPasswordCredentials.class);
    }

    public boolean isPasswordValid(Credentials credentials) {
        return new Random().nextBoolean(); // dummy implementation - use real credentials in real implementation
    }
}
