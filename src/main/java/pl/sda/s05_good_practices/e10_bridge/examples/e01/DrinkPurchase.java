package pl.sda.s05_good_practices.e10_bridge.examples.e01;

public interface DrinkPurchase {
    Drink buy(final Double cost);
}
