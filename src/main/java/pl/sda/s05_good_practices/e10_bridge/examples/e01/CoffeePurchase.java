package pl.sda.s05_good_practices.e10_bridge.examples.e01;

public class CoffeePurchase implements DrinkPurchase{
    @Override
    public Drink buy(Double cost) {
        System.out.println("Buying a coffee for " + cost);
        return new Coffee();
    }
}
