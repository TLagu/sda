package pl.sda.s05_good_practices.e10_bridge.examples.e01;

public enum Taste {
    BITTER, SWEET, CHOCOLATE, NUT, LEMON
}
