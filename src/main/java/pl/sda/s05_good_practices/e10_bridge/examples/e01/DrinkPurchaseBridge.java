package pl.sda.s05_good_practices.e10_bridge.examples.e01;

public class DrinkPurchaseBridge {
    public static void main(String[] args) {
        CoffeePurchase coffeePurchase = new CoffeePurchase();
        coffeePurchase.buy(20.0);
        TeaPurchase teaPurchase = new TeaPurchase();
        teaPurchase.buy(20.0);
    }
}
