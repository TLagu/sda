package pl.sda.s05_good_practices.e10_bridge.examples.e01;

public interface Drink {
    String getVolume();
    boolean isAddictive();
    int getNumberOfSugarLumps();
    Taste getTaste();
}
