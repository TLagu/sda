package pl.sda.s08_jdbc_hibernate.e06_hibernate_mappedsuperclass;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;

public class ProductMain {
    public static void main(String[] args) {
        File f = new File("c:\\Users\\Lagu\\IdeaProjects\\sda\\src\\main\\resources\\pl\\sda\\s08_jdbc_hibernate\\e06_hibernate_mappedsuperclass\\hibernate.cfg.xml");
        SessionFactory sessionFactory = new Configuration()
                .configure(f)
                .buildSessionFactory();
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();

        SpecialProduct specialProduct = new SpecialProduct(null, "special product", BigDecimal.valueOf(100L), "special", "desc");
        session.save(specialProduct);

        final SpecialProduct specialProductFromDB = session.get(SpecialProduct.class, 1);

        System.out.println("Produkty z zamówienia: " + specialProductFromDB.getDescription());

        String hql = "FROM SpecialProduct";
        final Query query = session.createQuery(hql);
        final List<SpecialProduct> list = query.list();
        list.forEach(System.out::println);

        transaction.commit();
        session.close();
        sessionFactory.close();

    }
}
