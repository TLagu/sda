package pl.sda.s08_jdbc_hibernate.e06_hibernate_mappedsuperclass;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import java.math.BigDecimal;

@NoArgsConstructor
@Setter
@Getter
@Entity
public class SpecialProduct extends Product {
    private String description;

    public SpecialProduct(Integer id, String name, BigDecimal price, String kind, String description) {
        super(id, name, price, kind);
        this.description = description;
    }

    @Override
    public String toString() {
        return "SpecialProduct{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", kind='" + kind + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
