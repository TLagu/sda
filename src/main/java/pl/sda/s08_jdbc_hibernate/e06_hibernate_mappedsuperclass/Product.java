package pl.sda.s08_jdbc_hibernate.e06_hibernate_mappedsuperclass;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@NoArgsConstructor
@Getter
@Setter
@MappedSuperclass
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer id;
    protected String name;
    @Column(columnDefinition = "DECIMAL(7,2)")
    protected BigDecimal price;
    protected String kind;

    public Product(Integer id, String name, BigDecimal price, String kind) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.kind = kind;
    }
}
