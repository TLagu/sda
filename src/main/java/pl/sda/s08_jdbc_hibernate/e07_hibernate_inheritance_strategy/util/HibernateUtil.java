package pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.File;

public final class HibernateUtil {

    private final static String intelliJ = "c:\\Users\\Lagu\\IdeaProjects\\";
    private final static String resources = intelliJ + "sda\\src\\main\\resources\\";
    private final static String project = resources + "pl\\sda\\s08_jdbc_hibernate\\e07_hibernate_inheritance_strategy\\";
    private final static File file = new File(project + "hibernate.cfg.xml");
    private static SessionFactory sessionFactory = new Configuration().configure(file).buildSessionFactory();

    private HibernateUtil() {

    }

    public static Session getSession() {
        return sessionFactory.openSession();
    }

    public static void closeSessionFactory() {
        sessionFactory.close();
    }
}
