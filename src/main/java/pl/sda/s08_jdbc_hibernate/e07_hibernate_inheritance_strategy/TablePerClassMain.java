package pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.table_per_class.DirectorV1;
import pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.table_per_class.EmployeeV1;
import pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.table_per_class.OfficeEmployeeV1;
import pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.util.HibernateUtil;

public class TablePerClassMain {
    public static void main(String[] args) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        EmployeeV1 officeEmployee = new OfficeEmployeeV1("Jan", "kowalski", "Excel");
        EmployeeV1 director = new DirectorV1("Michał", "Nowak", "administracja");

        session.save(officeEmployee);
        session.save(director);

        transaction.commit();
        session.close();
        HibernateUtil.closeSessionFactory();
    }
}
