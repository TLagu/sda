package pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.compositekeys;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@IdClass(AccountId.class)
public class Account {

    @Id
    @Column(length = 24)
    private String number;
    @Id
    @Column(length = 20)
    private String type;

    @Column(columnDefinition = "DECIMAL(7,2)")
    private BigDecimal amount;
}
