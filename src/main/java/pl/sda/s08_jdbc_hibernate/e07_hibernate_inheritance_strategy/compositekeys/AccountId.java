package pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.compositekeys;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class AccountId implements Serializable {

    private String number;
    private String type;

}
