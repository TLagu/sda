package pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.joined;

import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@Getter
@Setter
@Entity
@PrimaryKeyJoinColumn(name = "id")
public class DirectorV3 extends EmployeeV3 {

    public DirectorV3(String name, String surname, String department) {
        super(name, surname);
        this.department = department;
    }

    @Column(length = 50)
    private String department;

    @Override
    public String toString() {
        return "DirectorV3{" +
                "department='" + department + '\'' +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
