package pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.joined;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class EmployeeV3 {

    public EmployeeV3(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer id;

    @Column(length = 25)
    protected String name;

    @Column(length = 25)
    protected String surname;

}
