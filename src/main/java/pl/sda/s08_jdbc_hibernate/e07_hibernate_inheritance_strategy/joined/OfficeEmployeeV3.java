package pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.joined;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@NoArgsConstructor
@Data
@Entity
@PrimaryKeyJoinColumn(name = "id")
public class OfficeEmployeeV3 extends EmployeeV3 {

    private String skills;

    public OfficeEmployeeV3(String name, String surname, String skills) {
        super(name, surname);
        this.skills = skills;
    }

}
