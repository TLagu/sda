package pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.single_table.DirectorV2;
import pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.single_table.EmployeeV2;
import pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.single_table.OfficeEmployeeV2;
import pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.util.HibernateUtil;

public class SingleTableMain {
    public static void main(String[] args) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        EmployeeV2 officeEmployeeV2 = new OfficeEmployeeV2("Jan", "kowalski", "Excel");
        EmployeeV2 directorV2 = new DirectorV2("Michał", "Nowak", "administracja");

        session.save(officeEmployeeV2);
        session.save(directorV2);

        transaction.commit();
        session.close();
        HibernateUtil.closeSessionFactory();
    }
}
