package pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.joined.DirectorV3;
import pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.joined.EmployeeV3;
import pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.joined.OfficeEmployeeV3;
import pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.util.HibernateUtil;

public class JoinedMain {
    public static void main(String[] args) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        EmployeeV3 officeEmployee = new OfficeEmployeeV3("Jan", "kowalski", "Excel");
        EmployeeV3 director = new DirectorV3("Michał", "Nowak", "administracja");

        session.save(officeEmployee);
        session.save(director);

        final DirectorV3 directorVFromDB = session.get(DirectorV3.class, 2);
        System.out.println("director from db: " + directorVFromDB);
        final EmployeeV3 employeeFromDB = session.get(EmployeeV3.class, 2);
        System.out.println("employee from db: " + employeeFromDB);

        transaction.commit();
        session.close();
        HibernateUtil.closeSessionFactory();
    }
}
