package pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.single_table;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@NoArgsConstructor
@Data
@Entity
@DiscriminatorValue("DIRECTOR")
public class DirectorV2 extends EmployeeV2 {

    public DirectorV2(String name, String surname, String department) {
        super(name, surname);
        this.department = department;
    }

    @Column(length = 50)
    private String department;

}
