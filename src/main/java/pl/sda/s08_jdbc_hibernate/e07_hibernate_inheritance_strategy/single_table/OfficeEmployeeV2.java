package pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.single_table;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@Data
@Entity
@DiscriminatorValue("OFFICE_EMPLOYEE")
public class OfficeEmployeeV2 extends EmployeeV2 {

    private String skills;

    public OfficeEmployeeV2(String name, String surname, String skills) {
        super(name, surname);
        this.skills = skills;
    }

}
