package pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.single_table;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@Data
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "employeeType")
public abstract class EmployeeV2 {

    public EmployeeV2(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer id;

    @Column(length = 25)
    protected String name;

    @Column(length = 25)
    protected String surname;

    @Column(insertable = false, updatable = false)
    protected String employeeType;

}
