package pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.table_per_class;

import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@Data
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class EmployeeV1 {

    public EmployeeV1(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Integer id;

    @Column(length = 25)
    protected String name;

    @Column(length = 25)
    protected String surname;

}
