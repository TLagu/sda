package pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.table_per_class;

import lombok.*;

import javax.persistence.Entity;

@NoArgsConstructor
@Data
@Entity
public class OfficeEmployeeV1 extends EmployeeV1 {

    private String skills;

    public OfficeEmployeeV1(String name, String surname, String skills) {
        super(name, surname);
        this.skills = skills;
    }
}
