package pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.table_per_class;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;

@NoArgsConstructor
@Data
@Entity
public class DirectorV1 extends EmployeeV1 {

    public DirectorV1(String name, String surname, String department) {
        super(name, surname);
        this.department = department;
    }

    @Column(length = 50)
    private String department;

}
