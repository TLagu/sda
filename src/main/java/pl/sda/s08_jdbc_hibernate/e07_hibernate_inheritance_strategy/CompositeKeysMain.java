package pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.compositekeys.Account;
import pl.sda.s08_jdbc_hibernate.e07_hibernate_inheritance_strategy.util.HibernateUtil;

import java.math.BigDecimal;

public class CompositeKeysMain {
    public static void main(String[] args) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Account account = new Account("12345", "indywidualne", new BigDecimal(1213.343));
        session.save(account);

        transaction.commit();
        session.close();
        HibernateUtil.closeSessionFactory();
    }
}
