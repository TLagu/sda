package pl.sda.s08_jdbc_hibernate.e05_hibernate_many2many;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Child {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(length = 30, nullable = false)
    private String firstName;
    @Column(length = 30, nullable = false)
    private String lastName;
    @ManyToMany
    @JoinTable(
            name = "child_parent",
            joinColumns = @JoinColumn(name = "child_id", referencedColumnName="id"),
            inverseJoinColumns = @JoinColumn(name="parent_id", referencedColumnName="id")
    ) // fakultatywne
    private List<Parent> parents;
}
