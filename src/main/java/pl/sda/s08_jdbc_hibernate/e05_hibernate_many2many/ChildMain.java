package pl.sda.s08_jdbc_hibernate.e05_hibernate_many2many;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;

public class ChildMain {
    public static void main(String[] args) {
        File f = new File("c:\\Users\\Lagu\\IdeaProjects\\sda\\src\\main\\resources\\pl\\sda\\s08_jdbc_hibernate\\e05_hibernate_many2many\\hibernate.cfg.xml");
        SessionFactory sessionFactory = new Configuration()
                .configure(f)
                .buildSessionFactory();
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();

        Parent parent1 = new Parent(null, "Anna", "Nowak", null);
        Parent parent2 = new Parent(null, "Jan", "Nowak", null);
        Child child1 = new Child(null, "Beata", "Nowak", Arrays.asList(parent1, parent2));
        Child child2 = new Child(null, "Krzysztof", "Nowak", Arrays.asList(parent1, parent2));
        parent1.setChildren(Arrays.asList(child1, child2));
        parent2.setChildren(Arrays.asList(child1, child2));
        session.save(child1);
        session.save(child2);
        session.save(parent1);
        session.save(parent2);

        final Parent parentFromDB = session.get(Parent.class, 1);
        final Child childFromDB = session.get(Child.class, 1);

        System.out.println("Dzieci: ");
        parentFromDB.getChildren().forEach(o -> System.out.println(o.getFirstName()));
        System.out.println("Rodzice: ");
        childFromDB.getParents().forEach(o -> System.out.println(o.getFirstName()));

        transaction.commit();
        session.close();
        sessionFactory.close();

    }
}
