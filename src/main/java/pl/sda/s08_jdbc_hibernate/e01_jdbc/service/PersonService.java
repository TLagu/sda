package pl.sda.s08_jdbc_hibernate.e01_jdbc.service;

import lombok.RequiredArgsConstructor;
import pl.sda.s08_jdbc_hibernate.e01_jdbc.dao.PersonDao;
import pl.sda.s08_jdbc_hibernate.e01_jdbc.model.Person;

import java.util.List;

@RequiredArgsConstructor
public class PersonService {
    private final PersonDao personDao;

    public Person getById(String id) {
        try {
            Integer parsedId = Integer.parseInt(id);
            return personDao.getById(parsedId);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Bad argument: " + id);
        }
    }

    public List<Person> getAll() {
        return personDao.getAll();
    }
}
