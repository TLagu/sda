package pl.sda.s08_jdbc_hibernate.e01_jdbc.util;

public enum Justification {
    LEFT, CENTER, RIGHT
}
