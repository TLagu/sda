package pl.sda.s08_jdbc_hibernate.e01_jdbc.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Displaying {
    private final static int ID_LEN = 12;
    private final static int FIRST_NAME_LEN = 80;
    private final static int LAST_NAME_LEN = 80;
    private final static int PESEL_LEN = 11;
    private final static String SEPARATOR = " | ";
    private final static int TOTAl_LEN = ID_LEN + FIRST_NAME_LEN + LAST_NAME_LEN + PESEL_LEN + 3 * SEPARATOR.length();

    public static void showData(final ResultSet resultSet) throws SQLException {
        List<String> line = new ArrayList<>();
        line.add(generateField("ID", ID_LEN, Justification.CENTER));
        line.add(generateField("First name", FIRST_NAME_LEN, Justification.CENTER));
        line.add(generateField("Last name", LAST_NAME_LEN, Justification.CENTER));
        line.add(generateField("PESEL", PESEL_LEN, Justification.CENTER));
        showLine(line);
        System.out.println("-".repeat(TOTAl_LEN));
        while(resultSet.next()) {
            line.clear();
            final String id = String.valueOf(resultSet.getInt("id"));
            line.add(generateField(id, ID_LEN, Justification.RIGHT));
            final String firstName = resultSet.getString("first_name");
            line.add(generateField(firstName, FIRST_NAME_LEN, Justification.LEFT));
            final String lastName = resultSet.getString("last_name");
            line.add(generateField(lastName, LAST_NAME_LEN, Justification.LEFT));
            final String pesel = resultSet.getString("pesel");
            line.add(generateField(pesel, PESEL_LEN, Justification.LEFT));
            showLine(line);
        }
    }

    private static String generateField(String value, int fieldLength, Justification justification) {
        int leftMargin = 0;
        int rightMargin = 0;
        switch (justification) {
            case LEFT:
                rightMargin = fieldLength - value.length();
                break;
            case CENTER:
                double mid = (fieldLength - value.length()) / 2.0;
                leftMargin = (int) Math.ceil(mid);
                rightMargin = (int) mid;
                break;
            case RIGHT:
                leftMargin = fieldLength - value.length();
                break;
        }
        return " ".repeat(leftMargin) + value + " ".repeat(rightMargin);
    }

    private static void showLine(List<String> fields) {
        System.out.println(String.join(SEPARATOR, fields));
    }
}
