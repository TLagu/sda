package pl.sda.s08_jdbc_hibernate.e01_jdbc.preparedStatement;

import pl.sda.s08_jdbc_hibernate.e01_jdbc.util.Displaying;
import pl.sda.s08_jdbc_hibernate.e01_jdbc.util.JDBCUtil;

import java.sql.*;

public class JDBCPreparedStatement {
    public static void main(String[] args) {
        String select = "SELECT * FROM person WHERE last_name = ? AND pesel NOT LIKE ?";
        try (final Connection connection = JDBCUtil.getConnection();
             final PreparedStatement preparedStatement = connection.prepareStatement(select);
             ) {
            preparedStatement.setString(1, "Nowak");
            preparedStatement.setString(2, "9%");
            Displaying.showData(preparedStatement.executeQuery());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
