package pl.sda.s08_jdbc_hibernate.e01_jdbc.model;

import lombok.AllArgsConstructor;
import lombok.Data;

//POJO
@AllArgsConstructor
@Data
public class Person {
    private Integer id;
    private String firstName;
    private String lastName;
    private String pesel;
}
