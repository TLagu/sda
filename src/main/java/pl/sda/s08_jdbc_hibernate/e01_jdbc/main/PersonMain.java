package pl.sda.s08_jdbc_hibernate.e01_jdbc.main;

import pl.sda.s08_jdbc_hibernate.e01_jdbc.dao.JDBCPersonDao;
import pl.sda.s08_jdbc_hibernate.e01_jdbc.service.PersonService;

public class PersonMain {
    public static void main(String[] args) {
        PersonService personService = new PersonService(new JDBCPersonDao());
        System.out.println(personService.getById("1"));
        System.out.println(personService.getAll());
    }
}
