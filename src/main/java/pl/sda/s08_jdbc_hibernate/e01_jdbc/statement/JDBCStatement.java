package pl.sda.s08_jdbc_hibernate.e01_jdbc.statement;

import pl.sda.s08_jdbc_hibernate.e01_jdbc.util.Displaying;
import pl.sda.s08_jdbc_hibernate.e01_jdbc.util.JDBCUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCStatement {
    public static void main(String[] args) {
        try (final Connection connection = JDBCUtil.getConnection();
             final Statement statement = connection.createStatement();) {
            String select = "SELECT * FROM person WHERE id = 1";
            Displaying.showData(statement.executeQuery(select));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
