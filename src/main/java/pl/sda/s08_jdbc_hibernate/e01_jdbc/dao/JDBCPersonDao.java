package pl.sda.s08_jdbc_hibernate.e01_jdbc.dao;

import pl.sda.s08_jdbc_hibernate.e01_jdbc.model.Person;
import pl.sda.s08_jdbc_hibernate.e01_jdbc.util.JDBCUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JDBCPersonDao implements PersonDao {

    @Override
    public Person getById(Integer id) {
        Person person = null;
        String selectById = "SELECT * FROM person WHERE id = ?";
        try (final Connection connection = JDBCUtil.getConnection()){
            final PreparedStatement preparedStatement = connection.prepareStatement(selectById);
            preparedStatement.setInt(1, id);
            final ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                person = getPerson(resultSet);
            }
        } catch (SQLException e) {
            return person;
        }
        return person;
    }

    @Override
    public List<Person> getAll() {
        List<Person> persons = new ArrayList<>();
        String selectAll = "SELECT * FROM person";
        try (final Connection connection = JDBCUtil.getConnection()){
            final Statement statement = connection.createStatement();
            final ResultSet resultSet = statement.executeQuery(selectAll);
            while (resultSet.next()) {
                persons.add(getPerson(resultSet));
            }
        } catch (SQLException e) {
            return persons;
        }
        return persons;
    }

    private Person getPerson(final ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String firstName = resultSet.getString("first_name");
        String lastName = resultSet.getString("last_name");
        String pesel = resultSet.getString("pesel");
        return new Person(id, firstName, lastName, pesel);
    }
}
