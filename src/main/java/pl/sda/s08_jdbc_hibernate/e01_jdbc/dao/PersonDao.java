package pl.sda.s08_jdbc_hibernate.e01_jdbc.dao;

import pl.sda.s08_jdbc_hibernate.e01_jdbc.model.Person;

import java.util.List;

public interface PersonDao {
    Person getById(Integer id);

    List<Person> getAll();
}
