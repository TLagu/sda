package pl.sda.s08_jdbc_hibernate.e01_jdbc.calableStatement;

import pl.sda.s08_jdbc_hibernate.e01_jdbc.util.Displaying;
import pl.sda.s08_jdbc_hibernate.e01_jdbc.util.JDBCUtil;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

public class JDBCCallableStatement {
    public static void main(String[] args) throws SQLException {
        String sqlQuery = "call selectAllPersons()";
        try (final Connection connection = JDBCUtil.getConnectionAlternative();
            final CallableStatement callableStatement = connection.prepareCall(sqlQuery)) {
            Displaying.showData(callableStatement.executeQuery());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
