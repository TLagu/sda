package pl.sda.s08_jdbc_hibernate.e01_jdbc.crud;

import pl.sda.s08_jdbc_hibernate.e01_jdbc.util.JDBCUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCCRUDE {
    public static void main(String[] args) {
        String insert = "INSERT INTO person VALUES (4, 'Adam', 'Małysz', '12345678901')";
        try (Connection connection = JDBCUtil.getConnection();
             final Statement statement = connection.createStatement();) {
            final int rowCount = statement.executeUpdate(insert);
            System.out.println("Zaktualizowano " + rowCount + " wierszy.");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
