package pl.sda.s08_jdbc_hibernate.e03_hibernate_one2one;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.io.File;
import java.time.LocalDate;

public class StudentMain {
    public static void main(String[] args) {
        File f = new File("c:\\Users\\Lagu\\IdeaProjects\\sda\\src\\main\\resources\\pl\\sda\\s08_jdbc_hibernate\\e03_hibernate_one2one\\hibernate.cfg.xml");
        SessionFactory sessionFactory = new Configuration()
                .configure(f)
                .buildSessionFactory();
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();

        Student student = new Student(null, "Michał", "Nowak", LocalDate.of(2000, 1, 1), null);
        StudentIndex studentIndex = new StudentIndex(null, "12345", student);
        session.save(student);
        session.save(studentIndex);

        final StudentIndex studentIndexFromDB = session.get(StudentIndex.class, 1);
        session.detach(studentIndexFromDB);
        System.out.println("Nazwisko studenta: " + studentIndexFromDB.getStudent().getLastName());

        transaction.commit();
        session.close();
        sessionFactory.close();
    }
}
