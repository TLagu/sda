package pl.sda.s08_jdbc_hibernate.e03_hibernate_one2one;

import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class StudentIndex {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(length = 30, nullable = false)
    private String number;
    @OneToOne
    private Student student;
}
