package pl.sda.s08_jdbc_hibernate.e03_hibernate_one2one;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(length = 30, nullable = false)
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    // nazwa musi być dokładnie (nawet co do wielkości znaków) jak w definicji StudentIndex (private Student student)
    // nie jest tworzone nowe pole, ale hibernate wie, że klucz jest po drugiej sronie i zadzaiała
    @OneToOne(mappedBy = "student")
    private StudentIndex studentIndex;
}
