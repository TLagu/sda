package pl.sda.s08_jdbc_hibernate.e02_hibernate.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import pl.sda.s08_jdbc_hibernate.e02_hibernate.entity.Person;

import java.io.File;

public class PersonMain {
    public static void main(String[] args) {
        File f = new File("c:\\Users\\Lagu\\IdeaProjects\\sda\\src\\main\\resources\\pl\\sda\\s08_jdbc_hibernate\\e02_hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = new Configuration()
                .configure(f)
                .buildSessionFactory();
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();

        session.save(new Person(null, "Michał", "Nowak", "12345678901"));

        // stan managed
        final Person personFromDB = session.get(Person.class, 1);
        System.out.println(personFromDB);
        session.detach(personFromDB);
        personFromDB.setPesel("12345678909");
        System.out.println(personFromDB);

        transaction.commit();
        session.close();
        sessionFactory.close();
    }
}
