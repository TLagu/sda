package pl.sda.s08_jdbc_hibernate.task01.entity;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum SpecialityType {
    INTERNISTA, CHIRURG, KARDIOLOG;

    private static final List<SpecialityType> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    public static SpecialityType randomSpeciality()  {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }
}
