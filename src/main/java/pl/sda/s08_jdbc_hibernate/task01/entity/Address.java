package pl.sda.s08_jdbc_hibernate.task01.entity;

import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@SQLDelete(sql = "UPDATE address SET status=false WHERE id=?")
@Where(clause = "status = true")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 50)
    private String locality;

    @Column(name = "post_code", length = 6)
    private String postCode;

    @Column(name = "street_name", length = 50)
    private String streetName;

    @Column(name = "street_number", length = 20)
    private String streetNumber;

    @Column(name = "flat_number", length = 20)
    private String flatNumber;

    @Column(nullable = false, columnDefinition = "boolean default true")
    private Boolean status;

    @Override
    public String toString() {
        return postCode + " " + locality + ", " + streetName + " " + streetNumber + " " + flatNumber;
    }
}
