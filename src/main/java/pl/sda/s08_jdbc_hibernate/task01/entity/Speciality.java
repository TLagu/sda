package pl.sda.s08_jdbc_hibernate.task01.entity;

import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@SQLDelete(sql = "UPDATE speciality SET status=false WHERE id=?")
@Where(clause = "status = true")
public class Speciality {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(value = EnumType.STRING)
    private SpecialityType name;

    @Column(nullable = false, columnDefinition = "boolean default true")
    private Boolean status;

    @ManyToMany(mappedBy = "specialities")
    private List<Doctor> doctors;

}
