package pl.sda.s08_jdbc_hibernate.task01.entity;

import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@SQLDelete(sql = "UPDATE doctor SET status=false WHERE id=?")
@Where(clause = "status = true")
public class Doctor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 25)
    private String firstName;

    @Column(length = 25)
    private String lastName;

    @Column(length = 11)
    private String pesel;

    private LocalDate birthDate;

    @Column(nullable = false, columnDefinition = "boolean default true")
    private Boolean status;

    @OneToMany(mappedBy = "doctor")
    private List<Appointment> appointments;

    @ManyToMany
    @JoinTable(name = "join_doc_spec",
            joinColumns = {
                    @JoinColumn(name = "doctor_id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "spec_id")
            })
    private List<Speciality> specialities;

    @Override
    public String toString() {
        return firstName + " " + lastName + " (" + pesel + ") " + birthDate;
    }
}
