package pl.sda.s08_jdbc_hibernate.task01.entity;

import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@SQLDelete(sql = "UPDATE appointment SET status=false WHERE id=?")
@Where(clause = "status = true")
public class Appointment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private Patient patient;

    @ManyToOne
    private Doctor doctor;

    @Column(name = "time_from")
    private LocalDateTime timeFrom;

    @Column(name = "time_to")
    private LocalDateTime timeTo;

    @Enumerated(value = EnumType.STRING)
    private AppointmentType name;

    private String description;

    @Column(nullable = false, columnDefinition = "boolean default true")
    private Boolean status;

    @Override
    public String toString() {
        return timeFrom + " - " + timeTo + ", " + name + ", " + description;
    }
}
