package pl.sda.s08_jdbc_hibernate.task01.entity;

import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@SQLDelete(sql = "UPDATE patient SET status=false WHERE id=?")
@Where(clause = "status = true")
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "first_name", length = 25)
    private String firstName;

    @Column(name = "middle_name", length = 25)
    private String secondName;

    @Column(name = "last_name", length = 50)
    private String lastName;

    @Column(length = 11)
    private String pesel;

    @Column(nullable = false, columnDefinition = "boolean default true")
    private Boolean status;

    @OneToOne
    private Address address;

    @OneToMany(mappedBy = "patient")
    private List<Appointment> appointments;

    @Override
    public String toString() {
        return firstName + " " + secondName + " " + lastName + " (" + pesel + ") ";
    }
}
