package pl.sda.s08_jdbc_hibernate.task01.util;

public class StaticValues {
    public static final String PATH = "c:\\Users\\Lagu\\IdeaProjects\\sda\\src\\main\\resources\\pl\\sda\\s08_jdbc_hibernate\\task01\\";
    public static final Integer NUMBER_OF_DOCTORS = 250;
    public static final Integer NUMBER_OF_ADDRESSES = 10000;
    public static final Integer NUMBER_OF_PATIENTS = 10000;
    public static final Integer NUMBER_OF_APPOINTMENTS = 20000;
}
