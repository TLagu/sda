package pl.sda.s08_jdbc_hibernate.task01.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.File;

public final class HibernateUtil {

    private final static File file = new File(StaticValues.PATH + "work\\hibernate.cfg.xml");
    private final static SessionFactory sessionFactory = new Configuration().configure(file).buildSessionFactory();

    private HibernateUtil() {

    }

    public static Session getSession() {
        return sessionFactory.openSession();
    }

    public static void closeSessionFactory() {
        sessionFactory.close();
    }
}
