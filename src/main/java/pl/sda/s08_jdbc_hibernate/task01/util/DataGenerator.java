package pl.sda.s08_jdbc_hibernate.task01.util;

import lombok.Getter;
import pl.sda.s08_jdbc_hibernate.task01.entity.*;

import java.time.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Getter
public class DataGenerator {
    private final Reader csvData = new Reader();
    private final List<Doctor> doctors = new ArrayList<>();
    private final List<Speciality> specialties = new ArrayList<>();
    private final List<Address> addresses = new ArrayList<>();
    private final List<Patient> patients = new ArrayList<>();
    private final List<Appointment> appointments = new ArrayList<>();

    public DataGenerator() throws Exception {
        generateSpecialties();
        generateDoctors();
        generateAddresses();
        generatePatients();
        generateAppointments();
    }

    private void generateSpecialties() {
        specialties.add(new Speciality(null, SpecialityType.INTERNISTA, true, null));
        specialties.add(new Speciality(null, SpecialityType.KARDIOLOG, true, null));
        specialties.add(new Speciality(null, SpecialityType.CHIRURG, true, null));
    }

    private void generateDoctors() {
        Random random = new Random();
        String firstName;
        String lastName;
        LocalDate birthDate;
        String pesel;
        Set<Speciality> specialitySet = new HashSet<>();
        List<Speciality> specialityList;
        int n = 0;
        for (int i = 0; i < StaticValues.NUMBER_OF_DOCTORS; i++) {
            birthDate = generateRandomDate(60L, 30L);
            pesel = generatePesel(birthDate);
            if (random.nextBoolean()) {
                firstName = csvData.getFirstNamesMale().get(random.nextInt(csvData.getFirstNamesMale().size()));
                lastName = csvData.getLastNamesMale().get(random.nextInt(csvData.getLastNamesMale().size()));
            } else {
                firstName = csvData.getFirstNamesFemale().get(random.nextInt(csvData.getFirstNamesFemale().size()));
                lastName = csvData.getLastNamesFemale().get(random.nextInt(csvData.getLastNamesFemale().size()));
            }
            n = random.nextInt(3);
            for (int j = 0; j < n; j++) {
                specialitySet.add(specialties.get(random.nextInt(3)));
            }
            specialityList = new ArrayList<>(specialitySet);
            doctors.add(new Doctor(null, firstName, lastName, pesel, birthDate, true, null, specialityList));
        }
    }

    private LocalDate generateRandomDate(long startYear, long stoYear) {
        long startSeconds = Instant.now().minus(Duration.ofDays(startYear * 365)).getEpochSecond();
        long endSeconds = Instant.now().minus(Duration.ofDays(stoYear * 365)).getEpochSecond();
        long randomDate = ThreadLocalRandom
                .current()
                .nextLong(startSeconds, endSeconds);
        ZoneId zone = ZoneId.of("Europe/Warsaw");
        return LocalDate.ofInstant(Instant.ofEpochSecond(randomDate), zone);
    }

    private String generatePesel(LocalDate birthDate) {
        Random random = new Random();
        return String.valueOf(birthDate.getYear()).substring(2)
                + String.format("%02d", birthDate.getMonthValue())
                + String.format("%02d", birthDate.getDayOfMonth())
                + String.format("%05d", random.nextInt(100000));
    }

    private void generateAddresses() {
        String locality = "Szczecin";
        String postCode;
        String streetName;
        String streetNumber;
        String flatNumber;
        Random random = new Random();
        for (int i = 0; i < StaticValues.NUMBER_OF_ADDRESSES; i++) {
            postCode = String.format("%02d", random.nextInt(100))
                    + "-"
                    + String.format("%03d", random.nextInt(1000));
            streetName = csvData.getStreets().get(random.nextInt(csvData.getStreets().size()));
            streetNumber = String.format("%02d", random.nextInt(100));
            flatNumber = String.format("%02d", random.nextInt(100));
            addresses.add(new Address(null, locality, postCode,
                    streetName, streetNumber, flatNumber, true));
        }
    }

    private void generatePatients() {
        String firstName;
        String secondName;
        String lastName;
        LocalDate birthDate;
        String pesel;
        Address address;
        Random random = new Random();
        for (int i = 0; i < StaticValues.NUMBER_OF_PATIENTS; i++) {
            if (random.nextBoolean()) {
                firstName = csvData.getFirstNamesMale().get(random.nextInt(csvData.getFirstNamesMale().size()));
                secondName = csvData.getFirstNamesMale().get(random.nextInt(csvData.getFirstNamesMale().size()));
                lastName = csvData.getLastNamesMale().get(random.nextInt(csvData.getLastNamesMale().size()));
            } else {
                firstName = csvData.getFirstNamesFemale().get(random.nextInt(csvData.getFirstNamesFemale().size()));
                secondName = csvData.getFirstNamesFemale().get(random.nextInt(csvData.getFirstNamesFemale().size()));
                lastName = csvData.getLastNamesFemale().get(random.nextInt(csvData.getLastNamesFemale().size()));
            }
            birthDate = generateRandomDate(100L, 0L);
            pesel = generatePesel(birthDate);
            address = addresses.get(random.nextInt(addresses.size()));
            patients.add(new Patient(null, firstName, secondName, lastName, pesel, true, address, null));
        }
    }

    private void generateAppointments() {
        Patient patient;
        Doctor doctor;
        LocalDateTime startTime = LocalDateTime.of(2000, 1, 1, 9, 0, 0);
        LocalDateTime endTime;
        AppointmentType appointmentType;
        Random random = new Random();
        int i = 0;
        while (i < StaticValues.NUMBER_OF_APPOINTMENTS) {
            startTime = getStartTime(startTime);
            endTime = startTime.plusMinutes(30);
            if (random.nextInt(5) == 4) {
                patient = patients.get(random.nextInt(patients.size()));
                doctor = doctors.get(random.nextInt(doctors.size()));
                appointmentType = AppointmentType.randomAppointment();
                appointments.add(new Appointment(null, patient, doctor, startTime, endTime,
                        appointmentType, null, true));
                i++;
            }
            startTime = endTime;
        }
    }

    private LocalDateTime getStartTime(LocalDateTime startTime) {
        return (startTime.getHour() >= 14)?startTime.plusDays(1).with(LocalTime.of(8, 0, 0)):startTime;
    }

}
