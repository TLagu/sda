package pl.sda.s08_jdbc_hibernate.task01.util;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import lombok.Getter;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

@Getter
public class Reader {
    private final List<String> firstNamesMale;
    private final List<String> firstNamesFemale;
    private final List<String> lastNamesMale;
    private final List<String> lastNamesFemale;
    private final List<String> streets;

    public Reader() throws Exception {
        firstNamesMale = readNames(StaticValues.PATH + "first_names_male.csv", 1, null);
        firstNamesFemale = readNames(StaticValues.PATH + "first_names_female.csv", 1, null);
        lastNamesMale = readNames(StaticValues.PATH + "last_names_male.csv", 1, "-");
        lastNamesFemale = readNames(StaticValues.PATH + "last_names_female.csv", 1, "-");
        streets = readNames(StaticValues.PATH + "last_names_female.csv", 1, null);
    }

    private List<String[]> readAll(String file, int skipLines) throws Exception {
        FileReader filereader = new FileReader(file);
        CSVParser parser = new CSVParserBuilder().withSeparator(',').build();
        CSVReader csvReader = new CSVReaderBuilder(filereader)
                .withCSVParser(parser)
                .withSkipLines(skipLines)
                .build();
        List<String[]> lines = csvReader.readAll();
        filereader.close();
        csvReader.close();
        return lines;
    }

    private List<String> readNames(String file, int skipLines, String separator) throws Exception {
        List<String> result = new ArrayList<>();
        List<String[]> lines = readAll(file, skipLines);
//        lines.stream()
//                .map(l -> Arrays.asList(l))
//                .flatMap(List::stream)
//                .forEach(System.out::println);
        for(String[] line : lines) {
            if (line == null || line.length == 0 || line[0].length() < 2) {
                continue;
            }
            if (separator != null) {
                String[] parts = line[0].split("-");
                for(int i = 0; i < parts.length; i++) {
                    parts[0] = parts[0].substring(0, 1).toUpperCase() + parts[0].substring(1);
                }
                result.add(String.join("-", parts));
//                result.add(Arrays.stream(line[0].split("-"))
//                        .map(p -> p.substring(0, 1).toUpperCase() + p.substring(1))
//                        .collect(Collectors.joining("-")));
            } else {
                result.add(line[0]);
            }
        }
        return result;
    }

}
