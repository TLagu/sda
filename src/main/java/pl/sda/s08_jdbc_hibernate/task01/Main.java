package pl.sda.s08_jdbc_hibernate.task01;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import pl.sda.s08_jdbc_hibernate.task01.util.DataGenerator;
import pl.sda.s08_jdbc_hibernate.task01.util.StaticValues;

import java.io.File;

public class Main {
    public static void main(String[] args) throws Exception {
        File f = new File(StaticValues.PATH + "create\\hibernate.cfg.xml");
        SessionFactory sessionFactory = new Configuration()
                .configure(f)
                .buildSessionFactory();
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();

        DataGenerator dataGenerator = new DataGenerator();
        dataGenerator.getAddresses().forEach(session::save);
        dataGenerator.getPatients().forEach(session::save);
        dataGenerator.getSpecialties().forEach(session::save);
        dataGenerator.getDoctors().forEach(session::save);
        dataGenerator.getAppointments().forEach(session::save);

        transaction.commit();
        session.close();
        sessionFactory.close();
    }
}
