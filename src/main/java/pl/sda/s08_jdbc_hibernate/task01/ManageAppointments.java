package pl.sda.s08_jdbc_hibernate.task01;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sda.s08_jdbc_hibernate.task01.dao.AppointmentDao;
import pl.sda.s08_jdbc_hibernate.task01.dao.DoctorDao;
import pl.sda.s08_jdbc_hibernate.task01.dao.PatientDao;
import pl.sda.s08_jdbc_hibernate.task01.entity.Appointment;
import pl.sda.s08_jdbc_hibernate.task01.entity.Doctor;
import pl.sda.s08_jdbc_hibernate.task01.entity.Patient;
import pl.sda.s08_jdbc_hibernate.task01.util.HibernateUtil;

import java.time.LocalDateTime;

public class ManageAppointments {
    private final static int doctorId = 25;
    private final static int patientId = 1;
    private final static String pesel = "95070267229";
    private final static String firstName = "HYUNJIN";
    private final static LocalDateTime fromTime = LocalDateTime.of(2020, 1, 1, 0, 0, 0);
    private final static LocalDateTime toTime = LocalDateTime.of(2020, 1, 10, 0, 0, 0);
    private static DoctorDao doctorDao;
    private static PatientDao patientDao;
    private static AppointmentDao appointmentDao;
    private final static Session session = HibernateUtil.getSession();

    public static void main(String[] args) {
        createObjects();
        showAllAppointmentsByDoctor(doctorId);
        showAllAppointmentsByPatient(patientId);
        showAllAppointmentsByTime(fromTime, toTime);

        System.out.println("Dane pacjenta z pesel 95070267229: " + patientDao.getPatientByPesel(pesel));
        Patient patient = patientDao.getById(patientId);
        patient.setFirstName("Nowe imię");
        patientDao.update(patient);
        System.out.println("Dane pacjenta (po modyfikacji): " + patientDao.getById(patientId));
        patientDao.delete(patientId);
        System.out.println("Dane pacjenta (po usumięciu): " + patientDao.getById(patientId));
        patientDao.undelete(patientId);
        System.out.println("Dane pacjenta (po przywróceniu): " + patientDao.getById(patientId));
        patient = patientDao.getById(patientId);
        patient.setFirstName(firstName);
        patientDao.update(patient);
        System.out.println("Dane pacjenta (po kolejnej zmianie imienia): " + patientDao.getById(patientId));

        HibernateUtil.closeSessionFactory();
    }

    public static void createObjects() {
        Transaction transaction = session.beginTransaction();
        doctorDao = new DoctorDao(session, Doctor.class);
        patientDao = new PatientDao(session, Patient.class);
        appointmentDao = new AppointmentDao(session, Appointment.class);
        transaction.commit();
    }

    public static void showAllAppointmentsByDoctor(int id) {
        System.out.println("Lista wizyt pacjentów doktora: " + doctorDao.getById(id));
        appointmentDao.getAllByDoctor(id).stream()
                .map(e -> " - " + e.toString() + " - " + e.getPatient())
                .forEach(System.out::println);
    }

    public static void showAllAppointmentsByPatient(int id) {
        System.out.println("Lista wizyt pacjenta: " + patientDao.getById(id));
        appointmentDao.getAllByPatient(id).stream()
                .map(e -> " - " + e.toString() + " - pacjent: " + e.getDoctor())
                .forEach(System.out::println);
    }

    public static void showAllAppointmentsByTime(LocalDateTime fromTime, LocalDateTime toTime) {
        System.out.println("Lista wizyt w terminie: " + fromTime + " - " + toTime);
        appointmentDao.getAllByTime(fromTime, toTime).stream()
                .map(e -> " - " + e.toString() + " - doktor: " + e.getDoctor() + ", pacjent: " + e.getPatient())
                .forEach(System.out::println);
    }
}
