package pl.sda.s08_jdbc_hibernate.task01.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

public abstract class AbstractDao<T> {

    protected Session session;

    protected Class<T> clazz;

    public AbstractDao(Session session, Class<T> clazz) {
        this.session = session;
        this.clazz = clazz;
    }

    public void save(T t) {
        Transaction transaction = session.beginTransaction();
        session.save(t);
        transaction.commit();
    }

    public T getById(Integer id) {
        return session.get(clazz, id);
    }

    public void update(T t) {
        Transaction transaction = session.beginTransaction();
        session.update(t);
        transaction.commit();
    }

    public void delete(Integer id) {
        Transaction transaction = session.beginTransaction();
        T t = getById(id);
        session.delete(t);
        transaction.commit();
    }

    public int undelete(Integer id) {
        Transaction transaction = session.beginTransaction();
        String hqlUpdate = "update " + clazz.getSimpleName() + " c set c.status = true where c.id = :id";
        int result = session.createQuery(hqlUpdate)
                .setParameter("id", id)
                .executeUpdate();
        transaction.commit();
        return result;
    }


}
