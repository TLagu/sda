package pl.sda.s08_jdbc_hibernate.task01.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;
import pl.sda.s08_jdbc_hibernate.task01.entity.Doctor;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class DoctorDao extends AbstractDao<Doctor> {

    public DoctorDao(Session session, Class<Doctor> clazz) {
        super(session, clazz);
    }

//    public Doctor getDoctorById(Integer id) {
//        CriteriaBuilder builder = session.getCriteriaBuilder();
//        CriteriaQuery<Doctor> query = builder.createQuery(clazz);
//        Root<Doctor> root = query.from(clazz);
//        query.select(root).where(builder.equal(root.get("identificator"), id));
//        Query<Doctor> doctorQuery = session.createQuery(query);
//        return doctorQuery.getResultStream()
//                .findFirst()
//                .orElseGet(null);
//    }
//
//    /**
//     * Metoda zwraca true jeśli doktor ma wolny czas w zadanym przedziale czasowym.
//     * Implementacja polega na odnalezieniu najwcześniejszej oraz najpóźniejszej wizyty doktora
//     * i traktując to jako zakres czasu przyrównuje do zakresu czasu podanrgo w argumencie.
//     */
//    public boolean isDoctorAvailable(Integer id, LocalDateTime dateFrom, LocalDateTime dateTo) {
//
//        Doctor doctor = getDoctorById(id);
//
//        if (doctor != null) {
//            List<Appointment> doctorAppointments = doctor.getAppointments();
//
//            Appointment minAppointment = null;
//            Appointment maxAppointment = null;
//
//            //Jeśli lista wizyt nie jest pusta
//            if (doctorAppointments != null) {
//
//                Optional<Appointment> min = doctorAppointments
//                        .stream()
//                        .min(Comparator.comparing(Appointment::getDateFrom));
//
//                if (min.isPresent()) {
//                    minAppointment = min.get();
//                }
//
//                Optional<Appointment> max = doctorAppointments
//                        .stream()
//                        .max(Comparator.comparing(Appointment::getDateTo));
//
//                if (max.isPresent()) {
//                    maxAppointment = max.get();
//                }
//
//
//                if (minAppointment != null && maxAppointment != null) {
//
//                    Interval interval1 = new Interval(dateFrom, true, dateTo, true);
//                    Interval interval2 = new Interval(minAppointment.getDateFrom(), true, maxAppointment.getDateTo(), true);
//                    return !interval1.overlaps(interval2);
//                }
//            }
//        }
//
//        return true; //jeśli nie ma żadnej wizyty - zwróć true czyli lekarz jest dostępny w każdym terminie
//    }
//
}
