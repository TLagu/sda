package pl.sda.s08_jdbc_hibernate.task01.dao;

import org.hibernate.Session;
import pl.sda.s08_jdbc_hibernate.task01.entity.Address;

public class AddressDao extends AbstractDao<Address> {

    public AddressDao(Session session, Class<Address> clazz) {
        super(session, clazz);
    }

}
