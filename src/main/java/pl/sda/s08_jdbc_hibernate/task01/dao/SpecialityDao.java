package pl.sda.s08_jdbc_hibernate.task01.dao;

import org.hibernate.Session;
import pl.sda.s08_jdbc_hibernate.task01.entity.Speciality;

public class SpecialityDao extends AbstractDao<Speciality> {

    public SpecialityDao(Session session, Class<Speciality> clazz) {
        super(session, clazz);
    }

}
