package pl.sda.s08_jdbc_hibernate.task01.dao;

import org.hibernate.Session;
import pl.sda.s08_jdbc_hibernate.task01.entity.Appointment;
import pl.sda.s08_jdbc_hibernate.task01.entity.Doctor;
import pl.sda.s08_jdbc_hibernate.task01.entity.Patient;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class AppointmentDao extends AbstractDao<Appointment> {

    public AppointmentDao(Session session, Class<Appointment> clazz) {
        super(session, clazz);
    }

    public List<Appointment> getAllByDoctor(Integer id) {
        Doctor doctor = session.get(Doctor.class, id);
        return (doctor == null)?(new ArrayList<>()):doctor.getAppointments();
    }

    public List<Appointment> getAllByPatient(Integer id) {
        Patient patient = session.get(Patient.class, id);
        return (patient == null)?(new ArrayList<>()):patient.getAppointments();
    }

    public List<Appointment> getAllByTime(LocalDateTime timeFrom, LocalDateTime timeTo) {
        return session
                .createQuery("from Appointment a where :timeFrom <= timeFrom and timeTo <= :timeTo")
                .setParameter("timeFrom", timeFrom)
                .setParameter("timeTo", timeTo)
                .list();
    }

}
