package pl.sda.s08_jdbc_hibernate.e04_hibernate_one2many;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class OrderDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(columnDefinition = "DECIMAL(7,2)")
    private BigDecimal totalAmount;
    private String products;
    private LocalDateTime date;
    @ManyToOne
    private Client client;
}
