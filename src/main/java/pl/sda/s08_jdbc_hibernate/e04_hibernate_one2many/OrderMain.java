package pl.sda.s08_jdbc_hibernate.e04_hibernate_one2many;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;

public class OrderMain {
    public static void main(String[] args) {
        File f = new File("c:\\Users\\Lagu\\IdeaProjects\\sda\\src\\main\\resources\\pl\\sda\\s08_jdbc_hibernate\\e04_hibernate_one2many\\hibernate.cfg.xml");
        SessionFactory sessionFactory = new Configuration()
                .configure(f)
                .buildSessionFactory();
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();

        Client client = new Client(null, "nick", null);
        OrderDetails order1 = new OrderDetails(null, BigDecimal.valueOf(100l), "woda", LocalDateTime.now(), client);
        OrderDetails order2 = new OrderDetails(null, BigDecimal.valueOf(200l), "piwo", LocalDateTime.now(), client);
        client.setOrders(Arrays.asList(order1, order2));
        session.save(order1);
        session.save(order2);
        session.save(client);

        final Client clientFromDB = session.get(Client.class, 1);
        // session.remove(clientFromDB);

        System.out.println("Produkty z zamówienia: ");
        clientFromDB.getOrders().forEach(o -> System.out.println(o.getProducts()));

        String hql = "SELECT AVG(totalAmount) FROM OrderDetails";
        System.out.println("avg: " + session.createQuery(hql).getSingleResult());

        transaction.commit();
        session.close();
        sessionFactory.close();

    }
}
