package pl.sda.s06_zaawansowane_programowanie.examples.e26;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Car {
    private String name;
    private String description;
    private CarType carType;

    @Override
    public String toString() {
        return " - name: " + name +
                ", description: " + description +
                ", carType: " + carType;
    }
}
