package pl.sda.s06_zaawansowane_programowanie.examples.e26;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
public class Warehouse {
    private List<Manufacturer> manufacturers;

    public void addManufacturer(Manufacturer manufacturer) {
        manufacturers.add(manufacturer);
    }

    // 1. Lista wszystkich Modeli,
    public List<Model> getModels() {
        return manufacturers.stream()
                .map(Manufacturer::getModels)
                .flatMap(List::stream)
                .map(m -> new Model(m.getName(), m.getProductionStartYear()))
                .collect(Collectors.toList());
    }

    // 2. Lista wszystkich aut
    public List<Car> getCars() {
        return manufacturers.stream()
                .map(Manufacturer::getModels)
                .flatMap(List::stream)
                .map(Model::getCars)
                .flatMap(List::stream)
                .distinct()
                .collect(Collectors.toList());
    }

    // 3. Lista wszystkich nazw producentów
    public List<String> getManufacturerNames() {
        return manufacturers.stream()
                .map(Manufacturer::getName)
                .collect(Collectors.toList());
    }

    // 4. Lista wszystkich lat założenia producentów
    public List<Integer> getManufacturerYears() {
        return manufacturers.stream()
                .map(Manufacturer::getYearOfCreation)
                .collect(Collectors.toList());
    }

    // 5. Lista wszystkich nazw modeli
    public List<String> getModelNames() {
        return manufacturers.stream()
                .map(Manufacturer::getModels)
                .flatMap(List::stream)
                .map(Model::getName)
                .collect(Collectors.toList());
    }

    // 6. Lista wszystkich lat startu produkcji modeli
    public List<Integer> getModelYears() {
        return manufacturers.stream()
                .map(Manufacturer::getModels)
                .flatMap(List::stream)
                .map(Model::getProductionStartYear)
                .collect(Collectors.toList());
    }

    // 7. Lista wszystkich nazw aut
    public List<String> getCarNames() {
        return manufacturers.stream()
                .map(Manufacturer::getModels)
                .flatMap(List::stream)
                .map(Model::getCars)
                .flatMap(List::stream)
                .map(Car::getName)
                .collect(Collectors.toList());
    }

    // 8. Lista wszystkich opisów aut
    public List<String> getCarDescriptions() {
        return manufacturers.stream()
                .map(Manufacturer::getModels)
                .flatMap(List::stream)
                .map(Model::getCars)
                .flatMap(List::stream)
                .map(Car::getDescription)
                .collect(Collectors.toList());
    }

    // 9. Lista modeli z parzystym rokiem startu produkcji
    public List<Model> getModelEvenYear() {
        return manufacturers.stream()
                .map(Manufacturer::getModels)
                .flatMap(List::stream)
                .filter(m -> m.getProductionStartYear() % 2 == 0)
                .map(m -> new Model(m.getName(), m.getProductionStartYear()))
                .collect(Collectors.toList());
    }

    // 10. Lista aut producentów z parzystym rokiem założenia
    public List<Car> getCarWhereModelOddYear() {
        return manufacturers.stream()
                .filter(m -> m.getYearOfCreation() % 2 == 0)
                .map(Manufacturer::getModels)
                .flatMap(List::stream)
                .map(Model::getCars)
                .flatMap(List::stream)
                .distinct()
                .collect(Collectors.toList());
    }

    // 11. Lista aut z parzystym rokiem startu produkcji modelu oraz nieparzystym rokiem założenia producenta
    public List<Car> getCarWhereManufacturerOddYearAndModelEvenYear() {
        return manufacturers.stream()
                .filter(m -> m.getYearOfCreation() % 2 == 1)
                .map(Manufacturer::getModels)
                .flatMap(List::stream)
                .filter(m -> m.getProductionStartYear() % 2 == 0)
                .map(Model::getCars)
                .flatMap(List::stream)
                .distinct()
                .collect(Collectors.toList());
    }

    // 12. Lista aut typu CABRIO z nieparzystym rokiem startu produkcji modelu i parzystym rokiem założenia producenta
    public List<Car> getCarCabrioWhereManufacturerEvenYearAndModelOddYear() {
        return manufacturers.stream()
                .filter(m -> m.getYearOfCreation() % 2 == 0)
                .map(Manufacturer::getModels)
                .flatMap(List::stream)
                .filter(m -> m.getProductionStartYear() % 2 == 1)
                .map(Model::getCars)
                .flatMap(List::stream)
                .filter(c -> (c.getCarType() == CarType.CABRIO))
                .distinct()
                .collect(Collectors.toList());
    }

    // 13. Lista aut typu SEDAN z modelu nowszego niż 2019 oraz rokiem założenia producenta mniejszym niż 1919
    public List<Car> getCarSedanWhereManufacturerYearBeforeAndModelYearAfter(int manufacturerYear, int modelYear) {
        return manufacturers.stream()
                .filter(m -> m.getYearOfCreation() < manufacturerYear)
                .map(Manufacturer::getModels)
                .flatMap(List::stream)
                .filter(m -> m.getProductionStartYear() > modelYear)
                .map(Model::getCars)
                .flatMap(List::stream)
                .filter(c -> (c.getCarType() == CarType.SEDAN))
                .distinct()
                .collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "Manufacturers:\n" + manufacturers.stream().map(Object::toString).collect(Collectors.joining("\n"));
    }
}
