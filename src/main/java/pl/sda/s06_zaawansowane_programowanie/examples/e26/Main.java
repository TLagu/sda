package pl.sda.s06_zaawansowane_programowanie.examples.e26;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    private static final List<Car> cars = new ArrayList<>();
    private static final List<Model> models = new ArrayList<>();
    private static final List<Manufacturer> manufacturers = new ArrayList<>();

    public static void main(String[] args) {
        generateCars();
        generateModels();
        generateManufacturers();
        Warehouse warehouse = new Warehouse(manufacturers);
        showStream("1. Models:\n  ", warehouse.getModels().stream());
        showStream("2. Cars:\n  ", warehouse.getCars().stream());
        System.out.println("3. Manufacturer names: " + warehouse.getManufacturerNames());
        System.out.println("4. Manufacturer years: " + warehouse.getManufacturerYears());
        showStream("5. Model names:\n  ", warehouse.getModelNames().stream());
        showStream("6. Model years:\n  ", warehouse.getModelYears().stream());
        showStream("6. Model years:\n  ", warehouse.getModelYears().stream());
        showStream("7. Car names:\n  ", warehouse.getCarNames().stream());
        showStream("8. Car descriptions:\n  ", warehouse.getCarDescriptions().stream());
        showStream("9. Models (even year):\n  ", warehouse.getModelEvenYear().stream());
        showStream("10. Cars (manufacturer even year):\n  ", warehouse.getCarWhereModelOddYear().stream());
        showStream("11. Cars (manufacturer odd year and model even year):\n  ",
                warehouse.getCarWhereManufacturerOddYearAndModelEvenYear().stream());
        showStream("12. Cars cabrio (manufacturer even year and model odd year):\n  ",
                warehouse.getCarCabrioWhereManufacturerEvenYearAndModelOddYear().stream());
        showStream("13. Cars sedan (manufacturer before year and model after year):\n  ",
                warehouse.getCarSedanWhereManufacturerYearBeforeAndModelYearAfter(1919, 2019).stream());
    }

    private static void generateCars() {
        cars.add(new Car("name 1", "description 1", CarType.CABRIO));
        cars.add(new Car("name 2", "description 2", CarType.COUPE));
        cars.add(new Car("name 3", "description 3", CarType.HATCHBACK));
        cars.add(new Car("name 4", "description 4", CarType.SEDAN));
        cars.add(new Car("name 5", "description 5", CarType.CABRIO));
        cars.add(new Car("name 6", "description 6", CarType.COUPE));
        cars.add(new Car("name 7", "description 7", CarType.HATCHBACK));
        cars.add(new Car("name 8", "description 8", CarType.SEDAN));
        cars.add(new Car("name 9", "description 9", CarType.CABRIO));
        cars.add(new Car("name 10", "description 10", CarType.COUPE));
        cars.add(new Car("name 11", "description 11", CarType.HATCHBACK));
    }

    private static void generateModels() {
        models.add(new Model("model 1", 2020, List.of(cars.get(0), cars.get(1), cars.get(2), cars.get(3))));
        models.add(new Model("model 2", 2011, List.of(cars.get(4), cars.get(5), cars.get(6))));
        models.add(new Model("model 3", 2002, List.of(cars.get(7), cars.get(8))));
        models.add(new Model("model 4", 1993, List.of(cars.get(9))));
        models.add(new Model("model 5", 1984, List.of(cars.get(10))));
    }

    private static void generateManufacturers() {
        manufacturers.add(new Manufacturer("name 1", 1900, List.of(models.get(0), models.get(1), models.get(2))));
        manufacturers.add(new Manufacturer("name 2", 1951, List.of(models.get(3), models.get(4))));
    }

    private static void showStream(String description, Stream stream){
        System.out.println(description + stream.map(Object::toString).collect(Collectors.joining("\n  ")));
    }
}
