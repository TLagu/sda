package pl.sda.s06_zaawansowane_programowanie.examples.e26;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
public class Manufacturer {
    private String name;
    private int yearOfCreation;
    private List<Model> models;

    @Override
    public String toString() {
        return " - name: " + name +
                ", yearOfCreation: " + yearOfCreation +
                ", models:\n  " + models.stream().map(Object::toString).collect(Collectors.joining("\n  "));
    }
}
