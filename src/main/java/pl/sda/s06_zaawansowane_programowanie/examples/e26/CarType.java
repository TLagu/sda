package pl.sda.s06_zaawansowane_programowanie.examples.e26;

public enum CarType {
    COUPE, CABRIO, SEDAN, HATCHBACK
}
