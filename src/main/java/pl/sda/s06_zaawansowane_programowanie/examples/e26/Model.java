package pl.sda.s06_zaawansowane_programowanie.examples.e26;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@RequiredArgsConstructor
@Getter
public class Model {
    private final String name;
    private final int productionStartYear;
    private List<Car> cars;

    @Override
    public String toString() {
        return " - name: " + name +
                ", productionStartYear: " + productionStartYear +
                ((cars == null)?"":(", cars:\n    " + cars.stream().map(Object::toString).collect(Collectors.joining("\n    "))));
    }
}
