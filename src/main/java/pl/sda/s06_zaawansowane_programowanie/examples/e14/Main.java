package pl.sda.s06_zaawansowane_programowanie.examples.e14;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    private static List<Integer> numbers = new ArrayList<>();
    private static Map<Integer, Integer> numberStatistics = new HashMap<>();
    private final static int MULTIPLY = 10;
    private final static int SIZE = 100000;

    public static void main(String[] args) {
        generateNumbers();
        System.out.println("Unique count: " + getUnique().size());
        System.out.println("Doubled count: " + getDoubled().size());
        System.out.println("Most common: " + getMostCommon(25));
        regenerateNumbers();
        System.out.println("Doubled count: " + getDoubled().size());
    }

    private static void generateNumbers() {
        Random random = new Random();
        int count;
        int number;
        for (int i = 0; i < SIZE; i++) {
            number = random.nextInt(MULTIPLY * SIZE);
            numbers.add(number);
            count = numberStatistics.getOrDefault(number, 0) + 1;
            numberStatistics.put(number, count);
        }
    }

    private static List<Map.Entry<Integer, Integer>> getUnique() {
        return numberStatistics.entrySet().stream()
                .filter(i -> i.getValue().equals(1))
                .collect(Collectors.toList());
    }

    private static List<Map.Entry<Integer, Integer>> getDoubled() {
        return numberStatistics.entrySet().stream()
                .filter(i -> i.getValue() > 1)
                .collect(Collectors.toList());
    }

    private static List<Map.Entry<Integer, Integer>> getMostCommon(int count) {
        return numberStatistics.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(count)
                .collect(Collectors.toList());
    }

    private static void regenerateNumbers() {
        List<Map.Entry<Integer, Integer>> toRegenerate = getDoubled();
        Random random = new Random();
        int key;
        int value;
        int r;
        for (Map.Entry<Integer, Integer> entry : toRegenerate){
            key = entry.getKey();
            value = entry.getValue();
            while (value > 1) {
                r = random.nextInt(MULTIPLY * SIZE);
                if(!numbers.contains(r)){
                    numbers.add(r);
                    numberStatistics.put(key, --value);
                }
            }
        }
    }
}
