package pl.sda.s06_zaawansowane_programowanie.examples.e07;

import java.util.ArrayList;
import java.util.List;

public class Gun {
    private List<String> bullets = new ArrayList<>();
    public final int maxBulletCount;

    public Gun(int maxBulletCount) {
        this.maxBulletCount = maxBulletCount;
    }

    private boolean isFull(){
        return bullets.size() >= maxBulletCount;
    }

    public void loadBullet(String bullet){
        if(isFull()){
            System.out.println("Maximum capacity has been reached (" + bullet + ").");
            return;
        }
        bullets.add(bullet);
        System.out.println("Loaded: " + bullet);
    }

    public boolean isLoaded(){
        return bullets.size() > 0;
    }

    public void shot(){
        if (!isLoaded()){
            System.out.println("Empty.");
            return;
        }
        System.out.println("Bullet fired: " + bullets.get(bullets.size() - 1));
        bullets.remove(bullets.size() - 1);
    }
}
