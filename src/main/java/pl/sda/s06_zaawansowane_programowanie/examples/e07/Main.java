package pl.sda.s06_zaawansowane_programowanie.examples.e07;

public class Main {
    public static void main(String[] args) {
        Gun gun = new Gun(3);
        System.out.println("Is loaded: " + gun.isLoaded());
        gun.loadBullet("First bullet");
        gun.loadBullet("Second bullet");
        gun.shot();
        gun.loadBullet("Third bullet");
        gun.loadBullet("Forth bullet");
        System.out.println("Is loaded: " + gun.isLoaded());
        gun.shot();
        gun.shot();
        gun.shot();
        gun.shot();
    }
}
