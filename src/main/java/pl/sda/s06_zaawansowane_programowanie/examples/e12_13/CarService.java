package pl.sda.s06_zaawansowane_programowanie.examples.e12_13;

import java.util.*;
import java.util.stream.Collectors;

public class CarService {
    private List<Car> cars = new ArrayList<>();

    public void addCar(Car car) {
        if (!cars.contains(car)) {
            cars.add(car);
        }
    }

    public void removeCar(Car car){
        cars.remove(car);
    }

    public List<Car> getCars(){
        return cars;
    }

    public List<Car> getCarsWithEngine(EngineType engineType){
        return cars.stream()
                .filter(c -> c.getEngineType() == engineType)
                .collect(Collectors.toList());
    }

    public List<Car> getCarsWithProductionYearBefore(int year){
        return cars.stream()
                .filter(c -> c.getYear() < year)
                .collect(Collectors.toList());
    }

    public Car getMostExpensiveCar(){
        return cars.stream()
                .max(Comparator.comparing(Car::getPrice))
                .orElseThrow(NoSuchElementException::new);
    }

    public Car getCheapestCar(){
        return cars.stream()
                .min(Comparator.comparing(Car::getPrice))
                .orElseThrow(NoSuchElementException::new);
    }

    public List<Car> getCarWithAtLeastXManufacturers(int count){
        return cars.stream()
                .filter(c -> c.getProducers().size() >= count)
                .collect(Collectors.toList());
    }

    public List<Car> getCarsSorted(SortType sortType){
        switch (sortType){
            case ASC:
                return cars.stream().sorted(Comparator.comparing(Car::getName)).collect(Collectors.toList());
            case DESC:
                return cars.stream().sorted(Comparator.comparing(Car::getName).reversed()).collect(Collectors.toList());
            default:
                throw new IllegalArgumentException("Błędny typ sortowania.");
        }
    }

    public boolean isCarOnTheList(Car car){
        return cars.contains(car);
    }

    public List<Car> getCarsManufacturedBy(Manufacturer manufacturer){
        return cars.stream().filter(c -> c.getProducers().contains(manufacturer)).collect(Collectors.toList());
    }

    public List<Car> getCarsProducedByManufacturerFoundIn(int year, OperatorType operatorType){
        switch(operatorType){
            case LESS_THAN:
                return cars.stream()
                        .filter(c -> c.getProducers().stream().anyMatch(p -> p.getYear() < year))
                        .collect(Collectors.toList());
            case GREATER_THAN:
                return cars.stream()
                        .filter(c -> c.getProducers().stream().anyMatch(p -> p.getYear() > year))
                        .collect(Collectors.toList());
            case LESS_THAN_OR_EQUAL_TO:
                return cars.stream()
                        .filter(c -> c.getProducers().stream().anyMatch(p -> p.getYear() <= year))
                        .collect(Collectors.toList());
            case GREATER_THAN_OR_EQUAL_TO:
                return cars.stream()
                        .filter(c -> c.getProducers().stream().anyMatch(p -> p.getYear() >= year))
                        .collect(Collectors.toList());
            case EQUAL:
                return cars.stream()
                        .filter(c -> c.getProducers().stream().anyMatch(p -> p.getYear() == year))
                        .collect(Collectors.toList());
            case DIFFERENT:
                return cars.stream()
                        .filter(c -> c.getProducers().stream().allMatch(p -> p.getYear() != year))
                        .collect(Collectors.toList());
            default:
                throw new IllegalArgumentException("Błędny typ operatora.");
        }
    }
}
