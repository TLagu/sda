package pl.sda.s06_zaawansowane_programowanie.examples.e12_13;

public enum OperatorType {
    LESS_THAN, GREATER_THAN,
    LESS_THAN_OR_EQUAL_TO, GREATER_THAN_OR_EQUAL_TO,
    EQUAL, DIFFERENT
}
