package pl.sda.s06_zaawansowane_programowanie.examples.e12_13;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Objects;

@AllArgsConstructor
@Getter
@Setter
public class Car {
    private String name;
    private String model;
    private double price;
    private int year;
    private List<Manufacturer> producers;
    private EngineType engineType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Double.compare(car.price, price) == 0
                && year == car.year
                && Objects.equals(name, car.name)
                && Objects.equals(model, car.model)
                && Objects.equals(producers, car.producers)
                && engineType == car.engineType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, model, price, year, producers, engineType);
    }

    @Override
    public String toString() {
        DecimalFormat formatter = new DecimalFormat("###,###,##0.00");
        return " - Nazwa: " + name +
                ", Model: " + model +
                ", Cena: " + formatter.format(price) +
                ", Rok produkcji: " + year +
                ", Lista producentów: " + producers +
                ", Typ silnika: " + engineType;
    }
}
