package pl.sda.s06_zaawansowane_programowanie.examples.e12_13;

import java.util.List;

public class Main {
    private static final CarService carService = new CarService();
    private static final Car carToRemove = new Car("Audi", "A10", 500000, 2025,
            List.of(new Manufacturer("VAG 30", 2023, "Socialist Republic of Poland")),
            EngineType.V12);
    private static final Manufacturer manufacturerToFind = new Manufacturer("VAG 1", 1995, "Germany");
    private static final Car carToFind = new Car("Skoda", "Felicia", 10000, 2010,
            List.of(manufacturerToFind,
                    new Manufacturer("VAG 2", 2000, "Germany")),
            EngineType.S6);

    public static void main(String[] args) {
        createBaseData();
        printList("1. Lista samochodów:",
                carService.getCars());
        carService.removeCar(carToRemove);
        printList("2. Lista samochodów (po usunięciu):",
                carService.getCars());
        printList("4. Lista aut z silnikiem V12:",
                carService.getCarsWithEngine(EngineType.V12));
        printList("5. Lista aut wyprodukowanych przed rokiem:",
                carService.getCarsWithProductionYearBefore(1999));
        System.out.println("6. Najdroższe auto:\n" + carService.getMostExpensiveCar());
        System.out.println("7. Najtańsze auto:\n" + carService.getCheapestCar());
        printList("8. Lista aut z co najmniej 3 producentami",
                carService.getCarWithAtLeastXManufacturers(3));
        printList("9.a. Lista aut posortowania rosnąco:",
                carService.getCarsSorted(SortType.ASC));
        printList("9.b. Lista aut posortowania malejąco:",
                carService.getCarsSorted(SortType.DESC));
        System.out.println("10. Czy auto na liście: " + carService.isCarOnTheList(carToFind));
        printList("11. Lista aut wyprodukowanych przez producenta: ",
                carService.getCarsManufacturedBy(manufacturerToFind));
        printList("12.a. Lista aut wyprodukowanych przez producenta z rokiem założenia przed 2010:",
                carService.getCarsProducedByManufacturerFoundIn(2010, OperatorType.LESS_THAN));
        printList("12.b. Lista aut wyprodukowanych przez producenta z rokiem założenia po 2010:",
                carService.getCarsProducedByManufacturerFoundIn(2010, OperatorType.GREATER_THAN));
        printList("12.c. Lista aut wyprodukowanych przez producenta z rokiem założenia przed lub w 2010:",
                carService.getCarsProducedByManufacturerFoundIn(2010, OperatorType.LESS_THAN_OR_EQUAL_TO));
        printList("12.d. Lista aut wyprodukowanych przez producenta z rokiem założenia po lub w 2010:",
                carService.getCarsProducedByManufacturerFoundIn(2010, OperatorType.GREATER_THAN_OR_EQUAL_TO));
        printList("12.e. Lista aut wyprodukowanych przez producenta w roku 2010:",
                carService.getCarsProducedByManufacturerFoundIn(2010, OperatorType.EQUAL));
        printList("12.f. Lista aut wyprodukowanych przez producenta poza rokiem 2010:",
                carService.getCarsProducedByManufacturerFoundIn(2010, OperatorType.DIFFERENT));
    }

    private static void printList(String description, List<Car> cars) {
        System.out.println(description);
        for (Car car : cars){
            System.out.println(car);
        }
    }

    private static void createBaseData(){
        carService.addCar(carToFind);
        carService.addCar(new Car("VW", "Golf", 5000, 1998,
                List.of(manufacturerToFind),
                EngineType.V6));
        carService.addCar(new Car("Audi", "A6", 50000, 2015,
                List.of(new Manufacturer("VAG 3", 2010, "Germany"),
                        new Manufacturer("VAG 4", 2011, "Germany"),
                        new Manufacturer("VAG 5", 2014, "Germany")),
                EngineType.V12));
        carService.addCar(carToRemove);
    }
}
