package pl.sda.s06_zaawansowane_programowanie.examples.e12_13;

public enum EngineType {
    V12, V8, V6, S6, S4, S3
}
