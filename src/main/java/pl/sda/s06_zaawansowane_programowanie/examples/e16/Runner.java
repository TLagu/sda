package pl.sda.s06_zaawansowane_programowanie.examples.e16;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Runner {
    BEGINNER(300, 500),
    INTERMEDIATE(200, 300),
    ADVANCED(150, 200);

    private int min;
    private int max;

    public static Runner getFitnessLevel(int time){
        for(Runner c : Runner.values()){
            if (c.min <= time && time <= c.max) {
                return c;
            }
        }
        return null;
    }
}
