package pl.sda.s06_zaawansowane_programowanie.examples.e16;

public class Main {
    public static void main(String[] args) {
        System.out.println("150 min: " + Runner.getFitnessLevel(150));
        System.out.println("200 min: " + Runner.getFitnessLevel(200));
        System.out.println("300 min: " + Runner.getFitnessLevel(300));
        System.out.println("400 min: " + Runner.getFitnessLevel(400));
        System.out.println("500 min: " + Runner.getFitnessLevel(500));
    }
}
