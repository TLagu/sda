package pl.sda.s06_zaawansowane_programowanie.examples.e39_b;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class Sensor implements Runnable, Tournament {
    private final AtomicInteger value;

    public Sensor(AtomicInteger value) {
        this.value = value;
    }

    @Override
    public void run() {
        LocalTime startTime = LocalTime.now();
        Random random = new Random();
        while (ChronoUnit.SECONDS.between(startTime, LocalTime.now()) < 60) {
            int lastValue = value.get();
            waitFor(random.nextInt(5000));
            synchronized (value) {
                if (value.get() == lastValue) {
                    value.set(random.nextInt(100));
                    System.out.println("Sensor (" + Thread.currentThread().getId()
                            + "): The value has changed.");
                    value.notifyAll();
                } else {
                    System.out.println("Sensor (" + Thread.currentThread().getId()
                            + "): The value has not been changed, the data has already been changed by another sensor.");
                }
            }
            System.out.println(ChronoUnit.SECONDS.between(startTime, LocalTime.now()));
        }
    }
}
