package pl.sda.s06_zaawansowane_programowanie.examples.e39_b;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Screen implements Runnable, Tournament {
    private final AtomicInteger value;

    public Screen(AtomicInteger value) {
        this.value = value;
    }

    @Override
    public void run() {
        LocalTime startTime = LocalTime.now();
        while (ChronoUnit.SECONDS.between(startTime, LocalTime.now()) < 60) {
            synchronized (value) {
                System.out.println("Screen (" + Thread.currentThread().getId() + "): " + value.get());
                try {
                    value.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
