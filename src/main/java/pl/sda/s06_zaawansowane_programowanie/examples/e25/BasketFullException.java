package pl.sda.s06_zaawansowane_programowanie.examples.e25;

public class BasketFullException extends Throwable {
    public BasketFullException(String message) {
        super (message);
    }
}
