package pl.sda.s06_zaawansowane_programowanie.examples.e25;

public class BasketEmptyException extends Throwable {
    public BasketEmptyException(String message) {
        super (message);
    }
}
