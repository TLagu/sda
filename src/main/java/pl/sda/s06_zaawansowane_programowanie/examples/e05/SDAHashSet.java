package pl.sda.s06_zaawansowane_programowanie.examples.e05;

import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@ToString
public class SDAHashSet<V> {
    private List<V> items = new ArrayList<>();

    public void add(V item) {
        if (!items.contains(item)) {
            items.add(item);
        }
    }

    public void remove(V item) {
        items.remove(item);
    }

    public int size() {
        return items.size();
    }

    public boolean contains(V item) {
        return items.contains(item);
    }

    public void clear() {
        items = new ArrayList<>();
    }
}
