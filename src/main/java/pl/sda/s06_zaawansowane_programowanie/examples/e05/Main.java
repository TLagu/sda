package pl.sda.s06_zaawansowane_programowanie.examples.e05;

public class Main {
    public static void main(String[] args) {
        SDAHashSet<String> stringSDAHashSet = new SDAHashSet<>();
        stringSDAHashSet.add("string 1");
        stringSDAHashSet.add("string 2");
        stringSDAHashSet.add("string 3");
        stringSDAHashSet.add("string 4");
        stringSDAHashSet.add("string 5");
        System.out.println("All: " + stringSDAHashSet);
        stringSDAHashSet.remove("string 3");
        System.out.println("remove 'string 3': " + stringSDAHashSet);
        System.out.println("exists 'string 4': " + stringSDAHashSet.contains("string 4"));
        System.out.println("size: " + stringSDAHashSet.size());
        stringSDAHashSet.clear();
        System.out.println("All: " + stringSDAHashSet);
    }
}
