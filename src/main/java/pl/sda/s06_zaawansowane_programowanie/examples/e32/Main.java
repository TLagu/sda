package pl.sda.s06_zaawansowane_programowanie.examples.e32;

import java.util.ArrayList;
import java.util.List;

public class Main {
    private static final String PATH =
            "c:\\Users\\Lagu\\IdeaProjects\\sda\\src\\main\\resources\\pl\\sda\\s06_zaawansowane_programowanie\\examples\\e32\\";

    public static void main(String[] args) {
        List<String> list_in = new ArrayList<>();
        list_in.add("First");
        list_in.add("Second");
        list_in.add("Third");
        list_in.add("Forth");
        ArrayIO.writeToFile(list_in, PATH + "file.txt");
        List<String> list_out = ArrayIO.readFromFile(PATH + "file.txt");
        System.out.println(list_out);
    }
}
