package pl.sda.s06_zaawansowane_programowanie.examples.e32;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ArrayIO {

    public static List<String> readFromFile(String fileName) {
        try {
            FileInputStream fis = new FileInputStream(fileName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<String> namesDes = (ArrayList<String>) ois.readObject();
            ois.close();
            fis.close();
            return namesDes;
        } catch (ClassNotFoundException | IOException e2) {
            e2.printStackTrace();
        }
        return Collections.emptyList();
    }

    public static void writeToFile(List<String> list, String fileName) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName));
            out.writeObject(list);
            out.flush();
            out.close();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
