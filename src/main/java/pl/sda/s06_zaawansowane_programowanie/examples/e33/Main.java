package pl.sda.s06_zaawansowane_programowanie.examples.e33;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

public class Main {
    private static final String PATH =
            "c:\\Users\\Lagu\\IdeaProjects\\sda\\src\\main\\resources\\pl\\sda\\s06_zaawansowane_programowanie\\examples\\e33\\";

    public static void main(String[] args) throws IOException {
        List<String> extensions = List.of(".png", ".jpg");
        try (BufferedWriter out = new BufferedWriter(new FileWriter(PATH + "file_list.txt"))) {
            Path path = Paths.get(PATH);
            Stream<Path> walk = Files.walk(path);
            walk.map(Object::toString).filter(f -> extensions.stream().anyMatch(f::endsWith)).forEach((o) -> {
                try {
                    out.write(o.toString() + "\n");
                    System.out.println(o);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
