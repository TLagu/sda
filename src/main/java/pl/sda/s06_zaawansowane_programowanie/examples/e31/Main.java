package pl.sda.s06_zaawansowane_programowanie.examples.e31;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Main {
    private static final String PATH =
            "c:\\Users\\Lagu\\IdeaProjects\\sda\\src\\main\\resources\\pl\\sda\\s06_zaawansowane_programowanie\\examples\\e31\\";
    private static final Map<String, Integer> wordStatistics = new HashMap<>();
    private static int maxWordLength;
    private static int firstColSize;
    private static int secondColSize;

    public static void main(String[] args) {
        String filename_in = "file_in.txt";
        String filename_out = "file_out.txt";
        String line;
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(PATH + filename_in), StandardCharsets.UTF_8))) {
            while ((line = in.readLine()) != null) {
                calculateWords(line);
            }
            System.out.println(wordStatistics);
        } catch (IOException e) {
            e.printStackTrace();
        }

        int totalSum = wordStatistics.values().stream().reduce(0, Integer::sum);
        firstColSize = Math.max(6, maxWordLength + 1);
        secondColSize = Math.max(6, String.valueOf(totalSum).length() + 1);

        try (BufferedWriter out = new BufferedWriter(
                new OutputStreamWriter(
                        new FileOutputStream(PATH + filename_out), StandardCharsets.UTF_8))) {
            saveHeaderToFile(out);
            saveBreakLineToFile(out);
            saveDataToFile(out);
            saveBreakLineToFile(out);
            saveFooterToFile(String.valueOf(totalSum), out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void calculateWords(String line){
        line = line.replaceAll("[^a-zA-Z0-9]", " ").replaceAll("\\s+", " ");
        String[] split = line.split(" ");
        Arrays.stream(split).forEach(s -> {
            wordStatistics.put(s, wordStatistics.getOrDefault(s, 0) + 1);
            if (maxWordLength < s.length()) {
                maxWordLength = s.length();
            }
        });
    }

    private static void saveHeaderToFile(BufferedWriter out) throws IOException {
        int firstColStart = (firstColSize - "Word".length()) / 2;
        int secondColStart = (int) Math.ceil((double) (secondColSize - "Count".length()) / 2);
        out.write(" ".repeat(firstColStart) + "Word" + " ".repeat(firstColSize - firstColStart - 4)
                + "|"
                + " ".repeat(secondColStart) + "Count" + " ".repeat(secondColSize - secondColStart - 5)
                + "\n");
    }

    private static void saveBreakLineToFile(BufferedWriter out) throws IOException {
        out.write("-".repeat(firstColSize + secondColSize + 1) + "\n");
    }

    private static void saveDataToFile(BufferedWriter out){
        wordStatistics.forEach((key, value) -> {
            try {
                out.write(key + " ".repeat(firstColSize - key.length())
                        + "|"
                        + " ".repeat(secondColSize - String.valueOf(value).length()) + value + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private static void saveFooterToFile(String secondFooter, BufferedWriter out) throws IOException {
        out.write(" ".repeat(firstColSize - "Total:".length() - 1) + "Total: "
                + "|"
                + " ".repeat(secondColSize - secondFooter.length()) + secondFooter + "\n");
    }
}
