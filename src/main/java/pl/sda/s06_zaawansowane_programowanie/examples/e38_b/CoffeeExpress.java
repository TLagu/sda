package pl.sda.s06_zaawansowane_programowanie.examples.e38_b;

public class CoffeeExpress {
    private boolean waterTankEmpty = false;
    private int waterLevel = 3;

    public synchronized void makeCoffee() throws InterruptedException {
        while (waterTankEmpty) {
            wait();
        }
        getWater();
        System.out.printf("%s zrobił kawkę!\n", Thread.currentThread().getName());
        notifyAll();
    }

    public synchronized void fillWaterTank() throws InterruptedException {
        while (!waterTankEmpty) {
            wait();
        }
        waterLevel = 3;
        waterTankEmpty = false;
        System.out.printf("%s napełnił zbiornik z wodą.", Thread.currentThread().getName());
        notifyAll();
    }

    private void getWater() {
        waterLevel--;
        if (waterLevel <= 0) {
            waterTankEmpty = true;
        }
    }
}
