package pl.sda.s06_zaawansowane_programowanie.examples.e39_a;

public interface Tournament {

    default void waitFor(int milliseconds){
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
