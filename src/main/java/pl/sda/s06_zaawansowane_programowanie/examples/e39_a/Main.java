package pl.sda.s06_zaawansowane_programowanie.examples.e39_a;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    private final static List<Callable<String>> tasks = new ArrayList<>();
    private final static int AVAILABLE_PROCESSORS =  Runtime.getRuntime().availableProcessors();

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(AVAILABLE_PROCESSORS);
        generateTasks();
        try {
            executorService.invokeAll(tasks);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void generateTasks() {
        AtomicInteger lastValue = new AtomicInteger();
        Random random = new Random();
        for (int i = 0; i < AVAILABLE_PROCESSORS; i++){
            if (random.nextBoolean()) {
                final int intI = i;
                tasks.add(() -> {
                    Thread thread = new Thread(new Screen(lastValue));
                    thread.start();
                    return String.valueOf(intI);
                });
            } else {
                final int intI = i;
                tasks.add(() -> {
                    Thread thread = new Thread(new Sensor(lastValue));
                    thread.start();
                    return String.valueOf(intI);
                });
            }
        }
    }
}
