package pl.sda.s06_zaawansowane_programowanie.examples.e39_a;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class Screen implements Runnable, Tournament {
    private final AtomicInteger value;

    public Screen(AtomicInteger value) {
        this.value = value;
    }

    @Override
    public void run() {
        LocalTime startTime = LocalTime.now();
        Random random = new Random();
        while (ChronoUnit.SECONDS.between(startTime, LocalTime.now()) < 30) {
            waitFor(random.nextInt(1000));
            synchronized (value) {
                System.out.println("Screen (" + Thread.currentThread().getName()
                        + "): " + value.get());
            }
        }
        System.out.println(ChronoUnit.SECONDS.between(startTime, LocalTime.now()));
    }
}
