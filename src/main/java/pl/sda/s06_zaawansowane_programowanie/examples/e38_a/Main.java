package pl.sda.s06_zaawansowane_programowanie.examples.e38_a;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Water water = new Water(100.0);
        Thread.sleep(2000);
        new Thread(new WaterManager(water, -200.0)).start();
        Thread.sleep(2000);
        new Thread(new WaterManager(water, 100.0)).start();
        Thread.sleep(2000);
        new Thread(new WaterManager(water, -200.0)).start();
        Thread.sleep(2000);
        new Thread(new WaterManager(water, -200.0)).start();
        Thread.sleep(2000);
        new Thread(new WaterManager(water, -200.0)).start();
        Thread.sleep(2000);
        new Thread(new WaterManager(water, 500.0)).start();
        Thread.sleep(2000);
        new Thread(new WaterManager(water, 100.0)).start();
    }
}
