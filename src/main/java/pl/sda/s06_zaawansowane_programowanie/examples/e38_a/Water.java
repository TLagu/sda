package pl.sda.s06_zaawansowane_programowanie.examples.e38_a;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Water {
    private double waterVolume;

    public synchronized void makeCoffee(double waterChange) {
        while (waterVolume < waterChange) {
            try {
                System.out.println("Waiting for water (" + Thread.currentThread().getId() + ").");
                wait();
            } catch (InterruptedException e) {
                System.out.println(e);
                System.out.println("Thread interrupted (" + Thread.currentThread().getId() + "): " + e);
            }
        }
        waterVolume -= waterChange;
        System.out.println("Making coffee (" + Thread.currentThread().getId() + "): " + waterVolume);
    }

    public synchronized void addingWater(double waterChange) {
        waterVolume += waterChange;
        System.out.println("Topping up the water (" + Thread.currentThread().getId() + "): " + waterVolume);
        notifyAll();
    }
}
