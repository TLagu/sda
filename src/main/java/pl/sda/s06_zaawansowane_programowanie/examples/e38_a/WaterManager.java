package pl.sda.s06_zaawansowane_programowanie.examples.e38_a;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class WaterManager implements Runnable {
    private Water waterVolume;
    private double waterChange;

    @Override
    public void run() {
        if (waterChange == 0
                || waterVolume.getWaterVolume() + waterChange < 1 - Double.MAX_VALUE
                || waterVolume.getWaterVolume() + waterChange > Double.MAX_VALUE) {
            throw new IllegalArgumentException("Transaction error - wrong water change value.");
        }
        if (waterChange < 0) {
            waterVolume.makeCoffee(Math.abs(waterChange));
        } else {
            waterVolume.addingWater(waterChange);
        }
    }
}
