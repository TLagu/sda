package pl.sda.s06_zaawansowane_programowanie.examples.e23;

import java.util.*;
import java.util.stream.Collectors;

public class Zoo {
    private Map<String, Integer> animals = new HashMap<>();

    public int getNumberOfAllAnimals(){
        return animals.values().stream()
                .reduce(0, Integer::sum);
    }

    public Map<String, Integer> getAnimalsCount(){
        return animals;
    }

    public Map<String, Integer> getAnimalsCountSorted(){
        return animals.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry<String, Integer>::getValue).reversed())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }

    public void addAnimals(String animal, int count){
        animals.put(animal, animals.getOrDefault(animal, 0) + count);
    }
}
