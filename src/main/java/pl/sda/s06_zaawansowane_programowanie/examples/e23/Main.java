package pl.sda.s06_zaawansowane_programowanie.examples.e23;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        Zoo zoo = new Zoo();
        zoo.addAnimals("Kot", random.nextInt(20));
        System.out.println("Animals: " + zoo.getAnimalsCount());
        zoo.addAnimals("Pies", random.nextInt(20));
        System.out.println("Animals: " + zoo.getAnimalsCount());
        zoo.addAnimals("Niedźwiedź", random.nextInt(20));
        System.out.println("Animals: " + zoo.getAnimalsCount());
        zoo.addAnimals("Psokot", random.nextInt(20));
        System.out.println("Animals: " + zoo.getAnimalsCount());
        zoo.addAnimals("Dzik", random.nextInt(20));
        System.out.println("Animals: " + zoo.getAnimalsCount());
        zoo.addAnimals("Kot", random.nextInt(20));
        System.out.println("Animals: " + zoo.getAnimalsCount());
        zoo.addAnimals("Pies", random.nextInt(20));
        System.out.println("Animals: " + zoo.getAnimalsCount());
        System.out.println("Animals count: " + zoo.getNumberOfAllAnimals());
        System.out.println("Animals count (sorted): " + zoo.getAnimalsCountSorted());
    }
}
