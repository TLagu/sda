package pl.sda.s06_zaawansowane_programowanie.examples.e21;

public class Cone extends C3DShape{
    private final int radius;
    private final int height;

    Cone(int radius, int height){
        this.radius = radius;
        this.height = height;
        calculatePerimeter();
        calculateArea();
        calculateVolume();
    }

    @Override
    public void calculatePerimeter() {
        perimeter = 2 * Math.PI * radius;
    }

    @Override
    public void calculateArea() {
        area = Math.PI * radius * (radius + Math.sqrt(radius * radius + height * height));
    }

    @Override
    public void calculateVolume() {
        volume = Math.PI * radius * radius * height / 3;
    }
}
