package pl.sda.s06_zaawansowane_programowanie.examples.e21;

import lombok.Getter;

@Getter
public abstract class Shape {
    protected double perimeter;
    protected double area;

    public abstract void calculatePerimeter();
    public abstract void calculateArea();
}
