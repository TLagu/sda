package pl.sda.s06_zaawansowane_programowanie.examples.e21;

public class Main {
    public static void main(String[] args) {
        Cone cone = new Cone(1, 1);
        System.out.println("Cone -> perimeter: " + cone.getPerimeter() + ", area: " + cone.getArea() + ", volume: " + cone.getVolume());
        Qube qube = new Qube(1);
        System.out.println("Qube -> perimeter: " + qube.getPerimeter() + ", area: " + qube.getArea() + ", volume: " + qube.getVolume());
    }
}
