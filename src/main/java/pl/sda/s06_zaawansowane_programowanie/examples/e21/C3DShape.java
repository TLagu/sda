package pl.sda.s06_zaawansowane_programowanie.examples.e21;

import lombok.Getter;

public abstract class C3DShape extends Shape {
    @Getter
    protected double volume;

    public abstract void calculateVolume();
}
