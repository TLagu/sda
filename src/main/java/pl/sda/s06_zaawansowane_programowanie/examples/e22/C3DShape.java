package pl.sda.s06_zaawansowane_programowanie.examples.e22;

import lombok.Getter;

public abstract class C3DShape extends Shape implements Fillable {
    @Getter
    protected double volume;

    public abstract void calculateVolume();

    @Override
    public void fill(double volume) {
        if (volume < this.volume) {
            System.out.println("Water volume (" + volume + "): Za mało wody.");
        } else if(volume == this.volume) {
            System.out.println("Water volume (" + volume + "): Woda po brzegi.");
        } else {
            System.out.println("Water volume (" + volume + "): Woda się przelała.");
        }
    }
}
