package pl.sda.s06_zaawansowane_programowanie.examples.e22;

public class Qube extends C3DShape {
    private final int sideA;

    Qube(int sideA){
        this.sideA = sideA;
        calculatePerimeter();
        calculateArea();
        calculateVolume();
    }

    @Override
    public void calculatePerimeter() {
        perimeter = 12 * sideA;
    }

    @Override
    public void calculateArea() {
        area = 6 * sideA * sideA;
    }

    @Override
    public void calculateVolume() {
        volume = sideA * sideA * sideA;
    }
}
