package pl.sda.s06_zaawansowane_programowanie.examples.e22;

public interface Fillable {
    void fill(double volume);
}
