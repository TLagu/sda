package pl.sda.s06_zaawansowane_programowanie.examples.e09;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(new Point2D(0, 0), new Point2D(1, 0));
        System.out.println(circle);
        System.out.println("Radius: " + circle.getRadius());
        System.out.println("Perimeter: " + circle.getPerimeter());
        System.out.println("Area: " + circle.getArea());
        System.out.println("SlicePoints: " + Arrays.toString(circle.getSlicePoints(90)));
    }
}
