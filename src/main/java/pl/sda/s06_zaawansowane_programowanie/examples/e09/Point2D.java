package pl.sda.s06_zaawansowane_programowanie.examples.e09;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class Point2D {
    private double x;
    private double y;
}
