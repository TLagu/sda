package pl.sda.s06_zaawansowane_programowanie.examples.e36;

public class Main {

    public static void main(String[] args) {
        Thread firstThread = new Thread(new ThreadPlaygroundRunnable("First Thread"));
        Thread secondThread = new Thread(new ThreadPlaygroundRunnable("Second Thread"));
        firstThread.start();
        secondThread.start();
    }
}
