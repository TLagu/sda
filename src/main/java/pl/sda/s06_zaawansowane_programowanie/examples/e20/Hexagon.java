package pl.sda.s06_zaawansowane_programowanie.examples.e20;

public class Hexagon extends Shape{
    private final int sideA;

    Hexagon(int sideA){
        this.sideA = sideA;
        calculatePerimeter();
        calculateArea();
    }

    @Override
    public void calculatePerimeter() {
        perimeter = 6 * sideA;
    }

    @Override
    public void calculateArea() {
        area = 3 * sideA * sideA * Math.sqrt(3) / 2;
    }
}
