package pl.sda.s06_zaawansowane_programowanie.examples.e20;

public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(4, 5);
        System.out.println("Rectangle -> perimeter: " + rectangle.getPerimeter() + ", area: " + rectangle.getArea());
        Triangle triangle = new Triangle(3, 4, 5);
        System.out.println("Triangle -> perimeter: " + triangle.getPerimeter() + ", area: " + triangle.getArea());
        Hexagon hexagon = new Hexagon(2);
        System.out.println("Hexagon -> perimeter: " + hexagon.getPerimeter() + ", area: " + hexagon.getArea());

    }
}
