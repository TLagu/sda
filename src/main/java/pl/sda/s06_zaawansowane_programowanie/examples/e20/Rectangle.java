package pl.sda.s06_zaawansowane_programowanie.examples.e20;

public class Rectangle extends Shape {
    private final int sideA;
    private final int sideB;

    Rectangle(int sideA, int sideB){
        this.sideA = sideA;
        this.sideB = sideB;
        calculatePerimeter();
        calculateArea();
    }

    @Override
    public void calculatePerimeter() {
        perimeter = 2 * sideA + 2 * sideB;
    }

    @Override
    public void calculateArea() {
        area = sideA * sideB;
    }
}
