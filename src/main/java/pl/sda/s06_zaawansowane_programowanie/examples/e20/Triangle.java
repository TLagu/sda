package pl.sda.s06_zaawansowane_programowanie.examples.e20;

public class Triangle extends Shape {
    private final int sideA;
    private final int sideB;
    private final int sideC;

    Triangle(int sideA, int sideB, int sideC){
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
        calculatePerimeter();
        calculateArea();
    }

    @Override
    public void calculatePerimeter() {
        perimeter = sideA + sideB + sideC;
    }

    @Override
    public void calculateArea() {
        double p = (double) (sideA + sideB + sideC) / 2;
        area = Math.sqrt(p * (p - sideA) * (p - sideB) * (p - sideC));
    }
}
