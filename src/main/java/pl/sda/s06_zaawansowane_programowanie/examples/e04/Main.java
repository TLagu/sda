package pl.sda.s06_zaawansowane_programowanie.examples.e04;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Map<String, Set<String>> map = new HashMap<>();
        Storage storage = new Storage(map);
        storage.addToStorage("hdhaksjhd", "hdjkhdasjd");
        storage.addToStorage("flkfj", "afafdasdf");
        storage.addToStorage("sadfdaf", "asfdfas");
        storage.addToStorage("dffdsf", "asfdfas");
        storage.addToStorage("zsfdfzs", "zsdfdfz");
        storage.addToStorage("fsdzdf", "dfsfdsf");
        storage.addToStorage("fsdzdf", "sdfdsfsd");
        storage.addToStorage("fsdzdf", "sdfdsfsd");
        storage.printValues("fsdzdf");
        storage.findValues("asfdfas");
    }
}
