package pl.sda.s06_zaawansowane_programowanie.examples.e04;

import lombok.AllArgsConstructor;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@AllArgsConstructor
public class Storage {
    private Map<String, Set<String>> data;

    public void addToStorage(String key, String value){
        if (!data.containsKey(key)) {
            data.put(key, new HashSet<>());
        }
        data.get(key).add(value);
    }

    public void printValues(String key){
        data.entrySet().stream()
                .filter(d -> d.getKey().equals(key))
                .forEach(s -> System.out.println("Key: " + s.getKey() + ", value: " + s.getValue()));
    }

    public void findValues(String value){
        data.entrySet().stream()
                .filter(d -> d.getValue().contains(value))
                .forEach(s -> System.out.println("Key: " + s.getKey() + ", value: " + s.getValue()));
    }
}
