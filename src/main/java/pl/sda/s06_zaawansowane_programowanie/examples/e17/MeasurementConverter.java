package pl.sda.s06_zaawansowane_programowanie.examples.e17;

public class MeasurementConverter {
    public double convert(int value, ConversionType conversionType){
        return (double) value * conversionType.getConverter();
    }
}
