package pl.sda.s06_zaawansowane_programowanie.examples.e17;

public class Main {
    public static void main(String[] args) {
        MeasurementConverter measurementConverter = new MeasurementConverter();
        System.out.println("CENTIMETERS_TO_INCHES: "
                + measurementConverter.convert(10, ConversionType.CENTIMETERS_TO_INCHES));
        System.out.println("INCHES_TO_CENTIMETERS: "
                + measurementConverter.convert(10, ConversionType.INCHES_TO_CENTIMETERS));
        System.out.println("CENTIMETERS_TO_INCHES: "
                + measurementConverter.convert(10, ConversionType.CENTIMETERS_TO_INCHES));
        System.out.println("INCHES_TO_CENTIMETERS: "
                + measurementConverter.convert(10, ConversionType.INCHES_TO_CENTIMETERS));
        System.out.println("KILOMETERS_TO_MILES: "
                + measurementConverter.convert(10, ConversionType.KILOMETERS_TO_MILES));
        System.out.println("MILES_TO_KILOMETERS: "
                + measurementConverter.convert(10, ConversionType.MILES_TO_KILOMETERS));
    }
}
