package pl.sda.s06_zaawansowane_programowanie.examples.e15;

public class Main {
    public static void main(String[] args) {
        Car car1 = Car.FERRARI;
        Car car2 = Car.OPEL;
        System.out.println("Ferrari: " + car1);
        System.out.println("Opel: " + car2);
        System.out.println("isPremium: " + car1.isPremium());
        System.out.println("isRegular: " + car2.isRegular());
        System.out.println("Ferrari isFaster: " + car1.isFasterThan(car2));
    }
}
