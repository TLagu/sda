package pl.sda.s06_zaawansowane_programowanie.examples.e15;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public enum Car implements Comparable<Car> {
    FERRARI(1000000, 570, true),
    PORSCHE(300000, 350, true),
    MERCEDES(250000, 250, false),
    BMW(250000, 250, false),
    OPEL(120000, 150, false),
    FIAT(60000, 100, false),
    TOYOTA(100000, 125, false);

    private int price;
    private int power;
    private boolean premium;

    public boolean isRegular() {
        return !isPremium();
    }

    public boolean isFasterThan(Car car) {
        CarComparator c = new CarComparator();
        return c.compare(this, car) > 0;
    }
}
