package pl.sda.s06_zaawansowane_programowanie.examples.e30;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Main {
    private static final String PATH =
            "c:\\Users\\Lagu\\IdeaProjects\\sda\\src\\main\\resources\\pl\\sda\\s06_zaawansowane_programowanie\\examples\\e30\\";

    public static void main(String[] args) {
        String filename_in = "file_in.txt";
        String filename_out = new StringBuilder(filename_in).reverse().toString();
        List<String> lines = new ArrayList<>();
        String line;
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(PATH + filename_in), StandardCharsets.UTF_8))) {
            while ((line = in.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedWriter out = new BufferedWriter(
                new OutputStreamWriter(
                        new FileOutputStream(PATH + filename_out), StandardCharsets.UTF_8))) {
            for (int i = lines.size() - 1; i >= 0; i--) {
                out.write(new StringBuilder(lines.get(i)).reverse().toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
