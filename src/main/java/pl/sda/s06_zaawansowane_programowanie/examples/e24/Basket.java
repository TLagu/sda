package pl.sda.s06_zaawansowane_programowanie.examples.e24;

import java.util.ArrayList;
import java.util.List;

public class Basket {
    List<String> basket = new ArrayList<>();
    public final static int MAX_BASKET_SIZE = 10;

    public void addToBasket(String element){
        if (basket.size() >= MAX_BASKET_SIZE) {
            throw new BasketFullException("Basket is full!");
        }
        basket.add(element);
    }

    public void removeFromBasket(){
        if (basket.size() <= 0) {
            throw new BasketEmptyException("Basket is empty!");
        }
        basket.remove(basket.size() - 1);
    }
}
