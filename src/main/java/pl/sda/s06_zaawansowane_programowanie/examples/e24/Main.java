package pl.sda.s06_zaawansowane_programowanie.examples.e24;

public class Main {
    public static void main(String[] args) {
        Basket basket = new Basket();
        //basket.removeFromBasket();
        basket.addToBasket("Butter");
        basket.addToBasket("Bread");
        basket.addToBasket("Milk");
        basket.addToBasket("Beer");
        basket.addToBasket("Vodka");
        basket.addToBasket("Vine");
        basket.addToBasket("Juice");
        basket.addToBasket("Water");
        basket.addToBasket("Cheese");
        basket.addToBasket("Meat");
        //basket.addToBasket("Jam");
    }
}
