package pl.sda.s06_zaawansowane_programowanie.examples.e24;

public class BasketEmptyException extends RuntimeException {
    public BasketEmptyException(String message) {
        super (message);
    }
}
