package pl.sda.s06_zaawansowane_programowanie.examples.e24;

public class BasketFullException extends RuntimeException {
    public BasketFullException(String message) {
        super (message);
    }
}
