package pl.sda.s06_zaawansowane_programowanie.examples.e03;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put("Java", 18);
        map.put("Python", 1);
        map.put("PHP", 0);
        showMapContent(map);
    }

    private static void showMapContent(Map<String, Integer> map){
        boolean test = false;
        for(Map.Entry<String, Integer> entry : map.entrySet()){
            if (!test) {
                test = true;
            } else {
                System.out.println(",");
            }
            System.out.print("Klucz " + entry.getKey() + ", wartość: " + entry.getValue());
        }
        System.out.println(".");
    }

    private static void showMapContent2(Map<String, Integer> map){
        Set<Map.Entry<String, Integer>> entries = map.entrySet();
        int rowNumber = 1;
        //for()
    }

    private static void showMapContent3(Map<String, Integer> map){
        Set<Map.Entry<String, Integer>> entries = map.entrySet();
        Iterator<Map.Entry<String, Integer>> iterator = entries.iterator();
        while (iterator.hasNext()){
            Map.Entry<String, Integer> entry = iterator.next();
            StringBuilder row = new StringBuilder("Klucz: ");
            row.append(entry.getKey()).append(", wartość: ").append(entry.getValue());
            if (iterator.hasNext()) {
                row.append(",");
            } else {
                row.append(".");
            }
            System.out.println(row);
        }
    }
}
