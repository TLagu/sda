package pl.sda.s06_zaawansowane_programowanie.examples.e11;

public interface Movable {
    void move(MoveDirection moveDirection);
}
