package pl.sda.s06_zaawansowane_programowanie.examples.e11;

public interface Resizable {
    void resize(double resizeFactor);
}
