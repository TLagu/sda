package pl.sda.s06_zaawansowane_programowanie.examples.e37;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ThreadPlaygroundRunnable implements Runnable {
    private final String name;

    @Override
    public void run() {
        for(int i = 0; i < 10; i++){
            System.out.println(i + ". thread: " + Thread.currentThread().getName() + ", name: " + name);
        }
    }
}
