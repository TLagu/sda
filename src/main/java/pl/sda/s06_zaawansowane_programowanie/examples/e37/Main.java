package pl.sda.s06_zaawansowane_programowanie.examples.e37;

import pl.sda.s06_zaawansowane_programowanie.examples.e36.ThreadPlaygroundRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Main {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        List<Callable<String>> tasks = new ArrayList<>();
        for (int i = 0; i < 10; i ++){
            String name = "TH_" + i;
            tasks.add(() -> {
                 Thread thread = new Thread(new ThreadPlaygroundRunnable(name));
                 thread.start();
                 return name;
             });
        }
        try {
            List<Future<String>> allResults = executorService.invokeAll(tasks);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
    }
}
