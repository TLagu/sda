package pl.sda.s06_zaawansowane_programowanie.examples.e28;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class NewArrayList<T> extends ArrayList {
    public List<Object> getEveryNthElement(int startIndex, int skip){
        return IntStream
                .range(0, this.size())
                .skip(skip)
                .filter(i -> (i - skip) % startIndex == 0)
                .mapToObj(this::get)
                .collect(Collectors.toList());
    }

    public void printEveryNthElement(int startIndex, int skip){
        IntStream
                .range(0, this.size())
                .skip(skip)
                .filter(i -> (i - skip) % startIndex == 0)
                .mapToObj(this::get)
                .forEach(System.out::println);
    }
}
