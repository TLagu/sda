package pl.sda.s06_zaawansowane_programowanie.examples.e28;

public class Main {
    public static void main(String[] args) {
        NewArrayList<Integer> list = new NewArrayList<>();
        list.add(2);
        list.add(4);
        list.add(6);
        list.add(8);
        list.add(10);
        list.add(12);
        list.add(14);
        list.add(16);
        list.add(18);
        list.add(20);
        list.add(22);
        list.add(24);
        list.add(26);
        list.printEveryNthElement(3, 5);
        System.out.println(list.getEveryNthElement(3, 5));
    }
}
