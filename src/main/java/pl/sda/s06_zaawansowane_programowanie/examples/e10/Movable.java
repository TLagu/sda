package pl.sda.s06_zaawansowane_programowanie.examples.e10;

/**
 * Interface przesuwający geometrie o wskazany wektor
 */
public interface Movable {
    /**
     * Przesuwa obiekt o wektor reprezentowany przez obiekt typu MoveDirection
     * @param moveDirection - wektor przesunięcia
     */
    void move(MoveDirection moveDirection);
}
