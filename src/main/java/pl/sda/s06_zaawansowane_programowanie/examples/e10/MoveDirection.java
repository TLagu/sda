package pl.sda.s06_zaawansowane_programowanie.examples.e10;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class MoveDirection {
    private double x;
    private double y;
}
