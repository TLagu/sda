package pl.sda.s06_zaawansowane_programowanie.examples.e10;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        System.out.println("Original -----------------------------");
        Circle circle = new Circle(new Point2D(0, 0), new Point2D(1, 0));
        System.out.println(circle);
        System.out.println("Radius: " + circle.getRadius());
        System.out.println("Perimeter: " + circle.getPerimeter());
        System.out.println("Area: " + circle.getArea());
        System.out.println("SlicePoints: " + Arrays.toString(circle.getSlicePoints(90)));

        System.out.println("Moved -----------------------------");
        MoveDirection moveDirection = new MoveDirection(5, 5);
        circle.move(moveDirection);
        System.out.println(circle);
        System.out.println("Radius: " + circle.getRadius());
        System.out.println("Perimeter: " + circle.getPerimeter());
        System.out.println("Area: " + circle.getArea());
        System.out.println("SlicePoints: " + Arrays.toString(circle.getSlicePoints(90)));
    }
}
