package pl.sda.s06_zaawansowane_programowanie.examples.e10;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Circle implements Movable {
    private Point2D center;
    private Point2D point;
    private double radius;

    public Circle(Point2D center, Point2D point){
        this.center = center;
        this.point = point;
        this.radius = Math.sqrt(Math.pow(center.getX() - point.getX(), 2) + Math.pow(center.getY() - point.getY(), 2));
    }

    public double getRadius(){
        return radius;
    }

    public double getPerimeter(){
        return 2 * Math.PI * radius;
    }

    public double getArea(){
        return Math.PI * radius * radius;
    }

    private Point2D slicePoint(int angle, Point2D lastPoint){
        double alpha = Math.PI * angle / 180;
        double sinAlpha = Math.sin(alpha);
        double cosAlpha = Math.cos(alpha);
        double x = Math.round(center.getX()
                + (lastPoint.getX() - center.getX()) * cosAlpha
                - (lastPoint.getY() - center.getY()) * sinAlpha);
        double y = Math.round(center.getY()
                + (lastPoint.getX() - center.getX()) * sinAlpha
                - (lastPoint.getY() - center.getY()) * cosAlpha);
        return new Point2D(x, y);
    }

    public Point2D[] getSlicePoints(int angle){
        Point2D[] points = new Point2D[3];
        points[0] = slicePoint(angle, point);
        points[1] = slicePoint(angle, points[0]);
        points[2] = slicePoint(angle, points[1]);
        return points;
    }

    @Override
    public void move(MoveDirection moveDirection) {
        center.move(moveDirection);
        point.move(moveDirection);
    }
}
