package pl.sda.s06_zaawansowane_programowanie.examples.e27;

import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

@AllArgsConstructor
public class Joiner<T> {
    private String separator;

    public String join(T... items){
        return Arrays.stream(items)
                .filter(Objects::nonNull)
                .map(Object::toString)
                .collect(Collectors.joining(separator));
    }
}
