package pl.sda.s06_zaawansowane_programowanie.examples.e08;

public interface Validator {
    int SUM_OF_DIMENSIONS = 300;
    int MIN_LENGTH = 30;
    float MAX_NO_EXPRESS_WEIGHT = 30.0f;
    float MAX_EXPRESS_WEIGHT = 15.0f;

    default boolean validate(Parcel input) {
        if (input.getXLength() + input.getYLength() + input.getZLength() > SUM_OF_DIMENSIONS) {
            System.out.printf("Suma wymiarów (x, y, z) przekracza %d.\n", SUM_OF_DIMENSIONS);
            return false;
        }
        if (input.getXLength() < MIN_LENGTH || input.getYLength() < MIN_LENGTH || input.getZLength() < MIN_LENGTH) {
            System.out.printf("Jeden z wymiarów jest mniejszy niż %d.\n", MIN_LENGTH);
            return false;
        }
        if (!input.isExpress() && input.getWeight() > MAX_NO_EXPRESS_WEIGHT
                || input.isExpress() && input.getWeight() > MAX_EXPRESS_WEIGHT) {
            System.out.printf("Waga przekracza %d (expres: %b).\n", MIN_LENGTH, input.isExpress());
            return false;
        }
        return true;
    }
}
