package pl.sda.s06_zaawansowane_programowanie.examples.e08;

public class ValidatorHelper implements Validator{

    @Override
    public boolean validate(Parcel input) {
        boolean result = true;
        if (input.getXLength() + input.getYLength() + input.getZLength() > SUM_OF_DIMENSIONS) {
            System.out.printf("Suma wymiarów (x, y, z) przekracza %d.\n", SUM_OF_DIMENSIONS);
            result = false;
        }
        if (input.getXLength() < MIN_LENGTH || input.getYLength() < MIN_LENGTH || input.getZLength() < MIN_LENGTH) {
            System.out.printf("Jeden z wymiarów jest mniejszy niż %d.\n", MIN_LENGTH);
            result = false;
        }
        if (!input.isExpress() && input.getWeight() > MAX_NO_EXPRESS_WEIGHT
                || input.isExpress() && input.getWeight() > MAX_EXPRESS_WEIGHT) {
            System.out.printf("Waga przekracza %d (expres: %b).\n",
                    (input.isExpress()?MAX_EXPRESS_WEIGHT:MAX_NO_EXPRESS_WEIGHT),
                    input.isExpress());
            result = false;
        }
        return result;
    }
}
