package pl.sda.s06_zaawansowane_programowanie.examples.e08;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Parcel {
    private int xLength;
    private int yLength;
    private int zLength;
    private float weight;
    private boolean isExpress;
}
