package pl.sda.s06_zaawansowane_programowanie.examples.e08;

public class Main {
    public static void main(String[] args) {
        System.out.println("1. ------------------");
        Parcel parcel = new Parcel(20, 20, 20, 20, true);
        ValidatorHelper validatorHelper = new ValidatorHelper();
        if (validatorHelper.validate(parcel)) {
            System.out.println("Parcel parameters are ok.");
        }
        System.out.println("2. ------------------");
        parcel = new Parcel(50, 50, 50, 10, true);
        if (validatorHelper.validate(parcel)) {
            System.out.println("Parcel parameters are ok.");
        }
    }
}
