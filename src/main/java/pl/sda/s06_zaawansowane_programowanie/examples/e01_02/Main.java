package pl.sda.s06_zaawansowane_programowanie.examples.e01_02;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<String> toSort = new ArrayList<>();
        toSort.add("Krzysztof");
        toSort.add("Michał");
        toSort.add("anna");
        toSort.add("Grażyna");
        toSort.add("Żaneta");
        toSort.add("Ślimak");
        toSort.add("Ącki");
        toSort.add(null);
        System.out.println("Original: " + toSort);
        System.out.println("By stream: " + sortStringList(toSort));
        System.out.println("Own comparator: " + sortStringListIgnoreCaseComparator(toSort));
    }

    private static List<String> sortStringList(List<String> toSort){
        return toSort.stream().sorted(Comparator.nullsFirst(String.CASE_INSENSITIVE_ORDER).reversed()).collect(Collectors.toList());
    }

    private static List<String> sortStringListIgnoreCaseComparator(List<String> toSort){
        toSort.sort(Comparator.nullsFirst(new MyOwnComparator()).reversed());
        return toSort;
    }

    private static class MyOwnComparator implements Comparator<String>{
        @Override
        public int compare(String o1, String o2) {
            return o1.compareToIgnoreCase(o2);
        }
    }
}
