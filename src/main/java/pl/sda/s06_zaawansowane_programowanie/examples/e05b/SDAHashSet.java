package pl.sda.s06_zaawansowane_programowanie.examples.e05b;

import lombok.AllArgsConstructor;
import lombok.ToString;

import java.util.*;

@AllArgsConstructor
@ToString
public class SDAHashSet extends HashSet<String> {
    private final Set<String> items = new HashSet<>();

    public boolean add(String item) {
        return items.add(item);
    }

    public void remove(String item) {
        items.remove(item);
    }

    public int size() {
        return items.size();
    }

    public boolean contains(String item) {
        return items.contains(item);
    }

    public void clear() {
        items.clear();
    }
}
