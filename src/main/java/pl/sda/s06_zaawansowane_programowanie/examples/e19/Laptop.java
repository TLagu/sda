package pl.sda.s06_zaawansowane_programowanie.examples.e19;

import lombok.Getter;
import lombok.Setter;
import pl.sda.s06_zaawansowane_programowanie.examples.e18.Computer;

import java.util.Objects;

@Getter
@Setter
public class Laptop extends Computer {
    private String battery;

    public Laptop(String processor, String ram, String gpu, String company, String model, String battery) {
        super(processor, ram, gpu, company, model);
        this.battery = battery;
    }

    @Override
    public String toString() {
        return "Laptop:" +
                "\n - processor: " + super.getProcessor() +
                "\n - ram: " + super.getRam() +
                "\n - gpu: " + super.getGpu() +
                "\n - company: " + super.getCompany() +
                "\n - model: " + super.getModel() +
                "\n - battery: " + battery;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Laptop laptop = (Laptop) o;
        return Objects.equals(battery, laptop.battery);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), battery);
    }
}
