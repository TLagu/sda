package pl.sda.s06_zaawansowane_programowanie.examples.e19;

public class Main {
    public static void main(String[] args) {
        Laptop laptop1 = new Laptop("Ryzen R7", "16GB", "GeForce 3080", "Nie kompany", "niezły", "battery");
        Laptop laptop2 = new Laptop("Ryzen R7", "16GB", "GeForce 3080", "Nie kompany", "niezły", "battery");
        Laptop laptop3 = new Laptop("Intel i7", "16GB", "GeForce 3080", "Nie kompany", "niezły", "battery");
        System.out.println(laptop1);
        System.out.println(laptop2);
        System.out.println(laptop3);
        System.out.println(laptop1.equals(laptop2));
        System.out.println(laptop1.equals(laptop3));
    }
}
