package pl.sda.s06_zaawansowane_programowanie.examples.e06;

import java.util.TreeMap;

public class Main {
    public static void main(String[] args) {
        TreeMap<String, String> map = new TreeMap<>();
        map.put("key 1", "value 1");
        map.put("key 5", "value 5");
        map.put("key 3", "value 3");
        map.put("key 2", "value 2");
        map.put("key 4", "value 4");
        showFirstAndLastEntry(map);
    }

    private static void showFirstAndLastEntry(TreeMap<String, String> map){
        System.out.println("First: " + map.firstEntry());
        System.out.println("Last: " + map.lastEntry());
    }
}
