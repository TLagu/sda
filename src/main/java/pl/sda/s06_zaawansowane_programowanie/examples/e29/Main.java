package pl.sda.s06_zaawansowane_programowanie.examples.e29;

import java.util.List;
import java.util.function.Predicate;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = List.of(2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30);
        Predicate<Integer> predicate = i -> (i % 10) == 0;
        System.out.println(new PartOf<Integer>().partOf(list, predicate) + " %");
    }
}
