package pl.sda.s06_zaawansowane_programowanie.examples.e29;

import java.util.List;
import java.util.function.Predicate;

public class PartOf<T> {
    public double partOf(List<T> array, Predicate<T> predicate){
        if (array == null || array.size() == 0) {
            throw new IllegalArgumentException("Array can not be empty.");
        }
        long count = array.stream()
                .filter(predicate)
                .count();
        return (double)  count / array.size() * 100;
    }

}
