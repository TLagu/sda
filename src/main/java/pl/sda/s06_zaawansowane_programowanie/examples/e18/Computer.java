package pl.sda.s06_zaawansowane_programowanie.examples.e18;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Computer {
    private String processor;
    private String ram;
    private String gpu;
    private String company;
    private String model;
}
