package pl.sda.s06_zaawansowane_programowanie.examples.e18;

public class Main {
    public static void main(String[] args) {
        Computer computer1 = new Computer("Ryzen R7", "16GB", "GeForce 3080", "Nie kompany", "niezły");
        Computer computer2 = new Computer("Ryzen R7", "16GB", "GeForce 3080", "Nie kompany", "niezły");
        Computer computer3 = new Computer("Intel i7", "16GB", "GeForce 3080", "Nie kompany", "niezły");
        System.out.println(computer1);
        System.out.println(computer2);
        System.out.println(computer3);
        System.out.println(computer1.equals(computer2));
        System.out.println(computer1.equals(computer3));
    }
}
