package pl.sda.s14_projekt_koncowy.server.utils;

public class OutputUtils {
    public static void throwExceptionIfTrue(boolean condition, String comment){
        if(condition) {
            throw new IllegalArgumentException(comment);
        }
    }

    public static Integer stringToInteger(String value){
        try {
            return Integer.valueOf(value);
        } catch (NumberFormatException e){
            return null;
        }
    }

    public static Boolean stringToBoolean(String value){
        try {
            return Boolean.valueOf(value);
        } catch (NumberFormatException e){
            return null;
        }
    }

}
