package pl.sda.s14_projekt_koncowy.server.categories;

public class CategoryReaderFromDB extends CategoryReader{

    public CategoryReaderFromDB() {
        readCategory();
    }

    @Override
    public void readCategory() {
        super.collectCategories();
    }
}
