package pl.sda.s14_projekt_koncowy.server.categories;

import lombok.Getter;
import lombok.ToString;

import java.io.IOException;
import java.util.List;

@Getter
@ToString
public class CategoryManager {
    private List<Category> categories;

    public CategoryManager() throws IOException {
        rereadCategories();
    }

    public void rereadCategories() throws IOException{
        CategoryReader categoryReader;
        switch(CategoryUtils.DATA_SOURCE){
            case DB:
                categoryReader = new CategoryReaderFromDB();
                break;
            case FILE:
                categoryReader = new CategoryReaderFromFile();
                break;
            default:
                throw new IllegalArgumentException("There is no such data source.");
        }
        categories = categoryReader.getCategories();
    }

    public void writeCategories() throws IOException{
        CategoryWriter categoryWriter;
        switch(CategoryUtils.DATA_SOURCE){
            case DB:
                categoryWriter = new CategoryWriterToDB();
                break;
            case FILE:
                categoryWriter = new CategoryWriterToFile();
                break;
            default:
                throw new IllegalArgumentException("There is no such data source.");
        }
        categoryWriter.save();
    }

}
