package pl.sda.s14_projekt_koncowy.server.categories;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@ToString
public class Category implements Comparable<Category> {
    private final Integer categoryID;
    @Setter(AccessLevel.PACKAGE)
    private String categoryName;
    private Boolean categoryStatus;
    private Integer parentID;
    @Setter(AccessLevel.PACKAGE)
    private List<Category> subcategories = new ArrayList<>();

    Category(Integer categoryID, String categoryName, Boolean categoryStatus){
        this.categoryID = categoryID;
        this.categoryName = categoryName;
        this.categoryStatus = categoryStatus;
    }

    Category(Integer categoryID, String categoryName, Boolean categoryStatus, Integer parentID){
        this.categoryID = categoryID;
        this.categoryName = categoryName;
        this.categoryStatus = categoryStatus;
        this.parentID = parentID;
    }

    void addSubcategory(Category subcategory){
        subcategory.setParentID(parentID);
        subcategories.add(subcategory);
    }

    private void setParentID(Integer parentID){
        this.parentID = parentID;
    }

    void deleteCategory(){
        setSubcategoryAsOpposite(false, false);
    }

    void restoreCategory(boolean allSubcategories){
        setSubcategoryAsOpposite(true, allSubcategories);
    }

    private void setCategoryAsOpposite(Boolean categoryStatus){
        if (!this.categoryStatus.equals(categoryStatus)){
            this.categoryStatus = categoryStatus;
        }
    }

    private void setSubcategoryAsOpposite(Boolean subcategoryStatus, boolean allSubcategories){
        if(subcategories.size() == 0){
            setCategoryAsOpposite(subcategoryStatus);
            return;
        }
        for (Category subcategory : subcategories) {
            if (subcategoryStatus){
                subcategory.restoreCategory(allSubcategories);
            } else {
                subcategory.deleteCategory();
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return Objects.equals(categoryID, ((Category) o).getCategoryID());
    }

    @Override
    public int hashCode() {
        return Objects.hash(categoryID);
    }

    @Override
    public int compareTo(Category o) {
        return (getCategoryID() - o.getCategoryID());
    }
}
