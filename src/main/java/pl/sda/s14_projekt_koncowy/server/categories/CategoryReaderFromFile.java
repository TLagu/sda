package pl.sda.s14_projekt_koncowy.server.categories;

import lombok.ToString;
import pl.sda.s14_projekt_koncowy.server.utils.OutputUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

@ToString
public class CategoryReaderFromFile extends CategoryReader{

    public CategoryReaderFromFile() throws IOException {
        readCategory();
    }

    private void getFileData() throws IOException {
        try (BufferedReader in = new BufferedReader(new FileReader(CategoryUtils.FILE_PATH))) {
            String line;
            while ((line = in.readLine()) != null) {
                if (line.isBlank()) {
                    continue;
                }
                Category category = parseCategory(line);
                OutputUtils.throwExceptionIfTrue(cachedCategories.containsKey(category.getCategoryID()),
                        "Such a category already exists: " + category.getCategoryID() + ".");
                cachedCategories.put(category.getCategoryID(), category);
                if (category.getParentID() == null || category.getParentID() <= 0) {
                    categories.add(new Category(category.getCategoryID(),
                            category.getCategoryName(),
                            category.getCategoryStatus()));
                }
            }
        }
    }

    private Category parseCategory(String line) {
        String[] category = line.split(";", 4);
        Integer categoryID = OutputUtils.stringToInteger(category[0]);
        String categoryName = category[1];
        Boolean categoryStatus = OutputUtils.stringToBoolean(category[2]);
        Integer parentID = OutputUtils.stringToInteger(category[3]);
        OutputUtils.throwExceptionIfTrue(category.length != 4,
                "There is not enough data on the line: " + Arrays.toString(category) + ".");
        OutputUtils.throwExceptionIfTrue((categoryID == null || categoryID.equals(parentID)),
                "The category cannot be its parent: " + parentID + ".");
        return new Category(categoryID, categoryName, categoryStatus, parentID);
    }

    @Override
    public void readCategory() throws IOException {
        getFileData();
        collectCategories();
    }
}
