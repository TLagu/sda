package pl.sda.s14_projekt_koncowy.server.categories;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.sda.s14_projekt_koncowy.server.utils.OutputUtils;

import java.io.IOException;
import java.util.*;

@ToString
public abstract class CategoryReader {
    @Getter @Setter
    protected List<Category> categories = new ArrayList<>();
    @Getter @Setter
    protected Map<Integer, Category> cachedCategories = new HashMap<>();

    private void throwIfCachedCategoryIsEmpty(){
        OutputUtils.throwExceptionIfTrue(cachedCategories.size() == 0, "No valid category found.");
    }

    private void throwIfDestinationCategoryIsEmpty(String comment){
        OutputUtils.throwExceptionIfTrue(categories.size() == 0, comment);
    }

    protected void collectCategories() {
        throwIfCachedCategoryIsEmpty();
        throwIfDestinationCategoryIsEmpty("There are no major categories.");
        removeInvalidCategories();
        createCategories();
        throwIfDestinationCategoryIsEmpty("Destination data cannot be empty");
    }

    private void createCategories(){
        // remove used categories
        for(Category category : categories){
            cachedCategories.remove(category.getCategoryID());
        }
        Category category;
        for(int i = 0; i < categories.size(); i++){
            category = categories.get(i);
            category.setSubcategories(addSubcategories(category));
            categories.set(i, category);
        }
    }

    private void removeInvalidCategories() {
        for(Map.Entry<Integer, Category> category : cachedCategories.entrySet()){
            Integer key = category.getValue().getParentID();
            if(!cachedCategories.containsKey(key)){
                cachedCategories.remove(key);
                System.out.println("There is no such overarching category.");
            }
        }
    }

    protected List<Category> addSubcategories(Category category) {
        List<Category> result = new ArrayList<>();
        for(Map.Entry<Integer, Category> cat : cachedCategories.entrySet()) {
            Integer key = cat.getKey();
            Category value = cat.getValue();
            if (category.getCategoryID().equals(value.getParentID())) {
                result.add(value);
                cachedCategories.remove(key);
                value.addSubcategory(value);
            }
        }
        return result;
    }

    public abstract void readCategory() throws IOException;
}
