package pl.sda.s14_projekt_koncowy.server;

import pl.sda.s14_projekt_koncowy.server.categories.CategoryManager;

import java.io.IOException;

public class CategoryDemonstration {
    public static void main(String[] args) throws IOException {
        CategoryManager categoryManager = new CategoryManager();
        System.out.println(categoryManager.getCategories());
    }
}
