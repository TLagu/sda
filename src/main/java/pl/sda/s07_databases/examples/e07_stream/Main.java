package pl.sda.s07_databases.examples.e07_stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    private static List<String> countries = new ArrayList<>();

    public static void main(String[] args) {
        countries.add("Polska");
        countries.add("Włochy");
        countries.add("Hiszpania");
        countries.add("Grecja");
        countries.add("Anglia");
        countries.add("Norwegia");
        countries.add("Niemcy");

        printAllClassic();
        System.out.println();
        printAllFunctional();
        System.out.println();
        printAllClassicFirstLetter("N");
        System.out.println();
        printAllFunctionalFirstLetter("N");
    }

    // programowanie klasyczne
    public static void printAllClassic() {
        for (String country : countries) {
            System.out.println(country);
        }
    }

    // programowanie funkcyjne
    public static void printAllFunctional() {
        countries.forEach(System.out::println);
    }

    // programowanie klasyczne
    public static void printAllClassicFirstLetter(String letter) {
        for (String country : countries) {
            if (country.substring(0, 1).equals(letter)) {
                System.out.println(country);
            }
        }
    }

    // programowanie funkcyjne
    public static void printAllFunctionalFirstLetter(String letter) {
        countries.stream()
                .filter(country -> country.substring(0, 1).equals(letter))
                .forEach(System.out::println);
    }

    public static List<Country> mapToCountryClassClassical(List<String> countries) {
        List<Country> result = new ArrayList<>();
        for (String country : countries) {
            result.add(new Country(country));
        }
        return result;
    }

    public static List<Country> mapTopCountryClassFunctional(List<String> countries) {
        return countries.stream()
                .map(Country::new)
                .collect(Collectors.toList());
    }

    private static class Country {
        private final String name;

        public Country(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "name -> " + name + "\n";
        }
    }
}
