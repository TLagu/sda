package pl.sda.s07_databases.examples.e08_stream;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class Main {
    private static List<Season> seasons = new ArrayList<>();

    public static void main(String[] args) {
        createStructures();
        System.out.println(seasons);
        System.out.println("Lista wszystkich epizodów.");
        listOfAllEpisodes();
        System.out.println("Lista wszystkich filmów.");
        listOfAllMovies();
        System.out.println("Lista wszystkich nazw sezonów");
        listOfAllSeasonNames();
        System.out.println("Lista wszystkich nuemrów sezonów.");
        listOfAllSeasonNumbers();
        System.out.println("Lista wszystkich nazw epizodów.");
        listOfAllEpisodeNames();
        System.out.println("Lista wszystkich numerów epizodów.");
        listOfAllEpisodeNumbers();
        System.out.println("lista wszystkich nazw wideo.");
        listOfAllVideoNames();
        System.out.println("Lista wszystkich adresów url dla każdego wideo");
        listOfAllVideoUrls();
        System.out.println("Tylko epzody z parzystych sezonów.");
        listOfAllEpisodesFromEvenSeasons();
        System.out.println("Tylko video z parzystych sezonów.");
        listOfAllMoviesFromEvenSeasons();
        System.out.println("Tylko video z parzystych epizodów i sezonów.");
        listOfAllMoviesFromEvenSeasonsAndEpisodes();
        System.out.println("Tylko video typu CLIP z parzystych epizodów i nieparzystych sezonów.");
        listOfAllMoviesFromOddSeasonsAndEvenEpisodesClipOnly();
        System.out.println("Tylko video typu PREVIEW z nieparzystych epizodów i parzystych sezonów.");
        listOfAllMoviesFromEvenSeasonsAndOddEpisodesPreviewOnly();
    }

    private static void createStructures() {
        Random random = new Random();
        int n = 1 + random.nextInt(5);
        for (int i = 1; i <= n; i++) {
            List<Episode> episodes = new ArrayList<>();
            int m = 1 + random.nextInt(3);
            for (int j = 1; j <= m; j++) {
                episodes.add(new Episode("Episode " + j, j, List.of(new Video("Title " + j, "url " + j, VideoType.randomValue()))));
            }
            seasons.add(new Season("Season " + i, i, episodes));
        }
    }

    private static void listOfAllEpisodes() {
        seasons.stream()
                .map(Season::getEpisodes)
                .flatMap(Collection::stream)
                .forEach(System.out::println);
    }

    private static void listOfAllMovies() {
        seasons.stream()
                .map(Season::getEpisodes)
                .flatMap(Collection::stream)
                .map(Episode::getVideos)
                .flatMap(Collection::stream)
                .forEach(System.out::println);
    }

    private static void listOfAllSeasonNames() {
        seasons.stream()
                .map(Season::getSeasonName)
                .forEach(System.out::println);
    }

    private static void listOfAllSeasonNumbers() {
        seasons.stream()
                .map(Season::getSeasonNumber)
                .forEach(System.out::println);
    }

    private static void listOfAllEpisodeNames() {
        seasons.stream()
                .map(Season::getEpisodes)
                .flatMap(Collection::stream)
                .map(Episode::getEpisodeName)
                .forEach(System.out::println);
    }

    private static void listOfAllEpisodeNumbers() {
        seasons.stream()
                .map(Season::getEpisodes)
                .flatMap(Collection::stream)
                .map(Episode::getEpisodeNumber)
                .forEach(System.out::println);
    }

    private static void listOfAllVideoNames() {
        seasons.stream()
                .map(Season::getEpisodes)
                .flatMap(Collection::stream)
                .map(Episode::getVideos)
                .flatMap(Collection::stream)
                .map(Video::getTitle)
                .forEach(System.out::println);
    }

    private static void listOfAllVideoUrls() {
        seasons.stream()
                .map(Season::getEpisodes)
                .flatMap(Collection::stream)
                .map(Episode::getVideos)
                .flatMap(Collection::stream)
                .map(Video::getUrl)
                .forEach(System.out::println);
    }

    private static void listOfAllEpisodesFromEvenSeasons() {
        seasons.stream()
                .filter(s -> s.getSeasonNumber() % 2 == 0)
                .map(Season::getEpisodes)
                .flatMap(Collection::stream)
                .forEach(System.out::println);
    }

    private static void listOfAllMoviesFromEvenSeasons() {
        seasons.stream()
                .filter(s -> s.getSeasonNumber() % 2 == 0)
                .map(Season::getEpisodes)
                .flatMap(Collection::stream)
                .map(Episode::getVideos)
                .flatMap(Collection::stream)
                .forEach(System.out::println);
    }

    private static void listOfAllMoviesFromEvenSeasonsAndEpisodes() {
        seasons.stream()
                .filter(s -> s.getSeasonNumber() % 2 == 0)
                .map(Season::getEpisodes)
                .flatMap(Collection::stream)
                .filter(e -> e.getEpisodeNumber() % 2 == 0)
                .map(Episode::getVideos)
                .flatMap(Collection::stream)
                .forEach(System.out::println);
    }

    private static void listOfAllMoviesFromOddSeasonsAndEvenEpisodesClipOnly() {
        seasons.stream()
                .filter(s -> s.getSeasonNumber() % 2 != 0)
                .map(Season::getEpisodes)
                .flatMap(Collection::stream)
                .filter(e -> e.getEpisodeNumber() % 2 == 0)
                .map(Episode::getVideos)
                .flatMap(Collection::stream)
                .filter(v -> v.getVideoType() == VideoType.CLIP)
                .forEach(System.out::println);
    }

    private static void listOfAllMoviesFromEvenSeasonsAndOddEpisodesPreviewOnly() {
        seasons.stream()
                .filter(s -> s.getSeasonNumber() % 2 == 0)
                .map(Season::getEpisodes)
                .flatMap(Collection::stream)
                .filter(e -> e.getEpisodeNumber() % 2 != 0)
                .map(Episode::getVideos)
                .flatMap(Collection::stream)
                .filter(v -> v.getVideoType() == VideoType.PREVIEW)
                .forEach(System.out::println);
    }

}
