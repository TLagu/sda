package pl.sda.s07_databases.examples.e08_stream;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class Video {
    private String title;
    private String url;
    private VideoType videoType;

    public Video(String title, String url, VideoType videoType) {
        this.title = title;
        this.url = url;
        this.videoType = videoType;
    }
}
