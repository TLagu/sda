package pl.sda.s07_databases.examples.e08_stream;

import java.util.List;
import java.util.Random;

public enum VideoType {
    CLIP, PREVIEW, EPISODE;
    private static final List<VideoType> VALUES = List.of(values());
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    public static VideoType randomValue()  {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }
}
