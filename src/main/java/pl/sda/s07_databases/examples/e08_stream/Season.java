package pl.sda.s07_databases.examples.e08_stream;

import lombok.Getter;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
public class Season {
    private String seasonName;
    private int seasonNumber;
    private List<Episode> episodes;

    public Season(String seasonName, int seasonNumber, List<Episode> episodes) {
        this.seasonName = seasonName;
        this.seasonNumber = seasonNumber;
        this.episodes = episodes;
    }
}
