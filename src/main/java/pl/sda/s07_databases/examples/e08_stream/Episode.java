package pl.sda.s07_databases.examples.e08_stream;

import lombok.Getter;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
public class Episode {
    private String episodeName;
    private int episodeNumber;
    private List<Video> videos;

    public Episode(String episodeName, int episodeNumber, List<Video> videos) {
        this.episodeName = episodeName;
        this.episodeNumber = episodeNumber;
        this.videos = videos;
    }
}
