package pl.sda.s07_databases.examples.e04_nio;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Main {
    private final static String PATH = "C:\\Users\\Lagu\\IdeaProjects\\sda\\src\\main\\resources\\pl\\sda\\s07_databases\\examples\\e04_nio\\";

    public static void main(String[] args) {
        Path p1 = Paths.get(PATH + "countries.txt");
        System.out.println(p1.getFileName());
        System.out.println(p1.getRoot());
        System.out.println(p1.getParent());
        System.out.println(p1.toAbsolutePath().getParent());
        System.out.println(p1.toAbsolutePath().getRoot());
        System.out.println("Istnieje? " + Files.exists(p1));
        System.out.println("Odczytać? " + Files.isReadable(p1));
        System.out.println("Zapisać? " + Files.isWritable(p1));
        System.out.println("Wykonać? " + Files.isExecutable(p1));
        System.out.println("Katalog? " + Files.isDirectory(p1));
        System.out.println("Plik? " + Files.isRegularFile(p1));

        Path p2 = Paths.get(PATH + "test");
        if (!Files.exists(p2)) {
            try {
                Files.createDirectory(p2);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        List<String> countries = null;
        try {
            countries = Files.readAllLines(p1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (String country : countries) {
            System.out.println(country);
        }

        // ominięcie przepełnienia pamięci
        List<String> countriesFunctionalRead = new ArrayList<>();
        try {
            Files.lines(p1).forEach(countriesFunctionalRead::add);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (String country : countriesFunctionalRead) {
            System.out.println(country);
        }

        Path p3 = Paths.get(PATH + "countries-copy.txt");
        try (BufferedWriter bf = Files.newBufferedWriter(p3)) {
            for(String country : countriesFunctionalRead) {
                bf.write(country);
                bf.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
