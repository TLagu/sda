package pl.sda.s07_databases.examples.e09_money;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Main {
    public static void main(String[] args) {
        BigDecimal priceAsDecimal = new BigDecimal("0.01").setScale(4);
        System.out.println(priceAsDecimal);
        System.out.println(BigDecimal.valueOf(1).divide(BigDecimal.valueOf(3), RoundingMode.HALF_UP));
    }
}
