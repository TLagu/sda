package pl.sda.s07_databases.examples.e06_functional;

import pl.sda.s01_podstawy.s08_classes.task02_grocery_shop.Supplier;

import java.util.Optional;

public class FunctionalInterfaceMain {
    public static void main(String[] args) {
        // klasa anonimowa
        Voiceable voiceable = new Voiceable() {
            @Override
            public void giveVoice() {
                System.out.println("Giving voice.");
            }
        };
        voiceable.giveVoice();

        Voiceable voiceableAsLambda = () -> System.out.println("Giving voice as lambda.");
        voiceableAsLambda.giveVoice();

        Optional<String> oName1 = Optional.ofNullable(null);
        oName1.ifPresent(System.out::println);
        Optional<String> oName2 = Optional.ofNullable("Bartosz");
        oName2.ifPresent(System.out::println);
        oName2
                .filter(n -> n.length() > 5)
                .map(Student::new)
                .ifPresent(System.out::println);
        oName2.ifPresent(System.out::println);
    }

    private static class Student {
        private final String name;

        public Student(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "name -> " + name;
        }
    }
}
