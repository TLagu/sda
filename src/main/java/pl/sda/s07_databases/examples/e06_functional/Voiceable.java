package pl.sda.s07_databases.examples.e06_functional;

@FunctionalInterface
public interface Voiceable {
    void giveVoice();
    default void silent() {
        System.out.println("Now quiet");
    }
}
