package pl.sda.s07_databases.examples.e03_io;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// Przygotuj plik „villages.txt" z nazwami wiosek (każda nazwa w oddzielnej linii).
//        ❖ Plik powinien zostać umieszczony w głównym katalogu projektu.
//        ❖ Napisz metodę main, która wyświetli wszystkie nazwy z pliku na konsoli.
public class Main {
    private final static String PATH = "C:\\Users\\Lagu\\IdeaProjects\\sda\\src\\main\\resources\\pl\\sda\\s07_databases\\examples\\e03_io\\";

    public static void main(String[] args) {
        List<String> villages = new ArrayList<>();
        File file = new File(PATH + "villages.txt");
        try (Scanner scanner = new Scanner(file)) {
            while(scanner.hasNextLine()) {
                villages.add(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        for (String country : villages) {
            System.out.println(country);
        }
    }
}
