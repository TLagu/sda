package pl.sda.s07_databases.examples.e01_io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static List<String> countries = new ArrayList<>();
    private final static String PATH = "C:\\Users\\Lagu\\IdeaProjects\\sda\\src\\main\\resources\\pl\\sda\\s07_databases\\examples\\e01_io\\";

    public static void main(String[] args) {
        countries.add("Polska");
        countries.add("Włochy");
        countries.add("Hiszpania");
        countries.add("Grecja");

        try {
            PrintWriter pw = new PrintWriter(PATH + "countries.txt");
            for (String country : countries) {
                pw.println(country);
            }
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        List<String> countriesFromFile = new ArrayList<>();
        try {
            File file = new File(PATH + "countries.txt");
            Scanner scanner = new Scanner(file);
            while(scanner.hasNextLine()) {
                countriesFromFile.add(scanner.nextLine());
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        for (String country : countriesFromFile) {
            System.out.println(country);
        }
    }
}
