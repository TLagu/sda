package pl.sda.s07_databases.examples.e05_nio;

import com.opencsv.CSVReader;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class LeagueFileService {
    private final static String PATH = "C:\\Users\\Lagu\\IdeaProjects\\sda\\src\\main\\resources\\pl\\sda\\s07_databases\\examples\\e05_nio\\league.txt";

    public Object save(League league) {
        Path file = Paths.get(PATH);
        try (BufferedWriter bf = Files.newBufferedWriter(file)) {
            for(Game game : league.getGames()) {
                bf.write(game.getTeamA());
                bf.write(";");
                bf.write(game.getTeamB());
                bf.write(";");
                bf.write(String.valueOf(game.getScoreA()));
                bf.write(";");
                bf.write(String.valueOf(game.getScoreB()));
                bf.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public League loadFromFile() {
        Path file = Paths.get(PATH);
        List<String> lines = null;
        try {
            lines = Files.readAllLines(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        League league = new League();
        String[] records;
        if (lines == null) throw new AssertionError();
        for (String line : lines) {
            records = line.split(";");
            league.addGame(new Game(records[0], records[1], Integer.parseInt(records[2]), Integer.parseInt(records[3])));
        }
        return league;
    }
}
