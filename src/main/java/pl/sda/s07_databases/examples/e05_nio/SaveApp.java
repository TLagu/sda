package pl.sda.s07_databases.examples.e05_nio;

public class SaveApp {
    public void main() {
        League league = new League();
        league.addGame(new Game("Team A", "Team B", 2, 1));
        league.addGame(new Game("Team A", "Team B", 2, 2));
        league.addGame(new Game("Team A", "Team C", 1, 1));
        league.addGame(new Game("Team A", "Team C", 1, 2));
        league.addGame(new Game("Team B", "Team C", 2, 2));
        league.addGame(new Game("Team B", "Team C", 2, 1));
        LeagueFileService lfs = new LeagueFileService();
        lfs.save(league);
    }
}
