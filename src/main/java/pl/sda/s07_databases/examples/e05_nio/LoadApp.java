package pl.sda.s07_databases.examples.e05_nio;

public class LoadApp {
    public League main () {
        LeagueFileService lfs = new LeagueFileService();
        return lfs.loadFromFile();
    }
}
