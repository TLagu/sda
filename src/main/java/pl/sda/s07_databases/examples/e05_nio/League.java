package pl.sda.s07_databases.examples.e05_nio;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class League {
    private List<Game> games = new ArrayList<>();

    public void addGame(Game game) {
        games.add(game);
    }
}
