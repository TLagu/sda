package pl.sda.s07_databases.examples.e05_nio;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class Game {
    private String teamA;
    private String teamB;
    private Integer scoreA;
    private Integer scoreB;
}
