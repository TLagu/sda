package pl.sda.s07_databases.examples.e02_io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// W metodzie main stwórz listę typu String i uzupełnij ją nazwami stolic europejskich.
// ❖ Na podstawie listy, wygeneruj plik „capitols.txt", który będzie  zawierał spis stolic (każda w oddzielnej linii).
// ❖ Zweryfikuj czy plik został poprawnie utworzony, domyślnie bez podania dodatkowej ścieżki powinien utworzyć się w projekcie.

public class Main {
    public static List<String> capitols = new ArrayList<>();
    private final static String PATH = "C:\\Users\\Lagu\\IdeaProjects\\sda\\src\\main\\resources\\pl\\sda\\s07_databases\\examples\\e02_io\\";

    public static void main(String[] args) {
        capitols.add("Warszawa");
        capitols.add("Berlin");
        capitols.add("Madryt");
        capitols.add("Paryż");

        try (PrintWriter pw = new PrintWriter(PATH + "capitols.txt")) {
            for (String country : capitols) {
                pw.println(country);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        List<String> countriesFromFile = new ArrayList<>();
        File file = new File(PATH + "capitols.txt");
        try (Scanner scanner = new Scanner(file)) {
            while(scanner.hasNextLine()) {
                countriesFromFile.add(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        for (String country : countriesFromFile) {
            System.out.println(country);
        }
    }
}
