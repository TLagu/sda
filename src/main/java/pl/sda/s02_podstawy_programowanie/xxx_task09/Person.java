package pl.sda.s02_podstawy_programowanie.xxx_task09;

import java.time.LocalDate;

public class Person {
    private String name;
    private String surname;
    private String pesel;
    private LocalDate dateOfBirth;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void validateAndSetName(String name) {
        this.name = PersonHelper.setValidStringOrNull(PersonHelper.NAME_PATTERN, name, PersonHelper.FieldName.NAME.label);
    }

    public String getSurname() {
        return this.surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void validateAndSetSurname(String surname) {
        this.surname = PersonHelper.setValidStringOrNull(PersonHelper.SURNAME_PATTERN, surname, PersonHelper.FieldName.SURNAME.label);
    }

    public String getPesel() {
        return this.pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public void validateAndSetPesel(String pesel) {
        this.pesel = PersonHelper.setValidPeselOrNull(pesel);
    }

    public LocalDate getDateOfBirth() {
        return this.dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setDateOfBirthFromPesel() {
        this.dateOfBirth = PersonHelper.getDateFromPesel(this.pesel);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n   Person info:");
        sb.append(String.format("\n   - Name: '%s'", this.name));
        sb.append(String.format("\n   - Surname: '%s'", this.surname));
        sb.append(String.format("\n   - PESEL: '%s'", this.pesel));
        sb.append(String.format("\n   - Date of birth: '%s'", this.dateOfBirth));
        return sb.toString();
    }
}
