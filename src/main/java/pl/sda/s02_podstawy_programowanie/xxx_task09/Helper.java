package pl.sda.s02_podstawy_programowanie.xxx_task09;

import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Helper {

    public static boolean isValidString(String stringPattern, String stringValue, String fieldName) {
        Pattern pattern = Pattern.compile(stringPattern);
        Matcher matcher = pattern.matcher(stringValue);
        if (!matcher.matches()) {
            Logger.log(String.format("Incorrect %s value (pattern).", fieldName));
            return false;
        }
        return true;
    }

    public static String setValidStringOrNull(String stringPattern, String stringValue, String fieldName) {
        Pattern pattern = Pattern.compile(stringPattern);
        Matcher matcher = pattern.matcher(stringValue);
        if (!matcher.matches()) {
            Logger.log(String.format("Incorrect %s value (pattern).", fieldName));
            return null;
        }
        return stringValue;
    }

    public static boolean isValidList(int size, int correctSize, String listName) {
        if (size != correctSize) {
            Logger.log(String.format("Wrong size of the '%s' list.", listName));
            return false;
        }
        return true;
    }

    public static LocalDate setValidateDate(int year, int month, int day, String fieldName) {
        try {
            return LocalDate.of(year, month, day);
        } catch (Exception e) {
            Logger.log(String.format("Invalid date format in the %s field.", fieldName));
            return null;
        }
    }
}
