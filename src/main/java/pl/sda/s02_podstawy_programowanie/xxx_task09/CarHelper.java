package pl.sda.s02_podstawy_programowanie.xxx_task09;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class CarHelper extends Helper {
    public final static String BRAND_PATTERN = "[A-Z][a-z]{2,29}([- ][A-Z][a-z]{2,29}){0,1}";
    public final static String MODEL_PATTERN = "[A-Za-z0-9 -]{2,29}";
    public final static String CAPACITY_PATTERN = "[1-9][0-9]{2,3}(( cm(2){0,1}){0,1})";

    public static LocalDate setValidDateFromStringOrNull(String stringValue, String fieldName) {
        try {
            return LocalDate.of(Integer.parseInt(stringValue), 1, 1);
        } catch (Exception e) {
            Logger.log(String.format("Incorrect %s value (LocalDate).", fieldName));
            return null;
        }
    }

    public static Type setValidTypeOrNull(String stringValue, String fieldName) {
        return Type.valueOfLabel(stringValue);
    }

    public enum Type {
        SUV("SUV"),
        COUPE("Coupe"),
        DUAL_COWL("Dual cowl"),
        FASTBACK("Fastback"),
        HATCHBACK("Hatchback"),
        CABRIOLET("Cabriolet"),
        COMBI("Combi"),
        LIFTBACK("Liftback"),
        LIMOUSINE("Limousine"),
        MICROVAN("Microvan"),
        MINIVAN("Minivan"),
        PICK_UP("Pick-up"),
        ROADSTER("Roadster"),
        SEDAN("Sedan"),
        TARGA("Targa"),
        VAN("Van");

        private static final Map<String, Type> BY_LABEL = new HashMap<>();

        static {
            for (Type g : values()) {
                BY_LABEL.put(g.label, g);
            }
        }

        public final String label;

        Type(String label) {
            this.label = label;
        }

        public static Type valueOfLabel(String label) {
            return BY_LABEL.get(label);
        }

        @Override
        public String toString() {
            return this.label;
        }
    }

    public enum FieldName {
        BRAND("Brand"),
        MODEL("Model"),
        PROD("Date of production"),
        ENGINE("Engine capacity"),
        TYPE("Type"),
        OWNER("Owner");

        private static final Map<String, FieldName> BY_LABEL = new HashMap<>();

        static {
            for (FieldName g : values()) {
                BY_LABEL.put(g.label, g);
            }
        }

        public final String label;

        FieldName(String label) {
            this.label = label;
        }

        public static FieldName valueOfLabel2(String label) {
            return BY_LABEL.get(label);
        }

        @Override
        public String toString() {
            return this.label;
        }
    }
}
