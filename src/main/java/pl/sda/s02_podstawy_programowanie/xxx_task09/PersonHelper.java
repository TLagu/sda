package pl.sda.s02_podstawy_programowanie.xxx_task09;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PersonHelper extends Helper {
    public final static String NAME_PATTERN = "[A-ZĄĆĘŁŃÓŚŹŻ][a-ząćęłńóśźż]{2,29}";
    public static final String SURNAME_PATTERN = "([a-z]{2,3} ){0,1}[A-ZĄĆĘŁŃÓŚŹŻ][a-ząćęłńóśźż]{2,29}(-[A-ZĄĆĘŁŃÓŚŹŻ][a-ząćęłńóśźż]{2,29}){0,1}";
    public static final String PESEL_PATTERN = "\\d{11}";

    public static String setValidPeselOrNull(String pesel) {
        if (!isValidString(PESEL_PATTERN, pesel, FieldName.PESEL.label)) {
            Logger.log("The 'PESEL' number should contain only numbers.");
            return null;
        }
        if (!isPeselCheckSumOk(pesel)) {
            Logger.log("The check sum in the 'PESEL' number does not match.");
            return null;
        }
        if (getDateFromPesel(pesel) == null) {
            return null;
        }
        return pesel;
    }

    public static boolean isPeselCheckSumOk(String pesel) {
        List<Integer> weightArray = Arrays.asList(1, 3, 7, 9, 1, 3, 7, 9, 1, 3);
        int sum = 0;
        for (int i = 0; i < weightArray.size(); i++) {
            sum += weightArray.get(i) * Character.getNumericValue(pesel.charAt(i));
        }
        int checksum = 10 - (sum % 10);
        checksum = (checksum == 10) ? 0 : checksum;
        return checksum == Character.getNumericValue(pesel.charAt(10));
    }

    public static LocalDate getDateFromPesel(String pesel) {
        if (pesel == null) {
            Logger.log("The 'PESEL' can not be null.");
            return null;
        }
        String monthString = pesel.substring(2, 4);
        String dayString = pesel.substring(4, 6);
        String monthType = monthString.substring(0, 1);
        int year = Integer.parseInt(pesel.substring(0, 2));
        int month = Integer.parseInt(monthString);
        int day = Integer.parseInt(dayString);
        switch (monthType) {
            case "9":
            case "8":
                year += 1800;
                month -= 80;
                break;
            case "7":
            case "6":
                year += 2200;
                month -= 60;
                break;
            case "5":
            case "4":
                year += 2100;
                month -= 40;
                break;
            case "3":
            case "2":
                year += 2000;
                month -= 20;
                break;
            case "1":
            case "0":
                year += 1900;
                break;
        }
        return setValidateDate(year, month, day, FieldName.PESEL.label);
    }

    public enum FieldName {
        NAME("Name"),
        SURNAME("Surname"),
        PESEL("PESEL"),
        BIRTHDAY("Birthday");

        private static final Map<String, FieldName> BY_LABEL = new HashMap<>();

        static {
            for (FieldName g : values()) {
                BY_LABEL.put(g.label, g);
            }
        }

        public final String label;

        FieldName(String label) {
            this.label = label;
        }

        public static FieldName valueOfLabel2(String label) {
            return BY_LABEL.get(label);
        }

        @Override
        public String toString() {
            return this.label;
        }
    }
}
