package pl.sda.s02_podstawy_programowanie.xxx_task09;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataReader {

    public Car parseString(String data) {
        List<String> classArray = Arrays.asList(data.split("-"));
        if (!Helper.isValidList(classArray.size(), 2, "Main array")) {
            return null;
        }
        List<String> carArray = this.extractValuesFromString(classArray.get(0));
        List<String> personArray = this.extractValuesFromString(classArray.get(1));
        if (!Helper.isValidList(carArray.size(), 5, "Car array")) {
            return null;
        }
        if (!Helper.isValidList(personArray.size(), 3, "Person array")) {
            return null;
        }
        Person person = new Person();
        person.validateAndSetName(personArray.get(0));
        person.validateAndSetSurname(personArray.get(1));
        person.validateAndSetPesel(personArray.get(2));
        person.setDateOfBirthFromPesel();

        Car car = new Car();
        car.validateAndSetBrand(carArray.get(0));
        car.validateAndSetModel(carArray.get(1));
        car.validateAndSetYearOfProduction(carArray.get(2));
        car.validateAndSetEngineCapacity(carArray.get(3));
        car.validateAndSetType(carArray.get(4));
        car.setOwner(person);
        return car;
    }

    private List<String> extractValuesFromString(String string) {
        Pattern pattern = Pattern.compile("\\[(.*?)\\]");
        Matcher matcher = pattern.matcher(string);
        List<String> arrayString = new ArrayList<>();
        while (matcher.find()) {
            arrayString.add(matcher.group(1));
        }
        return arrayString;
    }
}
