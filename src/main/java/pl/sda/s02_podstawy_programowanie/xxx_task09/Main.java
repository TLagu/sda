package pl.sda.s02_podstawy_programowanie.xxx_task09;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String string = "[Volkswagen][Passat][2007][1968 cm2][Combi]-[Jan][Kowalski][99010178148]";
        Logger.log(string);
        DataReader dr = new DataReader();
        Car car = dr.parseString(string);
        System.out.println(car);
    }

    private static String getString(String description) {
        System.out.print(description);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
