package pl.sda.s02_podstawy_programowanie.xxx_task09;

import java.time.LocalDate;

public class Car {
    private String brand;
    private String model;
    private LocalDate yearOfProduction;
    private String engineCapacity;
    private CarHelper.Type type;
    private Person owner;

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void validateAndSetBrand(String brand) {
        this.brand = CarHelper.setValidStringOrNull(CarHelper.BRAND_PATTERN, brand, CarHelper.FieldName.BRAND.label);
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void validateAndSetModel(String model) {
        this.model = CarHelper.setValidStringOrNull(CarHelper.MODEL_PATTERN, model, CarHelper.FieldName.MODEL.label);
    }

    public void setYearOfProduction(LocalDate yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }

    public void validateAndSetYearOfProduction(String yearOfProduction) {
        this.yearOfProduction = CarHelper.setValidDateFromStringOrNull(yearOfProduction, CarHelper.FieldName.PROD.label);
    }

    public void setEngineCapacity(String engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public void validateAndSetEngineCapacity(String engineCapacity) {
        this.engineCapacity = CarHelper.setValidStringOrNull(CarHelper.CAPACITY_PATTERN, engineCapacity, CarHelper.FieldName.ENGINE.label);
    }

    public void setType(CarHelper.Type type) {
        this.type = type;
    }

    public void validateAndSetType(String type) {
        this.type = CarHelper.setValidTypeOrNull(type, CarHelper.FieldName.TYPE.label);
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Car info:");
        sb.append(String.format("\n- Brand: '%s'", this.brand));
        sb.append(String.format("\n- Model: '%s'", this.model));
        sb.append(String.format("\n- Date of production: '%s'", this.yearOfProduction));
        sb.append(String.format("\n- Engine capacity: '%s'", this.engineCapacity));
        sb.append(String.format("\n- Type: '%s'", this.type));
        sb.append(String.format("\n- Owner:%s", this.owner));
        return sb.toString();
    }
}
