package pl.sda.s02_podstawy_programowanie.task15;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        calculate();
    }

    private static int getValueFromUser() {
        System.out.print("Enter the number: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static void calculate() {
        int[] intArray = new int[10];
        for (int i = 0; i < 10; i++) {
            intArray[i] = getValueFromUser();
        }
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < i; j++) {
                if (intArray[i] == intArray[j]) {
                    System.out.println("The number " + intArray[i] + " is doubled");
                    break;
                }
            }
        }
    }

}
