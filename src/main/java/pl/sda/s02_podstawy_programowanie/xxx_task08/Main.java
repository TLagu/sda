package pl.sda.s02_podstawy_programowanie.xxx_task08;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    private final static int numberOfArrays = 3;
    private final static int randomRange = 20;
    private static int[][] arrays = new int[numberOfArrays][];

    public static void main(String[] args) {
        for (int i = 0; i < numberOfArrays; i++) {
            generateArray(i, getInteger("Enter the size of the array (" + i + "): "));
        }
        showArrays();
        showArray("Result: Array (concat) {", concatArray());
        showArray("Result: Array (unique) {", showConcatUniqueArray());
    }

    private static int getInteger(String description) {
        System.out.print(description);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static void generateArray(int index, int size) {
        arrays[index] = new int[size];
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            arrays[index][i] = random.nextInt(randomRange);
        }
    }

    private static void showArrays() {
        for (int i = 0; i < numberOfArrays; i++) {
            showArray("Result: Array " + i + " {", arrays[i]);
        }
    }

    private static void showArray(String description, int[] array) {
        System.out.print(description);
        String separator = "";
        for (int i : array) {
            System.out.print(separator + i);
            if (separator.equals("")) {
                separator = ", ";
            }
        }
        System.out.println("}");
    }

    private static int[] concatArray() {
        int sizeOfAll = 0;
        for (int i = 0; i < numberOfArrays; i++) {
            sizeOfAll += arrays[i].length;
        }
        int[] concatArray = new int[sizeOfAll];
        int shift = 0;
        for (int i = 0; i < numberOfArrays; i++) {
            for (int j = 0; j < arrays[i].length; j++) {
                concatArray[j + shift] = arrays[i][j];
            }
            shift += arrays[i].length;
        }
        return concatArray;
    }

    private static int[] showConcatUniqueArray() {
        int[] concatArray = concatArray();
        int[] toDeleteArray = new int[concatArray.length];
        int toDelete = 0;
        for (int i = 1; i < concatArray.length; i++) {
            for (int j = 0; j < i; j++) {
                if (concatArray[i] == concatArray[j]) {
                    toDeleteArray[i] = 1;
                    toDelete++;
                }
            }
        }
        int[] uniqueArray = new int[concatArray.length - toDelete + 1];
        int index = 0;
        for (int i = 0; i < toDeleteArray.length; i++) {
            if (toDeleteArray[i] == 0) {
                uniqueArray[index++] = concatArray[i];
            }
        }
        return uniqueArray;
    }

}
