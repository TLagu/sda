package pl.sda.s02_podstawy_programowanie.xxx_task05;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String userString = getString("Enter string: ");
        int length = userString.length();
        char[] charArray = new char[length];
        calculateRecurrence(userString, charArray, length, length - 1, 0);
    }

    private static String getString(String description) {
        System.out.print(description);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    private static void calculateRecurrence(String string, char[] charArray, int length, int stop, int index) {
        for (int i = 0; i < length; i++) {
            charArray[index] = string.charAt(i);
            if (index == stop) {
                for (int j = 0; j < length; j++) {
                    System.out.print(charArray[j]);
                }
                System.out.println();
            } else {
                calculateRecurrence(string, charArray, length, stop, index + 1);
            }
        }
    }
}
