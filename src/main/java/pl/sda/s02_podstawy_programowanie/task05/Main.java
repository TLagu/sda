package pl.sda.s02_podstawy_programowanie.task05;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        calculate(getValue());
    }

    private static int getValue() {
        System.out.print("Enter the number: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static void calculate(int value) {
        for (int i = 2; i < value; i++) {
            if (Main.isPrimaryNumber(i)) {
                System.out.println("Primary number: " + i);
            }
        }
    }

    private static boolean isPrimaryNumber(int number) {
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if (number % i == 0) return false;
        }
        return true;
    }
}
