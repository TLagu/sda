package pl.sda.s02_podstawy_programowanie.task07;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        int number = getValue();
        ArrayList<Integer> fibonacci = calculate(number);
        System.out.print("Value List (standard): ");
        String coma = "";
        for (Integer value : fibonacci) {
            System.out.print(coma + value);
            if (coma.equals("")) {
                coma = ", ";
            }
        }
        System.out.println();
        System.out.println("Value List (recurrence): " + fibonacci(number));
    }

    private static int getValue() {
        System.out.print("Enter the number: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static ArrayList<Integer> calculate(int value) {
        ArrayList<Integer> fibonacci = new ArrayList<>();
        fibonacci.add(1);
        fibonacci.add(1);
        for (int i = 2; i < value; i++) {
            fibonacci.add(
                    fibonacci.get(fibonacci.size() - 2) + fibonacci.get(fibonacci.size() - 1)
            );
        }
        return fibonacci;
    }

    private static int fibonacci(int n) {
        if (n <= 1) return n;
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}
