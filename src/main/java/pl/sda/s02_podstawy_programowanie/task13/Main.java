package pl.sda.s02_podstawy_programowanie.task13;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        calculate(getValueFromUser());
    }

    private static String getValueFromUser() {
        System.out.print("Enter the string: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    private static void calculate(String userString) {
        String[] words = userString.split(" ");
        String space = "";
        for (String word : words) {
            System.out.print(space + word + " " + word);
            if (space.equals("")) {
                space = " ";
            }
        }
    }

}
