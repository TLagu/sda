package pl.sda.s02_podstawy_programowanie.xxx_task14;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        game();
    }

    private static void game() {
        String choice = "y";
        int lives;
        String word;
        String hiddenWord;
        Scanner scanner = new Scanner(System.in);
        int hint;
        while (choice.equals("y")) {
            lives = 5;
            word = generateString();
            System.out.println(word);
            hiddenWord = "_".repeat(word.length());
            hint = 1;
            while (lives > 0) {
                System.out.println("Code: " + hiddenWord + " - (you have " + lives + " lives)" + word);
                if (hint > 0) {
                    System.out.print("Can I tell you one of the numbers [y/n]: ");
                    choice = scanner.nextLine();
                    if (choice.equals("y")) {
                        char[] chars = hiddenWord.toCharArray();
                        Random random = new Random();
                        int randomValue = random.nextInt(chars.length);
                        chars[randomValue] = word.charAt(randomValue);
                        hiddenWord = String.valueOf(chars);
                        hint--;
                        System.out.println("Code: " + hiddenWord + " - (you have " + lives + " lives)" + word);
                    }
                }
                System.out.print("Enter a code: ");
                choice = scanner.nextLine().toLowerCase();
                if (choice.length() != 4) {
                    System.out.println("Incorrect code length!");
                    lives--;
                    continue;
                }
                if (!word.equals(choice)) {
                    System.out.print("Incorrect code!");
                    System.out.println(word.compareTo(choice) > 0 ? " The value entered is too low!" : " Value given too much!");
                    lives--;
                    continue;
                }
                System.out.println("You guessed the code :)");
                break;
            }
            if (lives == 0) {
                System.out.println("You have lost, you have made too many mistakes in your life.");
            }
            System.out.print("Do you want to end the game [y/n]: ");
            choice = scanner.nextLine();
        }
    }

    private static String generateString() {
        Random random = new Random();
        String[] array = {String.valueOf(random.nextInt(10)),
                String.valueOf(random.nextInt(10)),
                String.valueOf(random.nextInt(10)),
                String.valueOf(random.nextInt(10))};
        return String.join("", array);
    }
}
