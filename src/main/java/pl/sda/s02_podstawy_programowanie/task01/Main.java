package pl.sda.s02_podstawy_programowanie.task01;

import java.util.Scanner;

public class Main {
    private static final double PI = 3.14;

    public static void main(String[] args) {
        double diameter = getDiameterFromUser();
        double circumference = PI * diameter;
        System.out.println("Circumference of the circle (3.14): " + calculate(PI, diameter));
        System.out.println("Circumference of the circle (math): " + calculate(Math.PI, diameter));
    }

    private static double calculate(double pi, double diameter) {
        return pi * diameter;
    }

    private static double getDiameterFromUser() {
        System.out.print("Enter the diameter of the circle: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }
}
