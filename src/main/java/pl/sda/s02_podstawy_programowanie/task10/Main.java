package pl.sda.s02_podstawy_programowanie.task10;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Result: " + calculate(getValue()));
    }

    private static int getValue() {
        System.out.print("Enter the number: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static int calculate(int value) {
        int sum = 0;
        String stringValue = Integer.valueOf(value).toString();
        for (int i = 0; i < stringValue.length(); i++) {
            sum += Character.getNumericValue(stringValue.charAt(i));
        }
        return sum;
    }
}
