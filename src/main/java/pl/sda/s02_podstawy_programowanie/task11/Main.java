package pl.sda.s02_podstawy_programowanie.task11;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    private final static String endString = "Starczy";

    public static void main(String[] args) {
        String textFromUser = "";
        ArrayList<String> usersString = new ArrayList<>();
        while (!textFromUser.equals(endString)) {
            textFromUser = getValueFromUser();
            if (!textFromUser.equals(endString)) usersString.add(textFromUser);
        }
        System.out.println("Result: " + getLongestString(usersString));
    }

    private static String getValueFromUser() {
        System.out.print("Enter the string: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    private static String getLongestString(ArrayList<String> usersString) {
        if (usersString.size() == 0) {
            return "No text was entered";
        }
        String longestString = usersString.get(0);
        for (String userString : usersString) {
            if (userString.length() > longestString.length()) {
                longestString = userString;
            }
        }
        return longestString;
    }

}
