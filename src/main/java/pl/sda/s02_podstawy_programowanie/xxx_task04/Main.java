package pl.sda.s02_podstawy_programowanie.xxx_task04;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        calculateArithmeticSequence(
                getDouble("Enter start value: "),
                getDouble("Enter difference value: "),
                getInteger("Enter count value: "));
        calculateGeometricSequence(
                getDouble("Enter start value: "),
                getDouble("Enter quotient value: "),
                getInteger("Enter count value: "));
    }

    private static int getInteger(String description) {
        System.out.print(description);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static double getDouble(String description) {
        System.out.print(description);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }

    private static void calculateArithmeticSequence(double startValue, double differenceValue, int countValue) {
        System.out.print("Result (elements of the arithmetic sequence): ");
        String separator = "";
        double element;
        double sum = 0;
        for (int i = 0; i < countValue; i++) {
            element = startValue + i * differenceValue;
            sum += element;
            System.out.print(separator + element);
            if (separator.equals("")) {
                separator = ", ";
            }
        }
        System.out.println("\nResult (sum of the arithmetic sequence): " + sum);
    }

    private static void calculateGeometricSequence(double startValue, double quotientValue, int countValue) {
        System.out.print("Result (elements of the geometric sequence): ");
        String separator = "";
        double element;
        double sum = 0;
        for (int i = 0; i < countValue; i++) {
            element = startValue * Math.pow(quotientValue, i);
            sum += element;
            System.out.print(separator + element);
            if (separator.equals("")) {
                separator = ", ";
            }
        }
        System.out.println("\nResult (sum of the geometric sequence): " + sum);
    }
}
