package pl.sda.s02_podstawy_programowanie.task19;

public class Author<ListArray> {
    private String surname;
    private String nationality;

    Author(String surname, String nationality) {
        this.surname = surname;
        this.nationality = nationality;
    }

    public String getSurname() {
        return surname;
    }
}
