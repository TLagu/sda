package pl.sda.s02_podstawy_programowanie.task19;

import java.util.ArrayList;

public class Main {
    private static ArrayList<Poem> poems = new ArrayList<>();

    public static void main(String[] args) {
        createData();
        Poem poem = getLongest();
        System.out.println("Result: surname (" + poem.getCreator().getSurname() + "), number of strophe (" + poem.getStropheNumbers() + ")");
    }

    private static void createData() {
        Author author1 = new Author("Surname 1", "Nationality 1");
        Author author2 = new Author("Surname 2", "Nationality 2");
        poems.add(new Poem(author1, 24));
        poems.add(new Poem(author1, 38));
        poems.add(new Poem(author1, 12));
        poems.add(new Poem(author2, 17));
        poems.add(new Poem(author2, 3));
        poems.add(new Poem(author2, 33));
    }

    private static Poem getLongest() {
        Poem result = poems.get(0);
        for (Poem poem : poems) {
            if (poem.getStropheNumbers() > result.getStropheNumbers()) {
                result = poem;
            }
        }
        return result;
    }
}
