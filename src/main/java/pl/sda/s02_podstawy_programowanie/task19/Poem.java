package pl.sda.s02_podstawy_programowanie.task19;

public class Poem {
    private Author creator;
    private int stropheNumbers;

    Poem(Author creator, int stropheNumbers) {
        this.creator = creator;
        this.stropheNumbers = stropheNumbers;

    }

    public int getStropheNumbers() {
        return stropheNumbers;
    }

    public Author getCreator() {
        return creator;
    }
}
