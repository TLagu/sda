package pl.sda.s02_podstawy_programowanie.task03;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int a = getValue("a");
        int b = getValue("b");
        int c = getValue("c");
        double delta = calculateDelta(a, b, c);
        if (delta < 0) {
            System.out.println("Result: Negative delta.");
        }
        System.out.println("Result: x1 = " + calculateX1(a, b, delta) + ", x2 = " + calculateX2(a, b, delta));

    }

    private static int getValue(String valueName) {
        System.out.print("Enter " + valueName + ": ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static double calculateDelta(int a, int b, int c) {
        return (double) b * b + 4 * a * c;
    }

    private static double calculateX1(int a, int b, double delta) {
        return (-b - Math.sqrt(delta)) / (2 * a);
    }

    private static double calculateX2(int a, int b, double delta) {
        return (-b + Math.sqrt(delta)) / (2 * a);
    }

}
