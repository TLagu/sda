package pl.sda.s02_podstawy_programowanie.task17;

import java.time.LocalDate;
import java.util.Scanner;
import java.time.temporal.ChronoUnit;

public class Main {
    public static void main(String[] args) {
        System.out.println("Result: " + calculate(getValueFromUser()));
    }

    private static String getValueFromUser() {
        System.out.print("Enter the date: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    private static long calculate(String dateString) {
        LocalDate date = LocalDate.parse(dateString);
        return ChronoUnit.DAYS.between(LocalDate.now(), date);
    }

}
