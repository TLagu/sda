package pl.sda.s02_podstawy_programowanie.task06;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        double sum = calculate(getValue());
        System.out.println("The sum of the sequence is: " + sum);
    }

    private static int getValue() {
        System.out.print("Enter the number: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static double calculate(int value) {
        double sum = 1;
        for (int i = 2; i <= value; i++) {
            sum += 1 / (double) i;
        }
        return sum;
    }
}
