package pl.sda.s02_podstawy_programowanie.task12;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Result " + calculate(getValueFromUser()) + "%");
    }

    private static String getValueFromUser() {
        System.out.print("Enter the string: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    private static double calculate(String userString) {
        int count = 0;
        for (int i = 0; i < userString.length() - 1; i++) {
            if (userString.charAt(i) == ' ') count++;
        }
        return (double) 100 * count / userString.length();
    }

}
