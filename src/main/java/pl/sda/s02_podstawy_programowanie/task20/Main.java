package pl.sda.s02_podstawy_programowanie.task20;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        boolean result = false;
        Random random = new Random();
        int randomValue = random.nextInt(100);
        while (!result) {
            int userValue = getValueFromUser();
            result = test(randomValue, userValue);
        }
    }

    private static int getValueFromUser() {
        System.out.print("Enter the number: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static boolean test(int randomValue, int userValue) {
        if (userValue > randomValue) {
            System.out.println("To many");
            return false;
        } else if (userValue < randomValue) {
            System.out.println("Not enough");
            return false;
        }
        System.out.println("Bingo!");
        return true;
    }
}
