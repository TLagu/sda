package pl.sda.s02_podstawy_programowanie.task18;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        if (calculate(getValueFromUser())) {
            System.out.println("Result: ok");
        } else {
            System.out.println("Result: false");
        }
    }

    private static String getValueFromUser() {
        System.out.print("Enter the string: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    private static boolean calculate(String userString) {
        Pattern sneezePattern = Pattern.compile(".*[a]+ psik.*");
        Matcher sneezeMatcher = sneezePattern.matcher(userString);
        return sneezeMatcher.find();
    }

}
