package pl.sda.s02_podstawy_programowanie.xxx_task12;

import java.util.*;

public class Main {
    private static University university;

    public static void main(String[] args) {
        university = new University();
        createData();
        System.out.println(university);
        university.sortByAgeAsc();
        System.out.println("Sort by Age (Asc):");
        System.out.println(university);
        university.sortByAgeDesc();
        System.out.println("Sort by Age (Desc):");
        System.out.println(university);
        university.sortByAverageGradeAsc();
        System.out.println("Sort by average grade (Asc):");
        System.out.println(university);
        System.out.println("Sort by average grade (Desc):");
        university.sortByAverageGradeDesc();
        System.out.println(university);
    }

    private static void createData() {
        Random random = new Random();
        university.addStudent(new Student("Jan",
                "Kowalski",
                20,
                new ArrayList<>(Arrays.asList(random.nextInt(4) + 2,
                        random.nextInt(4) + 2,
                        random.nextInt(4) + 2,
                        random.nextInt(4) + 2,
                        random.nextInt(4) + 2,
                        random.nextInt(4) + 2))));
        university.addStudent(new Student("Marcin",
                "Nowak",
                21,
                new ArrayList<>(Arrays.asList(random.nextInt(4) + 2,
                        random.nextInt(4) + 2,
                        random.nextInt(4) + 2,
                        random.nextInt(4) + 2,
                        random.nextInt(4) + 2,
                        random.nextInt(4) + 2,
                        random.nextInt(4) + 2))));
        university.addStudent(new Student("Ewelina",
                "Kochanowska",
                19,
                new ArrayList<>(Arrays.asList(random.nextInt(4) + 2,
                        random.nextInt(4) + 2,
                        random.nextInt(4) + 2,
                        random.nextInt(4) + 2))));
        university.addStudent(new Student("Marzena",
                "Przybylska",
                23,
                new ArrayList<>(Arrays.asList(random.nextInt(4) + 2,
                        random.nextInt(4) + 2,
                        random.nextInt(4) + 2,
                        random.nextInt(4) + 2,
                        random.nextInt(4) + 2))));
        university.addStudent(new Student("Jan",
                "Gwizdalski",
                24,
                new ArrayList<>(Arrays.asList(random.nextInt(4) + 2,
                        random.nextInt(4) + 2))));
    }
}
