package pl.sda.s02_podstawy_programowanie.xxx_task12;

import java.util.ArrayList;
import java.util.List;

public class University {
    private List<Student> students;

    public University() {
        students = new ArrayList<>();
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public void addStudent(Student student) {
        this.students.add(student);
    }

    public void sortByAgeAsc() {
        Student tmp;
        if (students.size() > 1) {
            for (int i = 0; i < students.size() - 1; i++) {
                for (int j = i + 1; j < students.size(); j++) {
                    if (students.get(i).getAge() > students.get(j).getAge()) {
                        tmp = students.get(i);
                        students.set(i, students.get(j));
                        students.set(j, tmp);
                    }
                }
            }
        }
    }

    public void sortByAgeDesc() {
        Student tmp;
        if (students.size() > 1) {
            for (int i = 0; i < students.size() - 1; i++) {
                for (int j = i + 1; j < students.size(); j++) {
                    if (students.get(i).getAge() < students.get(j).getAge()) {
                        tmp = students.get(i);
                        students.set(i, students.get(j));
                        students.set(j, tmp);
                    }
                }
            }
        }
    }

    public void sortByAverageGradeAsc() {
        Student tmp;
        if (students.size() > 1) {
            for (int i = 0; i < students.size() - 1; i++) {
                for (int j = i + 1; j < students.size(); j++) {
                    if (students.get(i).getAverageGrade() > students.get(j).getAverageGrade()) {
                        tmp = students.get(i);
                        students.set(i, students.get(j));
                        students.set(j, tmp);
                    }
                }
            }
        }
    }

    public void sortByAverageGradeDesc() {
        Student tmp;
        if (students.size() > 1) {
            for (int i = 0; i < students.size() - 1; i++) {
                for (int j = i + 1; j < students.size(); j++) {
                    if (students.get(i).getAverageGrade() < students.get(j).getAverageGrade()) {
                        tmp = students.get(i);
                        students.set(i, students.get(j));
                        students.set(j, tmp);
                    }
                }
            }
        }
    }

    @Override
    public String toString() {
        return "University{" +
                "students=" + students +
                '}';
    }
}
