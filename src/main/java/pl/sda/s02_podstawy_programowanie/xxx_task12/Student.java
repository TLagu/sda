package pl.sda.s02_podstawy_programowanie.xxx_task12;

import java.util.List;

public class Student {
    private String name;
    private String surname;
    private int age;
    private List<Integer> grades;
    private double averageGrade;

    public Student(String name, String surname, int age, List<Integer> grades) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.grades = grades;
        this.calculateAverageGrade();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFullName() {
        String fullName = this.name;
        fullName.concat(" ");
        fullName.concat(this.surname);
        return fullName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<Integer> getGrades() {
        return grades;
    }

    public void setGrades(List<Integer> grades) {
        this.grades = grades;
    }

    public void addGrade(int grade) {
        this.grades.add(grade);
        this.calculateAverageGrade();
    }

    public double getAverageGrade() {
        return averageGrade;
    }

    public void calculateAverageGrade() {
        this.averageGrade = 0;
        for (int grade : this.grades) {
            this.averageGrade += grade;
        }
        this.averageGrade /= grades.size();
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", grades=" + grades +
                ", averageGrade=" + averageGrade +
                '}';
    }
}
