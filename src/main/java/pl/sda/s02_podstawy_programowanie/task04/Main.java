package pl.sda.s02_podstawy_programowanie.task04;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        calculate(getValue());
    }

    private static int getValue() {
        System.out.print("Enter the maximum number: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static void calculate(int maxValue) {
        for (int i = 1; i <= maxValue; i++) {
            if (i % 3 == 0 && i % 7 == 0) {
                System.out.println("Pif paf (" + i + ")");
            } else if (i % 3 == 0) {
                System.out.println("Pif (" + i + ")");
            } else if (i % 7 == 0) {
                System.out.println("Paf (" + i + ")");
            }
        }
    }
}
