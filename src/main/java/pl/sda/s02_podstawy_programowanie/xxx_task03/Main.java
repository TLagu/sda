package pl.sda.s02_podstawy_programowanie.xxx_task03;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Result: " + calculate(getInteger("Enter value: ")));
    }

    private static int getInteger(String description) {
        System.out.print(description);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static int calculate(int value) {
        int sum = 1;
        for (int i = 2; i <= value; i++) {
            sum *= i;
        }
        return sum;
    }

}
