package pl.sda.s02_podstawy_programowanie.task16;

import java.util.Scanner;

public class Main {
    private final static int numberOfNumbers = 10;
    private static int[] intArray = new int[numberOfNumbers];

    public static void main(String[] args) {
        for (int i = 0; i < numberOfNumbers; i++) {
            intArray[i] = getInteger("Enter number (" + (i + 1) + "): ");
        }
        System.out.println("Result: " + getLongestAscendingString());
    }

    private static int getInteger(String description) {
        System.out.print(description);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static int getLongestAscendingString() {
        int length = 1;
        int maxLen = 1;
        for (int i = 1; i < numberOfNumbers; i++) {
            if (intArray[i] > intArray[i - 1]) {
                length++;
                if (length > maxLen) {
                    maxLen = length;
                }
            } else {
                length = 1;
            }
        }
        return maxLen;
    }

}
