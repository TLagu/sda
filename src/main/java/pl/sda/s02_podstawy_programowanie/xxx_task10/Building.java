package pl.sda.s02_podstawy_programowanie.xxx_task10;

import java.util.ArrayList;

public class Building {
    private final String buildingName;
    private final ArrayList<Floor> floors = new ArrayList<>();

    public Building(String buildingName) {
        this.buildingName = buildingName;
    }

    public void addFloor(Floor floor) {
        floors.add(floor);
    }

    public ArrayList<Room> getRoomsOnFloor(int floorNumber) {
        for (Floor floor : floors) {
            if (floor.getFloorNumber() == floorNumber) {
                return floor.getRooms();
            }
        }
        return null;
    }

    public Double getAreaOfRoomsOnFloor(int floorNumber) {
        for (Floor floor : floors) {
            if (floor.getFloorNumber() == floorNumber) {
                return floor.getAreaOfRooms();
            }
        }
        return null;
    }

    public Integer getNumberOfWindowsOnFloor(int floorNumber) {
        for (Floor floor : floors) {
            if (floor.getFloorNumber() == floorNumber) {
                return floor.getNumberOfWindows();
            }
        }
        return null;
    }

    public Integer getNumberOfDoorsOnFloor(int floorNumber) {
        for (Floor floor : floors) {
            if (floor.getFloorNumber() == floorNumber) {
                return floor.getNumberOfDoors();
            }
        }
        return null;
    }

    public Floor findRoomByName(String roomName) {
        for (Floor floor : floors) {
            Room room = floor.findRoomByName(roomName);
            if (room != null) {
                Floor resultFloor = new Floor(floor.getFloorNumber());
                resultFloor.addRoom(room);
                return resultFloor;
            }
        }
        return null;
    }

    public Room findSmallestRoom() {
        Room smallestRoom = floors.get(0).findSmallestRoom();
        for (Floor floor : floors) {
            if (smallestRoom.getArea() < floor.findSmallestRoom().getArea()) {
                smallestRoom = floor.findSmallestRoom();
            }
        }
        return smallestRoom;
    }

    public Room findLargestRoom() {
        Room largestRoom = floors.get(0).findLargestRoom();
        for (Floor floor : floors) {
            if (largestRoom.getArea() > floor.findLargestRoom().getArea()) {
                largestRoom = floor.findLargestRoom();
            }
        }
        return largestRoom;
    }
}
