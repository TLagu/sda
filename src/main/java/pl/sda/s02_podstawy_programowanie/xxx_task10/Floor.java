package pl.sda.s02_podstawy_programowanie.xxx_task10;

import java.util.ArrayList;

public class Floor {
    private final int floorNumber;
    private final ArrayList<Room> rooms = new ArrayList<>();

    public Floor(int floorNumber) {
        this.floorNumber = floorNumber;
    }

    public void addRoom(Room room) {
        rooms.add(room);
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    public ArrayList<Room> getRooms() {
        return rooms;
    }

    public double getAreaOfRooms() {
        double areaOfRooms = 0;
        for (Room room : rooms) {
            areaOfRooms += room.getArea();
        }
        return areaOfRooms;
    }

    public int getNumberOfWindows() {
        int numberOfWindows = 0;
        for (Room room : rooms) {
            numberOfWindows += room.getNumberOfWindows();
        }
        return numberOfWindows;
    }

    public int getNumberOfDoors() {
        int numberOfDoors = 0;
        for (Room room : rooms) {
            numberOfDoors += room.getNumberOfDoors();
        }
        return numberOfDoors;
    }

    public Room findRoomByName(String roomName) {
        for (Room room : rooms) {
            if (room.getName().equals(roomName)) {
                return room;
            }
        }
        return null;
    }

    public Room findSmallestRoom() {
        Room smallestRoom = rooms.get(0);
        for (Room room : rooms) {
            if (room.getArea() < smallestRoom.getArea()) {
                smallestRoom = room;
            }
        }
        return smallestRoom;
    }

    public Room findLargestRoom() {
        Room largestRoom = rooms.get(0);
        for (Room room : rooms) {
            if (room.getArea() > largestRoom.getArea()) {
                largestRoom = room;
            }
        }
        return largestRoom;
    }

    @Override
    public String toString() {
        return "Floor{" +
                "Floor number = " + floorNumber +
                "\n    Rooms = " + rooms +
                "\n}";
    }
}
