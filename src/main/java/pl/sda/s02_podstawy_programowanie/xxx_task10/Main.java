package pl.sda.s02_podstawy_programowanie.xxx_task10;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main {
    private static Building building;

    public static void main(String[] args) {
        generateBuilding();
        char key = 'x';
        while (key != 48) {
            switch (key) {
                case 49:
                    ArrayList<Room> rooms = building.getRoomsOnFloor(getIntFromUser("Enter the floor number: "));
                    if (rooms != null) {
                        for (Room room : rooms) {
                            System.out.println(room);
                        }
                    }
                    break;
                case 50:
                    System.out.println("Area = " + building.getAreaOfRoomsOnFloor(getIntFromUser("Enter the floor number: ")));
                    break;
                case 51:
                    System.out.println("Number of windows = " + building.getNumberOfWindowsOnFloor(getIntFromUser("Enter the floor number: ")));
                    break;
                case 52:
                    System.out.println("Number of doors = " + building.getNumberOfDoorsOnFloor(getIntFromUser("Enter the floor number: ")));
                    break;
                case 53:
                    Floor floor = building.findRoomByName(getStringFromUser("Enter the room name: "));
                    System.out.println("Room info = " + floor);
                    break;
                case 54:
                    System.out.println("Smallest room = " + building.findSmallestRoom());
                    break;
                case 55:
                    System.out.println("Largest room = " + building.findLargestRoom());
                    break;
            }
            showMenu();
            key = getCharFromUser("Please enter your choice: ");

        }
    }

    private static void generateBuilding() {
        // Building
        building = new Building("First building");
        // First floor
        Floor floor = new Floor(1);
        floor.addRoom(new Room("11", 2.53d, 4.0d, 5.0d, 1, 2));
        floor.addRoom(new Room("12", 2.53d, 2.5d, 3.0d, 1, 1));
        floor.addRoom(new Room("13", 2.53d, 5.0d, 7.0d, 1, 3));
        floor.addRoom(new Room("14", 2.53d, 4.5d, 5.0d, 1, 2));
        building.addFloor(floor);
        // Second floor
        floor = new Floor(2);
        floor.addRoom(new Room("21", 2.50d, 4.5d, 5.5d, 1, 3));
        floor.addRoom(new Room("22", 2.50d, 2.0d, 3.5d, 1, 1));
        floor.addRoom(new Room("23", 2.50d, 5.5d, 7.5d, 2, 4));
        floor.addRoom(new Room("24", 2.50d, 4.0d, 5.5d, 1, 2));
        building.addFloor(floor);
        // Third floor
        floor = new Floor(3);
        floor.addRoom(new Room("31", 2.65d, 3.5d, 5.5d, 2, 2));
        floor.addRoom(new Room("32", 2.65d, 7.0d, 8.5d, 2, 4));
        floor.addRoom(new Room("33", 2.65d, 3.5d, 4.5d, 1, 1));
        floor.addRoom(new Room("34", 2.65d, 2.0d, 3.5d, 1, 2));
        building.addFloor(floor);
    }

    private static char getCharFromUser(String description) {
        System.out.print(description);
        Scanner scanner = new Scanner(System.in);
        return scanner.next().charAt(0);
    }

    private static int getIntFromUser(String description) {
        System.out.print(description);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static String getStringFromUser(String description) {
        System.out.print(description);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    private static void showMenu() {
        System.out.println("[1] Display all rooms on the floor");
        System.out.println("[2] Display the areas of the rooms on the floor");
        System.out.println("[3] Display the number of windows on the floor");
        System.out.println("[4] Display the number of doors on the floor");
        System.out.println("[5] Find a room by name");
        System.out.println("[6] Find the smallest room");
        System.out.println("[7] Find the largest room");
        System.out.println("[0] Exit the program");
    }

}
