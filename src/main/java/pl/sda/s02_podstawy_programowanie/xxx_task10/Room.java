package pl.sda.s02_podstawy_programowanie.xxx_task10;

public class Room {
    private final String name;
    private final double height;
    private final double width;
    private final double length;
    private final double area;
    private final double volume;
    private final int numberOfDoors;
    private final int numberOfWindows;

    public Room(String name, double height, double width, double length, int numberOfDoors, int numberOfWindows) {
        this.name = name;
        this.height = height;
        this.width = width;
        this.length = length;
        this.area = width * length;
        this.volume = this.area * height;
        this.numberOfDoors = numberOfDoors;
        this.numberOfWindows = numberOfWindows;
    }

    public String getName() {
        return name;
    }

    public double getArea() {
        return area;
    }

    public int getNumberOfDoors() {
        return numberOfDoors;
    }

    public int getNumberOfWindows() {
        return numberOfWindows;
    }

    @Override
    public String toString() {
        return "Room{" +
                "Room name = '" + name + '\'' +
                ", Height =" + String.format("% .2f", height) +
                ", Width =" + String.format("% .2f", width) +
                ", Length =" + String.format("% .2f", length) +
                ", Room area =" + String.format("% .4f", area) +
                ", Volume =" + String.format("% .4f", volume) +
                ", Number of doors = " + numberOfDoors +
                ", Number od windows = " + numberOfWindows +
                '}';
    }
}
