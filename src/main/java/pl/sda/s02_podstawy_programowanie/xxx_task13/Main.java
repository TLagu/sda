package pl.sda.s02_podstawy_programowanie.xxx_task13;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        game();
    }

    private static void game() {
        String choice = "y";
        int lives;
        String word;
        String hiddenWord;
        char[] chars;
        Scanner scanner = new Scanner(System.in);
        while (choice.equals("y")) {
            lives = 5;
            word = getWord().toLowerCase();
            hiddenWord = "_".repeat(word.length());
            while (lives > 0) {
                System.out.println("Hidden word: " + hiddenWord + " - (you have " + lives + " lives)");
                System.out.print("Enter a letter: ");
                choice = scanner.nextLine().toLowerCase();
                if (choice.length() > 1) {
                    System.out.println("Too many letters are given, you have lost!");
                    break;
                }
                if (hiddenWord.contains(choice)) {
                    System.out.println("You have already entered such a letter!");
                    lives--;
                    continue;
                }
                if (!word.contains(choice)) {
                    System.out.println("There is no such letter in the word!");
                    lives--;
                    continue;
                }
                System.out.println("You guessed the letter :)");
                chars = hiddenWord.toCharArray();
                for (int i = 0; i < word.length(); i++) {
                    if (word.charAt(i) == choice.charAt(0)) {
                        chars[i] = word.charAt(i);
                    }
                }
                hiddenWord = String.valueOf(chars);
                if (hiddenWord.equals(word)) {
                    System.out.println("Hooray, you guessed the word.");
                    break;
                }
            }
            if (lives == 0) {
                System.out.println("You have lost, you have made too many mistakes in your life.");
            }
            System.out.print("Do you want to end the game [y/n]: ");
            choice = scanner.nextLine();
        }
    }

    private static String getWord() {
        System.out.print("Enter the word (if empty, then from the password generator): ");
        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine().trim();
        if (word.isBlank()) {
            word = generateWord();
            System.out.print("Word taken from the generator.");
        }
        return word;
    }

    private static String generateWord() {
        List<String> words = new ArrayList<>(Arrays.asList("pacynka", "robal", "kot"));
        Random random = new Random();
        return words.get(random.nextInt(words.size()));
    }
}
