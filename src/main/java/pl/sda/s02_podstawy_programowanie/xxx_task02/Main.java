package pl.sda.s02_podstawy_programowanie.xxx_task02;

import java.util.Scanner;

public class Main {
    private final static int minValue = 0;
    private final static int maxValue = 200;

    public static void main(String[] args) {
        convertNumericToWord();
    }

    private static int getInteger(String description) {
        System.out.print(description);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static String showOnes(char value) {
        switch (value) {
            case '1':
                return "jeden";
            case '2':
                return "dwa";
            case '3':
                return "trzy";
            case '4':
                return "cztery";
            case '5':
                return "pięć";
            case '6':
                return "sześć";
            case '7':
                return "siedem";
            case '8':
                return "osiem";
            case '9':
                return "dziewięć";
            default:
                return "";
        }
    }

    private static String showTens(char value) {
        switch (value) {
            case '1':
                return "dziesięć";
            case '2':
                return "dwadzieścia";
            case '3':
                return "trzydzieści";
            case '4':
                return "czterdzieści";
            case '5':
                return "pięćdziesiąt";
            case '6':
                return "sześćdziesiąt";
            case '7':
                return "siedemdziesiąt";
            case '8':
                return "osiemdziesiąt";
            case '9':
                return "dziewięćdziesiąt";
            default:
                return "";
        }
    }

    private static String showHundreds(char value) {
        if (value == '1') {
            return "sto";
        }
        return "";
    }

    private static void convertNumericToWord() {
        int number = getInteger("Enter the number <" + minValue + ", " + maxValue + "): ");
        if (number < minValue) {
            System.out.println("Result: the value is to small");
            return;
        }
        if (number >= maxValue) {
            System.out.println("Result: the value is to big");
        }
        String stringNumber = Integer.valueOf(number).toString();
        String wordValue = showOnes(stringNumber.charAt(stringNumber.length() - 1));
        if (stringNumber.length() > 1) {
            if (!wordValue.equals("")) {
                wordValue = " " + wordValue;
            }
            wordValue = showTens(stringNumber.charAt(stringNumber.length() - 2)) + wordValue;
        }
        if (stringNumber.length() > 2) {
            if (!wordValue.equals("")) {
                wordValue = " " + wordValue;
            }
            wordValue = showHundreds(stringNumber.charAt(stringNumber.length() - 3)) + wordValue;
        }
        System.out.println("Result: " + wordValue);
    }
}
