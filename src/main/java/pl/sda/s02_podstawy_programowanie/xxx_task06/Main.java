package pl.sda.s02_podstawy_programowanie.xxx_task06;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        char startChar = getChar("Enter start character: ");
        char stopChar = getChar("Enter stop character: ");
        showData(startChar, stopChar);
        showDataInReverseOrder(startChar, stopChar);
        showDataUpperCase(startChar, stopChar);
    }

    private static char getChar(String description) {
        System.out.print(description);
        Scanner scanner = new Scanner(System.in);
        return scanner.next().charAt(0);
    }

    private static void showData(char startChar, char stopChar) {
        System.out.print("Result: ");
        String separator = "";
        for (int i = startChar; i <= stopChar; i++) {
            System.out.print(separator + ((char) i));
            if (separator.equals("")) {
                separator = ", ";
            }
        }
        System.out.println();
    }

    private static void showDataInReverseOrder(char startChar, char stopChar) {
        System.out.print("Result (reverse order): ");
        String separator = "";
        for (int i = stopChar; i >= startChar; i--) {
            System.out.print(separator + ((char) i));
            if (separator.equals("")) {
                separator = ", ";
            }
        }
        System.out.println();
    }

    private static void showDataUpperCase(char startChar, char stopChar) {
        System.out.print("Result (upper case): ");
        String separator = "";
        for (int i = startChar; i <= stopChar; i++) {
            System.out.print(separator + Character.toUpperCase((char) i));
            if (separator.equals("")) {
                separator = ", ";
            }
        }
        System.out.println();
    }
}
