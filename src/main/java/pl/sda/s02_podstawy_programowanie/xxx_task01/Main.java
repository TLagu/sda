package pl.sda.s02_podstawy_programowanie.xxx_task01;

import java.util.Random;
import java.util.Scanner;

public class Main {
    private static int[] array;

    public static void main(String[] args) {
        int n = getInteger("Enter n value: ");
        int max = getInteger("Enter max value: ");
        generateIntArray(n, max);
        showAllValues();
        showEvenValues();
        showOddDivisibleByThreeValues();
        showRangeValues();
        int userValue = getInteger("Enter your value: ");
        showLowerValues(userValue);
        showDividedIntoTablesValues();
        showInReverseOrder();
    }

    private static int getInteger(String description) {
        System.out.print(description);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static void generateIntArray(int n, int max) {
        array = new int[n];
        for (int i = 0; i < n; i++) {
            Random randomValue = new Random();
            array[i] = randomValue.nextInt(max);
        }
    }

    private static void showAllValues() {
        System.out.print("All values: ");
        String separator = "";
        for (int j : array) {
            System.out.print(separator + j);
            if (separator.equals("")) {
                separator = ", ";
            }
        }
        System.out.println();
    }

    private static void showEvenValues() {
        System.out.print("Even values: ");
        String separator = "";
        for (int j : array) {
            if (j % 2 == 0) {
                System.out.print(separator + j);
                if (separator.equals("")) {
                    separator = ", ";
                }
            }
        }
        System.out.println();
    }

    private static void showOddDivisibleByThreeValues() {
        System.out.print("Odd values: ");
        String separator = "";
        for (int j : array) {
            if (j % 2 == 1 && j % 3 == 0) {
                System.out.print(separator + j);
                if (separator.equals("")) {
                    separator = ", ";
                }
            }
        }
        System.out.println();
    }

    private static void showAverageValue() {
        int sum = 0;
        for (int j : array) {
            sum += j;
        }
        System.out.println("Average value: " + (double) sum / array.length);
    }

    private static void showRangeValues() {
        int minValue = array[0];
        int minIndex = 0;
        int maxValue = array[0];
        int maxIndex = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < minValue) {
                minValue = array[i];
                minIndex = i;
            }
            if (array[i] > maxValue) {
                maxValue = array[i];
                maxIndex = i;
            }
        }
        System.out.println("Range values - min: " + minValue + " (index " + minIndex + "), max: " + maxValue + " (index " + maxIndex + ")");
    }

    private static void showLowerValues(int userValue) {
        int count = 0;
        for (int j : array) {
            if (j < userValue) {
                count++;
            }
        }
        if (count == 0) {
            System.out.println("Not a single value was found");
            return;
        }
        int[] lowerValues = new int[count];
        System.out.print("Lower values: ");
        String separator = "";
        int index = 0;
        for (int j : array) {
            if (j < userValue) {
                System.out.print(separator + j);
                lowerValues[index] = j;
                index++;
                if (separator.equals("")) {
                    separator = ", ";
                }
            }
        }
        System.out.println();
    }

    private static void showDividedIntoTablesValues() {
        int oddCount = 0;
        int evanCount = 0;
        for (int j : array) {
            if (j % 2 == 1) {
                evanCount++;
            }
        }
        oddCount = array.length - evanCount;
        int[] oddArray = new int[oddCount];
        int[] evanArray = new int[evanCount];
        int oddIndex = 0;
        int evanIndex = 0;
        for (int j : array) {
            if (j % 2 == 0) {
                oddArray[oddIndex] = j;
                oddIndex++;
            } else {
                evanArray[evanIndex] = j;
                evanIndex++;
            }
        }
        int[][] allArray = new int[2][];
        allArray[0] = oddArray;
        allArray[1] = evanArray;
        System.out.print("Divided into tables values: ");
        String separator = "";
        for (int i = 0; i < allArray.length; i++) {
            for (int j = 0; j < allArray[i].length; j++) {
                System.out.print(separator + "{" + i + "," + j + ": " + allArray[i][j] + "}");
                if (separator.equals("")) {
                    separator = ", ";
                }
            }
        }
        System.out.println();
    }

    private static void showInReverseOrder() {
        System.out.print("All values: ");
        String separator = "";
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(separator + array[i]);
            if (separator.equals("")) {
                separator = ", ";
            }
        }
        System.out.println();
    }
}
