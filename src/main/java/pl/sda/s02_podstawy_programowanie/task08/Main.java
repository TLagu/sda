package pl.sda.s02_podstawy_programowanie.task08;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        double value1 = getNumber();
        char operator = getOperator();
        double value2 = getNumber();
        calculate(value1, operator, value2);
    }

    private static double getNumber() {
        System.out.print("Enter the number: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }

    private static char getOperator() {
        System.out.print("Enter the operator: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.next().charAt(0);
    }

    private static void calculate(double value1, char operator, double value2) {
        double result;
        switch (operator) {
            case '+':
                result = value1 + value2;
                break;
            case '-':
                result = value1 - value2;
                break;
            case '/':
                if (value2 == 0) {
                    System.out.println("Result: Wrong number");
                    return;
                }
                result = value1 / value2;
                break;
            case '*':
                result = value1 * value2;
                break;
            default:
                System.out.println("Result: Wrong operator");
                return;
        }

        System.out.println("Result: " + result);
    }
}
