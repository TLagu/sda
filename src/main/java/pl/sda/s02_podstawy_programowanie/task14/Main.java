package pl.sda.s02_podstawy_programowanie.task14;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Result: " + calculate(getValueFromUser(), getValueFromUser()));
    }

    private static char getValueFromUser() {
        System.out.print("Enter the character: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.next().charAt(0);
    }

    private static int calculate(char firstChar, char secondChar) {
        int firstCode = (int) firstChar;
        int secondCode = (int) secondChar;
        return secondCode - firstCode - 1;
    }
}
