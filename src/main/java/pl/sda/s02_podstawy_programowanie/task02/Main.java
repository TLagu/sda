package pl.sda.s02_podstawy_programowanie.task02;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        double bmi = calculate(getWeightFromUser(), getHeightFromUser());
        System.out.println("BMI = " + bmi);
        if (18.5 < bmi && bmi < 24.9) {
            System.out.println("Result: Optimal BMI");
        } else {
            System.out.println("Result: BMI not optimal");
        }
    }

    private static double getWeightFromUser() {
        System.out.print("Enter the weight [kg]: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }

    private static int getHeightFromUser() {
        System.out.print("Enter your height [cm]: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static double calculate(double weight, int height) {
        return weight / Math.pow((double) height / 100, 2);
    }

}
