package pl.sda.s02_podstawy_programowanie.task09;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int waveSize = getValue();
        System.out.println("Static method: ");
        drawStars(waveSize);
        System.out.println("Dynamic method: ");
        int waveHeight = getValue();
        drawStars2(waveSize, waveHeight);
    }

    private static int getValue() {
        System.out.print("Enter the number: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static void drawStars(int numberOfStars) {
        String firstLine = "";
        String secondLine = "";
        String thirdLine = "";
        String forthLine = "";
        for (int i = 0; i < numberOfStars; i++) {
            int rest = i % 8;
            switch (rest) {
                case 0:
                case 7:
                    firstLine += "*";
                    secondLine += " ";
                    thirdLine += " ";
                    forthLine += " ";
                    break;
                case 1:
                case 6:
                    firstLine += " ";
                    secondLine += "*";
                    thirdLine += " ";
                    forthLine += " ";
                    break;
                case 2:
                case 5:
                    firstLine += " ";
                    secondLine += " ";
                    thirdLine += "*";
                    forthLine += " ";
                    break;
                case 3:
                case 4:
                    firstLine += " ";
                    secondLine += " ";
                    thirdLine += " ";
                    forthLine += "*";
                    break;
            }
        }
        System.out.println(firstLine);
        System.out.println(secondLine);
        System.out.println(thirdLine);
        System.out.println(forthLine);
    }

    private static void drawStars2(int numberOfStars, int height) {
        ArrayList<String> lines = new ArrayList<>();
        int width = 2 * height;
        for (int i = 0; i < height; i++) {
            lines.add("");
        }
        for (int i = 0; i < numberOfStars; i++) {
            int rest = i % width;
            for (int j = 0; j < height; j++) {
                if (rest == j || rest == (width - j - 1)) {
                    lines.set(j, lines.get(j) + "*");
                } else {
                    lines.set(j, lines.get(j) + " ");
                }
            }
        }
        for (int i = 0; i < height; i++) {
            System.out.println(lines.get(i));
        }
    }

}
