package pl.sda.s02_podstawy_programowanie.xxx_task07;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String vowels = "aąeęioóuy";
        String userString = getString("Enter string: ");
        System.out.println("Result (vowels): " + calculateVowel(userString, vowels));
        System.out.println("Result (consonant): " + calculateConsonant(userString, vowels));
        showUpperCase(userString);
        showFirstAndLastAsUpperCase(userString);
        showInReverseOrder(userString);
        String alphabet = "aąbcćdeęfghijklłmnńoóprsśtuvwxyzźż";
        calculateNumberOfOccurrences(userString);
    }

    private static String getString(String description) {
        System.out.print(description);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    private static int calculateVowel(String userString, String vowels) {
        int count = 0;
        for (int i = 0; i < userString.length(); i++) {
            if (vowels.contains(userString.substring(i, i + 1))) {
                count++;
            }
        }
        return count;
    }

    private static int calculateConsonant(String userString, String vowels) {
        return userString.length() - calculateVowel(userString, vowels);
    }

    private static void showUpperCase(String userString) {
        System.out.println("Result (upper case): " + userString.toUpperCase());
    }

    private static void showFirstAndLastAsUpperCase(String userString) {
        if (userString.length() > 2) {
            System.out.println("Result (first and last as upper case): "
                    + userString.substring(0, 1).toUpperCase()
                    + userString.substring(1, userString.length() - 1)
                    + userString.substring(userString.length() - 1).toUpperCase());
        } else {
            showUpperCase(userString);
        }
    }

    private static void showInReverseOrder(String userString) {
        System.out.print("Result (in reverse order): ");
        for (int i = userString.length() - 1; i >= 0; i--) {
            System.out.print(userString.charAt(i));
        }
        System.out.println();
    }

    private static void calculateNumberOfOccurrences(String userString) {
        int maxValue = userString.charAt(0);
        for (int i = 0; i < userString.length(); i++) {
            if (userString.charAt(i) > maxValue || Character.toUpperCase(userString.charAt(i)) > maxValue) {
                maxValue = userString.charAt(i);
            }
        }
        int[] count = new int[maxValue + 1];
        for (int i = 0; i < userString.length(); i++) {
            count[userString.charAt(i)]++;
        }
        System.out.print("Result (number of occurrences): ");
        String separator = "";
        for (int i = 0; i <= maxValue; i++) {
            if (count[i] > 0) {
                System.out.print(separator + (char) i + "=" + count[i]);
                if (separator.equals("")) {
                    separator = ", ";
                }
            }
        }
        System.out.println();
    }
}
