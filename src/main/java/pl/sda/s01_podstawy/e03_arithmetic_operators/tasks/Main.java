package pl.sda.s01_podstawy.e03_arithmetic_operators.tasks;

public class Main {
    public static void main(String[] args) {
        System.out.println("##################");
        System.out.println("### EXERCISE 1 ###");
        System.out.println("##################");
        System.out.println("Zadeklaruj po kilka zmiennych różnych typów i potestuj wywoływanie na nich działań +, -, *, /, %.");
        int intValue = 10;
        long longValue = 12L;
        double doubleValue = 14.0d;
        System.out.println("Sum, " + (intValue + longValue + doubleValue));
        System.out.println("Diff, " + (intValue - longValue - doubleValue));
        System.out.println("Mult/Div, " + (intValue * longValue / doubleValue));
        System.out.println("Mod/sum, " + ((intValue % longValue) + doubleValue));
        System.out.println("Oblicz średnią arytmetyczną z tych liczb.");
        System.out.println("Average, " + ((intValue + longValue + doubleValue) / 3));

        System.out.println("\n##################");
        System.out.println("### EXERCISE 2 ###");
        System.out.println("##################");
        System.out.println("Zadeklaruj zmienną typu int i potestuj operację ++ i -- zarówno w opcji pre jak i post.");
        int intValue2 = 13;
        System.out.println("l++: " + (intValue2++));
        System.out.println("++l: " + (++intValue2));
        System.out.println("l--: " + (intValue2--));
        System.out.println("--l: " + (--intValue2));

        System.out.println("\n##################");
        System.out.println("### EXERCISE 3 ###");
        System.out.println("##################");
        System.out.println("Zadeklaruj dwie zmienne odpowiadające bokom prostokąta i policz jego pole oraz obwód.");
        double a = 10.0d;
        double b = 12.0d;
        System.out.println("Perimeter: " + (2 * (a + b)));
        System.out.println("Area: " + (a * b));

        System.out.println("\n##################");
        System.out.println("### EXERCISE 4 ###");
        System.out.println("##################");
        System.out.println("Zadeklaruj dwie zmienne odpowiadające bokom (przyprostokątnym) trójkąta prostokątnego i policz jego pole.");
        System.out.println("Area: " + (a * b / 2));

        System.out.println("\n##################");
        System.out.println("### EXERCISE 5 ###");
        System.out.println("##################");
        System.out.println("Zadeklaruj zmienną odpowiadającą średnicy koła i policz jego pole i obwód.");
        double d = 10.0d;
        System.out.println("Circumference: " + (Math.PI * d));
        System.out.println("Area: " + (d / 2 * Math.pow(Math.PI, 2)));

        System.out.println("\n##################");
        System.out.println("### EXERCISE 6 ###");
        System.out.println("##################");
        System.out.println("Zadeklaruj trzy zmienne odpowiadające bokom trójkąta i sprawdź, czy jest on prostokątny.");
        double aa = 3.0d;
        double bb = 4.0d;
        double cc = 5.0d;
        if (aa >= bb && aa >= cc) {
            if (Math.pow(aa, 2) == Math.pow(bb, 2) + Math.pow(cc, 2)) {
                System.out.println("Right-angled triangle");
            } else {
                System.out.println("NOT Right-angled triangle");
            }
        } else if (bb >= aa && bb >= cc) {
            if (Math.pow(bb, 2) == Math.pow(aa, 2) + Math.pow(cc, 2)) {
                System.out.println("Right-angled triangle");
            } else {
                System.out.println("NOT Right-angled triangle");
            }
        } else if (cc >= aa && cc >= bb) {
            if (Math.pow(cc, 2) == Math.pow(aa, 2) + Math.pow(bb, 2)) {
                System.out.println("Right-angled triangle");
            } else {
                System.out.println("NOT Right-angled triangle");
            }
        } else {
            System.out.println("NOT Right-angled triangle");
        }
    }
}
