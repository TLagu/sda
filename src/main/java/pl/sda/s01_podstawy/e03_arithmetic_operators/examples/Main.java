package pl.sda.s01_podstawy.e03_arithmetic_operators.examples;

public class Main {
    public static void main(String[] args) {
        System.out.println("Inc/Dec");
        int x = 5;
        System.out.println(x++);
        System.out.println(++x);
        System.out.println(--x);
        System.out.println(x--);

        System.out.println("Comparison");
        int i = 8, j = 8;
        System.out.println(i == j);
        System.out.println(i != j);
        System.out.println(i < 24);
        System.out.println(j != 5);
        System.out.println(i >= 8);

        int fiveValue = 5;
        Integer fiveObject = 5;
        Integer fiveObjectByNew = new Integer(5);
        Integer anotherFiveObjectByNew = new Integer(5);

        System.out.println(fiveValue == fiveObject);
        System.out.println(fiveValue == fiveObjectByNew);
        System.out.println(fiveObject == fiveObjectByNew);
        System.out.println(fiveObjectByNew == anotherFiveObjectByNew);

        System.out.println(fiveObject.equals(fiveObjectByNew));
        System.out.println(fiveObject.equals(anotherFiveObjectByNew));
        System.out.println(fiveObjectByNew.equals(anotherFiveObjectByNew));

        int twoHundredValue = 200;
        Integer twoHundredObject = 200;
        System.out.println(twoHundredValue == twoHundredObject);

        int ii = 8, jj = 8;
        Integer k = 8;
        Integer l = 8;
        System.out.println(ii == jj);
        System.out.println(k.equals(l));
        System.out.println(l.equals(j));
    }
}
