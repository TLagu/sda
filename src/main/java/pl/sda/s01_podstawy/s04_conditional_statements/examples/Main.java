package pl.sda.s01_podstawy.s04_conditional_statements.examples;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        System.out.println("IF statement");
        int totalPrice = 1200;
        int discount = 200;
        if (totalPrice > 1000) {
            totalPrice = totalPrice - discount;
        }
        System.out.println("Total price: " + totalPrice);

        int age = 21;
        int dayNr = DayOfWeek.from(LocalDate.now()).getValue();
        int amount = totalPrice;
        if ((age > 18 && dayNr == 6) || amount >= 1000) {
            System.out.println("Condition 2: TRUE");
        }

        System.out.println("\nIF ELSE statement");
        if (age >= 18) {
            System.out.println("Jesteś dorosły");
        } else {
            System.out.println("Jesteś niepełnoletni");
        }

        int dayOfTheWeekNr = DayOfWeek.from(LocalDate.now()).getValue();
        if (dayOfTheWeekNr == 7) {
            System.out.println("Dziś jest niedziela - święto!");
        } else if (dayOfTheWeekNr == 6) {
            System.out.println("Dziś jest sobota - wolne!");
        } else {
            System.out.println("Dziś jest dzień tygodnia - praca!");
        }

        System.out.println("\nTernary operator");
        String text = age >= 18 ? "dorosły" : "niepełnoletni";
        System.out.println("Jesteś " + text);
        if (age >= 18) {
            text = "dorosły";
        } else {
            text = "niepełnoletni";
        }
        System.out.println("Jesteś " + text);
        System.out.println(true ? 1 : 3.1415);

        System.out.println("\nSWITCH statement");
        int valueOnTheDice = 5;
        switch (valueOnTheDice) {
            case 1:
                System.out.println("Wyrzucono wartość 1");
                break;
            case 2:
                System.out.println("Wyrzucono wartość 2");
                break;
            case 3:
                System.out.println("Wyrzucono wartość 3");
                break;
            case 4:
                System.out.println("Wyrzucono wartość 4");
                break;
            case 5:
                System.out.println("Wyrzucono wartość 5");
                break;
            case 6:
                System.out.println("Wyrzucono wartość 6");
                break;
            default:
                System.out.println("Błąd albo kość zatrzymała się na kancie!");
        }
        switch (valueOnTheDice) {
            case 1:
                System.out.println("Wyrzucono wartość 1");
            case 2:
                System.out.println("Wyrzucono wartość 2");
            case 3:
                System.out.println("Wyrzucono wartość 3");
            case 4:
                System.out.println("Wyrzucono wartość 4");
            case 5:
                System.out.println("Wyrzucono wartość 5");
            case 6:
                System.out.println("Wyrzucono wartość 6");
            default:
                System.out.println("Błąd albo kość zatrzymała się na kancie!");
        }
        switch (valueOnTheDice) {
            case 1:
            case 3:
            case 5:
                System.out.println("Wyrzucono nieparzystą liczbę oczek");
                break;
            case 2:
            case 4:
            case 6:
                System.out.println("Wyrzucono parzystą liczbę oczek");
                break;
            default:
                System.out.println("Błąd albo kość zatrzymała się na kancie!");
        }
    }
}
