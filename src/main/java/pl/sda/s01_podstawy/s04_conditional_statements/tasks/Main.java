package pl.sda.s01_podstawy.s04_conditional_statements.tasks;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        System.out.println("##################");
        System.out.println("### EXERCISE 1 ###");
        System.out.println("##################");
        System.out.println("Zasymuluj rzut kostką do gry");
        System.out.println(" - wyświetl informację, czy wynik jest parzysty, czy nieparzysty");
        System.out.println(" - jeśli wyrzucono wartość 6 wypisz dodatkowo napis WYGRAŁEŚ");
        int valueOnTheDice = (int) (Math.random() * 6 + 1);
        if (valueOnTheDice % 2 == 1) {
            System.out.println("Odd number (" + valueOnTheDice + ")");
        } else {
            System.out.println("Even number (" + valueOnTheDice + ")");
            if (valueOnTheDice == 6) {
                System.out.println("You win");
            }
        }

        System.out.println("\n##################");
        System.out.println("### EXERCISE 2 ###");
        System.out.println("##################");
        System.out.println("Korzystając z klasy oferującej operacje na dacie i czasie pobierz:");
        System.out.println(" - aktualną godzinę");
        System.out.println(" - nr dnia tygodnia");
        System.out.println(" - nr dnia w miesiącu");
        System.out.println(" - nr miesiąca");
        System.out.println(" - informację o porze dnia (ustalając arbitralnie): rano, do południa, po południu, wieczór, noc");
        System.out.println(" - informację o dniu: pracujący, wolny, święto");
        System.out.println(" - informację, czy jest już po wypłacie (przyjmując, że wypłata przychodzi do 10 każdego miesiąca)");
        System.out.println(" - informację o porze roku: wiosna, lato, jesień, zima");
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalTime now = LocalTime.now();
        System.out.println("Current time: " + now);
        System.out.println("Day of week: " + localDate.getDayOfWeek().getValue());
        System.out.println("Day of month: " + localDate.getDayOfMonth());
        System.out.println("Month: " + localDate.getMonthValue());
        LocalTime part1 = LocalTime.parse("06:00:00");
        LocalTime part2 = LocalTime.parse("10:00:00");
        LocalTime part3 = LocalTime.parse("12:00:00");
        LocalTime part4 = LocalTime.parse("18:00:00");
        if (now.isBefore(part1)) {
            System.out.println(now + " - night");
        } else if (now.isBefore(part2)) {
            System.out.println(now + " - morning");
        } else if (now.isBefore(part3)) {
            System.out.println(now + " - noon");
        } else if (now.isBefore(part4)) {
            System.out.println(now + " - afternoon");
        } else {
            System.out.println(now + " - evening");
        }
        int dayOfWeek = DayOfWeek.from(localDate).getValue();
        int year = localDate.getYear();
        int easter;
        int d = (19 * (year % 19) + 24) % 30;
        int e = (2 * (year % 4) + 4 * (year % 7) + 6 * d + 5) % 7;
        LocalDate[] dates = {
                LocalDate.parse(year + "-01-01"),
                LocalDate.parse(year + "-01-06"),
                LocalDate.parse(year + "-03-22").plusDays(d + e),
                LocalDate.parse(year + "-03-22").plusDays(d + e + 1),
                LocalDate.parse(year + "-05-01"),
                LocalDate.parse(year + "-05-03"),
                LocalDate.parse(year + "-03-22").plusDays(d + e + 49),
                LocalDate.parse(year + "-03-22").plusDays(d + e + 60),
                LocalDate.parse(year + "-08-15"),
                LocalDate.parse(year + "-11-01"),
                LocalDate.parse(year + "-11-11"),
                LocalDate.parse(year + "-12-25"),
                LocalDate.parse(year + "-12-26")
        };
        if (dayOfWeek > 5) {
            System.out.println("Day off");
        } else if (Arrays.asList(dates).contains("localDate")) {
            System.out.println("Holiday");
        } else {
            System.out.println("Work day");
        }

        int month = localDate.getMonthValue();
        LocalDate parta = LocalDate.of(year, month, 10);
        if (parta.isAfter(localDate)) {
            System.out.println("Before payday");
        } else {
            System.out.println("After payday");
        }

        LocalDate spring = LocalDate.of(year, 3, 21);
        LocalDate summer = LocalDate.of(year, 6, 22);
        LocalDate autumn = LocalDate.of(year, 9, 23);
        LocalDate winter = LocalDate.of(year, 12, 22);
        if ((localDate.isEqual(spring) || localDate.isAfter(spring)) && localDate.isBefore(summer)) {
            System.out.println("Spring");
        } else if ((localDate.isEqual(summer) || localDate.isAfter(summer)) && localDate.isBefore(autumn)) {
            System.out.println("Summer");
        } else if ((localDate.isEqual(autumn) || localDate.isAfter(autumn)) && localDate.isBefore(winter)) {
            System.out.println("Autumn");
        } else {
            System.out.println("Winter");
        }
    }
}
