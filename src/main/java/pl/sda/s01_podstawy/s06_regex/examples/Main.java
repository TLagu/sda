package pl.sda.s01_podstawy.s06_regex.examples;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
/*
        System.out.print("E-mail: ");
        String firstText = scanner.nextLine();
        Pattern emailPattern = Pattern.compile("[a-zA-Z]+\\.*_*[a-zA-Z]*@[a-zA-Z0-9]+\\.[a-zA-Z]+");
        Matcher emailMatcher = emailPattern.matcher(firstText);
        System.out.println(emailMatcher.matches());
        emailMatcher.reset();
        System.out.println(emailMatcher.find());

        System.out.print("\nStreet: ");
        String secondText = scanner.nextLine();
        Pattern streetPattern = Pattern.compile("[au]l\\. [A-ZĄĆĘŁŃÓŚŹŻ][a-ząćęłńóśźż]{1,20} [0-9]{1,3}");
        Matcher streetMatcher = streetPattern.matcher(secondText);
        System.out.println(streetMatcher.matches());
        streetMatcher.reset();
        System.out.println(streetMatcher.find());
*/
        System.out.println("#################### Exercise 1 #######################");
        System.out.print("Address: ");
        String address = scanner.nextLine();
        Pattern postCodePattern = Pattern.compile("\\d{2}\\-\\d{3}");
        Matcher postCodeMatcher = postCodePattern.matcher(address);
        System.out.println("Post code: " + postCodeMatcher.find());
        Pattern addressPattern = Pattern.compile("\\d{2}\\-\\d{3} [A-ZĄĆĘŁŃÓŚŹŻ][a-ząćęłńóśźż]{1,20}");
        Matcher addressMatcher = addressPattern.matcher(address);
        System.out.println("Address: " + addressMatcher.find());
    }
}
