package pl.sda.s01_podstawy.e01_first_program.task01;

public class Main {
    private static final char upperLeftCorner = '/';
    private static final char upperRightCorner = '\\';
    private static final char lowerRightCorner = '/';
    private static final char lowerLeftCorner = '\\';
    private static final char horizontalLine = '-';
    private static final char verticalLine = '|';

    public static void main(String[] args) {
        System.out.println("##################");
        System.out.println("### EXERCISE 1 ###");
        System.out.println("##################");
        System.out.println("Napisz program (stwórz nową klasę z metodą main), który wyświetla na ekran Hello, <Twoje imię>! zamiast Hello, World!.");
        String myName = "Jan Kowalski";
        System.out.println("Hello, " + myName);
        System.out.println("");

        System.out.println("##################");
        System.out.println("### EXERCISE 2 ###");
        System.out.println("##################");
        System.out.println("Napisz program, który wyświetla Twoją wizytówkę.");
        String name = "Jan Kowalski";
        String street = "ul. Konstytucji 3-go Maja";
        String flatNumber = "12/11";
        String postCode = "00-000";
        String locality = "Szczecin";

        String line1 = name.trim();
        String line2 = (street + " " + flatNumber).trim();
        String line3 = (postCode + " " + locality).trim();

        int length = Math.max (Math.max (line1.length(), line2.length()), line3.length());
        if (length > 0) {
            showBackgroundLine(upperLeftCorner, horizontalLine, length + 2, upperRightCorner);
            //showBackgroundLine(verticalLine, ' ', length + 2, verticalLine);
            showTextLine(line1, length);
            showTextLine(line2, length);
            showTextLine(line3, length);
            //showBackgroundLine(verticalLine, ' ', length + 2, verticalLine);
            showBackgroundLine(lowerLeftCorner, horizontalLine, length + 2, lowerRightCorner);
        } else {
            System.out.println("All lines are blank!");
        }
    }

    private static int spacesBefore(String line, int len) {
        int lenght = (int) Math.floor(((double) len - (double) line.length()) / 2);
        return lenght;
    }

    private static int spacesAfter(String line, int len) {
        int lenght = (int) Math.ceil(((double) len - (double) line.length()) / 2);
        return lenght;
    }

    private static void multiplySign(char character, int count) {
        for (int i = 0; i < count; i++) {
            System.out.print(character);
        }
    }

    private static void showTextLine(String line, int len) {
        int spacesBefore;
        int spacesAfter;
        spacesBefore = spacesBefore(line, len);
        spacesAfter = spacesAfter(line, len);
        System.out.print(verticalLine);
        multiplySign(' ', spacesBefore + 1);
        System.out.print(line);
        multiplySign(' ', spacesAfter + 1);
        System.out.println(verticalLine);
    }

    private static void showBackgroundLine(char firstChar, char backgroundChar, int len, char lastChar) {
        System.out.print(firstChar);
        multiplySign(backgroundChar, len);
        System.out.println(lastChar);
    }
}
