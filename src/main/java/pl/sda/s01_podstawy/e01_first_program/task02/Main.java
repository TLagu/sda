package pl.sda.s01_podstawy.e01_first_program.task02;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    static Scanner in = new Scanner( System.in );
    static int maxButtonLen = 25;

    public static void main(String[] args) {
        clearScreen();
        menu();
    }

    static void menu() {
        int option = 0;
        String[] menuList = { "File", "Edit", "Very, very extremely long string", "Help", "Exit" };
        do {
            showMenu(menuList);
            switch( option ) {
                case 1 : firstOption();
                    break;
                case 2 : secondOption();
                    break;
                case 3 : thirdOption();
                    break;
                case 4 : forthOption();
                    break;
            }
            option = in.nextInt();
            clearScreen();
        } while( option != 5 );
    }

    static String showButton(String buttonName, Integer num) {
        String numString = " (" + num.toString() + ")";
        int lenNumber = numString.length();
        int lenString = buttonName.length();
        String name;
        int left = 0;
        int right = 0;
        if (lenString + lenNumber > maxButtonLen) {
            name = buttonName.substring(0, maxButtonLen - lenNumber - 4) + "..." + numString;
        } else {
            name = buttonName + numString;
            left = ((maxButtonLen - name.length()) / 2);
            right = maxButtonLen - name.length() - left;
        }
        return "| " + " ".repeat(left) + name + " ".repeat(right) + " |";
    }

    static void showMenu(String[] menuList) {
        int i = 0;
        for (String menuPos:menuList) {
            i++;
            System.out.print(showButton(menuPos, i));
        }
        System.out.println();
    }

    static void firstOption() {
        System.out.println( "First option" );
    }

    static void secondOption() {
        System.out.println( "Second option" );
    }

    static void thirdOption() {
        System.out.println( "Third option" );
    }

    static void forthOption() {
        System.out.println( "Forth option" );
    }

    public static void clearScreen() {
        try {
            if (System.getProperty("os.name").contains("Windows"))
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            else
                Runtime.getRuntime().exec("clear");
        } catch (IOException | InterruptedException ex) {}
    }
}
