package pl.sda.s01_podstawy.s08_classes.examples;

import java.util.Scanner;

public class Main {
    static int[][] ships = new int[10][10];
    static int building[][][];

    public static void main(String[] args) {

        System.out.println("Exercise 1");
        generateShipsBoard();
        int column = getShootValue("Podaj kolumnę: ");
        int row = getShootValue("Podaj wiersz: ");
        checkIfShipWasShoot(column, row);

        System.out.println("Exercise 2");
        classString stringExercises2 = new classString();
        System.out.println(stringExercises2.cutStringToMaxSize("asdfghjk", 4));
        stringExercises2.showStringIfLong("wewweqweqwe", 6);
        classInteger integerExercises3 = new classInteger();
        System.out.println(integerExercises3.sumFromOneToN(3));

        System.out.println("Exercise 3");
        building = new int[][][] {
                {{1, 4}, {3, 3}, {1, 1}, {2, 1}},
                {{2, 2}, {4, 5}, {1, 2}, {3, 7}},
                {{1, 1}, {2, 3}, {3, 2}, {4, 1}}
        };
        peopleLivingInBuilding();

        movePeopleFromApartment(2, 3, 1, 1);
        peopleLivingInBuilding();

        movePeopleFromApartment(2, 3, 1, 3);
        peopleLivingInBuilding();

        movePeopleToApartment(2, 3, 1, 3);
        peopleLivingInBuilding();

        movePeopleToApartment(2, 3, 1, 3);
        peopleLivingInBuilding();

        classWallet wallet = new classWallet();
        wallet.getMoneyInWallet();
        classMoney money = new classMoney();
        money.setZloty(5);
        money.setGrosz(50);
        wallet.addMoneyToWallet(money);

        wallet.getMoneyInWallet();

        money.setZloty(2);
        money.setGrosz(0);
        if (wallet.canAffordShopping(money)) {
            wallet.takeMoneyFromWallet(money);
        }
        wallet.getMoneyInWallet();

        money.setZloty(4);
        money.setGrosz(0);
        wallet.takeMoneyFromWallet(money);
        wallet.getMoneyInWallet();
    }

    private static void movePeopleFromApartment(int stairCase, int floor, int flat, int peopleLeaving) {
        if (building[stairCase][floor][flat] < peopleLeaving) {
            building[stairCase][floor][flat] = 0;
        } else {
            building[stairCase][floor][flat] -= peopleLeaving;
        }
    }

    private static void movePeopleToApartment(int stairCase, int floor, int flat, int peopleLeaving) {
        building[stairCase][floor][flat] += peopleLeaving;
    }

    private static void peopleLivingInBuilding() {
        int peopleLivingInBuilding = 0;
        for (int i = 0; i < building.length; i++) {
            for (int j = 0; j < building[i].length; j++) {
                for (int k = 0; k < building[i][j].length; k++) {
                    peopleLivingInBuilding += building[i][j][k];
                }
            }
        }
        System.out.println(peopleLivingInBuilding + " people are living in this building.");
    }


    static void checkIfShipWasShoot(int column, int row) {
        if (ships[column][row] == 1) {
            System.out.println("Trafiłeś!!!");
        } else {
            System.out.println("Pudło");
        }
    }

    static void generateShipsBoard () {
        ships[0][0] = 1;
        ships[0][1] = 1;
        ships[0][2] = 1;

        ships[2][4] = 1;
        ships[2][5] = 1;

        ships[6][6] = 1;
        ships[6][7] = 1;
        ships[6][8] = 1;
        ships[6][9] = 1;
    }

    static int getShootValue(String description) {
        Scanner scanner = new Scanner(System.in);
        System.out.print(description);
        return scanner.nextInt();
    }
}
