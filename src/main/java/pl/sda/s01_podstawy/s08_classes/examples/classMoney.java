package pl.sda.s01_podstawy.s08_classes.examples;

public class classMoney {
    private int zloty = 0;
    private int grosz = 0;

    void addMoney(classMoney money) {
        if (grosz + money.grosz > 99) {
            this.grosz = this.grosz + money.grosz - 100;
            this.zloty = this.zloty + money.zloty + 1;
        } else {
            this.grosz += money.grosz;
            this.zloty += money.zloty;
        }
    }

    void minusMoney(classMoney money) {
        if (this.isSmallerThan(money)) {
            System.out.println("We can't take that much money!");
            return;
        }
        if (this.grosz - money.grosz < 0) {
            this.grosz = this.grosz - money.grosz + 100;
            this.zloty = this.zloty - money.zloty - 1;
        } else {
            this.grosz -= money.grosz;
            this.zloty -= money.zloty;
        }
    }

    public int getZloty () {
        return this.zloty;
    }

    public void setZloty (int zloty) {
        this.zloty = zloty;
    }

    public int getGrosz () {
        return this.grosz;
    }

    public void setGrosz (int grosz) {
        this.grosz = grosz;
    }

    boolean isGreaterThenOrEquals(classMoney money) {
        if (this.zloty > money.getZloty()) {
            return true;
        }
        if (this.zloty == money.getZloty() && this.grosz > money.getGrosz()) {
            return true;
        }
        return false;
    }

    boolean isSmallerThan(classMoney money) {
        if (this.zloty < money.getZloty()) {
            return true;
        }
        if (this.zloty == money.getZloty() && this.grosz < money.getGrosz()) {
            return true;
        }
        return false;
    }
}
