package pl.sda.s01_podstawy.s08_classes.examples;

public class classString {
    String cutStringToMaxSize(String stringToCut, int maxStringSize) {
        if (stringToCut.length() <= maxStringSize) {
            return stringToCut;
        }
        return stringToCut.substring(0, maxStringSize);
    }
    void showStringIfLong(String stringToPrint, int minStringSize) {
        if (stringToPrint.length() < minStringSize) {
            return;
        }
        System.out.println(stringToPrint);
    }
}
