package pl.sda.s01_podstawy.s08_classes.examples;

public class classWallet {
    classMoney moneyInWallet = new classMoney();

    void addMoneyToWallet(classMoney money) {
        moneyInWallet.addMoney(money);
    }

    void takeMoneyFromWallet(classMoney money) {
        moneyInWallet.minusMoney(money);
    }

    void getMoneyInWallet () {
        System.out.println(String.format("in wallet we have %d zloty and %d groszy.", moneyInWallet.getZloty(), moneyInWallet.getGrosz()));
    }

    boolean canAffordShopping (classMoney money) {
        return moneyInWallet.isGreaterThenOrEquals(money);
    }
}
