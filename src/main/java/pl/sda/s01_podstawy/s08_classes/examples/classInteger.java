package pl.sda.s01_podstawy.s08_classes.examples;

public class classInteger {
    int sumFromOneToN (int n) {
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            sum += i;
        }
        return sum;
    }
}
