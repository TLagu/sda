package pl.sda.s01_podstawy.s08_classes.task03_packages.firstbookexample;

public class Book {
    private String title;
    private String author;
    private int numberOfPages;
    private float price;
    public static int numberOfCreatedBooks;

    public Book(String title, String author, int numberOfPages, float price) {
        this.title = title;
        this.author = author;
        this.numberOfPages = numberOfPages;
        this.price = price;
        this.numberOfCreatedBooks++;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public static void riseBookPrice(Book book, float price) {
        book.setPrice(book.price + price);
    }

    public void riseBookPrice(float price) {
        this.price += price;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", numberOfPages=" + numberOfPages +
                ", price=" + price +
                ", numberOfCreatedBooks=" + numberOfCreatedBooks +
                '}';
    }

}
