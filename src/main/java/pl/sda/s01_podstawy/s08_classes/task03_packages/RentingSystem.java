package pl.sda.s01_podstawy.s08_classes.task03_packages;

import pl.sda.s01_podstawy.s08_classes.task03_packages.secondbook.Book;

import java.util.Arrays;

public class RentingSystem {
    private Book[] booksToRent;

    public RentingSystem(Book[] booksToRent) {
        this.booksToRent = booksToRent;
    }

    public Book[] getBooksToRent() {
        return booksToRent;
    }

    public void addBookToCollection(Book newBook) {
        Book[] newBooksToRent = new Book[booksToRent.length + 1];
        for (int i = 0; i < booksToRent.length; i++) {
            newBooksToRent[i] = booksToRent[i];
        }
        newBooksToRent[booksToRent.length] = newBook;
        booksToRent = newBooksToRent;
    }

    public Book rentBook(String title) {
        for (Book book : booksToRent) {
            if (title.equals(book.getTitle())) {
                if (book.isRent()) {
                    System.out.println("The book is already on loan.");
                    return null;
                }
                book.setRent(true);
                System.out.println("The book has been borrowed.");
                return book;
            }
        }
        System.out.println("No books in the library.");
        return null;
    }

    public void returnBook(String title) {
        for (Book book : booksToRent) {
            if (title.equals(book.getTitle())) {
                if (book.isRent()) {
                    book.setRent(false);
                    System.out.println("The book has been returned.");
                    return;
                }
                System.out.println("The book was not on loan.");
                return;
            }
        }
        System.out.println("No books in the library.");
    }

    @Override
    public String toString() {
        return "RentingSystem{" +
                "booksToRent=" + Arrays.toString(booksToRent) +
                '}';
    }
}
