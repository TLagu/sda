package pl.sda.s01_podstawy.s08_classes.task03_packages;

import pl.sda.s01_podstawy.s08_classes.task03_packages.secondbook.Book;

public class FunctioningLibrary {
    public static void main(String[] args) {
        Book firstBook = new Book("Quo Vadis", "Homer", 350, 69.99f);
        Book secondBook = new Book("Krzyżacy", "Henryk Sienkiewicz", 300, 75.05f);
        Book thirdBook = new Book("Ogniem i mieczem", "Henryk Sienkiewicz", 280, 39.99f);
        Book[] threeBooks = new Book[] {firstBook, secondBook, thirdBook};
        RentingSystem rentingSystem = new RentingSystem(threeBooks);
        Book firstRentBook = rentingSystem.rentBook("Harry Potter");
        System.out.println(firstRentBook);
        rentingSystem.addBookToCollection(new Book("Harry Potter", "J. K. Rowling", 320, 27.50f));
        Book secondRentBook = rentingSystem.rentBook("Harry Potter");
        System.out.println(secondRentBook);
        Book thirdRentBook = rentingSystem.rentBook("Harry Potter");
        System.out.println(thirdRentBook);

        rentingSystem.returnBook("Harry Potter 2");
        System.out.println(rentingSystem.toString());
        rentingSystem.returnBook("Harry Potter");
        System.out.println(rentingSystem.toString());
    }
}
