package pl.sda.s01_podstawy.s08_classes.task03_packages.firstbookexample;

public class Library {
    public static void main(String[] args) {
        Book firstBook = new Book("Quo Vadis", "Homer", 350, 69.99f);
        System.out.println(firstBook);

        Book secondBook = new Book("Krzyżacy", "Henryk Sienkiewicz", 300, 75.05f);
        System.out.println(secondBook);
        System.out.println(firstBook);

        Book thirdBook = new Book("Ogniem i mieczem", "Henryk Sienkiewicz", 280, 39.99f);
        System.out.println(firstBook);
        System.out.println(secondBook);
        System.out.println(thirdBook);

        Book.riseBookPrice(thirdBook, 10.0f);
        System.out.println(thirdBook);
        thirdBook.riseBookPrice(10.0f);
        System.out.println(thirdBook);
    }
}
