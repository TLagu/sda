package pl.sda.s01_podstawy.s08_classes.task03_packages;

import pl.sda.s01_podstawy.s08_classes.task03_packages.secondbook.Book;

public class Library {
    public static void main(String[] args) {
        Book firstBook = new Book("Iliada", "Homer", 250, 30.0f);
        pl.sda.s01_podstawy.s08_classes.task03_packages.secondbook.Book secondBook =
                new pl.sda.s01_podstawy.s08_classes.task03_packages.secondbook.Book("Odysea", "Homer", 300, 30.0f);

    }
}
