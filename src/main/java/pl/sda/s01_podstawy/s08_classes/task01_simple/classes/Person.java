package pl.sda.s01_podstawy.s08_classes.task01_simple.classes;

import pl.sda.s01_podstawy.s08_classes.task01_simple.utils.Gender;
import pl.sda.s01_podstawy.s08_classes.task01_simple.utils.Helper;
import pl.sda.s01_podstawy.s08_classes.task01_simple.utils.PersonHelper;
import pl.sda.s01_podstawy.s08_classes.task01_simple.utils.StaticValues;

public class Person {
    private String name;
    private String surname;
    private Gender gender;
    private Integer age;
    private String pesel;

    public Person(String name, String surname, Gender gender, int age, String pesel) {
        this.setName(name);
        this.setSurname(surname);
        this.setGender(gender);
        this.setAge(age);
        this.setPesel(pesel);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = PersonHelper.validateAndGetValue(StaticValues.NAME_PATTERN, name, "name");
    }

    public String getSurname() {
        return this.surname;
    }

    public void setSurname(String surname) {
        this.surname = PersonHelper.validateAndGetValue(StaticValues.SURNAME_PATTERN, surname, "surname");
    }

    public Gender getGender() {
        return this.gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = Helper.validateAndGetValue(StaticValues.MIN_AGE,
                StaticValues.MAX_AGE,
                age,
                this.gender.getLabel());
    }

    public String getPesel() {
        return this.pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = PersonHelper.validateAndGetValue(StaticValues.PESEL_PATTERN, pesel, "pesel");
    }

    public String getFullName() {
        return String.format("%s %s", this.getName(), this.getSurname());
    }

    public String hasReachedRetirementAge() {
        return PersonHelper.hasReachedRetirementAge(this.gender, this.getFullName(), this.age);
    }

    public String getNumberOfYearsUntilRetirement() {
        return PersonHelper.getNumberOfYearsUntilRetirement(this.gender, this.getFullName(), this.age);
    }

    public String getAgeDifference(Person person) {
        return PersonHelper.getAgeDifference(this, person);
    }

    public String getPersonData() {
        StringBuilder data = new StringBuilder(String.format("Data of person %s:", this.getFullName()));
        data.append(String.format("\n- Gender: %s", this.getGender()));
        data.append(String.format("\n- Age: %d", this.getAge()));
        data.append(String.format("\n- PESEL: %s", this.getPesel()));
        return data.toString();
    }
}
