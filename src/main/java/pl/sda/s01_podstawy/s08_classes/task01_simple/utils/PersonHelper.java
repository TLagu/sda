package pl.sda.s01_podstawy.s08_classes.task01_simple.utils;

import pl.sda.s01_podstawy.s08_classes.task01_simple.classes.Person;

public class PersonHelper extends Helper {

    public static String hasReachedRetirementAge(Gender gender, String fullName, int age) {
        if (gender == null) {
            return String.format("No gender filled (%s)", fullName);
        }
        String template = (age >= gender.getAge()) ? "%s has reached retirement age." : "%s has not reached retirement age.";
        return String.format(template, fullName);
    }

    public static String getNumberOfYearsUntilRetirement(Gender gender, String fullName, int age) {
        if (gender == null) {
            return String.format("No gender filled (%s)", fullName);
        }
        if (age < gender.getAge()) {
            return String.format("%s has %d years left until his retirement.", fullName, gender.getAge() - age);
        }
        return String.format("%s has reached retirement age.", fullName);
    }

    public static String getAgeDifference(Person firstPerson, Person secondPerson) {
        return String.format("The age difference between %s and %s is %d.",
                firstPerson.getFullName(),
                secondPerson.getFullName(),
                Math.abs(firstPerson.getAge() - secondPerson.getAge()));
    }
}
