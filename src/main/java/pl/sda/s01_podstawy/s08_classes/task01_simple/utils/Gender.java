package pl.sda.s01_podstawy.s08_classes.task01_simple.utils;

public enum Gender {
    MALE("Male", 65),
    FEMALE("Female", 60);

    private final String label;
    private final int age;

    Gender(String label, int age) {
        this.label = label;
        this.age = age;
    }

    public String getLabel() {
        return this.label;
    }

    public int getAge() {
        return this.age;
    }

    @Override
    public String toString() {
        return this.label;
    }
}
