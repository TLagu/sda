package pl.sda.s01_podstawy.s08_classes.task01_simple.utils;

import pl.sda.s01_podstawy.s08_classes.task01_simple.classes.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Helper {
    public static String validateAndGetValue(String pattern, String value, String fieldName) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(value);
        if (m.matches()) {
            return value;
        }
        Logger.log(String.format("Incorrect %s value.", fieldName));
        return null;
    }

    public static Integer validateAndGetValue(int min, int max, int value, String fieldName) {
        if (min <= value && value <= max) {
            return value;
        }
        Logger.log(String.format("Incorrect %s value.", fieldName));
        return null;
    }
}
