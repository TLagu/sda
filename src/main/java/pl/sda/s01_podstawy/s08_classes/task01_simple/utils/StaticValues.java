package pl.sda.s01_podstawy.s08_classes.task01_simple.utils;

public class StaticValues {
    public static final String NAME_PATTERN = "[A-ZĄĆĘŁŃÓŚŹŻ][a-ząćęłńóśźż]{2,29}";
    public static final String SURNAME_PATTERN = "([a-z]{2,3} ){0,1}[A-ZĄĆĘŁŃÓŚŹŻ][a-ząćęłńóśźż]{2,29}(-[A-ZĄĆĘŁŃÓŚŹŻ][a-ząćęłńóśźż]{2,29}){0,1}";
    public static final String PESEL_PATTERN = "\\d{11}";
    public static final int MIN_AGE = 0;
    public static final int MAX_AGE = 120;
}
