package pl.sda.s01_podstawy.s08_classes.task02_grocery_shop;

public class Product {
    private String name;
    private int amount;
    private String unit;
    private float price;

    public Product() {
    }

    public Product(String name, int amount, String unit, float price) {
        this.name = name;
        this.amount = amount;
        this.unit = unit;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getUnit() {
        return unit;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", amount=" + amount +
                ", unit='" + unit + '\'' +
                ", price=" + price +
                '}';
    }
}
