package pl.sda.s01_podstawy.s08_classes.task02_grocery_shop;

import java.util.Arrays;
import java.util.Date;

public class GroceryStore {
    public static void main(String[] args) {
        Warehouse warehouse = new Warehouse();

        Supplier deliverySupplier = new Supplier(
                "ul. Kwiatowa 5",
                "Mleczarnia szczęśliwa krowa",
                1234567890);

        Product[] products = new Product[4];
        products[0] = new Product("Mleko", 3, "sztuk", 2.50f);
        products[1] = new Product("Ser", 350, "kilogram", 22.40f);
        products[2] = new Product("Pomidor", 60, "kilogram", 7.03f);
        products[3] = new Product("Cebula", 40, "kilogram", 2.70f);

        Delivery nextDelivery = new Delivery(new Date(), deliverySupplier, 199.99f, products);

        System.out.println(deliverySupplier.toString());
        System.out.println(Arrays.toString(products));
        System.out.println(nextDelivery.toString());
        warehouse.setNextDelivery(nextDelivery);
    }
}
