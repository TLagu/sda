package pl.sda.s01_podstawy.s08_classes.task02_grocery_shop;

import java.util.Arrays;
import java.util.Date;

public class Delivery {
    private Date deliveryDate;
    private Supplier supplier;
    private float deliveryPrice;
    private Product[] deliveredProducts;

    public Delivery(Date deliveryDate, Supplier supplier, float deliveryPrice, Product[] deliveredProducts) {
        this.deliveryDate = deliveryDate;
        this.supplier = supplier;
        this.deliveryPrice = deliveryPrice;
        this.deliveredProducts = deliveredProducts;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public float getDeliveryPrice() {
        return deliveryPrice;
    }

    public void setDeliveryPrice(float deliveryPrice) {
        this.deliveryPrice = deliveryPrice;
    }

    public Product[] getDeliveredProducts() {
        return deliveredProducts;
    }

    public void setDeliveredProducts(Product[] deliveredProducts) {
        this.deliveredProducts = deliveredProducts;
    }

    @Override
    public String toString() {
        return "Delivery{" +
                "deliveryDate=" + deliveryDate +
                ", supplier=" + supplier +
                ", deliveryPrice=" + deliveryPrice +
                ", deliveredProducts=" + Arrays.toString(deliveredProducts) +
                '}';
    }
}
