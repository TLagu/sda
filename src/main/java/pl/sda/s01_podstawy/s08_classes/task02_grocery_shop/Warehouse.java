package pl.sda.s01_podstawy.s08_classes.task02_grocery_shop;

import java.util.Arrays;

public class Warehouse {
    Product[] productsInStore = new Product[1000];
    Delivery nextDelivery;

    public Product[] getProductsInStore() {
        return productsInStore;
    }

    public void setProductsInStore(Product[] productsInStore) {
        this.productsInStore = productsInStore;
    }

    public Delivery getNextDelivery() {
        return nextDelivery;
    }

    public void setNextDelivery(Delivery nextDelivery) {
        this.nextDelivery = nextDelivery;
    }

    @Override
    public String toString() {
        return "Warehouse{" +
                "productsInStore=" + Arrays.toString(productsInStore) +
                ", nextDelivery=" + nextDelivery +
                '}';
    }
}
