package pl.sda.s01_podstawy.s08_classes.task07_computer_shop.common;

import java.util.ArrayList;
import java.util.List;

public class HierarchicalCodeLists_tmp2<T> {
    private T data;
    private List<HierarchicalCodeLists_tmp2<T>> children = new ArrayList<>();
    private HierarchicalCodeLists_tmp2<T> parent;

    public HierarchicalCodeLists_tmp2(T data) {
        this.data = data;
    }

    public HierarchicalCodeLists_tmp2<T> addChild(HierarchicalCodeLists_tmp2<T> child) {
        child.setParent(this);
        this.children.add(child);
        return child;
    }

    public void addChildren(List<HierarchicalCodeLists_tmp2<T>> children) {
        children.forEach(each -> each.setParent(this));
        this.children.addAll(children);
    }

    public List<HierarchicalCodeLists_tmp2<T>> getChildren() {
        return children;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    private void setParent(HierarchicalCodeLists_tmp2<T> parent) {
        this.parent = parent;
    }

    public HierarchicalCodeLists_tmp2<T> getParent() {
        return parent;
    }
}
