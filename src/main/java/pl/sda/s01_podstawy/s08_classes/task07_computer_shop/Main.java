package pl.sda.s01_podstawy.s08_classes.task07_computer_shop;

import pl.sda.s01_podstawy.s08_classes.task07_computer_shop.common.HierarchicalCodeLists_tmp2;
import pl.sda.s01_podstawy.s08_classes.task07_computer_shop.computerEquipment.ProcessorCodeLists;

public class Main {
    private static ProcessorCodeLists processorCodeLists;

    public static void main(String[] args) {
        createData();
    }

    private static void createData() {
        processorCodeLists = new ProcessorCodeLists();
        HierarchicalCodeLists_tmp2<String> root = new HierarchicalCodeLists_tmp2<>("root");
    }
}
