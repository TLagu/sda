package pl.sda.s01_podstawy.s08_classes.task07_computer_shop.computerEquipment;

import java.util.ArrayList;

public class CodeLists {

    class HierarchicalCodeList {
        int parent;
        String value;

        public HierarchicalCodeList(int newParent, String newValue) {
            parent =  newParent;
            value = newValue;
        }

        public int getParent(){ return parent; }
        public String getValue(){ return value; }
        public void setParent(int newParent){ parent =  newParent; }
        public void setValue(String newValue){ value = newValue; }
    }

    public static void addToList (ArrayList<String> originalList,
                                  String stringToAdd) {
        for (String value: originalList) {
            if (value.equals(stringToAdd)) {
                System.out.println("Already found '" + stringToAdd + "'. Could not add to list.");
            }
        }
        originalList.add(stringToAdd);
    }

    public static boolean changeFromTo (ArrayList<String> originalList,
                                        String fromString,
                                        String toString,
                                        String content) {
        for (int i = 0; i < originalList.size(); i++) {
            if (originalList.get(i).equals(fromString)) {
                originalList.set(i, toString);
                System.out.println("'" + fromString + "' changed to '" + toString + "'.");
                return true;
            }
        }
        System.out.println("Could not find '" + content + "'.");
        return false;
    }

    public static Integer getIndex (ArrayList<String> originalList,
                                    String stringToFind) {
        for (int i = 0; i < originalList.size(); i++) {
            if (originalList.get(i).equals(stringToFind)) return i;
        }
        return null;
    }

    public static void addToList2D (ArrayList<HierarchicalCodeList> originalList,
                                    HierarchicalCodeList rowToAdd) {
        for (HierarchicalCodeList value: originalList) {
            if (value.getParent() == rowToAdd.getParent() && value.getValue().equals(rowToAdd.getValue())) {
                System.out.println("Already found '" + rowToAdd.getValue() + "'. Could not add to list.");
            }
        }
        originalList.add(rowToAdd);
    }

    public static boolean changeFromTo2D (ArrayList<HierarchicalCodeList> originalList,
                                          HierarchicalCodeList fromRow,
                                          String toString,
                                          String content) {
        for (HierarchicalCodeList hierarchicalCodeList : originalList) {
            if (hierarchicalCodeList.getParent() == fromRow.getParent() && hierarchicalCodeList.getValue().equals(fromRow.getValue())) {
                hierarchicalCodeList.setValue(toString);
                System.out.println("'" + fromRow.getValue() + "' changed to '" + toString + "'.");
                return true;
            }
        }
        System.out.println("Could not find '" + content + "'.");
        return false;
    }

    public static Integer getIndex2D (ArrayList<HierarchicalCodeList> originalList,
                                      HierarchicalCodeList rowToFind) {
        for (int i = 0; i < originalList.size(); i++) {
            if (originalList.get(i).getParent() == rowToFind.getParent() && originalList.get(i).getValue().equals(rowToFind.getValue())) return i;
        }
        return null;
    }

}
