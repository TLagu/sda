package pl.sda.s01_podstawy.s08_classes.task07_computer_shop.computerEquipment;

import java.util.ArrayList;

public class ProcessorCodeLists extends CodeLists {
    private static ArrayList<HierarchicalCodeList> processorSeriesList = new ArrayList<>();
    private static ArrayList<HierarchicalCodeList> processorTypeList = new ArrayList<>();
    private static ArrayList<String> processorProducerList = new ArrayList<>();
    private final String processorProducerName = "Processor producer";
    private static ArrayList<String> processorSocketList = new ArrayList<>();
    private static ArrayList<String> processorGraphicCardList = new ArrayList<>();

    public ProcessorCodeLists () {
        int parentIndex;
        int parentIndex2;
        CodeLists.addToList(processorProducerList, "AMD");
        parentIndex = CodeLists.getIndex(processorProducerList,"AMD");
        CodeLists.addToList2D(processorTypeList, new HierarchicalCodeList(parentIndex, "Ryzen 3"));
        parentIndex2 = CodeLists.getIndex2D(processorTypeList, new CodeLists.HierarchicalCodeList(parentIndex, "Ryzen 3"));
        CodeLists.addToList2D(processorSeriesList, new HierarchicalCodeList(parentIndex2, "1000"));
        CodeLists.addToList2D(processorSeriesList, new HierarchicalCodeList(parentIndex2, "2000"));
        CodeLists.addToList2D(processorSeriesList, new HierarchicalCodeList(parentIndex2, "3000"));
        CodeLists.addToList2D(processorTypeList, new HierarchicalCodeList(parentIndex, "Ryzen 5"));
        CodeLists.addToList2D(processorTypeList, new HierarchicalCodeList(parentIndex, "Ryzen 7"));
        CodeLists.addToList2D(processorTypeList, new HierarchicalCodeList(parentIndex, "Ryzen 9"));
        CodeLists.addToList(processorProducerList, "Intel");
        parentIndex = CodeLists.getIndex(processorProducerList,"Intel");
        CodeLists.addToList2D(processorTypeList, new HierarchicalCodeList(parentIndex, "Intel i3"));
        CodeLists.addToList2D(processorTypeList, new HierarchicalCodeList(parentIndex, "Intel i5"));
        CodeLists.addToList2D(processorTypeList, new HierarchicalCodeList(parentIndex, "Intel i7"));
        CodeLists.addToList2D(processorTypeList, new HierarchicalCodeList(parentIndex, "Intel i9"));
        CodeLists.addToList(processorProducerList, "AMD");
        CodeLists.addToList2D(processorTypeList,
                new HierarchicalCodeList(CodeLists.getIndex(processorProducerList,"AMD"), "Ryzen 3"));
    }

}
