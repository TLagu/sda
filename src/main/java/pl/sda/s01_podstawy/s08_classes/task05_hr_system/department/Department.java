package pl.sda.s01_podstawy.s08_classes.task05_hr_system.department;

import pl.sda.s01_podstawy.s08_classes.task05_hr_system.common.Address;
import pl.sda.s01_podstawy.s08_classes.task05_hr_system.employee.Employee;

import java.util.List;

// stworzyc pola przechowujace dane o placowce firmy:
// nazwa placowki, adres (skorzystaj z klasy Address),
// tablica pracownikow Employee[], miesieczny koszt utrzymania lokalu,
// szef placowki - rowniez klasa Employee
// stworzyc konstruktor, gettery, settery, toString

// stworzyc metode, ktora mowi, ile zarabiaja w sumie pracownicy placowki

// stworzyc metode, ktora zwraca calkowity koszt utrzymania placowki

public class Department {
    private String name;
    private Address address;
    private List<Employee> employees;
    private double costOfPremises;
    private Employee boss;

    public Department(String name, Address address, List<Employee> employees, double costOfPremises, Employee boss) {
        this.name = name;
        this.address = address;
        this.employees = employees;
        this.costOfPremises = costOfPremises;
        this.boss = boss;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public double getCostOfPremises() {
        return costOfPremises;
    }

    public void setCostOfPremises(double costOfPremises) {
        this.costOfPremises = costOfPremises;
    }

    public Employee getBoss() {
        return boss;
    }

    public void setBoss(Employee boss) {
        this.boss = boss;
    }

    public double getAllEarnings() {
        double sum = 0;
        for (Employee employee: employees) sum += employee.getEarnings();
        return sum;
    }

    public double getCosts() {
        double sum;
        sum = this.getAllEarnings();
        sum += this.costOfPremises;
        return sum;
    }

    public int getNumberOfEmployee (String firstName, String lastName) {
        for (int i = 0; i < this.employees.size(); i++) {
            if (this.employees.get(i).getFirstName().equals(firstName)
                    && this.employees.get(i).getLastName().equals(lastName)) {
                return i;
            }
        }
        return -1;
    }

    public Employee getEmployee (String firstName, String lastName) {
        for (Employee employee: this.employees) {
            if (employee.getFirstName().equals(firstName) && employee.getLastName().equals(lastName)) {
                return employee;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "Department{" +
                "name='" + name + '\'' +
                ", address=" + address +
                ", employees=" + employees.toString() +
                ", costOfPremises=" + costOfPremises +
                ", boss=" + boss +
                '}';
    }
}
