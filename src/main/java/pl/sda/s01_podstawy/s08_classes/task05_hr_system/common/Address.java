package pl.sda.s01_podstawy.s08_classes.task05_hr_system.common;

// stworzyc pola przechowujace adres
// zostawiam wam dowolnosc, ale pamietajcie o konstruktorze, getterach, setterach i toString

public class Address {
    private String street;
    private String streetNo;
    private String flatNo;
    private String city;
    private String postCode;

    public Address (String street, String streetNo, String flatNo, String city, String postCode) {
        this.street = street;
        this.streetNo = streetNo;
        this.flatNo = flatNo;
        this.city = city;
        this.postCode = postCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNo() {
        return streetNo;
    }

    public void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    public String getFlatNo() {
        return flatNo;
    }

    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @Override
    public String toString() {
        return "Address{" +
                "street='" + street + '\'' +
                ", streetNo='" + streetNo + '\'' +
                ", flatNo='" + flatNo + '\'' +
                ", city='" + city + '\'' +
                ", postCode='" + postCode + '\'' +
                '}';
    }
}
