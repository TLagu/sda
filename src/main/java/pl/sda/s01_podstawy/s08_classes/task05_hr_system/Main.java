package pl.sda.s01_podstawy.s08_classes.task05_hr_system;

import pl.sda.s01_podstawy.s08_classes.task05_hr_system.common.Address;
import pl.sda.s01_podstawy.s08_classes.task05_hr_system.department.Department;
import pl.sda.s01_podstawy.s08_classes.task05_hr_system.department.DepartmentUtils;
import pl.sda.s01_podstawy.s08_classes.task05_hr_system.employee.Employee;

import java.util.ArrayList;
import java.util.List;

public class Main {
    private static List<Department> departments = new ArrayList<>();

    public static void main(String[] args) {
        createDepartments();
        int sum = 0;
        for (Department department: departments) {
            System.out.println(department.getName() + ": " + department.getCosts());
            sum += department.getCosts();
        }
        System.out.println("All costs: " + sum);
        departments.get(0).getEmployees().get(0).giveARise(300);
        sum = 0;
        for (Department department: departments) {
            System.out.println(department.getName() + ": " + department.getCosts());
            sum += department.getCosts();
        }
        System.out.println("All costs: " + sum);
        Employee employee = departments.get(0).getEmployee("Jan", "Kowalski");
        if (employee != null) {
            DepartmentUtils departmentUtils = new DepartmentUtils();
            departmentUtils.relocateEmployee(departments.get(0), employee, departments.get(1));
            sum = 0;
            for (Department department : departments) {
                System.out.println(department.getName() + ": " + department.getCosts());
                sum += department.getCosts();
            }
            System.out.println("All costs: " + sum);
        }
    }

    private static void createDepartments() {
        List<Employee> employees1 = new ArrayList<>();
        employees1.add(new Employee(
                "Jan",
                "Kowalski",
                "12345678901",
                new Address("ul. Wojska Polskiego", "12", "10", "Chrząszczyce", "00-000"),
                7000.0d,
                "Pracownik"
        ));
        employees1.add(new Employee(
                "Krzysztof",
                "Nowak",
                "12345678901",
                new Address("ul. Piastów", "1", "4", "Wąszczyce", "00-000"),
                6000.0d,
                "Pracownik"
        ));
        employees1.add(new Employee(
                "Grzegorz",
                "Brzęczyszczykiewicz",
                "12345678901",
                new Address("ul. Wojska Polskiego", "12", "10", "Chrząszczyce", "00-000"),
                6500.0d,
                "Pracownik"
        ));
        employees1.add(new Employee(
                "Helga",
                "Niedomagalska",
                "12345678901",
                new Address("ul. Wojska Polskiego", "12", "10", "Chrząszczyce", "00-000"),
                7100.0d,
                "Pracownik"
        ));
        employees1.add(new Employee(
                "Henryk",
                "Walezy",
                "12345678901",
                new Address("ul. Wojska Polskiego", "12", "10", "Chrząszczyce", "00-000"),
                7200.0d,
                "Pracownik"
        ));
        employees1.add(new Employee(
                "Jan",
                "Poniatowski",
                "12345678901",
                new Address("ul. Wojska Polskiego", "12", "10", "Chrząszczyce", "00-000"),
                7400.0d,
                "Pracownik"
        ));
        departments.add(new Department("Department no 1",
                new Address("Street", "1", "2", "Szczecin", "00-000"),
                employees1,
                15000.0d,
                new Employee(
                        "Krzyszfof",
                        "Bęcki",
                        "12345678901",
                        new Address("ul. Wojska Polskiego", "12", "10", "Chrząszczyce", "00-000"),
                        10000.0d,
                        "Kierownik"
                )
        ));
        List<Employee> employees2 = new ArrayList<>();
        employees2.add(new Employee(
                "Jan",
                "Kowalski",
                "12345678901",
                new Address("ul. Wojska Polskiego", "12", "10", "Chrząszczyce", "00-000"),
                7000.0d,
                "Pracownik"
        ));
        employees2.add(new Employee(
                "Krzysztof",
                "Nowak",
                "12345678901",
                new Address("ul. Piastów", "1", "4", "Wąszczyce", "00-000"),
                6000.0d,
                "Pracownik"
        ));
        employees2.add(new Employee(
                "Grzegorz",
                "Brzęczyszczykiewicz",
                "12345678901",
                new Address("ul. Wojska Polskiego", "12", "10", "Chrząszczyce", "00-000"),
                6500.0d,
                "Pracownik"
        ));
        employees2.add(new Employee(
                "Helga",
                "Niedomagalska",
                "12345678901",
                new Address("ul. Wojska Polskiego", "12", "10", "Chrząszczyce", "00-000"),
                7100.0d,
                "Pracownik"
        ));
        departments.add(new Department("Department no 2",
                new Address("Street", "1", "2", "Szczecin", "00-000"),
                employees2,
                12000.0d,
                new Employee(
                        "Krzyszfof",
                        "Bęcki",
                        "12345678901",
                        new Address("ul. Wojska Polskiego", "12", "10", "Chrząszczyce", "00-000"),
                        10000.0d,
                        "Kierownik"
                )
        ));

    }
}
