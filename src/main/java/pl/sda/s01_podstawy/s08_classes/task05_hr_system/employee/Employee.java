package pl.sda.s01_podstawy.s08_classes.task05_hr_system.employee;

// stworzyc pola prywatne, ktore beda przechowywac informacje o pracowniku:
// imie, nazwisko, pesel, adres, miesieczne zarobki, stanowisko
// do adresu uzyc klasy Address
// stworzyc konstruktor, gettery, settery i toString
// zakladamy, ze imie i nazwisko sie nie zmieniaja, wiec nie dajemy setterow do tych dwoch pol

// stworzyc metode, ktora bedzie dawac podwyzke pracownikowi (po prostu zwiekszy jego zarobki o kwote podana
// w parametrze metody)

import pl.sda.s01_podstawy.s08_classes.task05_hr_system.common.Address;

public class Employee {
    private final String firstName;
    private final String lastName;
    private String pesel;
    private Address address;
    private double earnings;
    private String position;

    public Employee(String firstName, String lastName, String pesel, Address address, double earnings, String position) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
        this.address = address;
        this.earnings = earnings;
        this.position = position;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Double getEarnings() {
        return earnings;
    }

    public void setEarnings(Double earnings) {
        this.earnings = earnings;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void giveARise(double rise) {
        this.earnings += rise;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", pesel='" + pesel + '\'' +
                ", address=" + address +
                ", earnings=" + earnings +
                ", position='" + position + '\'' +
                '}';
    }
}
