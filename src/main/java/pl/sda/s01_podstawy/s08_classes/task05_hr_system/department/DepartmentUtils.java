package pl.sda.s01_podstawy.s08_classes.task05_hr_system.department;

import pl.sda.s01_podstawy.s08_classes.task05_hr_system.employee.Employee;

// stworzyc cialo ponizszej metody statycznej, ktora przerzuci pracownika z jednej placowki do drugiej

// przy pomocy metody .equals() znaleźć pracownika w relocateFromDepartment i potem analogicznie jak
// w klasie RentingSystem.addBookToCollection zwiekszyc liste pracownikow w relocateToDepartment
// a nastepnie zabrac tego jednego pracownika z relocateFromDepartment i zmniejszyc rozmiar tablicy o 1

public class DepartmentUtils {

    public static void relocateEmployee(Department relocateFromDepartment,
                                        Employee employeeToRelocate,
                                        Department relocateToDepartment) {
        int i = relocateFromDepartment.getNumberOfEmployee(employeeToRelocate.getFirstName(), employeeToRelocate.getLastName());
        if (i != -1) {
            relocateToDepartment.getEmployees().add(employeeToRelocate);
            relocateFromDepartment.getEmployees().remove(i);
        } else {
            System.out.println("I did not find an employee.");
        }
    }
}
