package pl.sda.s01_podstawy.s08_classes.task06_last_day.timeExamples;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class TimeCounter {
    public static void main(String[] args) {
        LocalTime localTime = LocalTime.now();
        System.out.println(localTime.toString());
        LocalDate localDate = LocalDate.now();
        System.out.println(localDate.toString());
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(localDateTime.toString());

        LocalTime secondLocalTime = LocalTime.now();
        secondLocalTime = secondLocalTime.minusHours(localTime.getHour())
                .minusMinutes(localTime.getMinute())
                .minusSeconds(localTime.getSecond())
                .minusNanos(localTime.getNano());
        System.out.println(secondLocalTime);
    }
}
