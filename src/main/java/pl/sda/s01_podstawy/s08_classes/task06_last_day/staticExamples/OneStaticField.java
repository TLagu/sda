package pl.sda.s01_podstawy.s08_classes.task06_last_day.staticExamples;

public class OneStaticField {
    private static int counter = 0;

    public int getRandom() {
        return random;
    }

    public void setRandom(int random) {
        this.random = random;
    }

    private int random = 10;

    public OneStaticField() {
        counter++;
    }

    public int getCounter() {
        return this.counter;
    }
}
