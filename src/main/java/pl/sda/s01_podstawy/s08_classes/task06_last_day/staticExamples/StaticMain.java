package pl.sda.s01_podstawy.s08_classes.task06_last_day.staticExamples;

import java.time.LocalTime;


public class StaticMain {
    public static void main(String[] args) {
        StaticUtils.showHoursAndMinutesOnly(LocalTime.now());
        StaticUtils.showHoursMinutesAndSecondsOnly(LocalTime.now());
        StaticUtils.showTime(LocalTime.now(), StaticUtils.hourMinuteAndSecondPattern);
        /*OneStaticField objectOne = new OneStaticField();
        System.out.println(objectOne.getCounter());
        OneStaticField objectTwo = new OneStaticField();
        System.out.println(objectOne.getCounter());
        OneStaticField objectThree = new OneStaticField();
        System.out.println(objectOne.getCounter());
        OneStaticField objectFor = new OneStaticField();
        System.out.println(objectOne.getCounter());*/

        OneStaticField[] fields = new OneStaticField[10];
        System.out.println(fields[0].getCounter());
        OneStaticField dummy = new OneStaticField();
        for (int i = 0; i < fields.length; i++) {
            fields[i] = dummy;
        }
        System.out.println(fields[0].getCounter());
        fields[0].setRandom(20);
        fields[1].setRandom(20);
        for (OneStaticField field: fields) {
            System.out.println(field.getRandom() + ", ");
        }

    }
}
