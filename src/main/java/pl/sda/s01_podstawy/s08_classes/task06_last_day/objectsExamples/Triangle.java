package pl.sda.s01_podstawy.s08_classes.task06_last_day.objectsExamples;

public class Triangle {
    private double sideALength;
    private double sideBLength;
    private double sideCLength;

    public Triangle(double sideALength, double sideBLength, double sideCLength) {
        this.sideALength = sideALength;
        this.sideBLength = sideBLength;
        this.sideCLength = sideCLength;
    }

    public double getSideALength() {
        return sideALength;
    }

    public void setSideALength(double sideALength) {
        this.sideALength = sideALength;
    }

    public double getSideBLength() {
        return sideBLength;
    }

    public void setSideBLength(double sideBLength) {
        this.sideBLength = sideBLength;
    }

    public double getSideCLength() {
        return sideCLength;
    }

    public void setSideCLength(double sideCLength) {
        this.sideCLength = sideCLength;
    }

    public double getArea() {
        double p = this.getCircumference() / 2;
        return Math.sqrt(p * (p - sideALength) * (p - sideBLength) * (p - sideCLength));
    }

    public double getCircumference() {
        return sideALength + sideBLength + sideCLength;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "sideALength=" + sideALength +
                ", sideBLength=" + sideBLength +
                ", sideCLength=" + sideCLength +
                '}';
    }
}
