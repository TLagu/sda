package pl.sda.s01_podstawy.s08_classes.task06_last_day.timeExamples;

import java.time.LocalDate;

public class TimeExcersises {
    public static void main(String[] args) {
        ApocalypseCounter apocalypseTime = new ApocalypseCounter(LocalDate.of(2123, 11, 22));
        System.out.println("Number of days to apocalypse: " + apocalypseTime.timeLeft(LocalDate.now()));
        MatrixFiller matrixFiller = new MatrixFiller ();
        matrixFiller.fillArray();
    }
}
