package pl.sda.s01_podstawy.s08_classes.task06_last_day.objectsExamples;

public class Rectangle {
    private double sideALength;
    private double sideBLength;

    public Rectangle(double sideALength, double sideBLength) {
        this.sideALength = sideALength;
        this.sideBLength = sideBLength;
    }

    public double getSideALength() {
        return sideALength;
    }

    public void setSideALength(double sideALength) {
        this.sideALength = sideALength;
    }

    public double getSideBLength() {
        return sideBLength;
    }

    public void setSideBLength(double sideBLength) {
        this.sideBLength = sideBLength;
    }

    public double getArea() {
        return sideALength * sideBLength;
    }

    public double getCircumference() {
        return 2 * sideALength + 2 * sideBLength;
    }

    @Override
    public String toString() {
        return "Square{" +
                "sideALength=" + sideALength +
                "sideBLength=" + sideBLength +
                '}';
    }
}
