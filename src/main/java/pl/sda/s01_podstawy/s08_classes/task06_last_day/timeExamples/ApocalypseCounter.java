package pl.sda.s01_podstawy.s08_classes.task06_last_day.timeExamples;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class ApocalypseCounter {
    private LocalDate apocalypseTime;

    public ApocalypseCounter(LocalDate apocalypseTime) {
        this.apocalypseTime = apocalypseTime;
    }

    public LocalDate getApocalypseTime() {
        return apocalypseTime;
    }

    public void setApocalypseTime(LocalDate apocalypseTime) {
        this.apocalypseTime = apocalypseTime;
    }

    public long timeLeft(LocalDate date) {
        return ChronoUnit.DAYS.between(date, this.apocalypseTime);
    }
}
