package pl.sda.s01_podstawy.s08_classes.task06_last_day.staticExamples;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class StaticUtils {
    public static final String hourAndMinutePattern = "HH:mm";
    public static final String hourMinuteAndSecondPattern = "HH:mm:ss";

    public static void showHoursAndMinutesOnly (LocalTime time) {
        System.out.println(time.format(DateTimeFormatter.ofPattern(hourAndMinutePattern)));
    }

    public static void showHoursMinutesAndSecondsOnly (LocalTime time) {
        System.out.println(time.format(DateTimeFormatter.ofPattern(hourMinuteAndSecondPattern)));
    }

    public static void showTime (LocalTime time, String pattern) {
        System.out.println(time.format(DateTimeFormatter.ofPattern(pattern)));
    }
}
