package pl.sda.s01_podstawy.s08_classes.task06_last_day.varArgsExamples;

import java.util.Arrays;

public class NumbersCalculations {
    public static int addNumbers(int... numbers) {
        int sum = 0;
        for (int number : numbers) sum += number;
        return sum;
    }

    public static long multiplyNumbers(int... numbers) {
        long result = 1;
        for (int number : numbers) result *= number;
        return result;
    }

    public static double averageValue(int... numbers) {
        int sum = 0;
        for (int number : numbers) sum += number;
        return sum / numbers.length;
    }

    public static long sumOfSquares(int... numbers) {
        long sum = 0;
        for (int number : numbers) sum += number * number;
        return sum;
    }

    public static long maxValue(int... numbers) {
        long max = numbers[0];
        for (int number : numbers) if (number > max) max = number;
        return max;
    }

    public static int minValue(int... numbers) {
        int min = numbers[0];
        for (int number : numbers) if (number < min) min = number;
        return min;
    }

    public static int sumOfPositives(int... numbers) {
        int sum = 0;
        for (int number : numbers) if (number > 0) sum += number;
        return sum;
    }

    public static int sumOfNegative(int... numbers) {
        int sum = 0;
        for (int number : numbers) if (number < 0) sum += number;
        return sum;
    }

    public static int minValue2(int... numbers) {
        Arrays.sort(numbers);
        return numbers[0];
    }

    public static int maxValue2(int... numbers) {
        Arrays.sort(numbers);
        return numbers[numbers.length - 1];
    }
}
