package pl.sda.s01_podstawy.s08_classes.task06_last_day.palindromeChecking;

import java.util.Scanner;

public class PalindromeMain {
    public static void main(String[] args) {
        Scanner scanner1 = new Scanner(System.in);
        System.out.print("Number: ");
        int number = scanner1.nextInt();
        System.out.println(PalindromeChecker.isPalindrome(number));
        Scanner scanner2 = new Scanner(System.in);
        System.out.print("String: ");
        String text = scanner2.nextLine();
        System.out.println(PalindromeChecker.isPalindrome(text));
    }
}
