package pl.sda.s01_podstawy.s08_classes.task06_last_day.varArgsExamples;

public class VarArgsMain {
    public static void main(String[] args) {
        System.out.println("Sum: " + NumbersCalculations.addNumbers(-1, 0, 1, 2, 3, 4, 5, 6));
        System.out.println("Multiply: " + NumbersCalculations.multiplyNumbers(-1, 0, 1, 2, 3, 4, 5, 6));
        System.out.println("Average value: " + NumbersCalculations.averageValue(-1, 0, 1, 2, 3, 4, 5, 6));
        System.out.println("The sum of the squares: " + NumbersCalculations.sumOfSquares(-1, 0, 1, 2, 3, 4, 5, 6));
        System.out.println("Max value (v. 1): " + NumbersCalculations.maxValue(-1, 0, 1, 2, 3, 4, 5, 6));
        System.out.println("Min value (v. 1): " + NumbersCalculations.minValue(-1, 0, 1, 2, 3, 4, 5, 6));
        System.out.println("Max value (v. 2): " + NumbersCalculations.maxValue2(-1, 0, 1, 2, 3, 4, 5, 6));
        System.out.println("Min value (v. 2): " + NumbersCalculations.minValue2(-1, 0, 1, 2, 3, 4, 5, 6));
        System.out.println("Sum of positives: " + NumbersCalculations.sumOfPositives(-1, 0, 1, 2, 3, 4, 5, 6));
        System.out.println("Sum of negative: " + NumbersCalculations.sumOfNegative(-1, 0, 1, 2, 3, 4, 5, 6));
    }
}
