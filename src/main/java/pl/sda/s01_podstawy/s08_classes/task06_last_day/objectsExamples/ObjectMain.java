package pl.sda.s01_podstawy.s08_classes.task06_last_day.objectsExamples;

public class ObjectMain {
    public static void main(String[] args) {
        System.out.println("Square: side length (5)");
        Square square = new Square(5);
        System.out.println("Area: " + square.getArea());
        System.out.println("Circumference: " + square.getCircumference());
        System.out.println("Rectangle: side length (4, 5)");
        Rectangle rectangle = new Rectangle(4, 5);
        System.out.println("Area: " + rectangle.getArea());
        System.out.println("Circumference: " + rectangle.getCircumference());
        System.out.println("Triangle: side length (3, 4, 5)");
        Triangle triangle = new Triangle(3, 4, 5);
        System.out.println("Area: " + triangle.getArea());
        System.out.println("Circumference: " + triangle.getCircumference());
        System.out.println("Circle: radius (5)");
        Circle circle = new Circle(5);
        System.out.println("Area: " + circle.getArea());
        System.out.println("Circumference: " + circle.getCircumference());

        Square square1 = new Square(5);
        Square square2 = new Square(6);
        System.out.println("Squares: A (" + square1.getSideLength() + "), B (" + square2.getSideLength() + ")");
        System.out.println("Area1 is bigger then area2: " + GeometricUtils.squareAreaIsBigger(square1, square2));
        System.out.println("Area2 is bigger then area1: " + GeometricUtils.squareAreaIsBigger(square2, square1));
        System.out.println("Circumference1 is bigger then circumference2: " + GeometricUtils.squareCircumferenceIsBigger(square1, square2));
        System.out.println("Circumference2 is bigger then circumference1: " + GeometricUtils.squareCircumferenceIsBigger(square2, square1));
        Rectangle rectangle1 = new Rectangle(4, 4);
        Rectangle rectangle2 = new Rectangle(5, 6);
        System.out.println("Rectangles: A (" + rectangle1.getSideALength() + ", " + rectangle1.getSideBLength() +
                "), B (" + rectangle2.getSideALength() + ", " + rectangle2.getSideBLength() + ")");
        System.out.println("Area1 is bigger then area2: " + GeometricUtils.rectangleAreaIsBigger(rectangle1, rectangle2));
        System.out.println("Area2 is bigger then area1: " + GeometricUtils.rectangleAreaIsBigger(rectangle2, rectangle1));
        System.out.println("Circumference1 is bigger then circumference2: " + GeometricUtils.rectangleCircumferenceIsBigger(rectangle1, rectangle2));
        System.out.println("Circumference2 is bigger then circumference1: " + GeometricUtils.rectangleCircumferenceIsBigger(rectangle2, rectangle1));
        Triangle triangle1 = new Triangle(3, 4, 4);
        Triangle triangle2 = new Triangle(4, 5, 6);
        System.out.println("Triangles: A (" + triangle1.getSideALength() + ", " + triangle1.getSideBLength() + ", " + triangle1.getSideCLength() +
                "), B (" + triangle2.getSideALength() + ", " + triangle2.getSideBLength() + ", " + triangle2.getSideCLength() + ")");
        System.out.println("Area1 is bigger then area2: " + GeometricUtils.triangleAreaIsBigger(triangle1, triangle2));
        System.out.println("Area2 is bigger then area1: " + GeometricUtils.triangleAreaIsBigger(triangle2, triangle1));
        System.out.println("Circumference1 is bigger then circumference2: " + GeometricUtils.triangleCircumferenceIsBigger(triangle1, triangle2));
        System.out.println("Circumference2 is bigger then circumference1: " + GeometricUtils.triangleCircumferenceIsBigger(triangle2, triangle1));
        Circle circle1 = new Circle(5);
        Circle circle2 = new Circle(6);
        System.out.println("Triangles: A (" + circle1.getRadius() + "), B (" + circle2.getRadius() + ")");
        System.out.println("Area1 is bigger then area2: " + GeometricUtils.circleAreaIsBigger(circle1, circle2));
        System.out.println("Area2 is bigger then area1: " + GeometricUtils.circleAreaIsBigger(circle2, circle1));
        System.out.println("Circumference1 is bigger then circumference2: " + GeometricUtils.circleCircumferenceIsBigger(circle1, circle2));
        System.out.println("Circumference2 is bigger then circumference1: " + GeometricUtils.circleCircumferenceIsBigger(circle2, circle1));
    }
}
