package pl.sda.s01_podstawy.s08_classes.task06_last_day.palindromeChecking;

public class PalindromeChecker {
    public static boolean isPalindrome(int number) {
        int numberOfDigits = 0;
        int divideBy = 1;
        while (number / divideBy > 0) {
            numberOfDigits++;
            divideBy *= 10;
        }
        divideBy = 10;
        int[] digitsArray = new int[numberOfDigits];
        for (int i = 0; i < numberOfDigits; i++) {
            digitsArray[i] = number % divideBy;
            number /= divideBy;
        }
        for (int i = 0; i < numberOfDigits / 2; i++){
            if (digitsArray[i] != digitsArray[numberOfDigits - 1 - i]) return false;
        }
        return true;
    }
    public static boolean isPalindrome(String text) {
        for (int i = 0; i < text.length() / 2; i++){
            if (text.charAt(i) != text.charAt(text.length() - 1 - i)) return false;
        }
        return true;
    }
}
