package pl.sda.s01_podstawy.s08_classes.task06_last_day.innerClass;

public class InnerClassRunner {
    public static void main(String[] args) {
        OuterClass outerClass = new OuterClass();
        OuterClass.MyInnerClass innerClass = outerClass.new MyInnerClass();
        OuterClass.MyInnerStaticClass innerStaticClass = new OuterClass.MyInnerStaticClass();
    }
}
