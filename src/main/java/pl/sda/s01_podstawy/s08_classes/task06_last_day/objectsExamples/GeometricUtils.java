package pl.sda.s01_podstawy.s08_classes.task06_last_day.objectsExamples;

public class GeometricUtils {
    public static boolean squareAreaIsBigger(Square square1, Square square2) {
        return square1.getArea() > square2.getArea();
    }
    public static boolean squareCircumferenceIsBigger(Square square1, Square square2) {
        return square1.getCircumference() > square2.getCircumference();
    }
    public static boolean rectangleAreaIsBigger(Rectangle rectangle1, Rectangle rectangle2) {
        return rectangle1.getArea() > rectangle2.getArea();
    }
    public static boolean rectangleCircumferenceIsBigger(Rectangle rectangle1, Rectangle rectangle2) {
        return rectangle1.getCircumference() > rectangle2.getCircumference();
    }
    public static boolean triangleAreaIsBigger(Triangle triangle1, Triangle triangle2) {
        return triangle1.getArea() > triangle2.getArea();
    }
    public static boolean triangleCircumferenceIsBigger(Triangle triangle1, Triangle triangle2) {
        return triangle1.getCircumference() > triangle2.getCircumference();
    }
    public static boolean circleAreaIsBigger(Circle circle1, Circle circle2) {
        return circle1.getArea() > circle2.getArea();
    }
    public static boolean circleCircumferenceIsBigger(Circle circle1, Circle circle2) {
        return circle1.getCircumference() > circle2.getCircumference();
    }
}
