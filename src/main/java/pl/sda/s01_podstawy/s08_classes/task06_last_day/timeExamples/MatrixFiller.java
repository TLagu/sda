package pl.sda.s01_podstawy.s08_classes.task06_last_day.timeExamples;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Random;
import java.util.Scanner;

public class MatrixFiller {

    public void fillArray () {
        Random random = new Random();
        System.out.print("Enter the dimension of the array: ");
        Scanner scanner = new Scanner(System.in);
        LocalDateTime startDate = LocalDateTime.now();
        int a = scanner.nextInt();
        long[][] array = new long[a][a];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) array[i][j] = random.nextLong();
        }
        Duration duration = Duration.between(startDate, LocalDateTime.now());
        long seconds = Math.abs(duration.getSeconds());
        System.out.println("Duration [s]: " + duration.getSeconds() + "." + duration.getNano());
    }
}
