package pl.sda.s01_podstawy.s08_classes.task04_computer;

public class PcCase extends Element {
    private int bay25Count;
    private int bay35Count;

    public PcCase(String name, int bay25Count, int bay35Count) {
        super(name);
        this.setBay25Count(bay25Count);
        this.setBay35Count(bay35Count);
    }

    public int getBay25Count() {
        return bay25Count;
    }

    public void setBay25Count(int bay25Count) {
        this.bay25Count = bay25Count;
    }

    public int getBay35Count() {
        return bay35Count;
    }

    public void setBay35Count(int bay35Count) {
        this.bay35Count = bay35Count;
    }

    @Override
    public String toString() {
        return "PcCase{" +
                "name='" + this.getName() + '\'' +
                ", bay25Count=" + this.getBay25Count() +
                ", bay35Count=" + this.getBay35Count() +
                '}';
    }
}
