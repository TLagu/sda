package pl.sda.s01_podstawy.s08_classes.task04_computer;

public class Monitor extends Element {
    public Monitor(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Monitor{" +
                "name='" + this.getName() + '\'' +
                '}';
    }
}
