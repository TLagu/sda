package pl.sda.s01_podstawy.s08_classes.task04_computer.helpers;

import pl.sda.s01_podstawy.s08_classes.task04_computer.Cpu;
import pl.sda.s01_podstawy.s08_classes.task04_computer.Gpu;
import pl.sda.s01_podstawy.s08_classes.task04_computer.Ram;
import pl.sda.s01_podstawy.s08_classes.task04_computer.logger.Logger;

import java.util.List;

public class ComputerHelper extends Helper {

    public static boolean isValidCpuCount(Cpu cpu, int cpuCount, int maxCpuCount, SocketList socket) {
        if (cpuCount >= maxCpuCount) {
            Logger.log(String.format("No more CPUs can be connected (%s).", cpu.getName()));
            return false;
        }
        if (!cpu.getSocket().equals(socket)) {
            Logger.log(String.format("The processor has a bad socket (%s).", cpu.getName()));
            return false;
        }
        return true;
    }

    public static boolean isValidCount(String elementName, int elementCount, int maxElementCount, FieldList fieldList) {
        if (elementCount < maxElementCount) {
            return true;
        }
        Logger.log(String.format("No more %s can be connected (%s).", fieldList, elementName));
        return false;
    }

    public static int getCountOfRamModules(List<Ram> rams) {
        int sum = 0;
        for (Ram ram: rams) {
            sum += ram.getModuleCount();
        }
        return sum;
    }

    public static int getCountOfMonitorOutput(List<Gpu> gpus) {
        int sum = 0;
        for (Gpu gpu: gpus) {
            sum += gpu.getMonitorCount();
        }
        return sum;
    }
}