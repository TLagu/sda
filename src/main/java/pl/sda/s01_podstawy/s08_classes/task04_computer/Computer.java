package pl.sda.s01_podstawy.s08_classes.task04_computer;

import pl.sda.s01_podstawy.s08_classes.task04_computer.helpers.ComputerHelper;
import pl.sda.s01_podstawy.s08_classes.task04_computer.helpers.FieldList;

import java.util.ArrayList;
import java.util.List;

public class Computer {
    private PcCase pcCase;
    private Psu psu;
    private Motherboard motherboard;
    private List<Cpu> cpus = new ArrayList<>();
    private List<Cooler> coolers = new ArrayList<>();
    private List<Ram> rams = new ArrayList<>();
    private List<Shdd> m2Drives = new ArrayList<>();
    private List<Shdd> sataDrives = new ArrayList<>();
    private List<Gpu> gpus = new ArrayList<>();
    private List<Monitor> monitors = new ArrayList<>();

    public PcCase getPcCase() {
        return pcCase;
    }

    public void setPcCase(PcCase pcCase) {
        this.pcCase = pcCase;
    }

    public Psu getPsu() {
        return psu;
    }

    public void setPsu(Psu psu) {
        this.psu = psu;
    }

    public Motherboard getMotherboard() {
        return motherboard;
    }

    public void setMotherboard(Motherboard motherboard) {
        this.motherboard = motherboard;
    }

    public List<Cpu> getCpu() {
        return cpus;
    }

    public void setCpu(List<Cpu> cpus) {
        this.cpus = cpus;
    }

    public void addCpu(Cpu cpu) {
        if (ComputerHelper.isValidCpuCount(cpu,
                this.cpus.size(),
                this.motherboard.getCpuCount(),
                this.motherboard.getSocket())) {
            this.cpus.add(cpu);
        }
    }

    public List<Cooler> getCooler() {
        return coolers;
    }

    public void setCooler(List<Cooler> coolers) {
        this.coolers = coolers;
    }

    public void addCooler(Cooler cooler) {
        if (ComputerHelper.isValidCount(cooler.getName(),
                this.coolers.size(),
                Math.min(this.motherboard.getCpuCount(), this.cpus.size()),
                FieldList.COOLER)) {
            this.coolers.add(cooler);
        }
    }

    public List<Ram> getRam() {
        return rams;
    }

    public void setRam(List<Ram> ram) {
        this.rams = ram;
    }

    public void addRam(Ram ram) {
        if (ComputerHelper.isValidCount(ram.getName(),
                ComputerHelper.getCountOfRamModules(this.rams) + ram.getModuleCount() - 1,
                this.motherboard.getRamCount(),
                FieldList.RAM)) {
            this.rams.add(ram);
        }
    }

    public List<Shdd> getM2Drive() {
        return m2Drives;
    }

    public void setM2Drive(List<Shdd> m2Drives) {
        this.m2Drives = m2Drives;
    }

    public void addM2Drive(Shdd m2Drive) {
        if (ComputerHelper.isValidCount(m2Drive.getName(),
                this.m2Drives.size(),
                this.motherboard.getM2Count(),
                FieldList.M2)) {
            this.m2Drives.add(m2Drive);
        }
    }

    public List<Shdd> getSataDrive() {
        return sataDrives;
    }

    public void setSataDrive(ArrayList<Shdd> sataDrives) {
        this.sataDrives = sataDrives;
    }

    public void addSataDrive(Shdd sataDrive) {
        if (ComputerHelper.isValidCount(sataDrive.getName(),
                this.sataDrives.size(),
                Math.min(this.motherboard.getSataCount(), this.pcCase.getBay35Count()),
                FieldList.SATA)) {
            this.sataDrives.add(sataDrive);
        }
    }

    public List<Gpu> getGpu() {
        return gpus;
    }

    public void setGpu(List<Gpu> gpus) {
        this.gpus = gpus;
    }

    public void addGpu(Gpu gpu) {
        if (ComputerHelper.isValidCount(gpu.getName(),
                this.gpus.size(),
                this.motherboard.getPcieCount(),
                FieldList.GPU)) {
            this.gpus.add(gpu);
        }
    }

    public List<Monitor> getMonitors() {
        return monitors;
    }

    public void setMonitors(List<Monitor> monitors) {
        this.monitors = monitors;
    }

    public void addMonitor(Monitor monitor) {
        if (ComputerHelper.isValidCount(monitor.getName(),
                this.monitors.size(),
                ComputerHelper.getCountOfMonitorOutput(this.gpus),
                FieldList.MONITOR)) {
            this.monitors.add(monitor);
        }
    }

    @Override
    public String toString() {
        return "Computer{" +
                "pcCase=" + pcCase + "\n" +
                ", psu=" + psu + "\n" +
                ", motherboard=" + motherboard + "\n" +
                ", cpu=" + cpus.toString() + "\n" +
                ", cooler=" + coolers.toString() + "\n" +
                ", ram=" + rams.toString() + "\n" +
                ", m2Drives=" + m2Drives.toString() + "\n" +
                ", sataDrives=" + sataDrives.toString() + "\n" +
                ", gpus=" + gpus.toString() + "\n" +
                ", monitors=" + monitors.toString() +
                '}';
    }
}
