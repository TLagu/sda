package pl.sda.s01_podstawy.s08_classes.task04_computer;

public class Shdd extends Element {
    private String interfaceDrive;

    public Shdd(String name, String interfaceDrive) {
        super(name);
        this.setInterfaceDrive(interfaceDrive);
    }

    public String getInterfaceDrive() {
        return interfaceDrive;
    }

    public void setInterfaceDrive(String interfaceDrive) {
        this.interfaceDrive = interfaceDrive;
    }

    @Override
    public String toString() {
        return "Shdd{" +
                "name='" + this.getName() + '\'' +
                ", interfaceDrive='" + this.getInterfaceDrive() + '\'' +
                '}';
    }
}
