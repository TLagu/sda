package pl.sda.s01_podstawy.s08_classes.task04_computer;

public class Gpu extends Element {
    private int monitorCount;

    public Gpu(String name, int monitorCount) {
        super(name);
        this.setMonitorCount(monitorCount);
    }

    public int getMonitorCount() {
        return monitorCount;
    }

    public void setMonitorCount(int monitorCount) {
        this.monitorCount = monitorCount;
    }

    @Override
    public String toString() {
        return "Gpu{" +
                "name='" + this.getName() + '\'' +
                "monitorCount" + this.getMonitorCount() +
                '}';
    }
}
