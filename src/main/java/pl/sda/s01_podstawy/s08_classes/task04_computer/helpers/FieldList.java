package pl.sda.s01_podstawy.s08_classes.task04_computer.helpers;

public enum FieldList {
    PC_CASE("PC Case"),
    PSU("Power Supply Unit"),
    MOBO("Motherboard"),
    CPU("CPUs"),
    COOLER("CPU Coolers"),
    RAM("RAM Memory"),
    M2("M2 Drives"),
    SATA("SATA Drives"),
    GPU("Graphic Cards"),
    MONITOR("Monitors");

    public final String label;

    FieldList(String label) {
        this.label = label;
    }

    public String valueOfLabel() {
        return this.label;
    }

    @Override
    public String toString() {
        return this.label;
    }
}
