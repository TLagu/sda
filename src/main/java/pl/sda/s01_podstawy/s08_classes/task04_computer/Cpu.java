package pl.sda.s01_podstawy.s08_classes.task04_computer;

import pl.sda.s01_podstawy.s08_classes.task04_computer.helpers.SocketList;

public class Cpu extends Element {
    private SocketList socket;

    public Cpu(String name, SocketList socket) {
        super(name);
        this.setSocket(socket);
    }

    public SocketList getSocket() {
        return socket;
    }

    public void setSocket(SocketList socket) {
        this.socket = socket;
    }

    @Override
    public String toString() {
        return "Cpu{" +
                "name='" + this.getName() + '\'' +
                ", socket='" + this.getSocket() + '\'' +
                '}';
    }
}
