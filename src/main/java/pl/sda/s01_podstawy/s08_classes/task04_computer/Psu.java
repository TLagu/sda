package pl.sda.s01_podstawy.s08_classes.task04_computer;

public class Psu extends Element {
    private int power;

    public Psu(String name, int power) {
        super(name);
        this.setPower(power);
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "Psu{" +
                "name='" + this.getName() + '\'' +
                ", power=" + this.getPower() +
                '}';
    }
}
