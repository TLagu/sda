package pl.sda.s01_podstawy.s08_classes.task04_computer;

import pl.sda.s01_podstawy.s08_classes.task04_computer.helpers.SocketList;
import pl.sda.s01_podstawy.s08_classes.task04_computer.logger.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Warehouse {
    private final List<PcCase> pcCases = new ArrayList<>();
    private final List<Psu> psus = new ArrayList<>();
    private final List<Motherboard> motherboards = new ArrayList<>();
    private final List<Cpu> cpus = new ArrayList<>();
    private final List<Cooler> coolers = new ArrayList<>();
    private final List<Ram> rams = new ArrayList<>();
    private final List<Shdd> m2 = new ArrayList<>();
    private final List<Shdd> sata = new ArrayList<>();
    private final List<Gpu> gpus = new ArrayList<>();
    private final List<Monitor> monitors = new ArrayList<>();

    public static void main(String[] args) {
        Warehouse warehouse = new Warehouse();
        warehouse.createData();
        Computer computer1 = warehouse.createComputer("be quiet! Pure Base 600",
                "be quiet! Pure Power 11 1000W CM",
                "ASUS TUF GAMING X570-PLUS",
                Arrays.asList("Intel Core i5-11600K", "AMD Ryzen 5 5600X"),
                Arrays.asList("be quiet! Dark Rock PRO 4", "Noctua NH-D15"),
                Arrays.asList("Corsair Vengeance RGB Pro SL Black 32GB [2x16GB 3600MHz DDR4 CL18 DIMM]",
                        "G.SKILL Trident Z RGB 64GB [4x16GB 3600MHz DDR4 CL17 DIMM]",
                        "Corsair Vengeance LPX 16GB [1x16GB 3600MHz DDR4 CL16 1.35V DIMM]"
                ),
                Arrays.asList("Samsung 980 Pro 500GB", "Samsung 980 Pro 500GB",
                        "Samsung 980 Pro 1TB", "Samsung 980 Pro 1TB",
                        "Samsung 980 Pro 2TB", "Samsung 980 Pro 2TB"
                ),
                Arrays.asList("Western Digital Ultrastar 18TB DC HC550",
                        "WD Gold 18TB",
                        "WD Red Pro 18TB"
                ),
                Arrays.asList("ASUS Radeon RX 6900 XT STRIX LC 16GB",
                        "ASUS GeForce RTX 3060 Ti STRIX Gaming 8GB OC V2 LHR"
                ),
                Arrays.asList("iiyama G-Master G2470HSU-B1 Red Eagle [0.8ms, 165Hz, FreeSync Premium]",
                        "iiyama G-Master G2740QSU-B1 Black Hawk [1ms, 75Hz, FreeSync]",
                        "iiyama ProLite XB3288UHSU-B1"
                )
        );
        Logger.log(computer1.toString());
        Computer computer2 = warehouse.createComputer("be quiet! Pure Base 600",
                "be quiet! Pure Power 11 1000W CM",
                "ASUS PRIME Z590-P",
                Arrays.asList("Intel Core i5-11600K", "AMD Ryzen 5 5600X"),
                Arrays.asList("be quiet! Dark Rock PRO 4", "Noctua NH-D15"),
                Arrays.asList("Corsair Vengeance RGB Pro SL Black 32GB [2x16GB 3600MHz DDR4 CL18 DIMM]",
                        "G.SKILL Trident Z RGB 64GB [4x16GB 3600MHz DDR4 CL17 DIMM]",
                        "Corsair Vengeance LPX 16GB [1x16GB 3600MHz DDR4 CL16 1.35V DIMM]"
                ),
                Arrays.asList("Samsung 980 Pro 500GB", "Samsung 980 Pro 500GB",
                        "Samsung 980 Pro 1TB", "Samsung 980 Pro 1TB",
                        "Samsung 980 Pro 2TB", "Samsung 980 Pro 2TB"
                ),
                Arrays.asList("Western Digital Ultrastar 18TB DC HC550",
                        "WD Gold 18TB",
                        "WD Red Pro 18TB"
                ),
                Arrays.asList("ASUS Radeon RX 6900 XT STRIX LC 16GB",
                        "ASUS GeForce RTX 3060 Ti STRIX Gaming 8GB OC V2 LHR",
                        "ASUS GeForce RTX 3060 Ti STRIX Gaming 8GB OC V2 LHR"
                ),
                Arrays.asList("iiyama G-Master G2470HSU-B1 Red Eagle [0.8ms, 165Hz, FreeSync Premium]",
                        "iiyama G-Master G2470HSU-B1 Red Eagle [0.8ms, 165Hz, FreeSync Premium]",
                        "iiyama G-Master G2470HSU-B1 Red Eagle [0.8ms, 165Hz, FreeSync Premium]",
                        "iiyama G-Master G2470HSU-B1 Red Eagle [0.8ms, 165Hz, FreeSync Premium]",
                        "iiyama G-Master G2470HSU-B1 Red Eagle [0.8ms, 165Hz, FreeSync Premium]",
                        "iiyama G-Master G2470HSU-B1 Red Eagle [0.8ms, 165Hz, FreeSync Premium]",
                        "iiyama G-Master G2470HSU-B1 Red Eagle [0.8ms, 165Hz, FreeSync Premium]",
                        "iiyama G-Master G2470HSU-B1 Red Eagle [0.8ms, 165Hz, FreeSync Premium]",
                        "iiyama G-Master G2470HSU-B1 Red Eagle [0.8ms, 165Hz, FreeSync Premium]",
                        "iiyama G-Master G2470HSU-B1 Red Eagle [0.8ms, 165Hz, FreeSync Premium]",
                        "iiyama G-Master G2470HSU-B1 Red Eagle [0.8ms, 165Hz, FreeSync Premium]",
                        "iiyama G-Master G2470HSU-B1 Red Eagle [0.8ms, 165Hz, FreeSync Premium]",
                        "iiyama G-Master G2740QSU-B1 Black Hawk [1ms, 75Hz, FreeSync]",
                        "iiyama ProLite XB3288UHSU-B1"
                )
        );
        Logger.log(computer2.toString());
    }

    private void createData() {
        // PC Cases
        pcCases.add(new PcCase("be quiet! Pure Base 600", 8, 3));
        pcCases.add(new PcCase("be quiet! Dark Base Pro 900", 14, 7));
        pcCases.add(new PcCase("Corsair Obsidian 500D", 3, 2));
        pcCases.add(new PcCase("Thermaltake The Tower 900", 2, 6));
        // PSU
        psus.add(new Psu("be quiet! Pure Power 11 600W CM", 600));
        psus.add(new Psu("be quiet! Pure Power 11 1000W CM", 1000));
        psus.add(new Psu("Corsair CV 650W CP-9020236-EU", 650));
        psus.add(new Psu("Corsair TX850M CP-9020130-EU", 850));
        psus.add(new Psu("Thermaltake Smart SE 630W Modular", 630));
        psus.add(new Psu("Thermaltake Toughpower Grand Riing 1050W", 1050));
        // Motherboard
        motherboards.add(new Motherboard("ASUS TUF GAMING X570-PLUS", SocketList.AMD_AM4, 1, 4, 8, 2, 2));
        motherboards.add(new Motherboard("ASUS PRIME Z590-P", SocketList.INTEL_1200, 1, 4, 4, 1, 2));
        motherboards.add(new Motherboard("Gigabyte X570 AORUS ELITE", SocketList.AMD_AM4, 1, 4, 6, 2, 2));
        motherboards.add(new Motherboard("Gigabyte Z590 GAMING X", SocketList.INTEL_1200, 1, 4, 6, 1, 2));
        // CPU
        cpus.add(new Cpu("Intel Core i5-11600K", SocketList.INTEL_1200));
        cpus.add(new Cpu("Intel Core i7-11700K", SocketList.INTEL_1200));
        cpus.add(new Cpu("Intel Core i9-11900K", SocketList.INTEL_1200));
        cpus.add(new Cpu("AMD Ryzen 5 5600X", SocketList.AMD_AM4));
        cpus.add(new Cpu("AMD Ryzen 7 5800X", SocketList.AMD_AM4));
        cpus.add(new Cpu("AMD Ryzen 9 5950X", SocketList.AMD_AM4));
        // Cooler
        coolers.add(new Cooler("Noctua NH-D15"));
        coolers.add(new Cooler("be quiet! Dark Rock PRO 4"));
        coolers.add(new Cooler("Gelid Phantom CC-Phantom-01-A"));
        // RAM
        rams.add(new Ram("Corsair Vengeance LPX 16GB [1x16GB 3600MHz DDR4 CL16 1.35V DIMM]", 1));
        rams.add(new Ram("Corsair Vengeance RGB Pro SL Black 32GB [2x16GB 3600MHz DDR4 CL18 DIMM]", 2));
        rams.add(new Ram("G.SKILL Trident Z RGB Neo AMD 32GB [2x16GB 3600MHz DDR4 XMP2 DIMM]", 2));
        rams.add(new Ram("G.SKILL Trident Z RGB 64GB [4x16GB 3600MHz DDR4 CL17 DIMM]", 4));
        // M.2
        m2.add(new Shdd("Samsung 980 Pro 500GB", "M.2"));
        m2.add(new Shdd("Samsung 980 Pro 1TB", "M.2"));
        m2.add(new Shdd("Samsung 980 Pro 2TB", "M.2"));
        // SATA
        sata.add(new Shdd("Western Digital Ultrastar 18TB DC HC550", "SATA"));
        sata.add(new Shdd("WD Gold 18TB", "SATA"));
        sata.add(new Shdd("WD Red Pro 18TB", "SATA"));
        // GPU
        gpus.add(new Gpu("ASUS Radeon RX 6700 XT ROG STRIX 12GB OC", 4));
        gpus.add(new Gpu("ASUS Radeon RX 6900 XT STRIX LC 16GB", 4));
        gpus.add(new Gpu("ASUS GeForce RTX 3060 Ti STRIX Gaming 8GB OC V2 LHR", 5));
        gpus.add(new Gpu("ASUS GeForce RTX 3080 Ti TUF Gaming 12GB OC", 5));
        // Monitor
        monitors.add(new Monitor("iiyama G-Master G2470HSU-B1 Red Eagle [0.8ms, 165Hz, FreeSync Premium]"));
        monitors.add(new Monitor("iiyama G-Master G2740QSU-B1 Black Hawk [1ms, 75Hz, FreeSync]"));
        monitors.add(new Monitor("iiyama ProLite XB3288UHSU-B1"));
    }

    private Computer createComputer(String pcCaseName,
                                    String psuName,
                                    String motherboardName,
                                    List<String> arrayOfCpus,
                                    List<String> arrayOfCoolers,
                                    List<String> arrayOfRams,
                                    List<String> arrayOfM2Drives,
                                    List<String> arrayOfSataDrives,
                                    List<String> arrayOfGpus,
                                    List<String> arrayOfMonitors) {
        Computer computer = new Computer();
        for (PcCase pcCase : pcCases) {
            if (pcCase.getName().equals(pcCaseName)) {
                computer.setPcCase(pcCase);
            }
        }
        for (Psu psu : psus) {
            if (psu.getName().equals(psuName)) {
                computer.setPsu(psu);
            }
        }
        for (Motherboard motherboard : motherboards) {
            if (motherboard.getName().equals(motherboardName)) {
                computer.setMotherboard(motherboard);
            }
        }
        for (String cpu : arrayOfCpus) {
            for (Cpu value : cpus) {
                if (value.getName().equals(cpu)) {
                    computer.addCpu(value);
                }
            }
        }
        for (String cooler : arrayOfCoolers) {
            for (Cooler value : coolers) {
                if (value.getName().equals(cooler)) {
                    computer.addCooler(value);
                }
            }
        }
        for (String ram : arrayOfRams) {
            for (Ram value : rams) {
                if (value.getName().equals(ram)) {
                    computer.addRam(value);
                }
            }
        }
        for (String m2Drive : arrayOfM2Drives) {
            for (Shdd shdd : m2) {
                if (shdd.getName().equals(m2Drive)) {
                    computer.addM2Drive(shdd);
                }
            }
        }
        for (String sataDrive : arrayOfSataDrives) {
            for (Shdd shdd : sata) {
                if (shdd.getName().equals(sataDrive)) {
                    computer.addSataDrive(shdd);
                }
            }
        }
        for (String gpu : arrayOfGpus) {
            for (Gpu value : gpus) {
                if (value.getName().equals(gpu)) {
                    computer.addGpu(value);
                }
            }
        }
        for (String monitor : arrayOfMonitors) {
            for (Monitor value : monitors) {
                if (value.getName().equals(monitor)) {
                    computer.addMonitor(value);
                }
            }
        }
        return computer;
    }

}
