package pl.sda.s01_podstawy.s08_classes.task04_computer;

public class Cooler extends Element {

    public Cooler(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Cooler{" +
                "name='" + this.getName() + '\'' +
                '}';
    }
}
