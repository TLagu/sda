package pl.sda.s01_podstawy.s08_classes.task04_computer.helpers;

public enum SocketList {
    INTEL_1200("Intel Socket 1200"),
    AMD_AM4("AMD Socket AM4");

    public final String label;

    SocketList(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }

    @Override
    public String toString() {
        return this.label;
    }
}
