package pl.sda.s01_podstawy.s08_classes.task04_computer;

public class Ram extends Element {
    private int moduleCount;

    public Ram(String name, int moduleCount) {
        super(name);
        this.setModuleCount(moduleCount);
    }

    public int getModuleCount() {
        return this.moduleCount;
    }

    public void setModuleCount(int moduleCount) {
        this.moduleCount = moduleCount;
    }

    @Override
    public String toString() {
        return "Ram{" +
                "name='" + this.getName() + '\'' +
                "moduleCount=" + this.getModuleCount() +
                '}';
    }
}
