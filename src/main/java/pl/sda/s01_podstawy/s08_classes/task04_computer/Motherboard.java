package pl.sda.s01_podstawy.s08_classes.task04_computer;

import pl.sda.s01_podstawy.s08_classes.task04_computer.helpers.SocketList;

public class Motherboard extends Element {
    private SocketList socket;
    private int cpuCount;
    private int ramCount;
    private int sataCount;
    private int m2Count;
    private int pcieCount;

    public Motherboard(String name, SocketList socket, int cpuCount, int ramCount, int sataCount, int m2Count, int pcieCount) {
        super(name);
        this.setSocket(socket);
        this.setCpuCount(cpuCount);
        this.setRamCount(ramCount);
        this.setSataCount(sataCount);
        this.setM2Count(m2Count);
        this.setPcieCount(pcieCount);
    }

    public SocketList getSocket() {
        return socket;
    }

    public void setSocket(SocketList socket) {
        this.socket = socket;
    }

    public int getCpuCount() {
        return cpuCount;
    }

    public void setCpuCount(int cpuCount) {
        this.cpuCount = cpuCount;
    }

    public int getRamCount() {
        return ramCount;
    }

    public void setRamCount(int ramCount) {
        this.ramCount = ramCount;
    }

    public int getSataCount() {
        return sataCount;
    }

    public void setSataCount(int sataCount) {
        this.sataCount = sataCount;
    }

    public int getM2Count() {
        return m2Count;
    }

    public void setM2Count(int m2Count) {
        this.m2Count = m2Count;
    }

    public int getPcieCount() {
        return pcieCount;
    }

    public void setPcieCount(int pcieCount) {
        this.pcieCount = pcieCount;
    }

    @Override
    public String toString() {
        return "Motherboard{" +
                "name=" + this.getName() +
                "socket=" + this.getSocket() +
                "cpuCount=" + this.getCpuCount() +
                ", ramCount=" + this.getRamCount() +
                ", sataCount=" + this.getSataCount() +
                ", m2Count=" + this.getM2Count() +
                ", pcieCount=" + this.getPcieCount() +
                '}';
    }
}
