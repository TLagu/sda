package pl.sda.s01_podstawy.s08_classes.task04_computer;

public class Element {
    private String name;

    public Element(String name) {
        this.setName(name);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
