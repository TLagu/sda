package pl.sda.s01_podstawy.e02_data_types.tasks;

import java.time.LocalDate;

public class Main {
    private static char upperLeftCorner = '/';
    private static char upperRightCorner = '\\';
    private static char lowerRightCorner = '/';
    private static char lowerLeftCorner = '\\';
    private static char horizontalLine = '-';
    private static char verticalLine = '|';

    public static void main(String[] args) {
        System.out.println("##################");
        System.out.println("### EXERCISE 1 ###");
        System.out.println("##################");
        System.out.println("Stwórz (zadeklaruj i zainicjalizuj) kilka zmiennych typów prymitywnych na podstawie rzeczy, które Cię otaczają lub z Twojej branży. Zastanów się nad wyborem typu. Wyświetl wartości.");
        short year = 2021;
        String companyName = "Company Name";
        System.out.println("Year: " + year + ", company name: " + companyName);

        System.out.println("\n##################");
        System.out.println("### EXERCISE 2 ###");
        System.out.println("##################");
        System.out.println("Spróbuj przypisać wartość spoza zakresu dla zmiennej. Zobacz, jaki wyświetla się wynik. Spróbuj użyć operatora rzutowania ( )");
        byte tmpYear = (byte) year;
        System.out.println("Year: " + tmpYear + ", company name: " + companyName);

        System.out.println("\n##################");
        System.out.println("### EXERCISE 3 ###");
        System.out.println("##################");
        System.out.println("Użyj klas opakowujących i spróbuj utworzyć dla wszystkich zmiennych (typów prymitywnych) z poprzedniego ćwiczenia ich odpowiedników obiektowych.");
        Short yearObject = year;
        System.out.println("Year: " + yearObject + ", company name: " + companyName);

        System.out.println("\n##################");
        System.out.println("### EXERCISE 4 ###");
        System.out.println("##################");
        System.out.println("Potestuj wybrane metody (xxxValue(), valueOf() i parseXXX()) wrapperów typów prostych.");
        Long longValue = 1234567890l;
        Integer integerValue = longValue.intValue();
        Short shortValue = longValue.shortValue();
        Byte byteValue = longValue.byteValue();
        System.out.println("Long: " + longValue + ", int: " + integerValue + ", short: " + shortValue + ", byte: " + byteValue);
        System.out.println("valueOf - deprecated");
        longValue = Long.parseLong("9876543210");
        integerValue = Integer.parseInt("98765432");
        shortValue = Short.parseShort("9876");
        byteValue = Byte.parseByte("98");
        System.out.println("Long: " + longValue + ", int: " + integerValue + ", short: " + shortValue + ", byte: " + byteValue);

        System.out.println("\n##################");
        System.out.println("### EXERCISE 5 ###");
        System.out.println("##################");
        System.out.println("Napisz program (stwórz nową klasę z metodą main), który wyświetla na ekran Hello, <Twoje imię>!. Niech Twoje imię będzie przechowywane w zmiennej typu String.");
        String myName = "Jan Kowalski";
        System.out.println("Hello, " + myName);

        System.out.println("\n##################");
        System.out.println("### EXERCISE 6 ###");
        System.out.println("##################");
        System.out.println("Napisz program, który wyświetla Twoją wizytówkę");
        String name = "Jan Kowalski";
        String street = "ul. Konstytucji 3-go Maja";
        String flatNumber = "12/11";
        String postCode = "00-000";
        String locality = "Szczecin";
        String phone = "+48 123 456 789";

        String line1 = name.trim();
        String line2 = (street + " " + flatNumber).trim();
        String line3 = (postCode + " " + locality).trim();
        String line4 = ("mobile: " + phone).trim();

        int length = Math.max (Math.max (line1.length(), line2.length()), line3.length());
        if (length > 0) {
            showBackgroundLine(upperLeftCorner, horizontalLine, length + 2, upperRightCorner);
            //showBackgroundLine(verticalLine, ' ', length + 2, verticalLine);
            showTextLine(line1, length);
            showTextLine(line2, length);
            showTextLine(line3, length);
            showTextLine(line4, length);
            //showBackgroundLine(verticalLine, ' ', length + 2, verticalLine);
            showBackgroundLine(lowerLeftCorner, horizontalLine, length + 2, lowerRightCorner);
        } else {
            System.out.println("All lines are blank!");
        }

        System.out.println("\n##################");
        System.out.println("### EXERCISE 7 ###");
        System.out.println("##################");
        System.out.println("Utwórz kilka zmiennych typu String i przetestuj przedstawione metody operujące na napisach.");
        String testText = "Ala ma kota i dwa niedźwiedzie";
        System.out.println("Lower case: " + testText.toLowerCase());
        System.out.println("Replace: " + testText.replace ("niedźwiedzie", "niedźwiedzie polarne"));

        System.out.println("\n##################");
        System.out.println("### EXERCISE 8 ###");
        System.out.println("##################");
        System.out.println("Utwórz po kilka zmiennych typów LocalDate, LocalTime oraz LocalDateTime i potestuj wywoływanie na nich kilku metod. Wyświetl wyniki.");
        System.out.println("Now: " + LocalDate.now());
        System.out.println("Fixed: " + LocalDate.of(2019, 04, 10));
        System.out.println("Fixed: " + LocalDate.of(2019, 04, 10).plusDays(30));
        System.out.println("Now: " + LocalDate.now().isLeapYear());
        System.out.println("Now: " + LocalDate.parse("2021-04-10"));
        System.out.println("Now: " + LocalDate.now().getDayOfMonth());
        System.out.println("Now: " + LocalDate.now().getMonthValue());
        System.out.println("Now: " + LocalDate.now().getMonth());
        System.out.println("Now: " + LocalDate.now().getYear());

        System.out.println("\n##################");
        System.out.println("### EXERCISE 9 ###");
        System.out.println("##################");
        System.out.println("Przetestuj działanie metod z klasy Math.");
        System.out.println("Power: " + Math.pow(3, 2));
        System.out.println("Square: " + Math.sqrt(9));
        System.out.println("Square: " + Math.max(2, 3));
    }

    private static int spacesBefore(String line, int len) {
        int lenght = (int) Math.floor(((double) len - (double) line.length()) / 2);
        return lenght;
    }

    private static int spacesAfter(String line, int len) {
        int lenght = (int) Math.ceil(((double) len - (double) line.length()) / 2);
        return lenght;
    }

    private static void multiplySign(char character, int count) {
        for (int i = 0; i < count; i++) {
            System.out.print(character);
        }
    }

    private static void showTextLine(String line, int len) {
        int spacesBefore;
        int spacesAfter;
        spacesBefore = spacesBefore(line, len);
        spacesAfter = spacesAfter(line, len);
        System.out.print(verticalLine);
        multiplySign(' ', spacesBefore + 1);
        System.out.print(line);
        multiplySign(' ', spacesAfter + 1);
        System.out.println(verticalLine);
    }

    private static void showBackgroundLine(char firstChar, char backgroundChar, int len, char lastChar) {
        System.out.print(firstChar);
        multiplySign(backgroundChar, len);
        System.out.println(lastChar);
    }
}
