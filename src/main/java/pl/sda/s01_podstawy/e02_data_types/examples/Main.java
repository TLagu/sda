package pl.sda.s01_podstawy.e02_data_types.examples;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Main {
    public static void main(String[] args) {
        System.out.println("Integer types");
        byte b = (byte) 56;
        short s = (short) 89;
        int i = 256;
        long l = 5500L;

        long sum = b + s + i + l;
        System.out.println(sum);

        long value1;
        long value2;
        value1 = 24 * 60 * 60 * 1000 * 1000;
        value2 = 24 * 60 * 60 * 1000;
        System.out.println("a / b = " + value1 / value2);
        value1 = 24 * 60 * 60 * 1000 * 1000 * 1000;
        value2 = 24 * 60 * 60 * 1000 * 1000;
        System.out.println("a / b = " + value1 / value2);
        value1 = 24 * 60 * 60L * 1000 * 1000;
        value2 = 24 * 60 * 60 * 1000;
        System.out.println("a / b = " + value1 / value2);

        System.out.println("\nReal types");
        float f = 3.141592F;
        double d = 2.718281828D;

        System.out.println(2.00 - 1.10);
        System.out.println(2.00 - 1.50);
        System.out.println(2.00 - 1.80);
        System.out.println("\nCharacter type");
        char a = 'a';
        char bigA = 'A';
        char newLine = '\n';
        char plus = '+';
        System.out.print("Hello, World!\n");
        System.out.print("\tHello, World!\n");
        System.out.print("\t\t\"Hello, World!\"\n");

        System.out.println("\nBoolean type");
        boolean isAdult = true;
        boolean active = false;

        System.out.print(isAdult && active);

        System.out.println("\nString object");
        System.out.println("Ala ma kota, a kot ma Alę!".length());
        String textSimple = "Ala ma kota, a kot ma Alę!";
        System.out.println(textSimple.substring(13, 25));
        String hello = "Hello, World!";
        hello.toUpperCase();
        System.out.println(hello);
        String.valueOf(23);
        String textObject = new String("Ala ma kota");
        textObject.endsWith("kota");

        System.out.println("\nDateTime object");
        System.out.println(LocalDate.now());
        System.out.println(LocalTime.now());
        System.out.println(LocalDateTime.now());

        System.out.println("\nMath object");
        System.out.println(Math.max(5, 10));
        System.out.println(Math.min(55, 100));
        System.out.println(Math.abs(-77));
        System.out.println(Math.ceil(3.55D));
        System.out.println(Math.floor(3.55D));
        System.out.println(Math.pow(2, 10));
        System.out.println(Math.random());
        System.out.println(Math.round(9.99D));
        System.out.println(Math.sqrt(81));
        System.out.println(Math.PI);
        System.out.println(Math.E);

        System.out.println("\nWrappers (deprecated)");
        byte byteValue = 56;
        Byte byteObject = byteValue;
        short shortValue = 89;
        Short shortObject = shortValue;
        int intValue = 256;
        Integer integerObject = intValue;
        long longValue = 5500L;
        Long longObject = longValue;
        float floatValue = 3.141592F;
        Float floatObject = floatValue;
        double doubleValue = 2.718281828D;
        Double doubleObject = doubleValue;
        char charValue = 'a';
        Character characterObject = charValue;
        boolean booleanValue = true;
        Boolean booleanObject = booleanValue;

        System.out.println("byteValue = " + byteValue);
        System.out.println("byteObject = " + byteObject);
        System.out.println("doubleValue = " + doubleValue);
        System.out.println("doubleObject = " + doubleObject);
        System.out.println("charValue = " + charValue);
        System.out.println("characterObject = " + characterObject);
        System.out.println("booleanValue = " + booleanValue);
        System.out.println("booleanObject = " + booleanObject);

        double doubleValueFromByte = byteObject.doubleValue();
        float floatValueFromInteger = integerObject.floatValue();
        char charValueFromCharacter = characterObject.charValue();
        boolean booleanValueFromBoolean = booleanObject.booleanValue();

        System.out.println("\nAutoboxing");
        Byte byteObject2 = 56;
        Short shortObject2 = 89;
        Integer integerObject2 = 256;
        Long longObject2 = 5500L;
        Float floatObject2 = 3.141592F;
        Double doubleObject2 = 2.718281828D;
        Character characterObject2 = 'a';
        Boolean booleanObject2 = true;

        System.out.println("\nUnboxing (deprecated)");
        byte byteValue2 = (byte) 56;
        short shortValue2 = (short) 89;
        int intValue2 = 256;
        long longValue2 = 5500L;
        float floatValue2 = 3.141592F;
        double doubleValue2 = 2.718281828D;
        char charValue2 = 'a';
        boolean booleanValue2 = Boolean.TRUE;

        System.out.println("\nConversion");
        byte b2 = (byte) 56;
        short s2 = (short) 89;
        int i2 = b2;
        long l2 = s2;
        float f2 = i2;

        byte a3 = 127;
        byte b3 = (byte) 130;
        float c3 = 3.14F;

        int d3 = 5;
        byte e3 = (byte) d3;
        long f3 = (long) c3;

        System.out.println("a = " + a3);
        System.out.println("b = " + b3);
        System.out.println("f = " + f3);

        System.out.println(Boolean.parseBoolean("true"));
        System.out.println(Boolean.parseBoolean("TRUE"));
        System.out.println(Boolean.parseBoolean("1"));
        System.out.println(Boolean.parseBoolean("0"));
        System.out.println(Integer.parseInt("123"));
        System.out.println(Double.parseDouble("3.1415"));
    }
}
