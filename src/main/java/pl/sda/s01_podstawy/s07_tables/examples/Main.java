package pl.sda.s01_podstawy.s07_tables.examples;

import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        int[] intTable1 = {1, 2, 3}; // tylko nowe wersje javy
        System.out.println(Arrays.toString(intTable1));
        System.out.println(intTable1[intTable1.length - 1]);
        int[] intTable2 = new int[]{1, 2, 3, 4};
        System.out.println(Arrays.toString(intTable2));
        System.out.println(intTable2[intTable2.length - 1]);

        int arraySize = 5;
        long[] firstLongArray = new long[arraySize];
        firstLongArray[0] = 1;
        firstLongArray[2] = 2;
        firstLongArray[4] = 3;
        for (int i = 0; i < arraySize; i++) {
            System.out.print(firstLongArray[i] + ", ");
        }
        System.out.println("\n" + firstLongArray.length);
        for (int i = 0; i < firstLongArray.length; i++) {
            System.out.print(firstLongArray[i] + ", ");
        }
        System.out.println("\nZadanie 1: ");
        int[] intTable3 = new int[30];
        for (int i = 0; i < intTable3.length; i++) {
            intTable3[i] = i + 1;
            System.out.print(intTable3[i] + ", ");
        }
        System.out.println();
        String[] stringTable = {
                "Harry Potter i kamień filozoficzny",
                "Harry Potter i komnata tajemnic",
                "Harry Potter i więzień Azkabanu",
                "Odyseja kosmiczna",
                "Odyseja czasu",
                "Parodia: Harry Potter",
                "Parodia: Harry Potter 2"
        };
        System.out.println("Zadanie 1.2: ");
        for (int i = 0; i < stringTable.length; i++) {
            switch (stringTable[i]) {
                case "Harry Potter i kamień filozoficzny":
                case "Harry Potter i komnata tajemnic":
                case "Harry Potter i więzień Azkabanu":
                    System.out.println(stringTable[i]);
            }
        }
        System.out.println("Zadanie 1.2 (wersja 2): ");
        for (String string: stringTable) {
            switch (string) {
                case "Harry Potter i kamień filozoficzny":
                case "Harry Potter i komnata tajemnic":
                case "Harry Potter i więzień Azkabanu":
                    System.out.println(string);
            }
        }
        System.out.println("Zadanie 1.3: ");
        Pattern harryPotterPattern = Pattern.compile("Harry Potter .+");
        Matcher harryPotterMatcher;
        for (int i = 0; i < stringTable.length; i++) {
            harryPotterMatcher = harryPotterPattern.matcher(stringTable[i]);
            if (harryPotterMatcher.find()) {
                System.out.println(stringTable[i]);
            }
        }
        System.out.println("Zadanie 1.3 (wersja 2): ");
        for (String string: stringTable) {
            harryPotterMatcher = harryPotterPattern.matcher(string);
            if (harryPotterMatcher.find()) {
                System.out.println(string);
            }
        }
        String[][] myStrings = new String[][]{
                {"Ala", "ma", "kota"},
                {"Kot", "ma", "Alę"}
        };
        for (String[] myString: myStrings) {
            System.out.println(Arrays.toString(myString));
        }
        int[][][] generatedNumber = new int[4][4][4];
        for (int i = 0; i < generatedNumber.length; i++) {
            for (int j = 0; j < generatedNumber[i].length; j++) {
                for (int k = 0; k < generatedNumber[i][j].length; k++) {
                    generatedNumber[i][j][k] = i + j + k;
                    System.out.print(generatedNumber[i][j][k] + " ");
                }
                System.out.println();
            }
            System.out.println();
        }
        System.out.println("\n\nZadanie 2.1: ");
        int[][] ships = new int[10][10];
        int n = 10;
        for (int i = 0; i < n; i++ ) {
            int firstCoord = (int) (Math.random() * 10);
            int secondCoord = (int) (Math.random() * 10);
            ships[firstCoord][secondCoord] = 1;
        }
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj x: ");
        int x = scanner.nextInt();
        System.out.print("Podaj y: ");
        int y = scanner.nextInt();
        boolean status = false;
        for (int i = 0; i < ships.length; i++) {
            for (int j = 0; j < ships[i].length; j++) {
                if (i == x && j == y) {
                    System.out.print("X");
                    if (ships[i][j] == 1) {
                        status = true;
                    }
                } else if (ships[i][j] == 0) {
                    System.out.print(" ");
                } else {
                    System.out.print("0");
                }
            }
            System.out.println();
        }
        if (status) {
            System.out.println("Trafiłeś!!!");
        } else {
            System.out.println("Pudło");
        }
        System.out.println("\n\nZadanie 2.2: ");
        int[][][] people = new int[3][3][3];
        int[] staircase0 = new int[]{1, 2, 4};
        int[] staircase1 = new int[]{2, 2, 2};
        int[] staircase2 = new int[]{3, 1, 6};
        people[0][0] = staircase0;
        people[0][1] = staircase0;
        people[0][2] = staircase0;
        people[1][0] = staircase1;
        people[1][1] = staircase1;
        people[1][2] = staircase1;
        people[2][0] = staircase2;
        people[2][1] = staircase2;
        people[2][2] = staircase2;
        for (int i = 0; i < people.length; i++) {
            int block = 0;
            System.out.println("###### Blok numer: " + i);
            for (int j = 0; j < people[i].length; j++) {
                int staircase = 0;
                for (int k = 0; k < people[i][j].length; k++) {
                    staircase += people[i][j][k];
                    block += people[i][j][k];
                }
                System.out.println("W klatce " + j + " mieszka " + staircase + " mieszkańców.");
            }
            System.out.println("W bloku " + i + " mieszka " + block + " mieszkańców.");
        }
    }
}
