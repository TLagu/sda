package pl.sda.s01_podstawy.s05_loops.examples;

import java.util.Scanner;

public class Main {
    private static boolean fast = true;
    public static void main(String[] args) {
        System.out.println("############# Exercise 1.1 #############");
        for (int i = 0; i < 3; i++) {
            System.out.println("Number of loop step is: " + (i + 1));
        }
        System.out.println("\n############# Exercise 1.2 #############");
        String[] pets = {"dog", "cat", "parrot"};
        for (String pet: pets) {
            System.out.println("Pet: " + pet);
        }
        System.out.println("\n############# Exercise 1.3 #############");
        int n = 10;
        int k = 8;
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            if (i <= k) {
                sum += i;
            }
        }
        System.out.println("Sum: " + sum);
        System.out.println("\n############# Exercise 1.4 #############");
        for (String pet : pets) {
            if (pet != "dog") {
                System.out.println("Pet: " + pet);
            }
        }
        System.out.println("\n############# Exercise 1.5 #############");
        int i = 0;
        while (i < 10) {
            System.out.println("Hello world");
            i++;
        }
        System.out.println("\n############# Exercise 1.6 #############");
        i = 0;
        do {
            System.out.println("Hello world");
            i++;
        } while (i < 10);
        System.out.println("\n############# Exercise 1.7 #############");
        i = 2;
        sum = 0;
        while (i < 11) {
            System.out.println(i);
            sum += i;
            i += 2;
        }
        System.out.println(sum);
        System.out.println("\n############# Exercise 1.8 #############");
        for (i = 0; i < 10; i++) {
            System.out.println("Hello World");
            if (i == 1) {
                break;
            }
        }
        System.out.println("\n############# Exercise 1.9 #############");
        for (i = 0; i < 10; i++) {
            if (i == 7) {
                continue;
            }
            System.out.println("Hello World " + i);
        }
        System.out.println("\n############# Exercise 1.10 #############");
        int nOddNumbers = 0;
        sum = 0;
        for (i = 1; i < 100; i++) {
            if ((i % 2 == 0) || (i % 3 == 0)) continue;
            System.out.println(i);
            sum += i;
            if (++nOddNumbers >= 20) break;
        }
        System.out.println(sum);
        System.out.println("\n############# Exercise 1.11 #############");
        Scanner scan = new Scanner(System.in);
        if (!fast) {
            System.out.print("Enter a value: ");
            String textLine = scan.nextLine();
            System.out.println(textLine);
        }
        System.out.println("\n############# Exercise 1.12 #############");
        if (!fast) {
            System.out.print("Your age: ");
            int intLine = scan.nextInt();
            System.out.println(intLine);
        }
        System.out.println("\n############# Exercise 1.13 #############");
        if (!fast) {
            System.out.print("First string: ");
            String textLine1 = scan.nextLine();
            System.out.print("Second string: ");
            String textLine2 = scan.nextLine();
            System.out.println(textLine1.concat(textLine2));
            System.out.println(textLine1.toUpperCase().concat(textLine2.toLowerCase()));
            System.out.println("Len 1: " + textLine1.length() + ", len 2: " + textLine2.length());
        }
        System.out.println("\n############# Exercise 1.14 #############");
        if (!fast) {
            System.out.print("int value: ");
            int intLine = scan.nextInt();
            sum = 0;
            if (intLine > 0) {
                for (i = 1; i < intLine; i++) sum += i;
            } else {
                for (i = -1; i > intLine; i--) sum += i;
            }
            System.out.println("sum: " + sum);
        }
        System.out.println("\n############# Exercise 1.15 #############");
        if (!fast) {
            System.out.print("double value (1): ");
            double doubleLine1 = scan.nextDouble();
            System.out.print("double value (2): ");
            double doubleLine2 = scan.nextDouble();
            System.out.print("double value (3): ");
            double doubleLine3 = scan.nextDouble();
            int average = (int) ((doubleLine1 + doubleLine2 + doubleLine3) / 3);
            System.out.println("average => (" + doubleLine1 + " + " + doubleLine2 + " + " + doubleLine3 + ") / 3 = " + average);
            System.out.println("squares list:");
            int j = 0;
            int val;
            while (j * j < average) {
                val = j * j;
                System.out.println(val);
                j++;
            }
        }
        //[A-ZĄĆĘŁŃÓŚŹŻ][a-ząćęłńóśźż]{1,10} [A-ZĄĆĘŁŃÓŚŹŻ][a-ząćęłńóśźż]{1,10}(-[A-ZĄĆĘŁŃÓŚŹŻ][a-ząćęłńóśźż]{0,10}){0,1}
        //[au]l\. [A-ZĄĆĘŁŃÓŚŹŻ][a-ząćęłńóśźż]{1,10} [0-9]{1,3}(\/\d{1,4})?
        System.out.println("\n\n\n\n############# Exercise 2.1 #############");
        for (i=1; i<=4; i++) {
            System.out.println("Java");
        }
        System.out.println("\n############# Exercise 2.2 #############");
        i=1;
        for ( ; i<=4; ) {
            System.out.println("Java");
            i++;
        }
        System.out.println("\n############# Exercise 2.3 #############");
        for (i=1; i<=10; i++) {
            System.out.println(i);
        }
        System.out.println("\n############# Exercise 2.4 #############");
        for (i=1; i<=10; i++) {
            System.out.print(i + " ");
        }
        System.out.println("\n############# Exercise 2.5 #############");
        for (i=10; i>=1; i--) {
            System.out.print(i + " ");
        }
        System.out.println("\n############# Exercise 2.6 #############");
        for (i=1; i<=10; i++) {
            System.out.println("i=" + i + "\ti^2=" + (i*i));
        }
        System.out.println("\n############# Exercise 2.7 #############");
        sum = 0;
        for (i=1; i<=10; i++) {
            sum += i;
        }
        System.out.println("Suma = " + sum);
        System.out.println("\n############# Exercise 2.8 #############");
        sum = 0;
        for (i=1; i<=10; i++) {
            if (i%2 == 0) {
                sum += i;
            }
        }
        System.out.println("Suma = " + sum);
        System.out.println("\n############# Exercise 2.9 #############");
        for (i=5; i<=100; i=i+5) {
            System.out.print(i + " ");
        }
        System.out.println("\n############# Exercise 2.10 #############");
        sum=0;
        int count=0;
        for (i=1; i<=200; i++) {
            sum += i;
            count++;
            if (sum >= 50) {
                break;
            }
        }
        System.out.println("Suma = " + sum);
        System.out.println("Liczba zsumowanych wartości = " + count);
        System.out.println("\n############# Exercise 2.11 #############");
        for (i=1; i<=20; i++) {
            if (i>=13 && i<=15) {
                continue;
            }
            System.out.print(i + " ");
        }
        System.out.println("\n############# Exercise 2.12 #############");
        int[] digits = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        for (int l : digits) {
            System.out.print(l + " ");
        }
        System.out.println("\n############# Exercise 2.13 #############");
        int oddCount = 0;
        int allCount = 0;
        for (int l : digits) {
            allCount++;
            if (l%2 != 0) {
                oddCount++;
                System.out.println(l + " is odd");
            }
        }
        System.out.println("Count: all=" + allCount + ", odd=" + oddCount);
        System.out.println("\n############# Exercise 2.14 #############");
        int max=10;
        i=1;
        while (i<=max) {
            System.out.print(i + " ");
            i++;
        }
        System.out.println("\n############# Exercise 2.15 #############");
        do {
            System.out.println("Java is great!!!");
        } while (false);
        System.out.println("\n############# Exercise 2.16 #############");
        for (i=1; i<=10; i++) {
            for (int j=1; j<=20; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
        System.out.println("\n############# Exercise 2.17 #############");
        for (i=1; i<=10; i++) {
            for (int j=1; j<=10; j++) {
                if ((i % 2 == 0 && j % 2 == 1) || (i % 2 == 1 && j % 2 == 0)) {
                    System.out.print("#");
                } else  {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
        System.out.println("\n############# Exercise 2.18 #############");
        System.out.println("\n############# Exercise 2.19 #############");
        System.out.println("\n############# Exercise 2.20 #############");
    }
}
