package pl.sda.s01_podstawy.s05_loops.tasks;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;

public class Main {
    public static void main(String[] args) {
        System.out.println("##################");
        System.out.println("### EXERCISE 1 ###");
        System.out.println("##################");
        System.out.println("Liczby od -20 do 20");
        int n = 20;
        int k = 6;
        for (int i = -n; i <= n; i++) {
            System.out.print(i + " ");
        }
        System.out.println("\n6 pierwszych liczb");
        int l = 0;
        for (int i = -n; i <= n; i++) {
            l++;
            if (l > k) {
                break;
            }
            System.out.print(i + " ");
        }
        System.out.println("\n6 ostatnich liczb");
        l = n - k;
        for (int i = -n; i <= n; i++) {
            if (i <= l) {
                continue;
            }
            System.out.print(i + " ");
        }
        System.out.println("\nwszystkie parzyste liczby");
        for (int i = -n; i <= n; i++) {
            if (i%2 == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println("\nwszystkie liczby oprócz cyfry 5");
        for (int i = -n; i <= n; i++) {
            if (i == 5) {
                continue;
            }
            System.out.print(i + " ");
        }
        System.out.println("\nwszystkie liczby do cyfry 7 włącznie");
        for (int i = -n; i <= n; i++) {
            System.out.print(i + " ");
            if (i >= 7) {
                break;
            }
        }
        System.out.println("\nwszystkie liczby podzielne przez 3");
        for (int i = -n; i <= n; i++) {
            if (i%3 == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println("\nsumę wszystkich liczb");
        int sum = 0;
        for (int i = -n; i <= n; i++) {
            sum += i;
        }
        System.out.print("sum: " + sum);
        System.out.println("\nsumę liczb większych lub równych 4");
        sum = 0;
        for (int i = -n; i <= n; i++) {
            if (i >= 4) {
                sum += i;
            }
        }
        System.out.print("sum: " + sum);
        System.out.println("\nwszystkie liczby oraz ich potęgi");
        for (int i = -n; i <= n; i++) {
            System.out.println("i: " + i + "\t\ti^2: " + (i * i));
        }
        System.out.println("wszystkie liczby oraz ich wartość modulo 10");
        for (int i = -n; i <= n; i++) {
            System.out.println("i: " + i + "\t\t(i)mod(10): " + (i%10));
        }
        System.out.println("##################");
        System.out.println("### EXERCISE 2 ###");
        System.out.println("##################");
        System.out.println("Korzystając z klasy oferującej operacje na dacie i czasie pobierz aktualny czas (godzinę, minutę i sekundę) oraz wypisz te wartości używając znaków \\*, których liczba równa się danej wartości. Dla utrudnienia, w jednym wierszu może być maksymalnie 10 znaków *.");
        LocalTime localTime = LocalTime.now();
        int hour = localTime.getHour();
        int minute = localTime.getMinute();
        int second = localTime.getSecond();
        System.out.println("Czas: " + localTime.truncatedTo(ChronoUnit.SECONDS));
        System.out.println("Hour: " + hour);
        for (int i = 0; i < hour; i++) {
            if (i > 0 && i % 10 == 0) {
                System.out.println("");
            }
            System.out.print("*");
        }
        System.out.println("\nMinute: " + minute);
        for (int i = 0; i < minute; i++) {
            if (i > 0 && i % 10 == 0) {
                System.out.println("");
            }
            System.out.print("*");
        }
        System.out.println("\nSecond: " + second);
        for (int i = 0; i < second; i++) {
            if (i > 0 && i % 10 == 0) {
                System.out.println("");
            }
            System.out.print("*");
        }
        System.out.println("\n##################");
        System.out.println("### EXERCISE 3 ###");
        System.out.println("##################");
        System.out.println("Strona z kalendarza");
        LocalDate localDate = LocalDate.now();
        System.out.println("Current Date = " + localDate);
        int currYear = localDate.getYear();
        int currMonth = localDate.getMonthValue();
        int currDay = localDate.getDayOfMonth();
        for (int i = 1; i <= 12; i++) {
            LocalDate date = LocalDate.of(currYear, i, 1);
            int lastDay = date.with(TemporalAdjusters.lastDayOfMonth()).getDayOfMonth();
            int weekDay = date.withDayOfMonth(1).getDayOfWeek().getValue();
            Month month = date.getMonth();
            showMonth(weekDay, lastDay, ((currMonth == i) ? currDay : 0), month);
        }
    }

    static private void showMonth(int weekDay, int lastDay, int currDay, Month month) {
        int spaces = (int) ((28 - month.toString().length()) / 2);

        System.out.println("\n" + (" ".repeat(spaces)) + month + "\n Mo  Tu  We  Th  Fr  Sa  Su ");
        if (weekDay > 1) {
            for (int i = 1; i < weekDay ; i++ ) {
                System.out.print("    ");
            }
        }
        for (int i = 1; i <= lastDay; i++) {
            if (((weekDay + i - 2)%7 == 0) && (i > 1)) {
                System.out.println("");
            }
            String space = "";
            if (i < 10) {
                space = " ";
            }
            if (i == currDay) {
                System.out.print("[" + space + i + "]");
            } else {
                System.out.print((" " + space + i + " "));
            }
        }
        System.out.println("");
    }
}
