package pl.sda.s03_podstawy_testy.tasks;

import java.time.LocalDate;

public class Validator {

    public static boolean isValid (LocalDate date) {
        if (date == null) {
            return false;
        }
        if (LocalDate.of(2021, 1, 1).isAfter(date)) {
            return false;
        }
        if (LocalDate.of (2021, 4, 1).isBefore(date)) {
            return false;
        }
        return true;
    }

    public static boolean isValid(String text) {
        if (text == null) {
            return false;
        }
        if (text.length() < 5 || 10 < text.length()) {
            return false;
        }
        return true;
    }
}
