package pl.sda.s03_podstawy_testy.tasks;

public class Calculator {
    public double add(double valueA, double valueB) {
        return valueA + valueB;
    }

    public double subtract(double valueA, double valueB) {
        return valueA - valueB;
    }

    public double multiply(double valueA, double valueB) {
        return valueA * valueB;
    }

    public double divide(double valueA, double valueB) {
        return valueA / valueB;
    }
}
