package pl.sda.s03_podstawy_testy.tasks;

import java.util.Arrays;

public class ArrayUtil {
    public static String[] removeElements(String[] sampleArray, String ... elementsToRemove) {
        System.out.println("sampleArray: " + Arrays.toString(sampleArray));
        System.out.println("elementsToRemove: " + Arrays.toString(elementsToRemove));
        if (sampleArray == null || elementsToRemove == null) {
            return new String[0];
        }

        int sizeOfNewTable = sampleArray.length;
        for (String s : elementsToRemove) {
            for (int j = 0; j < sampleArray.length; j++) {
                if (sampleArray[j].equals(s)) {
                    sizeOfNewTable--;
                    sampleArray[j] = null;
                }
            }
        }

        String[] result = new String[sizeOfNewTable];
        int tempIndex = 0;
        for (String s : sampleArray) {
            if (s != null) {
                result[tempIndex] = s;
                tempIndex++;
            }
        }
        System.out.println("result: " + Arrays.toString(result));
        return result;
    }
}
