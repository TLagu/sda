package pl.sda.s04_zaawansowane.e11_functionals.task04;

import java.util.HashMap;

public class PhoneBookService {
    public static void main(String[] args) {
        PhoneBook phoneBook = new PhoneBook();
        HashMap<String, String> entries = phoneBook.getPhoneBookEntries();
        System.out.println(phoneBook.getPhoneBookEntries());
        System.out.println(phoneBook.findPhoneNumberByName("Jos de Vos"));
        System.out.println(phoneBook.findNameByPhoneNumber("016/161617"));
    }
}
