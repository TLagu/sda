package pl.sda.s04_zaawansowane.e11_functionals.task05;

public class PhoneBookService {
    public static void main(String[] args) {
        PhoneBook phoneBook = new PhoneBook();
        System.out.println(phoneBook.getPhoneBookEntries());
        System.out.println(phoneBook.findPhoneNumberByName("Jos de Vos"));
        System.out.println(phoneBook.findNameByPhoneNumber("016/161617"));
        System.out.println("-------------------");
        PhoneBookCrawler phoneBookCrawler = new PhoneBookCrawler(phoneBook);
        //System.out.println(phoneBookCrawler.findPhoneNumberByNameAndPunishIfNothingFound("Jos de Vo"));
        System.out.println(phoneBookCrawler.findPhoneNumberByNameAndPunishIfNothingFound("Jos de Vos"));
        System.out.println(phoneBookCrawler.findPhoneNumberByNameAndPrintPhoneBookIfNothingFound("Jos de Vo"));
        System.out.println(phoneBookCrawler.findPhoneNumberByNameOrNameByPhoneNumber("Jos de Vos", null));
        System.out.println(phoneBookCrawler.findPhoneNumberByNameOrNameByPhoneNumber(null, "016/161617"));
        System.out.println(phoneBookCrawler.getPhoneBook());
    }
}
