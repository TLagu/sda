package pl.sda.s04_zaawansowane.e11_functionals.task05;

import java.util.Optional;

public class PhoneBookCrawler {
    private final PhoneBook phoneBook;

    public PhoneBookCrawler(PhoneBook phoneBook) {
        this.phoneBook = phoneBook;
    }

    public String findPhoneNumberByNameAndPunishIfNothingFound(String name) {
        Optional<String> possibleNumber = phoneBook.findPhoneNumberByName(name);
        return possibleNumber.orElseThrow(IllegalArgumentException::new);
    }

    public String findPhoneNumberByNameAndPrintPhoneBookIfNothingFound(String name) {
        Optional<String> possibleNumber = phoneBook.findPhoneNumberByName(name);
        return possibleNumber.orElseGet(phoneBook::toString);
    }

    public String findPhoneNumberByNameOrNameByPhoneNumber(String name, String phoneNumber) {
        Optional<String> possibleNumber = Optional.empty();
        Optional<String> possibleName = Optional.empty();
        if (name != null && !name.isBlank()) {
            possibleNumber = phoneBook.findPhoneNumberByName(name);
        }
        if (phoneNumber != null && !phoneNumber.isBlank()) {
            possibleName = phoneBook.findNameByPhoneNumber(phoneNumber);
        }
        if (possibleNumber.isPresent()) {
            return possibleNumber.get();
        } else if (possibleName.isPresent()) {
            return possibleName.get();
        }
        return null;
    }

    public PhoneBook getPhoneBook() {
        return phoneBook;
    }
}
