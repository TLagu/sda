package pl.sda.s04_zaawansowane.e11_functionals.task03;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;

public class StringFormatter {
    public static void main(String[] args) {
        List<String> values = List.of("First string", "Second string", "Third string");
        //System.out.println(format(p -> p.toUpperCase(Locale.ROOT)));
        System.out.println(Arrays.toString(format(values, p -> p.toUpperCase(Locale.ROOT)).toArray()));
    }

    private static List<String> format(List<String> values, Function<String, String> formatter) {
        List<String> results = new ArrayList<>();
        for(String value : values) {
            results.add(formatter.apply(value));
        }
        return results;
    }

    private static String format(Function<String, String> formatter) {
        String value = "Ja";
        return formatter.apply(value);
    }
}
