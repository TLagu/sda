package pl.sda.s04_zaawansowane.e11_functionals.examples.e02;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class FunctionExample {
    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(new Employee("Edward"), new Employee("Marcin"));
        showEmployee(employees, Employee::getName);
    }

    static void showEmployee(List<Employee> employees, Function<Employee, String> showFunction) {
        for (Employee employee : employees) {
            System.out.println(showFunction.apply(employee));
        }
    }
}
