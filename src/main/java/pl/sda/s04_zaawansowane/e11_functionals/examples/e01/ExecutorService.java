package pl.sda.s04_zaawansowane.e11_functionals.examples.e01;

public class ExecutorService {

    static void testExecutor(Executor executor) {
        executor.executor(2, 3);
        System.out.println("xxxxxxx");
        executor.executor(5);
    }

    static void sampleExecutor(int x) {
        System.out.println("in sample executor...: x = " + x);
    }

    public static void main(String[] args) {
        // 1. tradycyjny
        ExecutorImpl executor = new ExecutorImpl();
        testExecutor(executor);

        // 2. klasa anonimowa
        System.out.println("-------------------------");
        testExecutor(new Executor() {
            @Override
            public void executor(int x) {
                System.out.println("In anonymous class...: x = " + x);
            }

//            @Override
//            public void executor(int x, int y) {
//                System.out.println("In anonymous class...: x = " + x + ", y = " + y);
//            }
        });

        // 3. lambda
        System.out.println("-------------------------");
        testExecutor(x -> System.out.println("In lambda expression...: x = " + x));

        // 4. lambda (exception)
//        System.out.println("-------------------------");
//        testExecutor(x -> {
//            throw new IllegalArgumentException();
//        });

        // 5. lambda (assignment)
        System.out.println("-------------------------");
        Executor lambdaExecutor = p -> System.out.println("In lambda expression... local variable");
        testExecutor(lambdaExecutor);

        // 6. method reference
        System.out.println("-------------------------");
        //Executor referenceExecutor = p -> ExecutorService::sampleExecutor(p);
        //Executor referenceExecutor = ExecutorService::sampleExecutor;
        testExecutor(ExecutorService::sampleExecutor);

        // 6. executor provider
        System.out.println("-------------------------");
        ExecutorProvider executorProvider = new ExecutorProvider();
        Executor extraExecutor = executorProvider::provide;
        testExecutor(extraExecutor);
    }
}
