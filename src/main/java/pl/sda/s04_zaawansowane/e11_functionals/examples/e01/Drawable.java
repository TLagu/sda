package pl.sda.s04_zaawansowane.e11_functionals.examples.e01;

public interface Drawable {
    default void executor(int x, int y) {
        System.out.println("In default method of Drawable: x = " + x + ", y = " + y);
    }
}
