package pl.sda.s04_zaawansowane.e11_functionals.examples.e02;

import java.util.function.Predicate;

public class PredicateExample {
    public static void main(String[] args) {
        Predicate<Integer> predicate = p -> p >= 0;
        checkTest(predicate, -1);
        checkTest(predicate, 0);
        checkTest(predicate, 100);
    }

    private static void checkTest(Predicate<Integer> predicate, int parameter) {
        System.out.println(predicate.test(parameter));
    }
}
