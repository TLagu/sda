package pl.sda.s04_zaawansowane.e11_functionals.examples.e02;

import java.util.Random;
import java.util.function.Supplier;

public class SupplierExample {
    public static void main(String[] args) {
        //printValue(() -> 5);
        Random random = new Random();
        //printValue(() -> random.nextInt(10));
        printValue(random::nextInt);
    }

    static void printValue(Supplier<Integer> intSupplier) {
        Integer intValue = intSupplier.get();
        System.out.println(intValue);
    }
}
