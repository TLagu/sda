package pl.sda.s04_zaawansowane.e11_functionals.examples.e02;

import java.util.Locale;
import java.util.function.Consumer;

public class ConsumerExample {
    public static void main(String[] args) {
        trimValue(s -> System.out.println(s.trim().toLowerCase(Locale.ROOT)), "          TeXt           ");
    }

    private static void trimValue(Consumer<String> stringTrim, String s) {
        stringTrim.accept(s);
    }
}
