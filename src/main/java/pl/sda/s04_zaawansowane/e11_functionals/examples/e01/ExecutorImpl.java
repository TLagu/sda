package pl.sda.s04_zaawansowane.e11_functionals.examples.e01;

import lombok.Getter;
import lombok.Setter;

public class ExecutorImpl implements Executor, Drawable {
    @Getter @Setter
    private String name;

    @Override
    public void executor(int x) {
        System.out.println("in executor(int x) ... (ExecutorImpl): x = " + x);
    }

    @Override
    public void executor(int x, int y) {
        Executor.super.executor(x, y);
        System.out.println("... In executor(int x, int y) of ExecutorImpl");
        Drawable.super.executor(x, y);
    }
}
