package pl.sda.s04_zaawansowane.e11_functionals.examples.e01;

public class ExecutorProvider {

    public void provide(int parameter) {
        System.out.println("In executor provider...");
    }
}
