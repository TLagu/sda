package pl.sda.s04_zaawansowane.e11_functionals.examples.e02;

import java.util.Optional;

public class OptionalService {

    private final static int AGE = 33;
    private final static String NAME = "Piotr";
    private final static String NAME2 = "Kazimierz";

    public static void main(String[] args) throws InterruptedException {
        OptionalService optionalService = new OptionalService();

        // 1.a.
        System.out.println("1.a. Standard: ");
        Integer age = optionalService.getAge(NAME);
        if (age != null) {
            System.out.println(age * age);
        }
        // 1.b.
        System.out.println("1.b. Standard: ");
        if (age != null) {
            int newValue = age * age;
            if (newValue > 600) {
                System.out.println(age * age);
            }
        }

        // 2.
        System.out.println("2. Optional: ");
        Optional<Integer> possibleAge1 = optionalService.getPossibleAge(NAME);
        if (possibleAge1 != null && possibleAge1.isPresent()) {
            Integer extractedAge = possibleAge1.get();
            System.out.println(extractedAge * extractedAge);
        } else {
            System.out.println(" - Optional is empty");
        }

        // 3.
        System.out.println("3. Optional if: ");
        optionalService.getPossibleAge(NAME).map(p -> p * p)
                .ifPresent(System.out::println);

        // 4.a.
        System.out.println("4.b. Optional if or else (filter): ");
        optionalService.getPossibleAge(NAME).map(p -> p * p).filter(p -> p > 600)
                .ifPresentOrElse(System.out::println, () -> System.out.println(" - Optional is empty"));

        // 5.
        Optional<String> possibleValue = Optional.ofNullable("Sample item");

        // 6.
        System.out.println("6. Optional or else: ");
//        Optional<Integer> optional2 = optionalService.getPossibleAge(NAME2);
//        System.out.println(optional2.orElse(100));
        Optional<Integer> optional2 = optionalService.getPossibleAge(NAME);
        Integer age2 = optional2.orElse(optionalService.getDefaultAge());
        System.out.println(age2);

        Optional<Integer> optional3 = optionalService.getPossibleAge(NAME);
        Integer age3 = optional3.orElseGet(optionalService::getDefaultAge);
        System.out.println(age3);
    }

    public Integer getAge(String name) {
        return (name.equalsIgnoreCase("piotr")) ? AGE : null;
    }

    public Optional<Integer> getPossibleAge(String name) {
        return (name.equalsIgnoreCase("piotr")) ? Optional.of(AGE) : Optional.empty();
    }

    public Integer getDefaultAge() {
        System.out.println("I'm in getDefaultAge method");
        int k = 10;
        for (int i = 0; i < 10; i++) {
            k++;
            k--;
        }
        return k;
    }
}
