package pl.sda.s04_zaawansowane.e11_functionals.examples.e01;

public interface Action {
    String execute(int x, int y);
}
