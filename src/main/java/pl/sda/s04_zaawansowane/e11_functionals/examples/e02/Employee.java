package pl.sda.s04_zaawansowane.e11_functionals.examples.e02;

class Employee {
    private String name;

    public Employee(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
