package pl.sda.s04_zaawansowane.e11_functionals.task02;

import java.util.List;
import java.util.function.Consumer;

public class ConsumerExample {

    public static void main(String[] args) {
        List<String> values = List.of("First string", "Second string", "Third string");
        Consumer<String> consumer = n -> System.out.println(n);
        values.forEach(consumer);
    }
}
