package pl.sda.s04_zaawansowane.e11_functionals.task01;

import java.util.ArrayList;
import java.util.List;

public class ArrayListExample {
    public static void main(String[] args) {
        List<String> values = List.of("First string", "Second string", "Third string");
        values.forEach(System.out::println);
    }
}
