package pl.sda.s04_zaawansowane.e14_generics.examples.e02;

public class numberBoxService {
    public static void main(String[] args) {
        NumberBox<Integer> intBox = new NumberBox<>(5);
        System.out.println(intBox.getValue());
    }
}
