package pl.sda.s04_zaawansowane.e14_generics.examples.e02;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class NumberBox<T extends Number> {
    private T value;
}
