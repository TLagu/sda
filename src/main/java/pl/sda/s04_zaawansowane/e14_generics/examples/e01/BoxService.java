package pl.sda.s04_zaawansowane.e14_generics.examples.e01;

import java.util.ArrayList;

public class BoxService {
    public static void main(String[] args) {
        var name = "Bartosz";
        var age = "21";
        var intList = new ArrayList<Integer>();
        Box<String> stringBox = new Box<>();

        stringBox.setItem("ball");
        System.out.println(stringBox.getItem());

        Box<Integer> box = new Box<>();
        // Tak nie można
        //Box<Number> box = new Box<Integer>();
    }
}
