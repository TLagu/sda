package pl.sda.s04_zaawansowane.e14_generics.examples.e03;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SuperBox<T extends Item & Comparable<T>> {
    private T value;
}
