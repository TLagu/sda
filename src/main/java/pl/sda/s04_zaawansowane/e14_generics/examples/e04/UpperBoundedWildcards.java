package pl.sda.s04_zaawansowane.e14_generics.examples.e04;

import java.util.ArrayList;
import java.util.List;

public class UpperBoundedWildcards {
    public static double sum(List<? extends Number> list) {
        double sum = 0.0;
        for (Number number : list) {
            sum += number.doubleValue();
        }
        return sum;
    }

    public static void main(String[] args) {
        List<Integer> intList = List.of(1, 2, 3);
        System.out.println(sum(intList));

        List<Double> doubleList = List.of(2.0, 3.0, 4.5);
        System.out.println(sum(doubleList));

//        List<? extends Number> mixedList = new ArrayList<>();
//        mixedList.add(1);


    }
}
