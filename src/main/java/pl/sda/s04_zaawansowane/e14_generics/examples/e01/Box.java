package pl.sda.s04_zaawansowane.e14_generics.examples.e01;

public class Box<T> {
    private T item;

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }
}
