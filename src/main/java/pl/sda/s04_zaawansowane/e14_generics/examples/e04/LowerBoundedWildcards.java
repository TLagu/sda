package pl.sda.s04_zaawansowane.e14_generics.examples.e04;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LowerBoundedWildcards {
    public static void addNumbers(List<? super Integer> list) {
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }
        for (Object o : list) {
            System.out.println(o);
        }
    }

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        addNumbers(list);
        // List.of tworzy niemodyfikowalną listę!!!
//        addNumbers(List.of(1, 2, 3));
//        addNumbers(List.of(new Object(), new Object(), new Object()));
//        addNumbers(List.of((byte) 1, (byte) 2, (byte) 3));
        System.out.println("--------------------------------");
        List<Object> objectList = new ArrayList<>();
        objectList.add(1);
        objectList.add("ball");
        objectList.add(new Date());
        addNumbers(objectList);
        System.out.println("--------------------------------");
        // to jest dolne ograniczenie, więc to niżej nie przejdzie.
//        List<Byte> byteList = new ArrayList<>();
//        objectList.add(1);
//        objectList.add(2);
//        objectList.add(3);
//        addNumbers(byteList);
    }
}
