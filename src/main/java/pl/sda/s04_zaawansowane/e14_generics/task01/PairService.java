package pl.sda.s04_zaawansowane.e14_generics.task01;

public class PairService {
    public static void main(String[] args) {
        Pair<Object1, Object2> pair1 = new Pair<>(new Object1(1, "Name"), new Object2(2, "Firstname", "lastname"));
        System.out.println(pair1);
        Pair<Object3, Object4> pair2 = new Pair<>(new Object3(3, new Object1(1, "Name")), new Object4(4, 10));
        System.out.println(pair2);
        Pair<Object5, Object1> pair3 = new Pair<>(new Object5(5, 12.4), new Object1(1, "Name"));
        System.out.println(pair3);
    }
}
