package pl.sda.s04_zaawansowane.e14_generics.task01;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class Object4 {
    private final long id;
    private final int edge;
}
