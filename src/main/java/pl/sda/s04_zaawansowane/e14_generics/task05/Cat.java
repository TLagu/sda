package pl.sda.s04_zaawansowane.e14_generics.task05;

import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class Cat {
    private final String name;
    private final double weight;
}
