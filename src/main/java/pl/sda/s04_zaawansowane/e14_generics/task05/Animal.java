package pl.sda.s04_zaawansowane.e14_generics.task05;

import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@ToString
public class Animal<T> {
    private final List<T> animals = new ArrayList<>();

    public void addAnimal(T animal) {
        animals.add(animal);
    }
}
