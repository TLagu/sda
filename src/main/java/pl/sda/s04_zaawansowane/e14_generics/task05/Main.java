package pl.sda.s04_zaawansowane.e14_generics.task05;

public class Main {
    public static void main(String[] args) {
        Animal<Cat> cats = new Animal<>();
        Animal<Dog> dogs = new Animal<>();
        cats.addAnimal(new Cat("cat 1", 10.0));
        cats.addAnimal(new Cat("cat 2", 12.0));
        cats.addAnimal(new Cat("cat 3", 8.0));
        cats.addAnimal(new Cat("cat 4", 20.0));
        dogs.addAnimal(new Dog("Dog 1", 20.0));
        dogs.addAnimal(new Dog("Dog 2", 30.0));
        dogs.addAnimal(new Dog("Dog 3", 40.0));
        dogs.addAnimal(new Dog("Dog 4", 25.0));
        dogs.addAnimal(new Dog("Dog 5", 15.0));
        System.out.println("Cats: " + cats);
        System.out.println("Dogs: " + dogs);
    }
}
