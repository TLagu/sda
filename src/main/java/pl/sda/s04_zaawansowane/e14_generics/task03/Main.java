package pl.sda.s04_zaawansowane.e14_generics.task03;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        System.out.println("Class: ");
        Swap<Integer> swap = new Swap<>();
        Integer[] intArray = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println(Arrays.toString(intArray));
        swap.swap(intArray, 4, 6);
        System.out.println(Arrays.toString(intArray));
        System.out.println("Method: ");
        System.out.println(Arrays.toString(Main.swap(intArray, 4, 6)));
    }

    public static <T> T[] swap(T[] array, int i, int j) {
        if (array.length == 0) {
            throw new IllegalArgumentException();
        }
        if (array.length <= i || i <= 0) {
            throw new IllegalArgumentException();
        }
        if (array.length <= j || j <= 0) {
            throw new IllegalArgumentException();
        }
        T tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
        return array;
    }
}
