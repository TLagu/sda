package pl.sda.s04_zaawansowane.e14_generics.task03;

public class Swap<T> {

    public T[] swap(T[] array, int i, int j) {
        if (array.length == 0) {
            throw new IllegalArgumentException();
        }
        if (array.length <= i || i <= 0) {
            throw new IllegalArgumentException();
        }
        if (array.length <= j || j <= 0) {
            throw new IllegalArgumentException();
        }
        T tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
        return array;
    }
}
