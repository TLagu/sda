package pl.sda.s04_zaawansowane.e14_generics.task04;

import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class Magazine {
    private final String title;
    private final String publishing_house;
}
