package pl.sda.s04_zaawansowane.e14_generics.task04;

public class Main {
    public static void main(String[] args) {
        Library<Book> books = new Library<>();
        books.addToLibrary(new Book("Book 1", "Author 1"));
        books.addToLibrary(new Book("Book 2", "Author 2"));
        books.addToLibrary(new Book("Book 3", "Author 3"));
        books.addToLibrary(new Book("Book 4", "Author 4"));
        books.addToLibrary(new Book("Book 5", "Author 5"));
        System.out.println(books);
        Library<Magazine> magazines = new Library<>();
        magazines.addToLibrary(new Magazine("Magazine 1", "Publishing home 1"));
        magazines.addToLibrary(new Magazine("Magazine 2", "Publishing home 2"));
        magazines.addToLibrary(new Magazine("Magazine 3", "Publishing home 3"));
        magazines.addToLibrary(new Magazine("Magazine 4", "Publishing home 4"));
        System.out.println(magazines);
        Library<Movie> movies = new Library<>();
        movies.addToLibrary(new Movie("Movie 1", "Director 1"));
        movies.addToLibrary(new Movie("Movie 2", "Director 2"));
        movies.addToLibrary(new Movie("Movie 3", "Director 3"));
        System.out.println(movies);
    }
}
