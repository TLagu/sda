package pl.sda.s04_zaawansowane.e14_generics.task04;

import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class Movie {
    private final String title;
    private final String director;
}
