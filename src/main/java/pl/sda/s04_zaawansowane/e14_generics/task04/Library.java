package pl.sda.s04_zaawansowane.e14_generics.task04;

import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@ToString
public class Library<T> {
    private List<T> library = new ArrayList<>();

    public void addToLibrary(T object) {
        library.add(object);
    }
}
