package pl.sda.s04_zaawansowane.e14_generics.task02;

public interface CountIf<T> {
    int countIf(T[] item);
}
