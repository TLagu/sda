package pl.sda.s04_zaawansowane.e14_generics.task02;

public class Main {
    public static void main(String[] args) {
        System.out.println("klasy: ");
        CountIf<Integer> object1 = new CountIf() {
            @Override
            public int countIf(Object[] item) {
                return item.length;
            }
        };
        Integer[] intArray = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println("Ilość obiektów: " + object1.countIf(intArray));
        CountIf<String> object2 = new CountIf() {
            @Override
            public int countIf(Object[] item) {
                return item.length;
            }
        };
        String[] stringArray = new String[]{"1", "2", "3", "4", "5"};
        System.out.println("Ilość obiektów: " + object2.countIf(stringArray));
        System.out.println("Metody: ");
        System.out.println("Ilość obiektów: " + Main.countIf(intArray));
        System.out.println("Ilość obiektów: " + Main.countIf(stringArray));
    }

    public static <T> int countIf(T[] object) {
        return object.length;
    }
}
