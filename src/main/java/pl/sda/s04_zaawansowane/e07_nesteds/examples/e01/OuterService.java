package pl.sda.s04_zaawansowane.e07_nesteds.examples.e01;

public class OuterService {
    public static void main(String[] args) {
        Outer outer = new Outer();
        outer.setOuterNestedStaticField(1);
        outer.setOuterNestedStaticField(2);
        Outer.Inner inner = outer.new Inner();

        Outer.NestedStatic nestedStatic = new Outer.NestedStatic();
    }
}
