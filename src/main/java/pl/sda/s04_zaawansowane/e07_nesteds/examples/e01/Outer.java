package pl.sda.s04_zaawansowane.e07_nesteds.examples.e01;

public class Outer {

    private static int outerNestedStaticField;
    private int outerInnerField;

    public static void setOuterNestedStaticField(int outerNestedStaticField) {
        Outer.outerNestedStaticField = outerNestedStaticField;
    }

    public void setOuterInnerField(int outerInnerField) {
        this.outerInnerField = outerInnerField;
    }

    public static class NestedStatic {
        public void printValues () {
            System.out.println(outerNestedStaticField);
        }
    }

    public class Inner {
        public void printValues () {
            System.out.println(outerNestedStaticField);
            System.out.println(outerInnerField);
        }
    }
}
