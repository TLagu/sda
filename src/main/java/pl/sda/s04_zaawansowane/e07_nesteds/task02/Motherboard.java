package pl.sda.s04_zaawansowane.e07_nesteds.task02;

public class Motherboard {
    private double price;

    public class Processor {
        private int cores;
        private String manufacturer;

        public double getCache() {
            return 4.3;
        }
    }

    public class Ram {
        private int memory;
        private String manufacturer;

        public String getClockSpeed() {
            return "3600 MHz";
        }
    }
}
