package pl.sda.s04_zaawansowane.e07_nesteds.task02;

public class MotherboardService {
    public static void main(String[] args) {
        Motherboard motherboard = new Motherboard();
        Motherboard.Processor processor = motherboard.new Processor();
        System.out.println("Cache: " + processor.getCache());
        Motherboard.Ram ram = motherboard.new Ram();
        System.out.println("Clock speed: " + ram.getClockSpeed());
    }
}
