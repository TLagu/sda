package pl.sda.s04_zaawansowane.e07_nesteds.task01;

public class ComputerService {
    public static void main(String[] args) {
        Computer.ComputerBuilder builder = new Computer.ComputerBuilder("1 TB", "128 GB");
        builder.setGraphicsCardEnabled(true);

        Computer computer = builder.build();
        System.out.println(computer);

    }
}
