package pl.sda.s04_zaawansowane.e07_nesteds.task03;

public enum CarType {
    ECONOMY("economy"),
    LUXURY("luxury"),
    OTHER_1("other 1"),
    OTHER_2("other 2");

    private final String carType;

    CarType(String carType) {
        this.carType = carType;
    }

    public String getCarType() {
        return carType;
    }
}
