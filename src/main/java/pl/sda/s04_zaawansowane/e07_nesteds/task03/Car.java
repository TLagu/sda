package pl.sda.s04_zaawansowane.e07_nesteds.task03;

public class Car {
    private String carBrand;
    private CarType carType;

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    @Override
    public String toString() {
        return "Car -> carBrand = '" + carBrand + ", carType = " + carType.getCarType();
    }

    public class Engine {
        private EngineType engineType;

        public EngineType getEngineType() {
            return engineType;
        }

        public void setEngineType() {
            if (carType == null) {
                return;
            }
            switch (carType) {
                case ECONOMY:
                    engineType = EngineType.DIESEL;
                    break;
                case LUXURY:
                    engineType = EngineType.ELECTRIC;
                    break;
                default:
                    engineType = EngineType.PATROL;
            }
        }

        @Override
        public String toString() {
            return "Engine -> engineType = " + engineType;
        }
    }
}
