package pl.sda.s04_zaawansowane.e07_nesteds.task03;

public enum EngineType {
    DIESEL("diesel"),
    ELECTRIC("electric"),
    PATROL("petrol");

    private final String engineType;

    EngineType(String engineType) {
        this.engineType = engineType;
    }

    public String getEngineType() {
        return engineType;
    }
}
