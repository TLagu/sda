package pl.sda.s04_zaawansowane.e07_nesteds.task03;

public class CarService {
    public static void main(String[] args) {
        Car car1 = new Car();
        car1.setCarBrand("VW");
        car1.setCarType(CarType.ECONOMY);
        Car.Engine engine1 = car1.new Engine();
        System.out.println(car1);
        System.out.println(engine1);
        engine1.setEngineType();
        System.out.println("--------------------");
        System.out.println(engine1);
        System.out.println("--------------------");
        System.out.println("--------------------");
        Car car2 = new Car();
        car2.setCarBrand("VW");
        car2.setCarType(CarType.OTHER_1);
        Car.Engine engine2 = car2.new Engine();
        System.out.println(car2);
        System.out.println(engine2);
        engine2.setEngineType();
        System.out.println("--------------------");
        System.out.println(engine2);
        System.out.println("--------------------");
    }
}
