package pl.sda.s04_zaawansowane.e06_annotations.examples.e01;

public class SdaCourse {
    @SdaAnnotation("110")
    private String name;
    private int duration;

    @SdaAnnotation("110")
    public SdaCourse(String name, int duration) {
        this.name = name;
        this.duration = duration;
    }

//    @SdaAnnotation("110")
    public String getDescription() {
        return "name -> " + name + ", duration -> " + duration;
    }
}
