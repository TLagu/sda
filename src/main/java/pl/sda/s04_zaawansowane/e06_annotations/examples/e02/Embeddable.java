package pl.sda.s04_zaawansowane.e06_annotations.examples.e02;

@FunctionalInterface
public interface Embeddable {
    String getObject();
}
