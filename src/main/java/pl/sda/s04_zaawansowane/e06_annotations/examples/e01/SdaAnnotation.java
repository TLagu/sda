package pl.sda.s04_zaawansowane.e06_annotations.examples.e01;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ElementType.CONSTRUCTOR, ElementType.FIELD})
public @interface SdaAnnotation {
    String value();

}
