package pl.sda.s04_zaawansowane.e09_local.task01;

public class UsernameValidator {
    public String validateName(String name) {
        class UserName {
            private static final String UNKNOWN = "unknown";
            private static final String INVALID = "invalid";
            String format() {
                if (name == null || name.isEmpty()) {
                    return UNKNOWN;
                }
                if (name.length() < 4) {
                    return INVALID;
                }
                return name;
            }
        }
        UserName userName = new UserName();
        return userName.format();
    }
}
