package pl.sda.s04_zaawansowane.e09_local.examples.e02;

import lombok.AllArgsConstructor;

import java.util.List;

public class LocalService {
    public static void main(String[] args) {
        List<String> names = List.of("Kasia", "Magda", "Gosia");
        List<String> surnames = List.of("Piszczyk", "Olszańska", "Budrzeńska");
        int someVariable = 3;

        @AllArgsConstructor
        class Name {
            private final String firstName;
            private final String lastName;
            // wszystkie pola uzyte tutaj z klasy zewnetrznej MUSZĄ być typu final (choćby niejawnie)
            public String getReadableName() {
                System.out.println("Hey I can use outer variable" + someVariable);
                return firstName + " " + lastName;
            }
        }

//        someVariable++;

        for (int idx = 0; idx < names.size(); idx++) {
            Name name = new Name(names.get(idx), surnames.get(idx));
            System.out.println(name.getReadableName());
        }
    }
}
