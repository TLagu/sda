package pl.sda.s04_zaawansowane.e09_local.examples.e01;

public class PersonService {
    public static void main(String[] args) {
        Person person1 = new Person("Tom", "Sawyer", 12);
        Person person2 = new Person();
        //person2.setAge(22);
        System.out.println(person1.getAge());
        System.out.println(person1);
    }
}
