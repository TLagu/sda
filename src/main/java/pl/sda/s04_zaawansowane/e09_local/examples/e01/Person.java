package pl.sda.s04_zaawansowane.e09_local.examples.e01;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter(AccessLevel.PACKAGE)
@Setter
@ToString
//@Data
public class Person {
    private String firstName;
    private String lastName;
    @Setter(AccessLevel.NONE)
    private int age;

}
