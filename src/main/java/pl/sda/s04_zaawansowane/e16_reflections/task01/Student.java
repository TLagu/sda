package pl.sda.s04_zaawansowane.e16_reflections.task01;

public class Student {
    private String firstName;
    private String lastName;
    private String courseRecord;
    private String degreeCourse;

    public Student() {

    }

    public Student(String firstName, String lastName, String courseRecord, String degreeCourse) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.courseRecord = courseRecord;
        this.degreeCourse = degreeCourse;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCourseRecord() {
        return courseRecord;
    }

    public void setCourseRecord(String courseRecord) {
        this.courseRecord = courseRecord;
    }

    public String getDegreeCourse() {
        return degreeCourse;
    }

    public void setDegreeCourse(String degreeCourse) {
        this.degreeCourse = degreeCourse;
    }
}
