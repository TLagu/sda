package pl.sda.s04_zaawansowane.e16_reflections.task01;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class StudentExample {
    public static void main(String[] args) {
        System.out.println("Methods available: ");
        final Method[] methods = Student.class.getDeclaredMethods();
        for(Method method : methods){
            System.out.println(" - " + method);
        }

        System.out.println("Available fields: ");
        final Field[] fields = Student.class.getDeclaredFields();
        for(Field field : fields){
            System.out.println(" - " + field);
        }

        System.out.println("Constructors available: ");
        final Constructor<Student>[] constructors = (Constructor<Student>[]) Student.class.getConstructors();
        for(Constructor<Student> constructor : constructors){
            System.out.println(" - " + constructor);
        }
    }
}
