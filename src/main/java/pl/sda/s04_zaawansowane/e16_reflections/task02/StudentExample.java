package pl.sda.s04_zaawansowane.e16_reflections.task02;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class StudentExample {
    public static void main(String[] args) throws
            ClassNotFoundException, InstantiationException, IllegalAccessException,
            NoSuchMethodException, InvocationTargetException, NoSuchFieldException {
        Class<?> studentClass = Class.forName("pl.sda.s04_zaawansowane.e16_reflections.task02.Student");

        Student student = (Student) studentClass.newInstance();

        Method setFirstNameMethod = studentClass.getDeclaredMethod("setFirstName", String.class);
        Method setLastNameMethod = studentClass.getDeclaredMethod("setLastName", String.class);
        Method setCourseRecordMethod = studentClass.getDeclaredMethod("setCourseRecord", String.class);
        Method setDegreeCourseMethod = studentClass.getDeclaredMethod("setDegreeCourse", String.class);
        Method getFirstNameMethod = studentClass.getDeclaredMethod("getFirstName");
        Method getLastNameMethod = studentClass.getDeclaredMethod("getLastName");
        Method getCourseRecordMethod = studentClass.getDeclaredMethod("getCourseRecord");
        Method getDegreeCourseMethod = studentClass.getDeclaredMethod("getDegreeCourse");

        setFirstNameMethod.invoke(student, "First Name");
        setLastNameMethod.invoke(student, "Last Name");
        setCourseRecordMethod.invoke(student, "Course Record");
        setDegreeCourseMethod.invoke(student, "Degree Course");
        System.out.println("The original values of the attributes: ");
        System.out.println("First Name: " + getFirstNameMethod.invoke(student));
        System.out.println("Last Name: " + getLastNameMethod.invoke(student));
        System.out.println("Course Record: " + getCourseRecordMethod.invoke(student));
        System.out.println("Degree Course: " + getDegreeCourseMethod.invoke(student));

        Field firstNameField = studentClass.getDeclaredField("firstName");
        Field lastNameField = studentClass.getDeclaredField("lastName");
        Field courseRecordField = studentClass.getDeclaredField("courseRecord");
        Field degreeCourseField = studentClass.getDeclaredField("degreeCourse");
        firstNameField.setAccessible(true);
        lastNameField.setAccessible(true);
        courseRecordField.setAccessible(true);
        degreeCourseField.setAccessible(true);
        firstNameField.set(student, "First Name 2");
        lastNameField.set(student, "Last Name 2");
        courseRecordField.set(student, "Course Record 2");
        degreeCourseField.set(student, "Degree Course 2");
        System.out.println("\nChanged attribute values: ");
        System.out.println("First Name: " + getFirstNameMethod.invoke(student));
        System.out.println("Last Name: " + getLastNameMethod.invoke(student));
        System.out.println("Course Record: " + getCourseRecordMethod.invoke(student));
        System.out.println("Degree Course: " + getDegreeCourseMethod.invoke(student));
    }
}
