package pl.sda.s04_zaawansowane.e16_reflections.examples.e04;

public class Car {
    private boolean isPrototype = true;
    private String name;
    private String model;

    public Car() {
    }

    public Car(String name, String model) {
        this.name = name;
        this.model = model;
    }

    public Car(boolean isPrototype, String name, String model) {
        this.isPrototype = isPrototype;
        this.name = name;
        this.model = model;
    }

    public boolean isPrototype() {
        return isPrototype;
    }

    public void setPrototype(boolean prototype) {
        isPrototype = prototype;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "Car{" +
                "isPrototype=" + isPrototype +
                ", name='" + name + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}
