package pl.sda.s04_zaawansowane.e16_reflections.examples.e02;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class CarExample {
    public static void main(String[] args)
            throws ClassNotFoundException, InstantiationException, IllegalAccessException,
            NoSuchMethodException, InvocationTargetException {
        Class<?> carClass = Class.forName("pl.sda.s04_zaawansowane.e16_reflections.examples.e02.Car");
        Car car = (Car) carClass.newInstance();
        Method setNameMethod = carClass.getDeclaredMethod("setName", String.class);
        Method setModelMethod = carClass.getDeclaredMethod("setModel", String.class);
        Method getNameMethod = carClass.getDeclaredMethod("getName");
        setNameMethod.invoke(car, "Porsche");
        setModelMethod.invoke(car, "K1");
        System.out.println("Get name: " + getNameMethod.invoke(car));
        System.out.println("Use method using reflection: ");
        System.out.println(car);
    }
}
