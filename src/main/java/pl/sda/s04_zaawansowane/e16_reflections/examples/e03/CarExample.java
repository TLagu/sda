package pl.sda.s04_zaawansowane.e16_reflections.examples.e03;

import java.lang.reflect.Field;

public class CarExample {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Car car = new Car();
        Field field = Car.class.getDeclaredField("name");
        field.setAccessible(true);
        field.set(car, "test");
        Field modelField = Car.class.getDeclaredField("model");
        modelField.setAccessible(true);
        modelField.set(car, "BMW");
        System.out.println("Set field using reflection: " + car);
    }
}
