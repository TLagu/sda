package pl.sda.s04_zaawansowane.e16_reflections.examples.e04;

import java.lang.reflect.Method;

public class CarExample {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        final Method[] methods1 = Car.class.getMethods();
        System.out.println("Lista metod publicznych w całej hierarchii klas lub interfejsów: ");
        for(final Method method : methods1){
            System.out.println(method);
        }
        final Method[] methods2 = Car.class.getDeclaredMethods();
        System.out.println("\nLista metod w danej klasie: ");
        for(final Method method : methods2){
            System.out.println(method);
        }
    }
}
