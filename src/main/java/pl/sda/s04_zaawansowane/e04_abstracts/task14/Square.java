package pl.sda.s04_zaawansowane.e04_abstracts.task14;

public class Square extends Rectangle {
    public Square(){
        super();
    }

    public Square(double width) {
        super(width, width);
    }

}
