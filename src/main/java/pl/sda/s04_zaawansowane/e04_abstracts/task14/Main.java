package pl.sda.s04_zaawansowane.e04_abstracts.task14;

public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(5.0);
        circle.setColor("red");
        circle.setFilled(true);
        System.out.println(circle);

        Rectangle rectangle = new Rectangle(2.0, 4.0);
        rectangle.setColor("green");
        System.out.println(rectangle);

        Square square = new Square(4.0);
        square.setColor("blue");
        System.out.println(square);
    }
}
