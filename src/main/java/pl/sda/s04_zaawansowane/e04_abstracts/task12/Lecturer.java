package pl.sda.s04_zaawansowane.e04_abstracts.task12;

import lombok.Getter;
import lombok.Setter;

import java.text.DecimalFormat;

@Getter
@Setter
public class Lecturer extends Person{
    private String specialization;
    private float salary;

    public Lecturer(String specialization, float salary) {
        this.specialization = specialization;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Lecturer: " + super.toString() +
                "\n - specialization: " + specialization +
                "\n - salary: " + new DecimalFormat("#,##0.0").format(salary);
    }
}
