package pl.sda.s04_zaawansowane.e04_abstracts.task12;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public abstract class Person {
    private String name;
    private String address;

    public Person() {
        name = "";
        address = "";
    }

    @Override
    public String toString() {
        return name + " -> " + address;
    }
}
