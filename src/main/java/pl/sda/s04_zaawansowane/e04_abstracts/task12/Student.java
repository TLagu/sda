package pl.sda.s04_zaawansowane.e04_abstracts.task12;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.text.DecimalFormat;

@AllArgsConstructor
@Getter
@Setter
public class Student extends Person {
    private String typeOfStudy;
    private int yearOfStudy;
    private float theCostOfStudies;

    @Override
    public String toString() {
        return "Student: " + super.toString() +
                "\n - type of study: " + typeOfStudy +
                "\n - year of study: " + yearOfStudy +
                "\n - the cost of studies: " + new DecimalFormat("#,##0.0").format(theCostOfStudies);
    }
}
