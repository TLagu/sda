package pl.sda.s04_zaawansowane.e04_abstracts.task12;

public class Main {
    public static void main(String[] args) {
        Student student = new Student("Specjalista od niczego", 2000, 15000f);
        student.setName("Grzegorz");
        student.setAddress("Daleko");

        Lecturer lecturer = new Lecturer("Kombinator", 10000f);
        lecturer.setName("Franciszek");
        lecturer.setAddress("Blisko");

        System.out.println(student);
        System.out.println(lecturer);
    }
}
