package pl.sda.s04_zaawansowane.e04_abstracts.task01;

public class PointAndShoot extends DigitalCamera {

    public PointAndShoot(String make, String model, double megapixels, double price) {
        super(make, model, megapixels, price);
    }

    @Override
    public void describe() {
        System.out.println("Make: " + super.getMake()
                + ", model: " + super.getModel()
                + ", megapixels: " + super.getMegapixels()
                + ", price: " + super.getPrice());
    }
}
