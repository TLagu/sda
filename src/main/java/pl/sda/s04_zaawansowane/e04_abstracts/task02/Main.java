package pl.sda.s04_zaawansowane.e04_abstracts.task02;

public class Main {
    public static void main(String[] args) {
        Employee employee = new Employee("Jan", "mężczyzna", 1);
        employee.work();
        employee.changeName("Krzysztof");
        employee.work();
    }
}
