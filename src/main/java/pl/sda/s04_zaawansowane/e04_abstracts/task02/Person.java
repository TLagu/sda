package pl.sda.s04_zaawansowane.e04_abstracts.task02;

public abstract class Person {
    private String name;
    private String gender;

    public Person(String name, String gender) {
        this.name = name;
        this.gender = gender;
    }

    public abstract void work();

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public void changeName(String name) {
        this.name = name;
    }
}
