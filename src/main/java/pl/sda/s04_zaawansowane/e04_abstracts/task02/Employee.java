package pl.sda.s04_zaawansowane.e04_abstracts.task02;

public class Employee extends Person {
    int empId;

    public Employee(String name, String gender, int empId) {
        super(name, gender);
        this.empId = empId;
    }

    @Override
    public void work() {
        System.out.println("name: " + super.getName()
                + ", gender: " + super.getGender()
                + ", empId: " + empId);
    }
}
