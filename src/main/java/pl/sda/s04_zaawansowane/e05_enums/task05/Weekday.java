package pl.sda.s04_zaawansowane.e05_enums.task05;

public enum Weekday {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;

    public boolean isWeekDay() {
        return !isHoliday();
    }

    public boolean isHoliday() {
        return ordinal() >= 5;
    }

    public void whichIsGreater(Weekday weekday) {
        int result = compareTo(weekday);
        if (result < 0) {
            System.out.println(name() + " jest poprzednikiem " + weekday.name());
        } else if (result == 0) {
            System.out.println("Te same dni tygodnia.");
        } else {
            System.out.println(name() + " jest następnikiem " + weekday.name());
        }
    }
}
