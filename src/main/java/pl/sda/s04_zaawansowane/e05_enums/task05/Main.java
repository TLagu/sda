package pl.sda.s04_zaawansowane.e05_enums.task05;

public class Main {
    public static void main(String[] args) {
        Weekday weekday1 = Weekday.FRIDAY;
        Weekday weekday2 = Weekday.SUNDAY;
        Weekday weekday3 = Weekday.MONDAY;
        System.out.println(weekday1.name() + " - isHoliday: " + weekday1.isHoliday() + ", isWeekDay: " + weekday1.isWeekDay());
        System.out.println(weekday2.name() + " - isHoliday: " + weekday2.isHoliday() + ", isWeekDay: " + weekday2.isWeekDay());
        System.out.println(weekday3.name() + " - isHoliday: " + weekday3.isHoliday() + ", isWeekDay: " + weekday3.isWeekDay());
        weekday1.whichIsGreater(weekday2);
        weekday1.whichIsGreater(weekday1);
        weekday1.whichIsGreater(weekday3);
    }
}
