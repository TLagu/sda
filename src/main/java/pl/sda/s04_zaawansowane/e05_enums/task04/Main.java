package pl.sda.s04_zaawansowane.e05_enums.task04;

public class Main {
    public static void main(String[] args) {
        PackageSize packageSize1 = PackageSize.getPackageSize(1, 2);
        System.out.println(packageSize1);
        PackageSize packageSize2 = PackageSize.getPackageSize(5, 10);
        System.out.println(packageSize2);
    }
}
