package pl.sda.s04_zaawansowane.e05_enums.task04;

public enum PackageSize {
    SMALL (0, 5),
    MEDIUM (5, 10),
    LARGE (10, 20);

    private final int min;
    private final int max;

    PackageSize(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public static PackageSize getPackageSize(int min, int max) {
        PackageSize[] values = values();
        for (PackageSize value: values) {
            if (value.min <= Math.min(min, max) && Math.max(min, max) <= value.max) {
                return value;
            }
        }
        return null;
    }
}
