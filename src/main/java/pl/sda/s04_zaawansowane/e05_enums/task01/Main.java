package pl.sda.s04_zaawansowane.e05_enums.task01;

public class Main {
    public static void main(String[] args) {
        Human human = new Human("Jan", 24, EyeColor.BLUE, HairColor.BLOND);
        System.out.println(human);
    }
}
