package pl.sda.s04_zaawansowane.e05_enums.task01;

public class Human {
    private final String name;
    private final int age;
    private final EyeColor eyeColor;
    private final HairColor hairColor;

    public Human(String name, int age, EyeColor eyeColor, HairColor hairColor) {
        this.name = name;
        this.age = age;
        this.eyeColor = eyeColor;
        this.hairColor = hairColor;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public EyeColor getEyeColor() {
        return eyeColor;
    }

    public HairColor getHairColor() {
        return hairColor;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", eyeColor=" + eyeColor.getColor() +
                ", hairColor=" + hairColor.getColor() +
                '}';
    }
}
