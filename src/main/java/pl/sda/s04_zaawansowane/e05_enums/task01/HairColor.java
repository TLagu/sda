package pl.sda.s04_zaawansowane.e05_enums.task01;

public enum HairColor {
    BLOND ("Blond"),
    CHESTNUT ("Szatyn");

    private String color;

    HairColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }
}
