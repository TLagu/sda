package pl.sda.s04_zaawansowane.e05_enums.task01;

public enum EyeColor {
    BROWN ("Brązowe"),
    GREEN ("Zielone"),
    BLUE ("Niebieskie");

    private String color;

    EyeColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }
}
