package pl.sda.s04_zaawansowane.e05_enums.examples.e01;

import com.google.gson.internal.bind.util.ISO8601Utils;

import java.util.Arrays;

public enum JsonEnumerationStrategy implements IdProvider {
    SNAKE_CASE(5, "snake_case"),
    CAMEL_CASE(2, "camel_case") {
        @Override
        public String getDescription() {
            return "extra description";
        }
    },
    KEBAB_CASE(-100, "kebab_case");

    private int id;
    private String name;

    JsonEnumerationStrategy(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public int getId() {
        return id;
    }

    public static JsonEnumerationStrategy of(int actualId) {
        JsonEnumerationStrategy[] values = values();
        for (JsonEnumerationStrategy value: values) {
            if (value.id == actualId) {
                return value;
            }
        }
        return null;
    }

    public static JsonEnumerationStrategy of(String actualName) {
        JsonEnumerationStrategy[] values = values();
        for (JsonEnumerationStrategy value: values) {
            if (value.name == actualName) {
                return value;
            }
        }
        return null;
    }

    @Override
    public String getDescription() {
        return Arrays.toString(values());
    }
}
