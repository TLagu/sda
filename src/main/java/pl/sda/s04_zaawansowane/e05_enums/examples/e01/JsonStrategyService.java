package pl.sda.s04_zaawansowane.e05_enums.examples.e01;

public class JsonStrategyService {
    public static void main(String[] args) {
        JsonEnumerationStrategy strategy1 = null;
        JsonEnumerationStrategy strategy2 = JsonEnumerationStrategy.KEBAB_CASE;
        System.out.println(strategy1 == strategy2);
        try {
            System.out.println(strategy1.equals(strategy2));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        System.out.println(strategy2.equals(strategy1));
        System.out.println(strategy2.getId());
        JsonEnumerationStrategy strategy3 = JsonEnumerationStrategy.of(5);
        try {
            System.out.println(strategy3 + ", " + strategy3.getId());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        JsonEnumerationStrategy strategy4 = JsonEnumerationStrategy.of("camel_case");
        System.out.println(strategy4 + ", " + strategy4.getId());
        JsonEnumerationStrategy strategy5 = JsonEnumerationStrategy.CAMEL_CASE;
        System.out.println(strategy5.getDescription());
        System.out.println(strategy5.name());
        System.out.println(JsonEnumerationStrategy.SNAKE_CASE.ordinal());
        System.out.println(JsonEnumerationStrategy.CAMEL_CASE.ordinal());
        System.out.println(JsonEnumerationStrategy.KEBAB_CASE.ordinal());
    }
}
