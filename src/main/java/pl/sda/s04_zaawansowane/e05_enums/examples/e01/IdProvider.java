package pl.sda.s04_zaawansowane.e05_enums.examples.e01;

public interface IdProvider {

    int getId();

    String getDescription();
}
