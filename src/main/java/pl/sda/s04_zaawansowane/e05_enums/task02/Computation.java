package pl.sda.s04_zaawansowane.e05_enums.task02;

public enum Computation  {
    MULTIPLY {
        @Override
        public double perform(double x, double y) {
            return x * y;
        }
    },
    DIVIDE {
        @Override
        public double perform(double x, double y) {
            return x / y;
        }
    },
    ADD {
        @Override
        public double perform(double x, double y) {
            return x + y;
        }
    },
    SUBTRACT {
        @Override
        public double perform(double x, double y) {
            return x - y;
        }
    };

    public double perform(double x, double y) {
        return 0;
    }


}
