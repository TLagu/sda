package pl.sda.s04_zaawansowane.e05_enums.task02;

public class Main {
    public static void main(String[] args) {
        System.out.println("Multiply: " + Computation.MULTIPLY.perform(2, 2));
        System.out.println("Divide: " + Computation.DIVIDE.perform(2, 2));
        System.out.println("Add: " + Computation.ADD.perform(2, 2));
        System.out.println("Subtract: " + Computation.SUBTRACT.perform(2, 2));
    }
}
