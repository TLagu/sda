package pl.sda.s04_zaawansowane.e05_enums.task03;

public class Main {
    public static void main(String[] args) {
        DocumentType[] documentTypes = DocumentType.values();
        for(DocumentType documentType: documentTypes) {
            switch (documentType) {
                case CONTRACT:
                    System.out.println("You selected contract (" + documentType + ").");
                    break;
                case INVOICE:
                    System.out.println("You selected invoice (" + documentType + ").");
                    break;
                case NOTARIAL_ACT:
                    System.out.println("You selected notarial act (" + documentType + ").");
                    break;
                case CERTIFICATE:
                    System.out.println("You selected certificate (" + documentType + ").");
                    break;
                default:
                    System.out.println("Unknown value.");
            }
        }
    }
}
