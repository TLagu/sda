package pl.sda.s04_zaawansowane.e05_enums.task03;

public enum DocumentType {
    CONTRACT,
    INVOICE,
    NOTARIAL_ACT,
    CERTIFICATE;
}
