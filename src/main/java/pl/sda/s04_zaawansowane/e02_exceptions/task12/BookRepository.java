package pl.sda.s04_zaawansowane.e02_exceptions.task12;

import java.util.ArrayList;
import java.util.List;

public class BookRepository {
    private final List<Book> books = new ArrayList<>();

    public void addToRepository(Book book) {
        books.add(book);
    }

    public void removeFromRepository(Book book) {
        books.remove(book);
    }

    public Book findByTitle(String title){
        for(Book book : books){
            if (book.getTitle().equals(title)) {
                return book;
            }
        }
        throw new NoBookFoundException("Title");
    }

    public Book findByID(int idx){
        if (0 <= idx && idx < books.size()){
            return books.get(idx);
        }
        throw new NoBookFoundException("ID");
    }

    public void removeByID(int idx){
        if (0 <= idx && idx < books.size()){
            books.remove(idx);
        }
    }

    @Override
    public String toString() {
        return "BookRepository{" +
                "books=" + books +
                '}';
    }
}
