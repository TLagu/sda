package pl.sda.s04_zaawansowane.e02_exceptions.task12;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

@AllArgsConstructor
@Getter
public class Book {
    private String isbn;
    private String title;
    private String author;
    private int year;

    @Override
    public String toString() {
        return "Book -> isbn = " + isbn +
                ", title = " + title +
                ", author = " + author +
                ", year = " + year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return year == book.year && isbn.equals(book.isbn) && title.equals(book.title) && author.equals(book.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isbn, title, author, year);
    }
}
