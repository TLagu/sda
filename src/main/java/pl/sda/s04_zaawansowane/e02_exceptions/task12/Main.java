package pl.sda.s04_zaawansowane.e02_exceptions.task12;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        BookRepository books = new BookRepository();
        books.addToRepository(new Book("isbn 1", "title 1", "author 1", 2000));
        books.addToRepository(new Book("isbn 2", "title 2", "author 2", 2001));
        books.addToRepository(new Book("isbn 3", "title 3", "author 3", 2002));
        books.addToRepository(new Book("isbn 4", "title 4", "author 4", 2003));
        books.addToRepository(new Book("isbn 5", "title 5", "author 5", 2004));
        books.addToRepository(new Book("isbn 6", "title 6", "author 6", 2005));
        System.out.println(books);
        try {
            books.findByTitle("title 1");
            books.findByID(12);
        } catch (NoBookFoundException e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
        books.removeFromRepository(new Book("isbn 1", "title 1", "author 1", 2000));
        books.removeByID(4);
        System.out.println(books);
    }
}
