package pl.sda.s04_zaawansowane.e02_exceptions.task12;

public class NoBookFoundException extends RuntimeException{
    public NoBookFoundException(String field) {
        super("Book by " + field + " not found");
    }
}
