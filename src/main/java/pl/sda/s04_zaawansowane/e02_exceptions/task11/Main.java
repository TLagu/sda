package pl.sda.s04_zaawansowane.e02_exceptions.task11;

public class Main {
    public static void main(String[] args) {
        System.out.println(divide(1, 2));
        System.out.println(divide(3, 0));
    }

    private static double divide(int x, int y) {
        if (y == 0) {
            throw new CannotDivideBy0Exception();
        }
        return (float) x / y;
    }
}
