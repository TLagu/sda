package pl.sda.s04_zaawansowane.e02_exceptions.task11;

public class CannotDivideBy0Exception extends RuntimeException {
    public CannotDivideBy0Exception() {
        super ("Cannot divide by 0!!!");
    }
}
