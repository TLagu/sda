package pl.sda.s04_zaawansowane.e02_exceptions.examples.e01;

public class IllegalAddressException extends IllegalArgumentException {
    public IllegalAddressException(final String address) {
        super (String.format("Provided address %s is not valid!", address));
    }
}
