package pl.sda.s04_zaawansowane.e02_exceptions.examples.e01;

public class Main {
    public void validate(String address) {
        if (address == null || address.isBlank()) {
            throw  new IllegalAddressException(address);
        }
    }

    public void validate2(String address) throws IllegalAddressEException {
        if (address == null || address.isBlank()) {
            throw new IllegalAddressEException(address);
        }
    }

    public static void main(String[] args) {
        Main main = new Main();
        String address = "   ";
        try {
            main.validate2(address);
        } catch (IllegalAddressEException e) {
            System.out.println("Illegal value (Exception)!!!");
        } catch (Exception e) {
            System.out.println("Illegal value (Global Exception)!!!");
        } finally {
            System.out.println("in section finally (Exception)!");
        }
        try {
            main.validate(address);
        } catch (IllegalAddressException e) {
            System.out.println("Illegal value (RuntimeException)!!!");
        } finally {
            System.out.println("in section finally (RuntimeException)!");
        }
    }
}
