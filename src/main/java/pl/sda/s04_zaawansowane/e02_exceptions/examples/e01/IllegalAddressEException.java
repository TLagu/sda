package pl.sda.s04_zaawansowane.e02_exceptions.examples.e01;

public class IllegalAddressEException extends Exception {
    public IllegalAddressEException(final String address) {
        super (String.format("Provided address %s is not valid!", address));
    }
}
