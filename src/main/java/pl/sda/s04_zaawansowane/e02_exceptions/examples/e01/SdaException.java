package pl.sda.s04_zaawansowane.e02_exceptions.examples.e01;

public class SdaException extends RuntimeException {
    public SdaException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
