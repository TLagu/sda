package pl.sda.s04_zaawansowane.e02_exceptions.task02;

public class IllegalAddressException extends Exception {
    public IllegalAddressException(final String message) {
        super (message);
    }
}
