package pl.sda.s04_zaawansowane.e02_exceptions.task02;

public class Main {
    public static void main(String[] args) throws IllegalAddressException {
        try {
            Address address = new Address(null, 1, null, null);
        } catch (IllegalAddressException e) {
            e.printStackTrace();
        } finally {
            System.out.println("End.");
        }
    }
}
