package pl.sda.s04_zaawansowane.e02_exceptions.task02;

public class Address {
    private String street;
    private int flatNumber;
    private String postCode;
    private String city;

    public Address(String street, int flatNumber, String postCode, String city) throws IllegalAddressException {
        String errorMessage = "";
        if (street == null) {
            errorMessage += "Street can not be null. ";
        }
        if (flatNumber <= 0) {
            errorMessage += "Flat number should be greater than 0. ";
        }
        if (postCode == null) {
            errorMessage += "Post code can not be null. ";
        }
        if (city == null) {
            errorMessage += "City can not be null. ";
        }
        if (!errorMessage.isEmpty()) {
            throw new IllegalAddressException(errorMessage);
        }
        this.street = street;
        this.flatNumber = flatNumber;
        this.postCode = postCode;
        this.city = city;
    }
}
