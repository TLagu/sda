package pl.sda.s04_zaawansowane.e02_exceptions.task01;

public class IllegalValueException extends IllegalArgumentException {
    public IllegalValueException(final int value) {
        super (String.format("Provided value %d is not valid!", value));
    }
}
