package pl.sda.s04_zaawansowane.e02_exceptions.task01;

public class Main {
    public static void main(String[] args) {
        int value = 2;
        try {
            System.out.println(value + "! = " + calculateFactorial(value));
        } catch (IllegalValueException e) {
            e.printStackTrace();
        } finally {
            System.out.println("End.");
        }
    }

    private static int calculateFactorial(int value) throws IllegalValueException {
        if (value <= 0) {
            throw new IllegalValueException(value);
        }
        int result = 1;
        for(int i = 1; i <= value; i++) {
            result *= i;
        }
        return result;
    }
}
