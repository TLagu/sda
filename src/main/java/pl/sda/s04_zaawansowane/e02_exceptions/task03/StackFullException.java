package pl.sda.s04_zaawansowane.e02_exceptions.task03;

public class StackFullException extends RuntimeException {
    public StackFullException() {
        super ("Stack is full!!!");
    }
}
