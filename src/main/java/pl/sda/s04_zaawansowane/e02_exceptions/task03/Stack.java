package pl.sda.s04_zaawansowane.e02_exceptions.task03;

public class Stack {
    private final int[] stack;
    private int actualPosition;

    public Stack(int size) {
        if (size < 0) {
            throw new IllegalArgumentException(size);
        }
        this.stack = new int[size];
    }

    public void push(int value) {
        if (actualPosition == stack.length) {
            throw new StackFullException();
        }
        stack[actualPosition++] = value;
    }

    public int pop() {
        if (actualPosition == 0) {
            throw new StackEmptyException();
        }
        return stack[--actualPosition];
    }

    public void clear() {
        actualPosition = 0;
    }

    public int top() {
        if (actualPosition == 0) {
            throw new StackEmptyException();
        }
        return stack[actualPosition - 1];
    }

    public int getSize() {
        return actualPosition;
    }

}
