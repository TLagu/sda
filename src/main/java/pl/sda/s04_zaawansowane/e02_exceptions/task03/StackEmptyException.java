package pl.sda.s04_zaawansowane.e02_exceptions.task03;

public class StackEmptyException extends RuntimeException {
    public StackEmptyException() {
        super ("Stack is empty!!!");
    }
}
