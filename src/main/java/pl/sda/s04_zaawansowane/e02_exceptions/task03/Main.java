package pl.sda.s04_zaawansowane.e02_exceptions.task03;

public class Main {
    public static void main(String[] args) {
        try {
            Stack stack1 = new Stack(0);
            System.out.println("Size: " + stack1.getSize());
            stack1.push(3);
        } catch (IllegalArgumentException | StackEmptyException | StackFullException e) {
            e.printStackTrace();
        } finally {
            System.out.println("First stack");
        }

        try {
            Stack stack2 = new Stack(5);
            System.out.println("Size: " + stack2.getSize());
            stack2.push(3);
            System.out.println("pop: " + stack2.pop());
            System.out.println("pop: " + stack2.pop());
        } catch (IllegalArgumentException | StackEmptyException | StackFullException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Second stack");
        }
    }
}
