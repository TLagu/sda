package pl.sda.s04_zaawansowane.e02_exceptions.task03;

public class IllegalArgumentException extends RuntimeException {
    public IllegalArgumentException(final int value) {
        super ("The value must be greater than 0!!!");
    }
}
