package pl.sda.s04_zaawansowane.e03_interfaces.task01;

public interface Movable {

    void moveUp();

    void moveDown();

    void moveLeft();

    void moveRight();
}
