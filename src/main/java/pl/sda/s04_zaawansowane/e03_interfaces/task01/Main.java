package pl.sda.s04_zaawansowane.e03_interfaces.task01;

public class Main {
    public static void main(String[] args) {
        Movable movableCircle = new MovableCircle(new MovablePoint(0, 0, 1, 1), 1);
        System.out.println(movableCircle);
        movableCircle.moveUp();
        System.out.println(movableCircle);
        movableCircle.moveLeft();
        System.out.println(movableCircle);
        movableCircle.moveRight();
        System.out.println(movableCircle);
        movableCircle.moveDown();
        System.out.println(movableCircle);
    }
}
