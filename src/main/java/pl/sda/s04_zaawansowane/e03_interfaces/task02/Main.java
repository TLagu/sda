package pl.sda.s04_zaawansowane.e03_interfaces.task02;

public class Main {
    public static void main(String[] args) {
        ResizableCircle resizableCircle = new ResizableCircle(2);
        System.out.println(resizableCircle);
        resizableCircle.resize(10);
        System.out.println(resizableCircle);
    }
}
