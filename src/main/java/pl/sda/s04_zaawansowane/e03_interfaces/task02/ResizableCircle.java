package pl.sda.s04_zaawansowane.e03_interfaces.task02;

public class ResizableCircle extends Circle implements Resizable {

    public ResizableCircle(double radius) {
        super(radius);
    }

    @Override
    public void resize(int percent) {
        super.radius = super.radius * (1 + (double) percent / 100);
    }

    @Override
    public String toString() {
        String superString = super.toString();
        return superString + ", ResizableCircle{" +
                "radius=" + radius +
                '}';
    }
}
