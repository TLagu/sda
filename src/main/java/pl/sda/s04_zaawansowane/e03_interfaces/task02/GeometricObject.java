package pl.sda.s04_zaawansowane.e03_interfaces.task02;

public interface GeometricObject {
    double getPerimeter();
    double getArea();
}
