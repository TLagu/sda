package pl.sda.s04_zaawansowane.e03_interfaces.examples.e03;

public interface InterfaceB {
    default void sampleDefaultMethod() {
        System.out.println("sampleDefaultMethod B...");
    }
}
