package pl.sda.s04_zaawansowane.e03_interfaces.examples.e03;

public class Item implements InterfaceA, InterfaceB {
    public static void main(String[] args) {
        Item item = new Item();
        item.sampleDefaultMethod();
    }

    @Override
    public void sampleDefaultMethod() {
        InterfaceA.super.sampleDefaultMethod();
    }
}
