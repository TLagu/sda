package pl.sda.s04_zaawansowane.e03_interfaces.examples.e01;

import com.sun.jdi.PathSearchingVirtualMachine;

public class SomeInterfaceImpl implements SomeInterface {
    @Override
    public void someMethod() {
        System.out.println("Some method implementation...");
    }

    public static void main(String[] args) {
        SomeInterface someInterface = new SomeInterfaceImpl();
    }
}
