package pl.sda.s04_zaawansowane.e03_interfaces.examples.e02;

public interface Encoder2 {
    default String encode(String encodeText) {
        return encodeText;
    }
    default String decode(String encodeText) {
        return encodeText;
    }
}
