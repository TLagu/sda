package pl.sda.s04_zaawansowane.e03_interfaces.examples.e01;

public interface MediaPlayer {
    String NAME = "unknown";

    void play();
    void stop();

    default void next() {
        //throw new NoSuchMechanizmException("not supported by default");
        System.out.println("Some method implementation...");
        next();
    }

    static void main(String[] args) {
        System.out.println("in main...");
    }
}
