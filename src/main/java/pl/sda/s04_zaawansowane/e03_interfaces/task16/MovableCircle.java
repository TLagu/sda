package pl.sda.s04_zaawansowane.e03_interfaces.task16;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class MovableCircle implements Movable{
    private MovablePoint point;
    private int radius;

    @Override
    public void moveUp() {
        point.moveUp();
    }

    @Override
    public void moveDown() {
        point.moveDown();
    }

    @Override
    public void moveLeft() {
        point.moveLeft();
    }

    @Override
    public void moveRight() {
        point.moveRight();
    }

    @Override
    public String toString() {
        return point.toString()  + ", radius = " + radius;
    }
}
