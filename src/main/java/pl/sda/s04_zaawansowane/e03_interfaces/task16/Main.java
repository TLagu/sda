package pl.sda.s04_zaawansowane.e03_interfaces.task16;

public class Main {
    public static void main(String[] args) {
        Movable point = new MovablePoint(0, 0, 1, 1);
        System.out.println(point);
        point.moveDown();
        System.out.println(point);
        point.moveRight();
        System.out.println(point);
        point.moveUp();
        point.moveUp();
        System.out.println(point);
        point.moveLeft();
        point.moveLeft();
        System.out.println(point);
        point.moveDown();
        point.moveDown();
        System.out.println(point);
        point.moveRight();
        System.out.println(point);
        point.moveUp();
        System.out.println(point);
    }
}
