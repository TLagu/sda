package pl.sda.s04_zaawansowane.e03_interfaces.task17;

public interface GeometricObject {
    double getPerimeter();
    double getArea();
}
