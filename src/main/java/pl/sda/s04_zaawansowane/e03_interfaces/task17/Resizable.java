package pl.sda.s04_zaawansowane.e03_interfaces.task17;

public interface Resizable {
    int percent = 0;
    void resize(int percent);
}
