package pl.sda.s04_zaawansowane.e03_interfaces.task17;

public class ResizableCircle extends Circle implements Resizable {

    public ResizableCircle(double radius) {
        super(radius);
    }

    @Override
    public void resize(int percent) {
        super.setRadius(super.getRadius() * (1.0 + percent / 100.0));
    }
}
