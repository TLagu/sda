package pl.sda.s04_zaawansowane.e03_interfaces.task17;

public class Main {
    public static void main(String[] args) {
        ResizableCircle circle = new ResizableCircle(2);
        System.out.println("Circle: " + circle);
        System.out.println("Perimeter: " + circle.getPerimeter());
        System.out.println("Area: " + circle.getArea());

        circle.resize(200);
        System.out.println("Circle: " + circle);
        System.out.println("Perimeter: " + circle.getPerimeter());
        System.out.println("Area: " + circle.getArea());
    }
}
