package pl.sda.s04_zaawansowane.e13_io.examples.e05;

import lombok.AllArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@ToString
@AllArgsConstructor
public class Movie implements Serializable {
    private final int id;
    private final String title;
    private final String type;
}
