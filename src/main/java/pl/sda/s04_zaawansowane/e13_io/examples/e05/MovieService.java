package pl.sda.s04_zaawansowane.e13_io.examples.e05;

import java.io.*;

public class MovieService {
    private static final String PATH = "e:\\praca\\java\\sda\\books\\java_zaawansowana\\resources\\";

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        writeObject();
        readObject();
    }

    public static void writeObject() throws IOException {
        try {
            Movie movieToSerialize = new Movie(1123, "Star Wars", "Start Wars IX");
            FileOutputStream fileOutputStream = new FileOutputStream(PATH + "movies.txt");
            ObjectOutputStream out = new ObjectOutputStream(fileOutputStream);
            out.writeObject(movieToSerialize);
            out.flush();
            out.close();
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public static void readObject() {
        try {
            FileInputStream fileIn = new FileInputStream(PATH + "movies.txt");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Movie movieToDeserialize = (Movie) in.readObject();
            in.close();
            fileIn.close();
            System.out.println(movieToDeserialize);
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}
