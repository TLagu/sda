package pl.sda.s04_zaawansowane.e13_io.examples.e01;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CharacterStream {
    private static final String PATH = "e:\\praca\\java\\sda\\books\\java_zaawansowana\\resources\\";
    public static void main(String[] args) throws IOException {
        FileReader in = null;
        FileWriter out = null;

        try {
            in = new FileReader(PATH + "user_in.txt");
            out = new FileWriter(PATH + "user_out.txt");
            int c;
            int nextChar;
            System.out.print("Text from file: ");
            while ((nextChar = in.read()) != -1) {
                out.append((char) nextChar);
                System.out.print((char) nextChar);
            }
        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        }
    }
}
