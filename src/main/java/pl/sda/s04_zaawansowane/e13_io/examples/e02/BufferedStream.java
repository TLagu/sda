package pl.sda.s04_zaawansowane.e13_io.examples.e02;

import java.io.*;

public class BufferedStream {
    private static final String PATH = "e:\\praca\\java\\sda\\books\\java_zaawansowana\\resources\\";

    public static void main(String[] args) throws IOException {

        try (
                BufferedReader in = new BufferedReader(new FileReader(PATH + "user_in.txt"));
                BufferedWriter out = new BufferedWriter(new FileWriter(PATH + "user_out.txt"))) {
            String line;
            System.out.println("Text from file: ");
            while ((line = in.readLine()) != null) {
                out.write(line);
                System.out.print(line);
            }
        }
    }
}
