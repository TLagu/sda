package pl.sda.s04_zaawansowane.e13_io.examples.e03;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class NIOExample {
    private static final String PATH = "e:\\praca\\java\\sda\\books\\java_zaawansowana\\resources\\";

    public static void main(String[] args) throws IOException {
        RandomAccessFile file = new RandomAccessFile(PATH + "user_in.txt", "r");
        FileChannel fileChannel = file.getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(512);
        while (fileChannel.read(byteBuffer) > 0) {
            byteBuffer.flip();
            while (byteBuffer.hasRemaining()) {
                System.out.println((char) byteBuffer.get());
            }
            byteBuffer.clear();
        }
        file.close();
    }
}
