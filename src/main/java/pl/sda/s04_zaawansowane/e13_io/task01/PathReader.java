package pl.sda.s04_zaawansowane.e13_io.task01;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class PathReader {
    private static final String PATH = "e:\\praca\\java\\sda\\books\\java_zaawansowana\\resources\\";

    public static void main(String[] args) throws IOException {
        try (BufferedWriter out = new BufferedWriter(new FileWriter(PATH + "file_list.txt"))) {
            Path path = Paths.get(PATH);
            Stream<Path> walk = Files.walk(path);
            walk.forEach((o) -> {
                try {
                    out.write(o.toString() + "\n");
                    System.out.println(o);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
