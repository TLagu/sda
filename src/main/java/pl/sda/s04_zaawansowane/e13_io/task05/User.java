package pl.sda.s04_zaawansowane.e13_io.task05;

import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class User {
    private final String firstName;
    private final String lastName;
    private final int age;
}
