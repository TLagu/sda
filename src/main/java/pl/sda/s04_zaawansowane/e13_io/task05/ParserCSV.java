package pl.sda.s04_zaawansowane.e13_io.task05;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ParserCSV {
    private static final String PATH = "e:\\praca\\java\\sda\\books\\java_zaawansowana\\resources\\";

    public static void main(String[] args) throws IOException {
        System.out.println(parseCSV(getFileData()));
    }

    public static List<String> getFileData() throws IOException {
        List<String> fileContent = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new FileReader(PATH + "user_info.txt"))) {
            String line;
            while ((line = in.readLine()) != null) {
                fileContent.add(line);
            }
        }
        return fileContent;
    }

    public static List<User> parseCSV(List<String> fileContent) {
        return fileContent.stream()
                .filter(l -> !l.isBlank())
                .map(l -> l.split(","))
                .filter(l -> l.length == 3)
                .map(u -> new User(u[0], u[1], Integer.parseInt(u[2])))
                .collect(Collectors.toList());
    }

}
