package pl.sda.s04_zaawansowane.e13_io.task04;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class LongestWord {
    private static final String PATH = "e:\\praca\\java\\sda\\books\\java_zaawansowana\\resources\\";

    public static void main(String[] args) throws IOException {
        System.out.println(findLongestWord(getFileData()));
    }

    public static List<String> getFileData() throws IOException {
        List<String> fileContent = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new FileReader(PATH + "longest_word.txt"))) {
            String line;
            while ((line = in.readLine()) != null) {
                fileContent.add(line);
            }
        }
        return fileContent;
    }

    public static String findLongestWord(List<String> fileContent) {
        Optional<String> longestWord = fileContent.stream()
                .map(l -> l.split(" "))
                .flatMap(Arrays::stream)
                .reduce((word1, word2) -> word1.length() > word2.length() ? word1 : word2);
        return longestWord.orElse(null);
    }
}
