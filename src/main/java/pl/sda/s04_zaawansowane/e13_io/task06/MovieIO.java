package pl.sda.s04_zaawansowane.e13_io.task06;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MovieIO {

    public static List<Movie> readFromFile(String fileName) {
        try {
            FileInputStream fis = new FileInputStream(fileName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<Movie> namesDes = (ArrayList<Movie>) ois.readObject();
            ois.close();
            fis.close();
            return namesDes;
        } catch (ClassNotFoundException | IOException e2) {
            e2.printStackTrace();
        }
        return Collections.emptyList();
    }

    public static void writeToFile(List<Movie> movies, String fileName) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName));
            out.writeObject(movies);
            out.flush();
            out.close();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
