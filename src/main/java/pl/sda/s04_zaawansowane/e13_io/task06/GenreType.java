package pl.sda.s04_zaawansowane.e13_io.task06;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GenreType {
    CRIME("Crime fiction"),
    COMEDY("comedy"),
    FANTASY("fantasy"),
    SCI_FI("sci-fi");

    private String name;
}
