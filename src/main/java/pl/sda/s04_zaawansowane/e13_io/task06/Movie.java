package pl.sda.s04_zaawansowane.e13_io.task06;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDate;

@AllArgsConstructor
@Getter
@ToString
public class Movie implements Serializable {
    private String title;
    private GenreType genre;
    private String director;
    private LocalDate release_year;
}
