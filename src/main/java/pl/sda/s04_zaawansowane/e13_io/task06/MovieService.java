package pl.sda.s04_zaawansowane.e13_io.task06;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MovieService {
    private static final String PATH = "e:\\praca\\java\\sda\\books\\java_zaawansowana\\resources\\";

    public static void main(String[] args) {
        List<Movie> movies = new ArrayList<>();
        System.out.println("Adding new instances of the Movie object to the ArrayList.");
        movies.add(new Movie("Tytuł 1", GenreType.CRIME, "Reżyser 1", LocalDate.of(2021, 1, 1)));
        movies.add(new Movie("Tytuł 2", GenreType.CRIME, "Reżyser 2", LocalDate.of(2021, 1, 2)));
        System.out.println(movies);
        System.out.println("Saving ArrayList to the file.");
        MovieIO.writeToFile(movies, PATH + "movies_in_out.txt");
        System.out.println("Reading ArrayList from the file.");
        movies = MovieIO.readFromFile(PATH + "movies_in_out.txt");
        movies.add(new Movie("Tytuł 3", GenreType.CRIME, "Reżyser 3", LocalDate.of(2021, 2, 1)));
        movies.add(new Movie("Tytuł 4", GenreType.CRIME, "Reżyser 4", LocalDate.of(2021, 2, 2)));
        movies.add(new Movie("Tytuł 5", GenreType.CRIME, "Reżyser 5", LocalDate.of(2021, 2, 3)));
        System.out.println(movies);
    }
}
