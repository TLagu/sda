package pl.sda.s04_zaawansowane.e13_io.task02;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class ReadLine {
    private static final String PATH = "e:\\praca\\java\\sda\\books\\java_zaawansowana\\resources\\";

    public static void main(String[] args) throws IOException {
        Path path = Paths.get(PATH);
        List<Path> fileList = Files.walk(path).collect(Collectors.toList());
        if (fileList.size() == 0) {
            return;
        }
        Scanner scanner = new Scanner(System.in);
        int choice = 1;
        while (choice > 0) {
            AtomicInteger idx = new AtomicInteger();
            fileList.forEach((o) -> System.out.println(idx.incrementAndGet() + " - " + o));
            System.out.print("Podaj plik do wczytania: ");
            choice = scanner.nextInt();
            if (fileList.size() <= choice || choice <= 0 || !Files.isRegularFile(fileList.get(choice - 1))) {
                System.out.println("Zly wybor!!!");
                continue;
            }
            try (BufferedReader in = new BufferedReader(new FileReader(String.valueOf(fileList.get(choice - 1))))) {
                String line;
                System.out.println("Text from file: " + fileList.get(choice));
                while ((line = in.readLine()) != null) {
                    System.out.println(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
