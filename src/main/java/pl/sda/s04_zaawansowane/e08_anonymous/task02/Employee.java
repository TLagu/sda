package pl.sda.s04_zaawansowane.e08_anonymous.task02;

public class Employee {
    private String name;
    private String surname;
    private String login;
    private String password;

    public String getName() {
        return name;
    }

    public void setName(String name, Validator validator) {
        if (validator.validate(name)) {
            this.name = name;
        }
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname, Validator validator) {
        if (validator.validate(surname)) {
            this.surname = surname;
        }
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login, Validator validator) {
        if (validator.validate(login)) {
            this.login = login;
        }
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password, Validator validator) {
        if (validator.validate(password)) {
            this.password = password;
        }
    }
}
