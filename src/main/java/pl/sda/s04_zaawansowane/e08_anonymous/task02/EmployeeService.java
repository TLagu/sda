package pl.sda.s04_zaawansowane.e08_anonymous.task02;

import java.util.Locale;

public class EmployeeService {
    public static void main(String[] args) {
        Validator name = new Validator() {
            @Override
            public boolean validate(String input) {
                return (input != null && !input.isEmpty());
            }
        };
        Validator surname = new Validator() {
            @Override
            public boolean validate(String input) {
                return (input != null && !input.isEmpty());
            }
        };
        Validator login = new Validator() {
            @Override
            public boolean validate(String input) {
                return (input != null && input.equals(input.toLowerCase(Locale.ROOT)));
            }
        };
        Validator password = new Validator() {
            @Override
            public boolean validate(String input) {
                return (input != null && input.length() > 5);
            }
        };

        Employee employee = new Employee();
        employee.setName("Tom", name);
        employee.setSurname("Sawyer", surname);
        employee.setLogin("Tom", login);
        employee.setPassword("pass", password);
        System.out.println("Employee:");
        System.out.println(" - name: " + employee.getName());
        System.out.println(" - surname: " + employee.getSurname());
        System.out.println(" - login: " + employee.getLogin());
        System.out.println(" - password: " + employee.getPassword());
    }
}
