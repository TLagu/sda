package pl.sda.s04_zaawansowane.e08_anonymous.task02;

public interface Validator {
    boolean validate(String input);
}
