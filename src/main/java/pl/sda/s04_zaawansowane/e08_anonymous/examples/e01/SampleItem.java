package pl.sda.s04_zaawansowane.e08_anonymous.examples.e01;

public class SampleItem {

    static {
        System.out.println("In static initializing block...");
    }

    public SampleItem() {
        System.out.println("In constructor");
    }

    {
        System.out.println("In initializing block...");
    }

    public static void main(String[] args) {
        SampleItem sampleItem1 = new SampleItem();
        System.out.println("--------------");
        SampleItem sampleItem2 = new SampleItem();
        System.out.println("--------------");
        SampleItem sampleItem3 = new SampleItem();
    }
}
