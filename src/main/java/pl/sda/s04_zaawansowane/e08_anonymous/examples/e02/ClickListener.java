package pl.sda.s04_zaawansowane.e08_anonymous.examples.e02;

public interface ClickListener {
    void onClick();
}
