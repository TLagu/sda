package pl.sda.s04_zaawansowane.e08_anonymous.examples.e02;

public class UiComponents {
    public static void main(String[] args) {
        ClickListener object = new ClickListener() {

            @Override
            public void onClick() {
                System.out.println("Click me!!!");
            }
        };

        object.onClick();
    }
}
