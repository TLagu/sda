package pl.sda.s04_zaawansowane.e08_anonymous.task01;

public interface HelloWorld {
    void greet();

    void greetSomeone(String someone);

}
