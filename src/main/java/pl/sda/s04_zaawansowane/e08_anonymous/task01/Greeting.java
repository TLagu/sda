package pl.sda.s04_zaawansowane.e08_anonymous.task01;

public class Greeting {

    public static void main(String[] args) {
        HelloWorld helloWorld = new HelloWorld() {

            @Override
            public void greet() {
                System.out.println("Hello");
            }

            @Override
            public void greetSomeone(String someone) {
                System.out.println("Hello " + someone);
            }
        };
        Greeting greeting = new Greeting();
        greeting.sayHello(helloWorld, "Ana");
        System.out.println("---------------");
        greeting.sayHello(helloWorld, "Tom");
    }

    public void sayHello(HelloWorld helloWorld, String name) {
        helloWorld.greet();
        helloWorld.greetSomeone(name);
    }
}
