package pl.sda.s04_zaawansowane.e15_threads.examples.e10;

public class VolatileThread implements Runnable {

    @Override
    public void run() {
        System.out.println("Starting some processing");
        try {
            Thread.sleep(3000L);
        } catch (InterruptedException e) {
            System.out.println("Ooops");
        }
        System.out.println("Processing finished");
        VolatileDemo.shouldStop = true;
    }
}
