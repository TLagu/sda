package pl.sda.s04_zaawansowane.e15_threads.examples.e03;

public class DummyPairIncrementer implements Runnable {

    private final Pair pair;

    public DummyPairIncrementer(Pair pair) {
        this.pair = pair;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {
                pair.incrementLeft();
                pair.incrementRight();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(pair.getLeft() + " " + pair.getRight());
    }
}
