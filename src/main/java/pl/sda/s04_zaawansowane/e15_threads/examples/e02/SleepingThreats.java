package pl.sda.s04_zaawansowane.e15_threads.examples.e02;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SleepingThreats implements Runnable {

    @Override
    public void run() {
        final List<Integer> ints = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            ints.add(new Random().nextInt(10));
        }
        Thread currentThread = Thread.currentThread();
        if (currentThread.isInterrupted()) {
            System.out.println("I was interrupted: " + currentThread.getId());
        }
        // final int sum = ints.stream().mapToInt(value -> value).sum();
        // Integer sum = ints.stream().reduce(0, (s, e) -> s + e);
        Integer sum = ints.stream().reduce(0, Integer::sum);
        System.out.println("Sum is " + sum);
        System.out.println("In thread: " + currentThread.getId());
    }
}
