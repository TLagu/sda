package pl.sda.s04_zaawansowane.e15_threads.examples.e01;

public class HelloWorldThread extends Thread {
    @Override
    public void run() {
        System.out.println("Hello world from another Thread");
        long currentThreadID = Thread.currentThread().getId();
        System.out.println("id = " + currentThreadID);
    }
}
