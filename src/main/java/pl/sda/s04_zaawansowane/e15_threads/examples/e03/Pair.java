package pl.sda.s04_zaawansowane.e15_threads.examples.e03;

public class Pair {
    private Integer left;
    private Integer right;

    public Pair(final Integer left, final Integer right) {
        this.left = left;
        this.right = right;
    }

    public void incrementLeft() throws InterruptedException {
        System.out.println("out of synchronized block (increment left) - 1");
        synchronized (this) {
            left++;
            Thread.sleep(2000);
            System.out.println("in synchronized block (increment left): " + left);
        }
        System.out.println("out of synchronized block (increment left) - 2");
    }

    public void incrementRight() throws InterruptedException {
        System.out.println("out of synchronized block (increment right) - 1");
        synchronized (this) {
            right++;
            Thread.sleep(2000);
            System.out.println("in synchronized block (increment right): " + right);
        }
        System.out.println("out of synchronized block (increment right) - 2");
    }

    public Integer getLeft() {
        return left;
    }

    public Integer getRight() {
        return right;
    }
}
