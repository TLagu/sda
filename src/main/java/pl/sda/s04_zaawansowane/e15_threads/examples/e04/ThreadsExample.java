package pl.sda.s04_zaawansowane.e15_threads.examples.e04;

import java.util.ArrayList;
import java.util.List;

public class ThreadsExample {
    public static void main(String[] args) throws InterruptedException {
        final List<Integer> ints = new ArrayList<>();
        final Thread threadA = new Thread(new SimpleThread(ints));
        final Thread threadB = new Thread(new SimpleThread(ints));

        threadA.start();
        threadB.start();

        threadA.join(1000L);
        threadB.join(1000L);

        System.out.println(ints.size());
    }
}
