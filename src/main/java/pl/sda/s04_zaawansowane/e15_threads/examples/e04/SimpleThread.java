package pl.sda.s04_zaawansowane.e15_threads.examples.e04;

import java.util.List;
import java.util.Random;

public class SimpleThread implements Runnable {
    private final List<Integer> ints;

    SimpleThread(final List<Integer> ints) {
        this.ints = ints;
    }

    @Override
    public void run() {
        synchronized (this.ints) {
            ints.add(new Random().nextInt(10));
        }
    }
}
