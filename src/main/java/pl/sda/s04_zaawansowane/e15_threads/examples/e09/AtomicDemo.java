package pl.sda.s04_zaawansowane.e15_threads.examples.e09;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class AtomicDemo {
    public static void main(String[] args) {
        final ExecutorService executorService = Executors.newFixedThreadPool(3);
        final AtomicInteger atomicInteger = new AtomicInteger();
        executorService.submit(new IncrementingThreads(atomicInteger));
        executorService.submit(new IncrementingThreads(atomicInteger));
        executorService.submit(new IncrementingThreads(atomicInteger));
        executorService.submit(new IncrementingThreads(atomicInteger));
        executorService.submit(new IncrementingThreads(atomicInteger));
        executorService.shutdown();
    }
}
