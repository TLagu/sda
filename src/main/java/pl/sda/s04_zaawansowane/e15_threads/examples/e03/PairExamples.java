package pl.sda.s04_zaawansowane.e15_threads.examples.e03;

public class PairExamples {
    public static void main(String[] args) {
        Pair pair = new Pair(0, 0);
        new Thread(new DummyPairIncrementer(pair)).start();
        new Thread(new DummyPairIncrementer(pair)).start();
    }
}
