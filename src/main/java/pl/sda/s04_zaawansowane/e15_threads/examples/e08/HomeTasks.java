package pl.sda.s04_zaawansowane.e15_threads.examples.e08;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HomeTasks {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        List<Callable<String>> tasks = Arrays.asList(
                () -> {
                    System.out.println("Wątek: " + Thread.currentThread().getName());
                    System.out.println("Robienie zakupów");
                    Thread.sleep(5000);
                    System.out.println("Wątek: " + Thread.currentThread().getName());
                    return "Zrobiono zakupy";
                },
                () -> {
                    System.out.println("Wątek: " + Thread.currentThread().getName());
                    System.out.println("Mycie naczuń");
                    Thread.sleep(2000);
                    System.out.println("Wątek: " + Thread.currentThread().getName());
                    return "Umyto naczynia";
                },
                () -> {
                    System.out.println("Wątek: " + Thread.currentThread().getName());
                    System.out.println("Sprzatanie pokoju");
                    Thread.sleep(1000);
                    System.out.println("Wątek: " + Thread.currentThread().getName());
                    return "Posprzątano pokój";
                }
        );

        try {
            String firstResult = executorService.invokeAny(tasks);
            System.out.println("PIERWSZY WYNIK: " + firstResult);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
    }
}
