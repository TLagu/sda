package pl.sda.s04_zaawansowane.e15_threads.examples.e02;

public class ThreadExamples {
    public static void main(String[] args) throws InterruptedException {
        final Thread sleepingThread = new Thread(new SleepingThreats());
        sleepingThread.start();
        Thread.sleep(2000L);
        sleepingThread.interrupt();
        System.out.println("In main...: " + Thread.currentThread().getId());
    }
}
