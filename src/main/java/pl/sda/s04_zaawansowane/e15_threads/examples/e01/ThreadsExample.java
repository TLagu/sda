package pl.sda.s04_zaawansowane.e15_threads.examples.e01;

public class ThreadsExample {
    public static void main(String[] args) {
        HelloWorldThread myThread = new HelloWorldThread();
        myThread.start();
        // w ten sposób nie wytworzymy nowego wątku - będzie sekwencja
        // myThread.run();
        System.out.println("In main: " + Thread.currentThread().getId());
    }
}
