package pl.sda.s04_zaawansowane.e15_threads.examples.e10;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class VolatileDemo {
    public static volatile boolean shouldStop = false;

    public static void main(String[] args) throws InterruptedException {
        final ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(new VolatileThread());
        while (!shouldStop) {
            Thread.sleep(100L);
            System.out.println("Waiting for signal to stop checking that volatile boolean is true");
        }
        executorService.shutdown();
    }
}
