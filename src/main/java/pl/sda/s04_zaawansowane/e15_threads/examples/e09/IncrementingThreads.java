package pl.sda.s04_zaawansowane.e15_threads.examples.e09;

import java.util.concurrent.atomic.AtomicInteger;

public class IncrementingThreads implements Runnable {
    private final AtomicInteger value;

    public IncrementingThreads(AtomicInteger value) {
        this.value = value;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100000; i++) {
            value.incrementAndGet();
        }
        System.out.println(value.get());
    }
}
