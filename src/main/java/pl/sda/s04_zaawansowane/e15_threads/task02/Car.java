package pl.sda.s04_zaawansowane.e15_threads.task02;

import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class Car {
    private final String name;
    private final String type;
}
