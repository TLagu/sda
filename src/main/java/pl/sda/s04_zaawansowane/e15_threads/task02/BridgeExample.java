package pl.sda.s04_zaawansowane.e15_threads.task02;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BridgeExample {
    private final static int NUMBER_OF_CARS = 5;

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        List<Callable<String>> tasks = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_CARS; i++) {
            int finalI = i + 1;
            tasks.add(() -> new Bridge().driveThrough(new Car("Name " + finalI, "Type " + finalI)));
        }

        try {
            executorService.invokeAll(tasks);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
    }
}
