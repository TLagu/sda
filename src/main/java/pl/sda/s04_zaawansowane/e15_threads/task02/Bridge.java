package pl.sda.s04_zaawansowane.e15_threads.task02;

public class Bridge {

    public String driveThrough(Car car) throws InterruptedException {
        System.out.println("The car " + car + " ran over the bridge.");
        Thread.sleep(5000L);
        System.out.println("The car " + car + " ran off the bridge.");
        return "The bridge is empty now.";
    }

}
