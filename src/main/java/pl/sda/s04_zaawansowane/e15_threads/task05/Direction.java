package pl.sda.s04_zaawansowane.e15_threads.task05;

import java.util.List;
import java.util.Random;

public enum Direction {
    NEXT, PREV;

    private static final List<Direction> VALUES = List.of(values());
    private static final Random RANDOM = new Random();

    public static Direction randomValue() {
        return VALUES.get(RANDOM.nextInt(VALUES.size()));
    }
}
