package pl.sda.s04_zaawansowane.e15_threads.task05;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MoveExample {
    public final static int ARRAY_SIZE = 25;
    public final static int START_INDEX = 10;
    public final static int NUMBER_OF_REPETITION = 100;

    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < ARRAY_SIZE; i++) {
            numbers.add(random.nextInt(100));
        }
        Move move = new Move(numbers, START_INDEX);
        for (int i = 0; i < NUMBER_OF_REPETITION; i++) {
            new Thread(new MoveTo(move, Direction.randomValue())).start();
        }
    }
}
