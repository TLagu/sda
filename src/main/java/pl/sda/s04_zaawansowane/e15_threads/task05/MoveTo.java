package pl.sda.s04_zaawansowane.e15_threads.task05;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MoveTo implements Runnable {
    private final Move move;
    private final Direction direction;

    @Override
    public void run() {
        switch (direction) {
            case NEXT:
                move.next();
                break;
            case PREV:
                move.prev();
                break;
            default:
                throw new IllegalArgumentException("Bad direction value.");
        }
    }
}
