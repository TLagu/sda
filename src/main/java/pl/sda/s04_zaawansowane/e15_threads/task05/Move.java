package pl.sda.s04_zaawansowane.e15_threads.task05;

import lombok.AllArgsConstructor;

import java.util.List;

//    Napisz strukturę danych, która umożliwi poruszanie się po tablicy w dwóch kierunkach:
//             do przodu ( next() )
//             do tyłu ( prev() )
//    Struktura danych powinna przechowywać aktualnie przeszukiwany index. Zadbaj o jego dodatkową
//    synchronizację.

@AllArgsConstructor
public class Move {
    private final List<Integer> numbers;
    private int index;

    public void next() {
        new Move(numbers, index).verificationOfData();
        synchronized (this) {
            if (index == numbers.size() - 1) {
                System.out.println("The index cannot be increased. The maximum has already been reached.");
                return;
            }
            index++;
            System.out.println("Next numbers[" + index + "]: " + numbers.get(index));
        }
    }

    public void prev() {
        synchronized (this) {
            if (index == 0) {
                System.out.println("The index cannot be decreased. The minimum has already been reached.");
                return;
            }
            index--;
            System.out.println("Prev numbers[" + index + "]: " + numbers.get(index));
        }
    }

    private void verificationOfData() {
        if (numbers == null || index < 0 || index >= numbers.size()) {
            throw new IllegalArgumentException("Invalid index or list value.");
        }
    }
}
