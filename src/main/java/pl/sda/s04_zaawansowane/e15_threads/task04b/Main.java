package pl.sda.s04_zaawansowane.e15_threads.task04b;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Account account = new Account(100.0);
        Thread.sleep(2000);
        new Thread(new AccountManager(account, -200.0)).start();
        Thread.sleep(2000);
        new Thread(new AccountManager(account, 100.0)).start();
        Thread.sleep(2000);
        new Thread(new AccountManager(account, -200.0)).start();
        Thread.sleep(2000);
        new Thread(new AccountManager(account, -200.0)).start();
        Thread.sleep(2000);
        new Thread(new AccountManager(account, -200.0)).start();
        Thread.sleep(2000);
        new Thread(new AccountManager(account, 500.0)).start();
        Thread.sleep(2000);
        new Thread(new AccountManager(account, 100.0)).start();
    }
}
