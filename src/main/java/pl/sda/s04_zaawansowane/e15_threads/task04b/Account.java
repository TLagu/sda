package pl.sda.s04_zaawansowane.e15_threads.task04b;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Account {
    private double balance;

    public synchronized void withdraw(double amount) {
        while (balance < amount) {
            try {
                System.out.println("Waiting for deposit (" + Thread.currentThread().getId() + ").");
                wait();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                System.out.println("Thread interrupted (" + Thread.currentThread().getId() + "): " + e);
            }
        }
        balance -= amount;
        System.out.println("Withdraw (" + Thread.currentThread().getId() + "): " + balance);
    }

    public synchronized void deposit(double amount) {
        balance += amount;
        System.out.println("Deposit (" + Thread.currentThread().getId() + "): " + balance);
        notifyAll();
    }
}
