package pl.sda.s04_zaawansowane.e15_threads.task04b;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class AccountManager implements Runnable {
    private Account account;
    private double amount;

    @Override
    public void run() {
        if (amount == 0
                || account.getBalance() + amount < 1 - Double.MAX_VALUE
                || account.getBalance() + amount > Double.MAX_VALUE) {
            throw new IllegalArgumentException("Transaction error - wrong amount.");
        }
        if (amount < 0) {
            account.withdraw(Math.abs(amount));
        } else {
            account.deposit(amount);
        }
    }
}
