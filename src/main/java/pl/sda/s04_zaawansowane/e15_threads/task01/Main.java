package pl.sda.s04_zaawansowane.e15_threads.task01;

public class Main {
    public static void main(String[] args) {
        new Thread(new PrimeNumbersCalculator(1000, 2000)).start();
        new Thread(new PrimeNumbersCalculator(14300, 17800)).start();
    }
}
