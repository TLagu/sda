package pl.sda.s04_zaawansowane.e15_threads.task01;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PrimeNumbersCalculator implements Runnable {
    private final int i;
    private final int j;

    @Override
    public void run() {
        if (i < 1 || j - i < 2) {
            return;
        }
        for (int i = this.i + 1; i < this.j - 1; i++) {
            int j;
            for (j = 2; j < i; j++) {
                if (i % j == 0) {
                    break;
                }
            }
            if (i > 2 && j == i) {
                System.out.println("Primary number: " + i);
            }
        }
    }
}
