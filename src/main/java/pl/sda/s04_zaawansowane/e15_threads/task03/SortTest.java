package pl.sda.s04_zaawansowane.e15_threads.task03;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SortTest {
    private final static int ARRAY_SIZE = 100000;

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        List<Callable<String>> tasks = new ArrayList<>();

        List<Integer> numbers = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < ARRAY_SIZE; i++) {
            numbers.add(random.nextInt());
        }
        tasks.add(
                () -> {
                    QuickSort sort = new QuickSort(numbers);
                    sort.sort();
                    return "QuickSort ended";
                }
        );
        tasks.add(
                () -> {
                    BubbleSort sort = new BubbleSort(numbers);
                    sort.sort();
                    return "BubbleSort ended";
                }
        );
        try {
            executorService.invokeAll(tasks);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
    }
}
