package pl.sda.s04_zaawansowane.e15_threads.task03;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@RequiredArgsConstructor
public class BubbleSort implements Sort {
    @Getter
    private final List<Integer> numbers;
    private BubbleSort bubbleSort;

    @Override
    public void sort() {
        if (numbers == null || numbers.size() < 2) {
            return;
        }
        System.out.println("BubbleSort started...");
        LocalTime start = LocalTime.now();
        for (int i = 0; i < numbers.size() - 1; i++) {
            for (int j = i + 1; j < numbers.size(); j++) {
                if (numbers.get(i) > numbers.get(j)) {
                    bubbleSort.swap(i, j);
                }
            }
        }
        System.out.println("BubbleSort ended. It take a: " + start.until(LocalTime.now(), ChronoUnit.SECONDS) + " seconds");
    }

    private void swap(int i, int j) {
        Integer tmp;
        tmp = numbers.get(i);
        numbers.set(i, numbers.get(j));
        numbers.set(j, tmp);
    }
}
