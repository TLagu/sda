package pl.sda.s04_zaawansowane.e15_threads.task03;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;

@AllArgsConstructor
@Getter
public class QuickSort implements Sort {
    private final List<Integer> numbers;

    // QuickSort implementation from https://www.withexample.com/quicksort-implementation-example-using-arraylist-java/
    static int nextIntInRange(int min, int max, Random rng) {
        if (min > max) {
            throw new IllegalArgumentException("Cannot draw random int from invalid range [" + min + ", " + max + "].");
        }
        int diff = max - min;
        if (diff >= 0 && diff != Integer.MAX_VALUE) {
            return (min + rng.nextInt(diff + 1));
        }
        int i;
        do {
            i = rng.nextInt();
        } while (i < min || i > max);
        return i;
    }

    @Override
    public void sort() {
        if (numbers == null || numbers.size() < 2) {
            return;
        }
        System.out.println("QuickSort started...");
        LocalTime start = LocalTime.now();
        startQuickStart(0, numbers.size() - 1);
        System.out.println("QuickSort ended. It take a: " + start.until(LocalTime.now(), ChronoUnit.SECONDS) + " seconds");
    }

    public void startQuickStart(int start, int end) {
        int q;
        if (start < end) {
            q = partition(start, end);
            startQuickStart(start, q);
            startQuickStart(q + 1, end);
        }
    }

    int partition(int start, int end) {
        int init = start;
        int length = end;

        Random r = new Random();
        int pivotIndex = nextIntInRange(start, end, r);
        int pivot = numbers.get(pivotIndex);

        while (true) {
            while (numbers.get(length) > pivot && length > start) {
                length--;
            }

            while (numbers.get(init) < pivot && init < end) {
                init++;
            }

            if (init < length) {
                int temp;
                temp = numbers.get(init);
                numbers.set(init, numbers.get(length));
                numbers.set(length, temp);
                length--;
                init++;
            } else {
                return length;
            }
        }

    }
}
