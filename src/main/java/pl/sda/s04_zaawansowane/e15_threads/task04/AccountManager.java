package pl.sda.s04_zaawansowane.e15_threads.task04;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class AccountManager {
    public static void main(String[] args) throws InterruptedException {
        final ExecutorService executorService = Executors.newFixedThreadPool(3);
        final AtomicInteger bankBalance = new AtomicInteger(100);

        executorService.submit(new Account(bankBalance, -200));
        Thread.sleep(1000);
        executorService.submit(new Account(bankBalance, 100));
        Thread.sleep(1000);
        executorService.submit(new Account(bankBalance, -200));
        Thread.sleep(1000);
        executorService.submit(new Account(bankBalance, -200));
        Thread.sleep(1000);
        executorService.submit(new Account(bankBalance, 500));
        executorService.shutdown();
    }
}
