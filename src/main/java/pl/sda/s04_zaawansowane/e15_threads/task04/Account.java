package pl.sda.s04_zaawansowane.e15_threads.task04;

import lombok.AllArgsConstructor;

import java.util.concurrent.atomic.AtomicInteger;

@AllArgsConstructor
public class Account implements Runnable {
    private final AtomicInteger bankBalance;
    private final Integer change;

    public synchronized void increasingBankBalance() {
        if (change == 0 || bankBalance.get() + change >= Integer.MAX_VALUE) {
            throw new IllegalArgumentException("Incorrect operation value.");
        }
        bankBalance.set(bankBalance.get() + change);
        System.out.println("Bank balance (increasing): " + bankBalance.get());
    }

    public synchronized void reductionBankBalance() throws InterruptedException {
        if (change == 0 || bankBalance.get() + change <= 1 - Integer.MAX_VALUE) {
            throw new IllegalArgumentException("Incorrect operation value.");
        }
        do {
            if (bankBalance.get() + change < 0) {
                System.out.println("Reduction is pending... (" + Thread.currentThread().getId() + ")");
                Thread.sleep(1000);
            }
        } while (bankBalance.get() + change < 0);
        System.out.println("Reduction begins... (" + Thread.currentThread().getId() + ")");
        bankBalance.set(bankBalance.get() + change);
        System.out.println("Bank balance (reduction " + Thread.currentThread().getId() + "): " + bankBalance.get());
    }

    @Override
    public void run() {
        if (change == 0) {
            throw new IllegalArgumentException("Incorrect operation value.");
        } else if (change > 0) {
            this.increasingBankBalance();
        } else {
            try {
                this.reductionBankBalance();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(bankBalance.get());
    }
}
