package pl.sda.s04_zaawansowane.e10_collections.examples.e03;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class QueueService {
    public static void main(String[] args) {
        QueueService queueService = new QueueService();
        queueService.showQueue();
        queueService.showDeque();
    }

    private void showQueue() {
        Queue<String> events = new LinkedList<>();
        events.offer("ButtonClicked");
        events.offer(null);

        String event1 = events.peek();
        String event2 = events.poll();
        String event3 = events.poll();

        System.out.println(event1);
        System.out.println(event2);
        System.out.println(event3);

        String event4 = events.peek();
        System.out.println(event4);

        System.out.println(events.isEmpty());
    }

    private void showDeque() {
        Deque<Integer> deque = new ArrayDeque<>();
        deque.offerLast(2);
        deque.offerFirst(1);
        System.out.println(deque.pollLast());
        System.out.println(deque.peekLast());
    }

}
