package pl.sda.s04_zaawansowane.e10_collections.examples.e06;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class User implements Comparable<User> {
    private final String name;
    private final int age;

    @Override
    public int compareTo(User o) {
        return age - o.age;
    }
}
