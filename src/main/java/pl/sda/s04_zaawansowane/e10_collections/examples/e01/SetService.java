package pl.sda.s04_zaawansowane.e10_collections.examples.e01;

import java.util.*;

public class SetService {

    public static void main(String[] args) {
        SetService setService = new SetService();
        setService.showHashSet();
        setService.showTreeSet();
        setService.showLinkedHashSet();
    }

    private void showHashSet() {
        final Set<Integer> numberSet = new HashSet<>();
        System.out.println("HashSet: " + numberSet.isEmpty());
        numberSet.addAll(Arrays.asList(addElement()));
        //numberSet.forEach(System.out::println);
        System.out.println(Arrays.toString(numberSet.toArray()));
    }

    private void showTreeSet() {
        final Set<Integer> numberSet = new TreeSet<>(Comparator.reverseOrder());
        System.out.println("TreeSet: " + numberSet.isEmpty());
        numberSet.addAll(Arrays.asList(addElement()));
        System.out.println(Arrays.toString(numberSet.toArray()));
    }

    private void showLinkedHashSet() {
        final Set<Integer> numberSet = new LinkedHashSet<>();
        System.out.println("LinkedHashSet: " + numberSet.isEmpty());
        numberSet.addAll(Arrays.asList(addElement()));
        System.out.println(Arrays.toString(numberSet.toArray()));
    }

    private static Integer[] addElement() {
        return new Integer[] {1, 17, 3, 2, 1};
    }
}
