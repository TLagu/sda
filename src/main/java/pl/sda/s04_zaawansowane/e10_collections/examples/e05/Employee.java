package pl.sda.s04_zaawansowane.e10_collections.examples.e05;

public class Employee extends Person {
    private String card;
    public Employee(int id, String name, String surname, String card) {
        super(id, name, surname);
        this.card = card;
    }
}
