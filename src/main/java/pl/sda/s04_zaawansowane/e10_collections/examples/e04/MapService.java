package pl.sda.s04_zaawansowane.e10_collections.examples.e04;

import java.util.*;

public class MapService {
    public static void main(String[] args) {
        MapService mapService = new MapService();
        mapService.showHashMap();
        System.out.println("----------------------------");
        mapService.showHashMapE();
        System.out.println("----------------------------");
        mapService.showTreeMap();
    }

    private void showHashMap() {
        System.out.println("HashMap: ");
        Map<Integer, String> ageToNames = new HashMap<>();
        ageToNames.put(11, "Andrzej");
        ageToNames.put(22, "Michał");
        ageToNames.put(33, "Janusz");
        ageToNames.remove(22);
        System.out.println(ageToNames.get(11));
        System.out.println(ageToNames.values());
    }

    private void showHashMapE() {
        Map<Integer, String> ageToNames = new HashMap<>();
        ageToNames.put(20, "Gosia");
        ageToNames.put(30, "Kasia");
        ageToNames.put(40, "Ania");

        Set<Integer> keys = ageToNames.keySet();
        for (Integer key: keys) {
            System.out.println("Key = " + key);
        }

        Collection<String> values = ageToNames.values();
        for (String value : values) {
            System.out.println("Value = " + value);
        }

        Set<Map.Entry<Integer, String>> entries = ageToNames.entrySet();
        for (Map.Entry<Integer, String> entry : entries) {
            System.out.println("key = " + entry.getKey() + ", value = " + entry.getValue());
        }
    }
    private void showTreeMap() {
        System.out.println("HashMap: ");
        Map<Integer, String> ageToNames = new TreeMap<>(Comparator.reverseOrder());
        ageToNames.put(33, "Janusz");
        ageToNames.put(11, "Andrzej");
        ageToNames.put(22, "Michał");
        System.out.println(ageToNames.values());
        ageToNames.forEach((key, value) -> System.out.println("key = " + key + ", value = " + value));
    }

}
