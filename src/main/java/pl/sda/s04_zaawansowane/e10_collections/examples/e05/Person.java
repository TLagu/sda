package pl.sda.s04_zaawansowane.e10_collections.examples.e05;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
//@EqualsAndHashCode(of = "id")
public class Person {
    private int id;
    private String name;
    private String surname;
/*
    // Automatic
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
*/
    // Manual
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        // w przypadku klas dziedziczacych nie zadziała
        if (!(o instanceof Person)) return false;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id;
    }
    @Override
    public int hashCode() {
        return id;
    }
}
