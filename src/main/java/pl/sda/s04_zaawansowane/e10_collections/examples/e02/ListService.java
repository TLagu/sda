package pl.sda.s04_zaawansowane.e10_collections.examples.e02;

import java.util.*;

public class ListService {
    public static void main(String[] args) {
        ListService listService = new ListService();
        listService.showArrayList();
        System.out.println("-----------------------");
        listService.showLinkedList();
    }

    private void showArrayList() {
        final List<String> names = new ArrayList<>(addElement());
        System.out.println("ArrayList:");
        for (int i = 0; i < names.size(); i++) {
            System.out.println(names.get(i));
        }
    }

    private void showLinkedList() {
        final List<String> names = new LinkedList<>(addElement());
        names.add(0, "Dupek");
        System.out.println("ArrayList:");
        for (final String name: names) {
            System.out.println(name);
        }
    }

    private static List<String> addElement() {
        return Arrays.asList("Andrzej", "Andrzej", "Grzegorz", "Andrzej", "Grzegorz");
    }
}
