package pl.sda.s04_zaawansowane.e10_collections.examples.e06;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class UserService {
    public static void main(String[] args) {
        List<User> users = Arrays.asList(
                new User("Piotr", 20),
                new User("Kazimierz", 67),
                new User("Jan", 23)
        );
        System.out.println("Klasa anonimowa dla interfejsu Comparator: ");
        Collections.sort(users, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        for (User user : users) {
            System.out.println(user);
        }
        System.out.println("----------------------");
        System.out.println("Wykorzystanie interfejsu Comparable (rosnąco): ");
        Collections.sort(users);
        for (User user : users) {
            System.out.println(user);
        }
        System.out.println("----------------------");
        System.out.println("Wykorzystanie interfejsu Comparable (malejąco): ");
        Collections.sort(users, Collections.reverseOrder());
        for (User user : users) {
            System.out.println(user);
        }
        System.out.println("----------------------");
    }
}
