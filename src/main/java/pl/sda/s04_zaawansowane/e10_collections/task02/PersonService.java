package pl.sda.s04_zaawansowane.e10_collections.task02;

import java.util.*;

public class PersonService {
    public static void main(String[] args) {
        System.out.println("--------------------- Comparable");
        byComparable(getData());
        System.out.println("--------------------- Comparator");
        byComparator(getData());
    }

    private static void byComparable(List<Person> persons) {
        // Sort
        Collections.sort(persons);
        System.out.println("------- All");
        showData(persons);
        System.out.println("------- Deleted");
        persons.remove(1);
        showData(persons);
    }

    private static void byComparator(List<Person> persons) {
        // Sort
        persons.sort(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getAge() - o2.getAge();
            }
        });
        System.out.println("------- All");
        showData(persons);
        System.out.println("------- Deleted");
        persons.remove(1);
        showData(persons);
    }

    private static List<Person> getData() {
        return new ArrayList(Arrays.asList(
                new Person("Adam", "Nowak", 44),
                new Person("Arkadiusz", "Kowalski", 33),
                new Person("Grzegorz", "Brzęczyszczykiewicz", 22)
        ));
    }

    private static void showData(List<Person> persons) {
        for(Person person : persons) {
            System.out.println(person);
        }
    }
}
