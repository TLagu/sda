package pl.sda.s04_zaawansowane.e10_collections.task02;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class Person implements Comparable<Person> {
    private String name;
    private String surname;
    @Getter
    private int age;

    @Override
    public int compareTo(Person o) {
        return age - o.age;
    }
}
