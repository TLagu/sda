package pl.sda.s04_zaawansowane.e10_collections.task07;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class randomNumberService {
    public static void main(String[] args) {
        Set<Integer> randomArray = new HashSet<>();
        Random random = new Random();
        int loop = 0;
        do {
            System.out.println("Loop: " + ++loop);
            randomArray.add(random.nextInt(20));
        } while (randomArray.size() <= 10);
        System.out.println(Arrays.toString(randomArray.toArray()));
    }
}
