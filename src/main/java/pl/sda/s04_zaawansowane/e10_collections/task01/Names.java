package pl.sda.s04_zaawansowane.e10_collections.task01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Names {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        names.add("Adam");
        names.add("Krzysztof");
        names.add("Robert");
        names.add("Jan");
        names.add("Bartosz");
        names.add("Wojciech");
        names.add("Maciej");
        System.out.println(Arrays.toString(names.toArray()));
        Collections.sort(names);
        System.out.println(Arrays.toString(names.toArray()));
    }
}
