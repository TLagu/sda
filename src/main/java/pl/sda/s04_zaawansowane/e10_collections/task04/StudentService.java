package pl.sda.s04_zaawansowane.e10_collections.task04;

import java.util.*;

public class StudentService {
    public static void main(String[] args) {
        StudentService studentService = new StudentService();
        Map<String, Double> students = studentService.getData();
        System.out.println("Scores:");
        Collection<Double> values = students.values();
        System.out.println(" - " + Arrays.toString(values.toArray()));
        System.out.println("Names:");
        Set<String> keys = students.keySet();
        System.out.println(" - " + Arrays.toString(keys.toArray()));
        System.out.println("Map (before modification):");
        students.forEach((key, value) -> System.out.println(" - " + key + " -> " + value));
        students.put("Arkadiusz", 4.7);
        System.out.println("Map (after modification):");
        students.forEach((key, value) -> System.out.println(" - " + key + " -> " + value));
    }

    private Map<String, Double> getData() {
        Map<String, Double> student = new HashMap<>();
        student.put("Arkadiusz", 4.2);
        student.put("Michał", 6.1);
        student.put("Waldemar", 9.3);
        student.put("Krzysztof", 4.6);
        student.put("Kamil", 5.3);
        return student;
    }
}
