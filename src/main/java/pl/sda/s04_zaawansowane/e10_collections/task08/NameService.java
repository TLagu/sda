package pl.sda.s04_zaawansowane.e10_collections.task08;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class NameService {
    private static final Map<String, Integer> CACHE = new HashMap<>();
    public static void main(String[] args) {
        NameService nameService = new NameService();
        System.out.println(nameService.countUniqueLetters("ala"));
        System.out.println(nameService.countUniqueLetters("piesek"));
        System.out.println(nameService.countUniqueLetters("kot"));
        System.out.println(nameService.countUniqueLetters("ala"));
        System.out.println(nameService.countUniqueLetters("piesek"));
        System.out.println(nameService.countUniqueLetters("wąż"));
    }

    public int countUniqueLetters (String name) {
        if (CACHE.containsKey(name)) {
            System.out.println("Result in cache...");
            return CACHE.get(name);
        }
        System.out.println("Counting...");
        char[] chars = name.toCharArray();
        Set<Character> uniqueChars = new HashSet<>();
        for (char tmpChar : chars) {
            uniqueChars.add(tmpChar);
        }
        int size = uniqueChars.size();
        CACHE.put(name, size);
        return size;
    }
}
