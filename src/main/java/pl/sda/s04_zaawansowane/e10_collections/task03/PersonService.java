package pl.sda.s04_zaawansowane.e10_collections.task03;

import java.util.*;

public class PersonService {
    public static void main(String[] args) {
        Map<Integer, Person> persons = getData();
        showData(persons);
        Person person1 = persons.get(1);
        System.out.println(person1);
        System.out.println(persons.containsKey(1));
        System.out.println(persons.containsValue(person1));
        Person person2 = new Person("Arkadiusz", "Kowalski", 33, 2);
        System.out.println(person2);
        System.out.println(persons.containsValue(person2));
    }

    private static Map<Integer, Person> getData() {
        Map<Integer, Person> persons = new HashMap<>();
        persons.put(1, new Person("Adam", "Nowak", 44, 1));
        persons.put(2, new Person("Arkadiusz", "Kowalski", 33, 2));
        persons.put(3, new Person("Grzegorz", "Brzęczyszczykiewicz", 22, 3));
        return persons;
    }

    private static void showData(Map<Integer, Person> persons) {
        persons.forEach((key, value) -> System.out.println("key = " + key + ", value = " + value));
    }
}
