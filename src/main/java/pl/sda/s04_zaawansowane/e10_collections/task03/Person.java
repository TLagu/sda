package pl.sda.s04_zaawansowane.e10_collections.task03;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
@EqualsAndHashCode(of = "id")
public class Person implements Comparable<Person> {
    private String name;
    private String surname;
    private int age;
    @Getter
    private int id;

    @Override
    public int compareTo(Person o) {
        return age - o.age;
    }
}
