package pl.sda.s04_zaawansowane.e10_collections.task05;

import java.util.ArrayList;
import java.util.List;

public class Item<T> {
    private final List<T> array = new ArrayList<>();;

    public void add(T element) {
        if (element == null) {
            throw new IllegalArgumentException("Element cannot be null...");
        }
        array.add(element);
    }

    public void remove(int index) {
        if (array.size() <= index || index < 0) {
            throw new IllegalArgumentException("index out of bound...");
        }
        array.remove(index);
    }

    public T get(int index) {
        if (array.size() <= index || index < 0) {
            throw new IllegalArgumentException("index out of bound...");
        }
        return array.get(index);
    }

    public void display() {
        array.forEach(this::displayOneElement);
    }

    public void displayOneElement(T value) {
        System.out.println(" - " + value);
    }
}
