package pl.sda.s04_zaawansowane.e10_collections.task05;

public class ItemService {
    public static void main(String[] args) {
        Item item1 = new Item();
        item1.add(1);
        item1.add("Second element");
        item1.add(3.0);
        item1.add("");
        //item1.add(null);
        System.out.println("List of items:");
        item1.display();
        item1.remove(1);
        System.out.println("List of items (after remove):");
        item1.display();
        System.out.println("Element: " + item1.get(1));
    }
}
