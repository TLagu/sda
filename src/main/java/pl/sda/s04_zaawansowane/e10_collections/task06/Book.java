package pl.sda.s04_zaawansowane.e10_collections.task06;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
public class Book {
    private int id;
    private String title;
    private String author;
    private String isbn;
    private int pageNum;
    private int yearOfPublication;
    private double price;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return id == book.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return " - Author -> " + author
                + ", title -> " + title
                + ", ISBN -> " + isbn
                + ", pages -> " + pageNum
                + ", year of publication -> " + yearOfPublication
                + ", price -> " + price;
    }
}
