package pl.sda.s04_zaawansowane.e10_collections.task06;

public class Library {
    public static void main(String[] args) {
        BookService library = new BookService();
        library.addBook(new Book(1, "Tom Sawyer", "Mark Twain", "83-06-02757-4", 687, 2000, 42));
        library.addBook(new Book(2, "Ogniem i mieczem", "Henryk Sienkiewicz", "83-01-05369-0", 501, 1985, 48));
        library.addBook(new Book(3, "Diuna", "Frank Herbert", "978-83-8188-138-8", 784, 2021, 33));
        //library.addBook(null);
        library.displayBooks();
        library.displayListOfBooksPublishedBefore(1999);
        //library.displayListOfBooksPublishedBefore(-5);
        library.displayTheMostExpensiveBook();
        library.displaySortedByAuthor();
        library.displaySortedByAuthorComparator();
        library.displaySortedByTitle();
        //library.findAndDisplayBookByAuthorAndTitle(null, null);
        library.findAndDisplayBookByAuthorAndTitle("Frank Herbert", "Diuna");
        library.removeBook(1);
        library.displayBooks();
    }
}
