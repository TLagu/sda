package pl.sda.s04_zaawansowane.e10_collections.task06;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class BookService {
    private final List<Book> books = new ArrayList<>();

    public void addBook(Book book) {
        if (book == null) {
            throw new IllegalArgumentException("Invalid book data!");
        }
        books.add(book);
    }

    public void removeBook(int index) {
        if (index < 0 || books.size() <= index) {
            throw new IllegalArgumentException("Index out of bound!");
        }
        books.remove(index);
    }

    public void displayBooks() {
        System.out.println("List of books:");
        if (libraryIsEmpty()) {
            return;
        }
        books.forEach(System.out::println);
    }

    public void displayListOfBooksPublishedBefore(int year) {
        if (year < 0 || Calendar.getInstance().get(Calendar.YEAR) < year) {
            throw new IllegalArgumentException("Year out of bound!");
        }
        System.out.println("List of books published before " + year + ":");
        books.stream().filter(book -> book.getYearOfPublication() < year).forEach(System.out::println);
    }

    public void displayTheMostExpensiveBook() {
        System.out.println("The most expensive book:");
        if (libraryIsEmpty()) {
            return;
        }
        Book mostExpensive = books.get(0);
        for (Book book : books) {
            if (book.getPrice() > mostExpensive.getPrice()) {
                mostExpensive = book;
            }
        }
        System.out.println(mostExpensive);
    }

    public void displaySortedByAuthor() {
        System.out.println("List of books sorted by author:");
        if (libraryIsEmpty()) {
            return;
        }
        books.sort(new Comparator<Book>() {
            @Override
            public int compare(Book o1, Book o2) {
                return o1.getAuthor().compareTo(o2.getAuthor());
            }
        });
        books.forEach(System.out::println);
    }

    public void displaySortedByAuthorComparator() {
        System.out.println("List of books sorted by author (Comparator):");
        if (libraryIsEmpty()) {
            return;
        }
        Comparator<Book> byAuthor = Comparator.comparing(Book::getAuthor).reversed();
        Supplier<TreeSet<Book>> car = () -> new TreeSet<Book>(byAuthor);
        TreeSet<Book> bookSet = books.stream().collect(Collectors.toCollection(car));
        bookSet.forEach(System.out::println);
    }

    public void displaySortedByTitle() {
        System.out.println("List of books sorted by title:");
        if (libraryIsEmpty()) {
            return;
        }
        Collections.sort(books, new Comparator<Book>() {
            @Override
            public int compare(Book o1, Book o2) {
                return o1.getTitle().compareTo(o2.getTitle());
            }
        });
        books.forEach(System.out::println);
    }

    public void findAndDisplayBookByAuthorAndTitle(String author, String title) {
        System.out.println("Wanted book:");
        if (libraryIsEmpty()) {
            return;
        }
        if (author == null || author.isBlank()) {
            throw new IllegalArgumentException(" - The author cannot be null");
        }
        if (title == null || title.isBlank()) {
            throw new IllegalArgumentException(" - The title cannot be null");
        }
        books.stream().filter(book -> author.equals(book.getAuthor()) && title.equals(book.getTitle()))
                .forEach(System.out::println);
    }

    private boolean libraryIsEmpty() {
        return books.size() <= 0;
    }
}
