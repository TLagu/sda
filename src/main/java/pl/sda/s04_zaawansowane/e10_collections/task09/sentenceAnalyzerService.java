package pl.sda.s04_zaawansowane.e10_collections.task09;

import java.util.*;

public class sentenceAnalyzerService {
    public static void main(String[] args) {
        System.out.print("Enter the sentence: ");
        Scanner scanner = new Scanner(System.in);
        String sentence = scanner.nextLine().replace(" ", "");
        char[] chars = sentence.toCharArray();
        Map<Character, List<Integer>> useOfTheSign = new HashMap<>();
        for (int i = 0; i < chars.length; i++) {
            List<Integer> charPositions = new ArrayList<>();
            if (useOfTheSign.containsKey(chars[i])) {
                charPositions = useOfTheSign.get(chars[i]);
            }
            charPositions.add(i);
            useOfTheSign.put(chars[i], charPositions);
        }
        for (Map.Entry<Character, List<Integer>> entry : useOfTheSign.entrySet()) {
            System.out.println(entry.getKey() + " -> " + Arrays.toString(entry.getValue().toArray()));
        }
    }
}
