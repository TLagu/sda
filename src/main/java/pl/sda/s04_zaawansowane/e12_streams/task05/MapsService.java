package pl.sda.s04_zaawansowane.e12_streams.task05;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MapsService {
    public static void main(String[] args) {
//        Dana jest lista:
//        List<String> items = Arrays.asList("pen", "book", "pen", "coin", "book", "desk", "book", "pen", "book", "coin");
//        Utwórz mapę, której kluczem jest element listy, natomiast wartością liczba wystąpień.
//        Wypisz zawartość mapy
        List<String> items = Arrays.asList("pen", "book", "pen", "coin", "book", "desk", "book", "pen", "book", "coin");
        Map<String, List<String>> mapList = items.stream().collect(Collectors.groupingBy(i -> i));
        Map<String, Integer> mapInt = mapList.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().size()));
        System.out.println(mapInt);
    }
}
