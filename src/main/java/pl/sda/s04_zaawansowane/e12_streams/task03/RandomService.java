package pl.sda.s04_zaawansowane.e12_streams.task03;

import java.util.Random;
import java.util.stream.Stream;

public class RandomService {
    public static void main(String[] args) {
//        Utwórz strumień losowych liczb całkowitych z zakresu od 0 do 10. Strumień powinien mieć rozmiar 8.
//        Wykorzystując metodę distinct(), wypisz na konsoli unikalne liczby
        Stream.generate(() -> {
            Random random = new Random();
            return random.nextInt(10);
        }).limit(8)
                .distinct()
                .forEach(System.out::println);
    }
}
