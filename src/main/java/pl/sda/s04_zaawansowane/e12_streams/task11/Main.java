package pl.sda.s04_zaawansowane.e12_streams.task11;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Main {
    private static final String[] numNames = {
            "",
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
            "ten"
    };
    private static final int NUMBER_OF_ARRAYS = 5;
    private static final int MAX_VALUE_IN_ARRAY = 5;

    public static void main(String[] args) {
        Random random = new Random();
        List<Integer> sizes = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ARRAYS; i++) {
            sizes.add(random.nextInt(MAX_VALUE_IN_ARRAY) + 1);
        }
        List<List<String>> collection = new ArrayList<>();
        for (Integer size : sizes) {
            List<String> subCollection = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                subCollection.add(numNames[random.nextInt(MAX_VALUE_IN_ARRAY) + 1]);
            }
            collection.add(subCollection);
        }
        System.out.println(collection);
        System.out.println();
        System.out.println(transform(collection));
        System.out.println();
        System.out.println(transformByStream(collection));
    }

    public static List<String> transform(List<List<String>> collection) {
        List<String> newCollection = new ArrayList<>();
        for (List<String> subCollection : collection) {
            for (String value : subCollection) {
                newCollection.add(value);
            }
        }
        return newCollection;
    }

    public static List<String> transformByStream(List<List<String>> collection) {
        return collection.stream().flatMap(List::stream).collect(Collectors.toList());
    }
}
