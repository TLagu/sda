package pl.sda.s04_zaawansowane.e12_streams.task15;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Main {
    private static final int SIZE_OF_INTEGER_ARRAY = 10;

    public static void main(String[] args) {
        Random random = new Random();
        List<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < SIZE_OF_INTEGER_ARRAY; i++){
            numbers.add(random.nextInt(10));
        }
        System.out.println(numbers);
        System.out.println();
        System.out.println(getString(numbers));
    }

    public static String getString(List<Integer> list){
        return list.stream()
                .map(l -> (l % 2 == 0 ? "e" : "o") + l)
                .collect(Collectors.joining(","));
    }
}
