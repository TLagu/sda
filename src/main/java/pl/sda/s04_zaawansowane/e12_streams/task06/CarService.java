package pl.sda.s04_zaawansowane.e12_streams.task06;

import pl.sda.s04_zaawansowane.e12_streams.task02.Car;

import java.util.Comparator;
import java.util.List;

public class CarService {
    //W oparciu o utworzoną w zadaniu 2 klasę Car, wypisz na konsoli samochód posiadający najwyższą cenę
    public static void main(String[] args) {
        List<Car> carList = List.of(
                new Car("VW", 100000),
                new Car("Ford", 90000),
                new Car("Citroen", 110000));
        carList.stream().sorted(Comparator.comparing(Car::getPrice, Comparator.reverseOrder())).forEach(System.out::println);
    }
}
