package pl.sda.s04_zaawansowane.e12_streams.task01;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamService {
    public static void main(String[] args) {
        List<String> words = List.of("pen", "coin", "desk", "chair");
        Supplier<Stream<String>> streamSupplier = words::stream;
        System.out.println("first / any item: " + streamSupplier.get().findAny().get());
        System.out.println("size: " + streamSupplier.get().count());
        String day = "Sunday";
        Stream<Character> stream2 = day.chars().mapToObj(i->(char)i);
        stream2.filter(p -> !p.equals('n')).forEach(System.out::println);

        System.out.println("------------------------------------");

        String any = words.stream().findAny().orElse("unknown");
        System.out.println(any);

        long count = words.size();
        System.out.println("size = " + count);

        day = "Sunday";
        char[] chars = day.toCharArray();
        String[] string = new String[chars.length];
        int i = 0;
        for (char element: chars) {
            string[i++] = String.valueOf(element);
        }

        String value = Arrays.stream(string)
                .filter(e -> !words.equals("n"))
                .collect(Collectors.joining());
        System.out.println(value);
        System.out.println("------------------------------------");

    }
}
