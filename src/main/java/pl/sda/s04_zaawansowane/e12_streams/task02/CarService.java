package pl.sda.s04_zaawansowane.e12_streams.task02;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CarService {
    public static void main(String[] args) {
        List<Car> cars = List.of(
                new Car("VW", 100000),
                new Car("Ford", 90000),
                new Car("Citroen", 110000)
        );
        Supplier<Stream<Car>> streamSupplier = cars::stream;
        System.out.println("1. Original: ");
        streamSupplier.get().forEach(System.out::println);
        System.out.println("2. Sorted by price: ");
        Comparator<Car> byAge = Comparator.comparingInt(Car::getPrice);
        Supplier<TreeSet<Car>> car = () -> new TreeSet<Car>(byAge);
        TreeSet<Car> carSet = cars.stream().collect(Collectors.toCollection(car));
        carSet.forEach(System.out::println);

        System.out.println("--------------------------");
        System.out.println("1. Sorted by price: ");
        cars.stream()
                .sorted(Comparator.comparing(Car::getPrice))
                .forEach(System.out::println);
        System.out.println("2. Sorted by name: ");
        cars.stream()
                .sorted(Comparator.comparing(Car::getName))
                .forEach(System.out::println);
    }
}
