package pl.sda.s04_zaawansowane.e12_streams.task02;

import lombok.Getter;

@Getter
public class Car {
    private final String name;
    private final int price;

    public Car(String name, int price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return " - name = '" + name + "', price = " + price;
    }
}
