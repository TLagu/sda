package pl.sda.s04_zaawansowane.e12_streams.task12_14;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class Person {
    private String name;
    private int age;
    private String nationality;

    public Person(String name, int age) {
        this(name, age, "");
    }
}
