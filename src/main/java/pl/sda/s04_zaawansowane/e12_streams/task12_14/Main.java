package pl.sda.s04_zaawansowane.e12_streams.task12_14;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

public class Main {

    public static void main(String[] args) {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person("Krzysztof", 13, "Polska"));
        personList.add(new Person("Anna", 25, "Polska"));
        personList.add(new Person("Michał", 11, "Polska"));
        personList.add(new Person("Hanna", 22, "Polska"));
        personList.add(new Person("Tadeusz", 54, "Polska"));
        personList.add(new Person("Joanna", 45, "Polska"));
        personList.add(new Person("Tomasz", 67, "Polska"));

        // Additional tasks (2)
        System.out.println("Oldest person:");
        System.out.println(" - " + getOldestPerson(personList));

        // Additional tasks (3)
        System.out.println("Children's list: ");
        System.out.println(" - " + getKidNames(personList));

        // Additional tasks (4)
        System.out.println("Separation into children and adults: ");
        Map<Boolean, List<Person>> partitionAdults = partitionAdults(personList);
        for (boolean key : partitionAdults.keySet()){
            System.out.println(" - " + (key ? "Child" : "Adult") + ":");
            for(Person person : partitionAdults.get(key)) {
                System.out.println("   - " + person);
            }
        }
    }

    public static Optional<Person> getOldestPerson(List<Person> people) {
        return people.stream().max(Comparator.comparingInt(Person::getAge));
    }

    public static Set<String> getKidNames(List<Person> people){
        return people.stream().filter(p -> p.getAge() < 18).map(Person::getName).collect(Collectors.toSet());
    }

    public static Map<Boolean, List<Person>> partitionAdults(List<Person> people) {
        return people.stream()
                .collect(toMap(
                        p -> p.getAge() >= 18,
                        p -> {
                            List<Person> list = new ArrayList<>();
                            list.add(p);
                            return list;
                        },
                        (left, right) -> {
                            left.addAll(right);
                            return left;
                        },
                        TreeMap::new
                ));
    }
}
