package pl.sda.s04_zaawansowane.e12_streams.task08.order;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.sda.s04_zaawansowane.e12_streams.task08.service.OrderStatus;
import pl.sda.s04_zaawansowane.e12_streams.task08.customer.Customer;
import pl.sda.s04_zaawansowane.e12_streams.task08.product.Product;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@Getter
public class Order {
    private Long id;
    private OrderStatus status;
    private LocalDate orderDate;
    private LocalDate deliveryDate;
    private List<Product> products;
    private Customer customer;
    private String lang;

    public String getMainData() {
        return "\n - id: " + id
                + ", status: " + ((lang.equals("pl")) ? status.getPlName() : status.getEnName())
                + ", orderDate: " + orderDate
                + ", deliveryDate: " + deliveryDate;
    }

    @Override
    public String toString() {
        return "\n - id: " + id
                + ", status: " + ((lang.equals("pl")) ? status.getPlName() : status.getEnName())
                + ", orderDate: " + orderDate
                + ", deliveryDate: " + deliveryDate
                + "\n   products: " + products
                + "\n   customer: " + customer;
    }
}
