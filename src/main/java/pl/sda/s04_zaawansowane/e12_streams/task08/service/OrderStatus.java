package pl.sda.s04_zaawansowane.e12_streams.task08.service;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@AllArgsConstructor
@Getter
public enum OrderStatus {
    ACCEPTED("Accepted", "Przyjęte"),
    IN_PROGRESS("In progress", "W realizacji"),
    PREPARED("Prepared for shipment", "Przygotowane do wysyłki"),
    SENT("Sent", "Wysłane"),
    REALIZED("Realized", "Zrealizowane");

    private static final List<OrderStatus> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();
    private final String enName;
    private final String plName;

    public static OrderStatus randomValue() {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }
}
