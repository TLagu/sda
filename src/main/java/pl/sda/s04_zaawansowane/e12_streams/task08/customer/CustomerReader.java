package pl.sda.s04_zaawansowane.e12_streams.task08.customer;

import pl.sda.s04_zaawansowane.e12_streams.task08.service.ReaderService;

import java.util.ArrayList;
import java.util.List;

public class CustomerReader {
    private static final List<Customer> customers = new ArrayList<>();

    public static List<Customer> customerReader() {
        for (int i = 1; i <= ReaderService.CUSTOMER_COUNT; i++) {
            customers.add(new Customer(
                    (long) i,
                    ReaderService.getName("Customer", i),
                    ReaderService.getRandomInteger(1, 4)));
        }
        return customers;
    }
}
