package pl.sda.s04_zaawansowane.e12_streams.task08;

import pl.sda.s04_zaawansowane.e12_streams.task08.service.CategoryType;

import java.time.LocalDate;

public class WarehouseService {
    public static void main(String[] args) {
        OrderService orderService = new OrderService();
        System.out.println("Wszystkie dane.");
        System.out.println(orderService);
        System.out.println("a) Metoda powinna zwrócić listę produktów należących do kategorii „Book” z ceną większą od 100.");
        System.out.println(orderService.getProductsFromCategoryWithPriceGreaterThan(CategoryType.BOOK, 100));
        System.out.println("b) Metoda powinna zwrócić listę zamówień, w których przynajmniej jeden produkt należy do kategorii „Baby”");
        System.out.println(orderService.getOrdersByCategory(CategoryType.BABY));
        System.out.println("c) Metoda powinna zwrócić listę produktów z kategorii „Toys” jednocześnie aplikując do tych produktów 10% zniżkę.");
        System.out.println(orderService.getProductsByCategoryWithDiscount(CategoryType.TOYS, 10));
        System.out.println("d) Metoda powinna zwrócić listę unikalnych produktów zamówionych przez klientów z poziomu 2 (atrybut tier) od 2021-02-01 do 2021-04-01.");
        System.out.println(orderService.getDistinctProductsByTierAndDate(2,
                LocalDate.of(2021, 2, 1),
                LocalDate.of(2021, 4, 1)));
        System.out.println("e) Metoda powinna zwracać najtańszy produkt z kategorii „Books”.");
        System.out.println(orderService.getCheapestProductByCategory(CategoryType.BOOK));
        System.out.println("f) Metoda powinna zwrócić listę trzech ostatnich zamówień.");
        System.out.println(orderService.getLastFewOrders(3));
        System.out.println("g) Metoda powinna zwracać listę wszystkich produktów, które zostały zamówione 15 marca 2021,");
        System.out.println("   jednocześnie wypisując na konsoli zamówienia, z których pochodziły (metoda peek).");
        System.out.println(orderService.getProductsOrderedOnSpecificDateAndPeekOrders(LocalDate.of(2021, 3, 15)));
        System.out.println("h) Metoda wyznacza sumę wszystkich zamówień, które miały miejsce w lutym 2021 roku.");
        System.out.println(orderService.getOrdersFromSpecificYearAndMonth(2021, 2));
        System.out.println("i) Metoda zwraca mapę zamówień pogrupowanych na podstawie identyfikatora klienta.");
        System.out.println(orderService.getOrdersGroupedByCustomers());
        System.out.println("j) Metoda zwraca mapę zamówień, której kluczem jest identyfikator zamówienia, natomiast wartością liczba produktów.");
        System.out.println(orderService.getOrderIdAndProductCount());
        System.out.println("k) Metoda zwraca mapę, której kluczem jest kategoria produktu, natomiast wartością najdroższy produkt.");
        System.out.println(orderService.getMostExpensiveProductFromEachCategory());
    }
}
