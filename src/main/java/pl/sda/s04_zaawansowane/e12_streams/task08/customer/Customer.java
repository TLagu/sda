package pl.sda.s04_zaawansowane.e12_streams.task08.customer;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Customer {
    private Long id;
    private String name;
    private Integer tier;

    @Override
    public String toString() {
        return "\n   - id: " + id + ", name: " + name + ", tier: " + tier;
    }
}
