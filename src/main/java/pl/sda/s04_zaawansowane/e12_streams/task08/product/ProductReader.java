package pl.sda.s04_zaawansowane.e12_streams.task08.product;

import pl.sda.s04_zaawansowane.e12_streams.task08.service.CategoryType;
import pl.sda.s04_zaawansowane.e12_streams.task08.service.ReaderService;

import java.util.ArrayList;
import java.util.List;

public class ProductReader {
    private static final List<Product> products = new ArrayList<>();
    private static Long bookId = 0L;

    public static List<Product> productReader() {
        generateProductsForSpecificCategory(CategoryType.BOOK);
        generateProductsForSpecificCategory(CategoryType.BABY);
        generateProductsForSpecificCategory(CategoryType.TOYS);
        return products;
    }

    private static void generateProductsForSpecificCategory(CategoryType category) {
        String name;
        int count = 0;
        int start;
        int multiplying;
        switch (category) {
            case BOOK:
                name = (ReaderService.LANG.equals("pl")) ? CategoryType.BOOK.getPlName() : CategoryType.BOOK.getEnName();
                count = ReaderService.BOOK_COUNT;
                start = 10;
                multiplying = 20;
                break;
            case BABY:
                name = (ReaderService.LANG.equals("pl")) ? CategoryType.BABY.getPlName() : CategoryType.BABY.getEnName();
                count = ReaderService.BABY_COUNT;
                start = 25;
                multiplying = 50;
                break;
            default:
                name = (ReaderService.LANG.equals("pl")) ? CategoryType.TOYS.getPlName() : CategoryType.TOYS.getEnName();
                count = ReaderService.TOYS_COUNT;
                start = 50;
                multiplying = 50;
        }
        for (int i = 1; i <= count; i++) {
            products.add(new Product(
                    ++bookId,
                    ReaderService.getName(name, i),
                    category,
                    ReaderService.getRandomDouble(start, multiplying)));
        }
    }
}
