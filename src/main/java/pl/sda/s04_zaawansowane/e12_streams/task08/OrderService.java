package pl.sda.s04_zaawansowane.e12_streams.task08;

import lombok.Getter;
import lombok.ToString;
import pl.sda.s04_zaawansowane.e12_streams.task08.customer.Customer;
import pl.sda.s04_zaawansowane.e12_streams.task08.customer.CustomerReader;
import pl.sda.s04_zaawansowane.e12_streams.task08.order.Order;
import pl.sda.s04_zaawansowane.e12_streams.task08.order.OrderReader;
import pl.sda.s04_zaawansowane.e12_streams.task08.product.Product;
import pl.sda.s04_zaawansowane.e12_streams.task08.product.ProductReader;
import pl.sda.s04_zaawansowane.e12_streams.task08.service.CategoryType;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

@Getter
@ToString
public class OrderService {
    private final List<Product> products;
    private final List<Customer> customers;
    private final List<Order> orders;

    //    W oparciu o powyższą implementację, zaimplementuj metody, które zrealizują postawione zadania.
    public OrderService() {
        this.products = ProductReader.productReader();
        this.customers = CustomerReader.customerReader();
        this.orders = OrderReader.orderReader(this.products, this.customers);
    }

    //    a) Metoda powinna zwrócić listę produktów należących do kategorii „Book” z ceną większą od 100.
    public List<Product> getProductsFromCategoryWithPriceGreaterThan(CategoryType category, int price) {
        return products.stream()
                .filter(p -> p.getCategory() == category)
                .filter(p -> p.getPrice() > price)
                .collect(Collectors.toList());
    }

    //    b) Metoda powinna zwrócić listę zamówień, w których przynajmniej jeden produkt należy do kategorii „Baby”.
    public List<Order> getOrdersByCategory(CategoryType category) {
        return orders.stream()
                .filter(c -> c.getProducts().stream().anyMatch(p -> p.getCategory() == category))
                .collect(Collectors.toList());
    }

    //    c) Metoda powinna zwrócić listę produktów z kategorii „Toys” jednocześnie aplikując do tych produktów 10% zniżkę.
    public List<Product> getProductsByCategoryWithDiscount(CategoryType category, int discount) {
        return products.stream()
                .filter(p -> p.getCategory() == category)
                .map(p -> new Product(p.getId(), p.getName(), p.getCategory(), p.getPrice() * (100 - discount) / 100))
                .collect(Collectors.toList());
    }

    //    d) Metoda powinna zwrócić listę unikalnych produktów zamówionych przez klientów z poziomu 2 (atrybut tier) od 2021-02-01 do 2021-04-01.
    public List<Product> getDistinctProductsByTierAndDate(int tier, LocalDate startDate, LocalDate endDate) {
        return orders.stream()
                .filter(p -> p.getCustomer().getTier() == tier)
                .filter(p -> 0 <= p.getOrderDate().compareTo(startDate) && p.getOrderDate().compareTo(endDate) <= 0)
                .map(Order::getProducts)
                .flatMap(List::stream)
                .distinct()
                .collect(Collectors.toList());
    }

    //    e) Metoda powinna zwracać najtańszy produkt z kategorii „Books”.
    public Optional<Product> getCheapestProductByCategory(CategoryType category) {
        return products.stream()
                .filter(p -> p.getCategory() == category)
                .min(Comparator.comparingDouble(Product::getPrice));
    }

    //    f) Metoda powinna zwrócić listę trzech ostatnich zamówień.
    public List<Order> getLastFewOrders(int numbers) {
        return orders.stream()
                .sorted(Comparator.comparing(Order::getOrderDate, Comparator.reverseOrder()))
                .limit(numbers)
                .collect(Collectors.toList());
    }

    //    g) Metoda powinna zwracać listę wszystkich produktów, które zostały zamówione 15 marca 2021,
//       jednocześnie wypisując na konsoli zamówienia, z których pochodziły (metoda peek).
    public List<Product> getProductsOrderedOnSpecificDateAndPeekOrders(LocalDate orderDate) {
        return orders.stream()
                .filter(o -> o.getOrderDate().equals(orderDate))
                .peek(o -> System.out.println(o.getMainData()))
                .map(Order::getProducts)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    //    h) Metoda wyznacza sumę wszystkich zamówień, które miały miejsce w lutym 2021 roku.
    public Double getOrdersFromSpecificYearAndMonth(int year, int month) {
        LocalDate fromDate = LocalDate.of(year, month, 1);
        LocalDate toDate = fromDate.withDayOfMonth(fromDate.lengthOfMonth());
        return orders.stream()
                .filter(o -> o.getOrderDate().isAfter(fromDate) || o.getOrderDate().equals(fromDate))
                .filter(o -> o.getOrderDate().isBefore(toDate) || o.getOrderDate().equals(toDate))
                .map(Order::getProducts)
                .flatMap(List::stream)
                .mapToDouble(Product::getPrice)
                .sum();
    }

    //    i) Metoda zwraca mapę zamówień pogrupowanych na podstawie identyfikatora klienta.
    public Map<Long, List<Order>> getOrdersGroupedByCustomers() {
        return orders.stream()
                .collect(toMap(
                        o -> (o.getCustomer().getId()),
                        o -> {
                            List<Order> list = new ArrayList<>();
                            list.add(o);
                            return list;
                        },
                        (left, right) -> {
                            left.addAll(right);
                            return left;
                        },
                        TreeMap::new
                ));
    }

    //    j) Metoda zwraca mapę zamówień, której kluczem jest identyfikator zamówienia, natomiast wartością liczba produktów.
    public Map<Long, Integer> getOrderIdAndProductCount() {
        return orders.stream()
                .collect(toMap(
                        Order::getId,
                        o -> (o.getProducts().size())
                ));
    }

    //    k) Metoda zwraca mapę, której kluczem jest kategoria produktu, natomiast wartością najdroższy produkt.
    public Map<CategoryType, Double> getMostExpensiveProductFromEachCategory() {
        return products.stream()
                .collect(Collectors.toMap(
                        Product::getCategory,
                        Product::getPrice,
                        Math::max));
    }
}
