package pl.sda.s04_zaawansowane.e12_streams.task08.order;

import pl.sda.s04_zaawansowane.e12_streams.task08.customer.Customer;
import pl.sda.s04_zaawansowane.e12_streams.task08.product.Product;
import pl.sda.s04_zaawansowane.e12_streams.task08.service.OrderStatus;
import pl.sda.s04_zaawansowane.e12_streams.task08.service.ReaderService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class OrderReader {
    private static final List<Order> orders = new ArrayList<>();

    public static List<Order> orderReader(List<Product> products, List<Customer> customers) {
        LocalDate orderDate;
        Random random = new Random();
        for (long i = 1; i <= ReaderService.ORDERS_COUNT; i++) {
            orderDate = ReaderService.getRandomDate(ReaderService.START_DATE, ReaderService.END_DATE);
            OrderStatus orderStatus = OrderStatus.randomValue();
            int productCount = 1 + random.nextInt(ReaderService.PRODUCT_IN_ORDERS - 1);
            List<Product> tmpProducts = IntStream.range(0, productCount).map(j -> random.nextInt(products.size() - 1))
                    .mapToObj(products::get).collect(Collectors.toList());
            orders.add(new Order(
                    i,
                    orderStatus,
                    orderDate,
                    (orderStatus == OrderStatus.REALIZED) ? ReaderService.getRandomDate(orderDate, LocalDate.now()) : null,
                    tmpProducts,
                    customers.get(random.nextInt(customers.size() - 1)),
                    ReaderService.LANG
            ));
        }
        return orders;
    }
}
