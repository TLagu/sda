package pl.sda.s04_zaawansowane.e12_streams.task08.product;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import pl.sda.s04_zaawansowane.e12_streams.task08.service.CategoryType;
import pl.sda.s04_zaawansowane.e12_streams.task08.service.ReaderService;

@AllArgsConstructor
@Getter
@Setter
public class Product {
    private Long id;
    private String name;
    private CategoryType category;
    private Double price;

    @Override
    public String toString() {
        return "\n   - id: " + id
                + ", name: " + name
                + ", category: " + ((ReaderService.LANG.equals("pl")) ? category.getPlName() : category.getEnName())
                + ", price: " + price;
    }
}
