package pl.sda.s04_zaawansowane.e12_streams.task08.service;

import java.time.LocalDate;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class ReaderService {
    public final static int BOOK_COUNT = 3;
    public final static int BABY_COUNT = 3;
    public final static int TOYS_COUNT = 3;
    public final static int CUSTOMER_COUNT = 3;
    public final static int ORDERS_COUNT = 3;
    public final static int PRODUCT_IN_ORDERS = 5;
    public final static String LANG = "pl";
    public final static LocalDate START_DATE = LocalDate.of(2021, 1, 1);
    public final static LocalDate END_DATE = LocalDate.now();

    public static String getName(String prefix, int suffix) {
        return prefix + " " + suffix;
    }

    public static double getRandomDouble(int start, int multiplying) {
        Random random = new Random();
        return start + Math.round(random.nextInt(100 * multiplying)) / 100;
    }

    public static int getRandomInteger(int start, int range) {
        Random random = new Random();
        return start + random.nextInt(range);
    }

    public static LocalDate getRandomDate(LocalDate startInclusive, LocalDate endExclusive) {
        long startEpochDay = startInclusive.toEpochDay();
        long endEpochDay = endExclusive.toEpochDay();
        return LocalDate.ofEpochDay(ThreadLocalRandom.current().nextLong(startEpochDay, endEpochDay));
    }
}
