package pl.sda.s04_zaawansowane.e12_streams.task08.service;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CategoryType {
    BOOK("Book", "Książka"),
    BABY("Baby", "Dziecko"),
    TOYS("Toys", "Zabawki");

    private String enName;
    private String plName;
}
