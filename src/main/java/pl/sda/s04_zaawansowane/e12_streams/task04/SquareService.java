package pl.sda.s04_zaawansowane.e12_streams.task04;

import java.util.List;

public class SquareService {
    public static void main(String[] args) {
        //Zdefiniuj skończony strumień liczb całkowitych. Wypisz na konsoli sumę kwadratów poszczególnych liczb.
        List.of(1, 2, 3, 4, 5).stream().forEach(i -> System.out.println(i * i));
    }
}
