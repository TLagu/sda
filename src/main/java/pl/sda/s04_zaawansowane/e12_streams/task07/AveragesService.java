package pl.sda.s04_zaawansowane.e12_streams.task07;

import java.util.Comparator;
import java.util.List;

public class AveragesService {
//    Zdefiniuj listę liczb całkowitych.
//    Wypisz na konsoli:
//      - sumę liczb,
//      - największą liczbą,
//      - najmniejszą liczbę oraz
//      - średnią arytmetyczną
    public static void main(String[] args) {
        List<Integer> intList = List.of(1, 4, 2, 6, 3, 7, 3, 7);
        System.out.print("Suma liczb: ");
        System.out.println(intList.stream().reduce(0, Integer::sum));
        System.out.print("Max: ");
        intList.stream().max(Comparator.naturalOrder()).ifPresent(System.out::println);
        System.out.print("Min: ");
        intList.stream().min(Comparator.naturalOrder()).ifPresent(System.out::println);
        System.out.println("Srednia arytmetyczna: ");
        System.out.println((double) intList.stream().reduce(0, Integer::sum) / intList.size());
    }
}
