package pl.sda.s04_zaawansowane.e12_streams.examples.e08;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class FindFirstDemo {
    public static void main(String[] args) {
        List<String> list = List.of("kto", "będzie", "pierwszym", "elementem");
        List<String> collectedList = list.stream()
                .sorted()
                .filter(e -> !e.startsWith("b"))
                .collect(Collectors.toList());
        System.out.println(collectedList);
        System.out.println("-------------------------");
        if (!collectedList.isEmpty()) {
            System.out.println(collectedList.get(0));
        }
        System.out.println("-------------------------");
        collectedList = list.stream()
                .sorted()
                .filter(e -> !e.startsWith("b"))
                .limit(1)
                .collect(Collectors.toList());
        System.out.println(collectedList);
        System.out.println("-------------------------");
        Optional<String> possibleElement = list.stream()
                .sorted()
                .filter(e -> !e.startsWith("b"))
                .findFirst();
        possibleElement.ifPresent(System.out::println);
        System.out.println("-------------------------");
        possibleElement = list.stream()
                .sorted()
                .filter(e -> !e.startsWith("b"))
                .findAny();
        possibleElement.ifPresent(System.out::println);
    }
}
