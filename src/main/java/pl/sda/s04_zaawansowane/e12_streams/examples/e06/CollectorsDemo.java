package pl.sda.s04_zaawansowane.e12_streams.examples.e06;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectorsDemo {
    public static void main(String[] args) {
        List<Integer> list = Stream.generate(() -> {
            Random random = new Random();
            return random.nextInt(10);
        }).limit(10)
                .peek(e -> System.out.println("#" + e + "#"))
                .distinct()
                .peek(e -> System.out.println("@" + e + "@"))
                .filter(e -> e % 2 == 0)
                .peek(e -> System.out.println("!" + e + "!"))
                .collect(Collectors.toList());
        System.out.println(list);
        System.out.println("---------------------------------");

        String sentence = Stream.of("This", "will", "be", "single", "sentence", "and", "...", "?")
                .filter(word -> word.length() > 3)
                .collect(Collectors.joining(", ", "[", "]"));
        System.out.println(sentence);
        System.out.println("---------------------------------");
        // Map<String, String>
        // Hello -> HELLO
        // ...
        Map<String, String> map = Stream.of("Hello", "from", "Stettin")
                .collect(Collectors.toMap(word -> word, word -> word.toUpperCase(Locale.ROOT)));
        System.out.println(map);
    }
}
