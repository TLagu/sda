package pl.sda.s04_zaawansowane.e12_streams.examples.e09;

import pl.sda.s04_zaawansowane.e12_streams.examples.e05.Person;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class ReduceDemo {
    public static void main(String[] args) {
        List<Person> people = List.of(new Person("bartosz", 12),
                new Person("Agnieszka", 10),
                new Person("Ewa", 11));
        Integer ageSum = people.stream()
                .map(Person::getAge)
                .reduce(0, Integer::sum);
        System.out.println(ageSum);
        System.out.println("----------------------------");
        ageSum = people.stream()
                .map(Person::getAge)
                .filter(age -> age > 18)
                .reduce(0, Integer::sum);
        System.out.println(ageSum);
        System.out.println("----------------------------");
        Optional<Integer> possibleAge = people.stream()
                .map(Person::getAge)
                .reduce(Integer::sum);
        possibleAge.ifPresentOrElse(
                System.out::println,
                () -> System.out.println("Empty optional - empty stream..."));
        System.out.println("----------------------------");
        possibleAge = people.stream()
                .map(Person::getAge)
                .filter(age -> age > 18)
                .reduce(Integer::sum);
        possibleAge.ifPresentOrElse(
                System.out::println,
                () -> System.out.println("Empty optional - empty stream..."));
    }
}
