package pl.sda.s04_zaawansowane.e12_streams.examples.e05;

import java.util.Comparator;
import java.util.List;

public class SortedDemo {
    public static void main(String[] args) {
        List<Person> people = List.of(new Person("Bartosz", 11),
                new Person("Karolina", 9),
                new Person("Witold", 10));
        people.stream()
                .sorted()
                .forEach(System.out::println);
        System.out.println("------------------------------");
        people.stream()
                .sorted(Comparator.comparing(Person::getName))
                .forEach(System.out::println);
    }
}
