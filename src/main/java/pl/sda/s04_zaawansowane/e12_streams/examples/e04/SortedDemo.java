package pl.sda.s04_zaawansowane.e12_streams.examples.e04;

import java.util.Arrays;
import java.util.Comparator;

public class SortedDemo {
    public static void main(String[] args) {
        Arrays.asList(6, 3, 6, 21, 20, 1).stream()
                .sorted(Comparator.reverseOrder())
                .forEach(System.out::println);
    }
}
