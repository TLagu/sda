package pl.sda.s04_zaawansowane.e12_streams.examples.e07;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GroupingByDemo {
    public static void main(String[] args) {
//        Map<Integer, List<String>> map = Stream.of("This", "is", "best", "course", "in", "SDA")
//                .collect(Collectors.groupingBy(String::length));
        Stream.of("This", "is", "best", "course", "in", "SDA")
                .collect(Collectors.groupingBy(String::length))
                .forEach((key, value) -> System.out.println(key + " " + value));
    }
}
