package pl.sda.s04_zaawansowane.e12_streams.examples.e02;

import java.util.List;
import java.util.stream.Stream;

public class FlatMapDemo {
    public static void main(String[] args) {
        Statistics statistics1 = new Statistics(2.0, List.of(1, 2, 3));
        Statistics statistics2 = new Statistics(2.5, List.of(2, 3, 2, 3));

        Stream<Statistics> stream = Stream.of(statistics1, statistics2);
        Stream<Integer> integerStream = stream.flatMap(s -> s.getValues().stream());
        integerStream.forEach(System.out::println);

        System.out.println("---------------------------");
        stream = Stream.of(statistics1, statistics2);
        Stream<List<Integer>> listStream = stream.map(s -> s.getValues());
        listStream.forEach(System.out::println);
        System.out.println("---------------------------");
        // nie powinniśmy tworzyć strumienia strumieni!!!
        // Stream<List<Integer>> streamStream = stream.map(s -> s.getValues().stream());
        // nie można dwukrotnie wykonać operacji terminalnej na strumieniach
        // integerStream.forEach(s -> System.out.println("!" + s + "!"));
        // tak powinna być wykonana operacja na strumieniach, a nie w kilku operacjach.
        Stream.of(statistics1, statistics2)
                .flatMap(s -> s.getValues().stream())
                .forEach(System.out::println);
    }
}
