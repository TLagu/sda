package pl.sda.s04_zaawansowane.e01_oop.Task13;

import lombok.Getter;
import lombok.Setter;

import java.text.DecimalFormat;

@Getter
@Setter
public class Rectangle extends Shape {
    private double width;
    private double length;

    public Rectangle() {
        super();
        this.width = 1;
        this.length = 1;
    }

    public Rectangle(double width, double length) {
        super();
        this.width = width;
        this.length = length;
    }

    public double getArea() {
        return width * length;
    }

    public double getPerimeter() {
        return 2 * width + 2 * length;
    }

    @Override
    public String toString() {
        return "Rectangle with width=" + new DecimalFormat("#,##0.00").format(width)
                + " and length=" + new DecimalFormat("#,##0.00").format(length)
                + " which is a subclass off " + super.toString();
    }
}
