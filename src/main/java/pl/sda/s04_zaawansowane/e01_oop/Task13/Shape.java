package pl.sda.s04_zaawansowane.e01_oop.Task13;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Shape {
    private String color;
    private boolean isFilled;

    public Shape() {
        color = "unknown";
        isFilled = false;
    }

    @Override
    public String toString() {
        return "Shape with color of " + color + " and " + (isFilled?"filled":"NotFilled");
    }
}
