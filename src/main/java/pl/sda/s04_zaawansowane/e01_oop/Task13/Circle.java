package pl.sda.s04_zaawansowane.e01_oop.Task13;

import java.text.DecimalFormat;

public class Circle extends Shape {
    private double radius;

    public Circle(double radius) {
        super();
        this.radius = 1.0f;
    }

    public Circle(String color, boolean isFilled, double radius) {
        super();
        this.radius = radius;
    }

    public double getArea() {
        return Math.PI * radius * radius;
    }

    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public String toString() {
        return "Circle with radius=" + new DecimalFormat("#,##0.00").format(radius) +
                " which is a subclass off " + super.toString();
    }
}
