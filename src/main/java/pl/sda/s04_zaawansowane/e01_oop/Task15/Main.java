package pl.sda.s04_zaawansowane.e01_oop.Task15;

public class Main {
    public static void main(String[] args) {
        Point2D startPoint = new Point2D(0, 0);
        Point2D endPoint = new Point2D(2, 2);
        Line line1 = new Line(startPoint, endPoint);
        System.out.println(line1.getLineLength());
        System.out.println(line1.getMiddleOfTheLine());

        Line line2 = new Line(1, 1, 4, 5);
        System.out.println(line2.getLineLength());
        System.out.println(line2.getMiddleOfTheLine());
    }
}
