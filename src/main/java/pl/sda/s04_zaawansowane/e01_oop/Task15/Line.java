package pl.sda.s04_zaawansowane.e01_oop.Task15;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Line {
    private Point2D startPoint;
    private Point2D endPoint;

    public Line(float startX, float startY, float endX, float endY) {
        this.startPoint = new Point2D(startX, startY);
        this.endPoint = new Point2D(endX, endY);
    }

    public double getLineLength() {
        float deltaX = startPoint.getX() - endPoint.getX();
        float deltaY = startPoint.getY() - endPoint.getY();
        return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
    }

    public Point2D getMiddleOfTheLine() {
        float middleX = (startPoint.getX() + endPoint.getX()) / 2;
        float middleY = (startPoint.getY() + endPoint.getY()) / 2;
        return new Point2D(middleX, middleY);
    }
}
