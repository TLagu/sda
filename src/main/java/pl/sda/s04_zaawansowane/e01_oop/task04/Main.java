package pl.sda.s04_zaawansowane.e01_oop.task04;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Cylinder cylinder1 = new Cylinder();
        System.out.println("Circle data: " + Arrays.toString(cylinder1.getCircleData()));
        System.out.println("All data: " + cylinder1);
        System.out.println("--------------------");
        Cylinder cylinder2 = new Cylinder(2);
        System.out.println("Circle data: " + Arrays.toString(cylinder2.getCircleData()));
        System.out.println("All data: " + cylinder2);
        System.out.println("--------------------");
        Cylinder cylinder3 = new Cylinder(2, 2);
        System.out.println("Circle data: " + Arrays.toString(cylinder3.getCircleData()));
        System.out.println("All data: " + cylinder3);
    }
}
