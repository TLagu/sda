package pl.sda.s04_zaawansowane.e01_oop.task04;

public class Cylinder {
    private Circle circle;
    private double height = 1.0;

    public Cylinder() {
        circle = new Circle();
    }

    public Cylinder(double radius) {
        circle = new Circle(radius);
    }

    public Cylinder(double radius, double height) {
        circle = new Circle(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return circle.getArea() * height;
    }

    public Object[] getCircleData() {
        return circle.getData();
    }

    @Override
    public String toString() {
        String superString = circle.toString();
        return ", Cylinder{" +
                "height=" + height +
                ", volume=" + getVolume() +
                ", circle={" + circle.toString() +
                '}';
    }
}
