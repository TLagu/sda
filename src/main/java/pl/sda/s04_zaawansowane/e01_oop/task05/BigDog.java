package pl.sda.s04_zaawansowane.e01_oop.task05;

public class BigDog extends Animal {
    @Override
    public void greeting() {
        System.out.println("Woow!");
    }

    public void greeting(Dog another) {
        System.out.println("Woooooowwwww!");
    }
}
