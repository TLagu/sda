package pl.sda.s04_zaawansowane.e01_oop.task02;

public class Main {
    public static void main(String[] args) {
        System.out.println("Circle:");
        System.out.print("- Default values: ");
        Circle circle1 = new Circle();
        System.out.println("Area: " + circle1.getArea()
                + ", Perimeter: " + circle1.getPerimeter()
                + ", All: " + circle1);
        System.out.print("- Only radius: ");
        Circle circle2 = new Circle(2);
        System.out.println("Area: " + circle2.getArea()
                + ", Perimeter: " + circle2.getPerimeter()
                + ", All: " + circle2);
        System.out.print("- All values: ");
        Circle circle3 = new Circle("blue", false, 2);
        System.out.println("Area: " + circle3.getArea()
                + ", Perimeter: " + circle3.getPerimeter()
                + ", All: " + circle3);

        System.out.println("Rectangle:");
        System.out.print("- Default values: ");
        Rectangle rectangle1 = new Rectangle();
        System.out.println("Area: " + rectangle1.getArea()
                + ", Perimeter: " + rectangle1.getPerimeter()
                + ", All: " + rectangle1);
        System.out.print("- Only width and length: ");
        Rectangle rectangle2 = new Rectangle(2, 4);
        System.out.println("Area: " + rectangle2.getArea()
                + ", Perimeter: " + rectangle2.getPerimeter()
                + ", All: " + rectangle2);
        System.out.print("- All values: ");
        Rectangle rectangle3 = new Rectangle(2, 4, "blue", false);
        System.out.println("Area: " + rectangle3.getArea()
                + ", Perimeter: " + rectangle3.getPerimeter()
                + ", All: " + rectangle3);

        System.out.println("Square:");
        System.out.print("- Default values: ");
        Square square1 = new Square();
        System.out.println("Area: " + square1.getArea()
                + ", Perimeter: " + square1.getPerimeter()
                + ", All: " + square1);
        System.out.print("- Only side: ");
        Square square2 = new Square(2);
        System.out.println("Area: " + square2.getArea()
                + ", Perimeter: " + square2.getPerimeter()
                + ", All: " + square2);
        System.out.print("- All values: ");
        Square square3 = new Square(2, "blue", false);
        System.out.println("Area: " + square3.getArea()
                + ", Perimeter: " + square3.getPerimeter()
                + ", All: " + square3);
    }
}
