package pl.sda.s04_zaawansowane.e01_oop.task01;

public class Circle {
    double radius = 1.0d;
    String color = "red";

    public Circle() {

    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setData(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }

    public Object[] getData () {
        return new Object[]{radius, color};
    }

    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", color='" + color + '\'' +
                '}';
    }
}
