package pl.sda.s04_zaawansowane.e01_oop.task01;

public class Cylinder extends Circle {
    private double height = 1.0;
    public Cylinder() {
    }

    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return super.getArea() * height;
    }

    @Override
    public String toString() {
        String superString = super.toString();
        return superString + ", Cylinder{" +
                "height=" + height +
                '}';
    }
}
