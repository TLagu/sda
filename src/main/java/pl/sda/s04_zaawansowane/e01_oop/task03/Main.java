package pl.sda.s04_zaawansowane.e01_oop.task03;

public class Main {
    public static void main(String[] args) {
        Line line = new Line(new Point(1, 1), new Point(2, 2));
        System.out.println(line.toString() + ", length: " + line.getLength());
    }
}
