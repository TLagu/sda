package pl.sda.s04_zaawansowane.e01_oop.examples.e05;

import com.google.gson.Gson;

public class JsonParser extends DataParser {
    @Override
    public Data parse() {
        validateData();
        Gson gson = new Gson();
        return gson.fromJson(data, Data.class);
    }
}
