package pl.sda.s04_zaawansowane.e01_oop.examples.e02;

public class Laptop extends Computer {

    private String battery;

    public Laptop(String cpu, String ram, String gpu, String battery) {
        super(cpu, ram, gpu);
        this.battery = battery;
    }

    public void configure() {
        super.configure();
        System.out.println("Configure battery: " + battery);
    }

    public static void main(String[] args) {
        Laptop laptop = new Laptop("cpu1", "ram1", "gpu1", "battery1");
        laptop.configure();
    }
}
