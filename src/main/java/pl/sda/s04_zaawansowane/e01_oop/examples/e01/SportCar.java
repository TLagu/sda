package pl.sda.s04_zaawansowane.e01_oop.examples.e01;

public class SportCar extends Car {
    public void drive() {
        turnOnEngine();
        System.out.println("I'm driving!");
    }

    public static void main(String[] args) {
        SportCar sportCar = new SportCar();
        sportCar.turnOnEngine();
        System.out.println("----------------");
        sportCar.drive();
        Truck truck = new Truck();
        System.out.println("----------------");
        truck.drive(sportCar);
    }
}
