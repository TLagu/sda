package pl.sda.s04_zaawansowane.e01_oop.examples.e07;

public class NetflixPlayer extends VodPlayer {
    @Override
    public void play(final String title) {
        System.out.println("Playing " + title + " on Netflix");
    }
}
