package pl.sda.s04_zaawansowane.e01_oop.examples.e07;

public abstract class VodPlayer {
    public abstract void play(String title);
}
