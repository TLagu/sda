package pl.sda.s04_zaawansowane.e01_oop.examples.e07;

public class AndroidTV {
    public static void main(String[] args) {
        final String player = "Netflix";
        VodPlayer vodPlayer = null;
        if (player.equals("Netflix")) {
            vodPlayer = new NetflixPlayer();
        } else if (player.equals("HBO")) {
            vodPlayer = new HBOGoPLayer();
        } else {
            vodPlayer = new DefaultPlayer();
        }
        playEpisode(vodPlayer, "GOT_S1E1");
    }

    static void playEpisode(VodPlayer vodPlayer, String title) {
        vodPlayer.play(title);
    }

}
