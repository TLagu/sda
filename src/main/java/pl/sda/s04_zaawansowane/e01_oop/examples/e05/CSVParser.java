package pl.sda.s04_zaawansowane.e01_oop.examples.e05;

public class CSVParser extends DataParser {
    @Override
    public Data parse() {
        validateData();
        String[] splitData = data.split(",");
        return new Data(splitData[0], splitData[1]);
    }
}
