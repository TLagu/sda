package pl.sda.s04_zaawansowane.e01_oop.examples.e05;

public class Data {
    private String info;
    private String value;

    public Data(String info, String value) {
        this.info = info;
        this.value = value;
    }
}
