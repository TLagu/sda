package pl.sda.s04_zaawansowane.e01_oop.Task11;

public class Main {
    public static void main(String[] args) {
        Point2D point2D = new Point2D();
        System.out.println(point2D);
        Point3D point3D = new Point3D(point2D.getX(), point2D.getY(), 2.0f);
        System.out.println(point3D);
    }
}
